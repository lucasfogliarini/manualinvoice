using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class PaymentCodeConfiguration : IEntityTypeConfiguration<PaymentCode>
    {
        public void Configure(EntityTypeBuilder<PaymentCode> builder)
        {
            builder.ToTable("DLB_NFMANUAL_PAYMENT_CODE");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("PAYMENT_CODE_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<PaymentCodeSequenceGenerator>();

            builder.Property(e => e.Code).HasColumnName("CODE").IsRequired().HasMaxLength(1);
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION").IsRequired().HasMaxLength(255);
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE").IsRequired();
            builder.Property(e => e.IsCreditCard).HasColumnName("IS_CREDIT_CARD");
        }
    }
}
