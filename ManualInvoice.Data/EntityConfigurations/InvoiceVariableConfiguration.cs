using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class InvoiceVariableConfiguration : IEntityTypeConfiguration<InvoiceVariable>
    {
        public void Configure(EntityTypeBuilder<InvoiceVariable> builder)
        {
            builder.ToTable("DLB_NFMANUAL_VARIABLES_INVOICE");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("VARIABLE_INVOICE_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<InvoiceVariableSequenceGenerator>();

            builder.Property(typeof(long?), "INVOICE_ID").IsRequired(false);
            builder.Property(typeof(int?), "VARIABLE_ID").IsRequired(false);
            builder.Property(typeof(int?), "USER_CREATION_ID").IsRequired(false);

            builder.Property(e => e.Value).HasColumnName("VALUE");
            builder.Property(e => e.DateCreate).HasColumnName("DT_CREATE");
            builder.Property(e => e.IsMandatory).HasColumnName("MANDATORY");

            builder.HasOne(e => e.Invoice).WithMany().HasForeignKey("INVOICE_ID").IsRequired(false);
            builder.HasOne(e => e.Variable).WithMany().HasForeignKey("VARIABLE_ID").IsRequired(false);
            builder.HasOne(e => e.User).WithMany().HasForeignKey("USER_CREATION_ID").IsRequired(false);
        }
    }
}

