﻿using ManualInvoice.Data.Converters;
using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.ToTable("DLB_NFMANUAL_TRANSACTION");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("TRANSACTION_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<TransactionSequenceGenerator>();

            builder.Property(e => e.Name).HasColumnName("NAME").IsRequired();
            builder.Property(e => e.Observation).HasColumnName("OBSERVATION");
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");
            builder.Property(e => e.BillToCode).HasColumnName("CD_BILL_TO");
            builder.Property(e => e.ShipToCode).HasColumnName("CD_SHIP_TO");
            builder.Property(e => e.Serie).HasColumnName("SERIE");
            builder.Property(e => e.Specie).HasColumnName("SPECIE");

            builder.Property(e => e.TransactionType).HasColumnName("TRANSACTION_TYPE")
                .HasConversion(new TransactionTypeConverter());
        }
    }
}
