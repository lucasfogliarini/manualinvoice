﻿using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("DLB_NFMANUAL_ORDER");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("ORDER_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<OrderSequenceGenerator>();

            builder.Property(typeof(long), "INVOICE_ID").IsRequired();

            builder.Property(e => e.OriginalDofId).HasColumnName("SYN_ORIG_DOF_ID").IsRequired();
            builder.Property(e => e.RecofNumOrder).HasColumnName("SYN_DELL_RECOF_NUM_ORDER").IsRequired(false);
            builder.Property(e => e.CustClassCode).HasColumnName("SYN_CUST_CLASS_CODE").IsRequired(false);
            builder.Property(e => e.OrderCreationDate).HasColumnName("SYN_ORDER_CREATION_DATE").IsRequired(false);
            builder.Property(e => e.DpsType).HasColumnName("SYN_DPS_TYPE").IsRequired(false);
            builder.Property(e => e.OrderRate).HasColumnName("SYN_ORDER_RATE").IsRequired(false);
            builder.Property(e => e.InfoShipping).HasColumnName("SYN_INFO_SHIPPING").IsRequired(false);
            builder.Property(e => e.InfoDuty).HasColumnName("SYN_INFO_DUTY").IsRequired(false);
            builder.Property(e => e.DirectOrderType).HasColumnName("SYN_DIRECT_ORDER_TYPE").IsRequired(false);
            builder.Property(e => e.PmtAmt1Lc).HasColumnName("SYN_PMT_AMT1_LC").IsRequired(false);
            builder.Property(e => e.PmtAmt2Lc).HasColumnName("SYN_PMT_AMT2_LC").IsRequired(false);
            builder.Property(e => e.PmtAmt3Lc).HasColumnName("SYN_PMT_AMT3_LC").IsRequired(false);
            builder.Property(e => e.MnAuditReason).HasColumnName("SYN_MN_AUDIT_REASON").IsRequired(false);
            builder.Property(e => e.SpecialInstructions).HasColumnName("SYN_SPECIAL_INST").IsRequired(false);
            builder.Property(e => e.PaymentTermCode).HasColumnName("SYN_PMT_TERM_CODE").IsRequired(false);
            builder.Property(e => e.Promo1).HasColumnName("SYN_PROMO1").IsRequired(false);
            builder.Property(e => e.Promo2).HasColumnName("SYN_PROMO2").IsRequired(false);
            builder.Property(e => e.Promo3).HasColumnName("SYN_PROMO3").IsRequired(false);
            builder.Property(e => e.ShipDate).HasColumnName("SYN_SHIP_DATE").IsRequired(false);
            builder.Property(e => e.SourceTypeCode).HasColumnName("SYN_SOURCE_TYPE_CODE").IsRequired(false);
            builder.Property(e => e.PaymentProvider1).HasColumnName("SYN_PMT_PROVIDER_1").IsRequired(false);
            builder.Property(e => e.PaymentProvider2).HasColumnName("SYN_PMT_PROVIDER_2").IsRequired(false);
            builder.Property(e => e.PaymentProvider3).HasColumnName("SYN_PMT_PROVIDER_3").IsRequired(false);
            builder.Property(e => e.PaymentNsu1).HasColumnName("SYN_PMT_NSU_1").IsRequired(false);
            builder.Property(e => e.PaymentNsu2).HasColumnName("SYN_PMT_NSU_2").IsRequired(false);
            builder.Property(e => e.PaymentNsu3).HasColumnName("SYN_PMT_NSU_3").IsRequired(false);
            builder.Property(e => e.PaymentNsu3).HasColumnName("SYN_PMT_NSU_3").IsRequired(false);

            builder.HasOne(e => e.Invoice).WithMany().HasForeignKey("INVOICE_ID").IsRequired();

            builder.HasMany(e => e.OrderItems).WithOne(e => e.Order).HasForeignKey("ORDER_ID").OnDelete(DeleteBehavior.Cascade);
        }
    }
}
