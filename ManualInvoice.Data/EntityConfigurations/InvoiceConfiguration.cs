﻿using ManualInvoice.Data.Converters;
using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data
{
    internal class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.ToTable("DLB_NFMANUAL_INVOICE");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("INVOICE_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<InvoiceSequenceGenerator>();

            builder.Property(typeof(int?), "BRANCH_ID").IsRequired(false);
            builder.Property(typeof(int?), "TRANSACTION_ID").IsRequired(false);
            builder.Property(typeof(int?), "REASON_ID").IsRequired(false);
            builder.Property(typeof(long?), "CARRIER_ID").IsRequired(false);
            builder.Property(e => e.RejectReasonId).HasColumnName("REASON_REJECT_ID").IsRequired(false);
            builder.Property(e => e.RejectUserId).HasColumnName("REJECT_USER_ID").IsRequired(false);
            builder.Property(typeof(int?), "SYSTEM_USER_ID").IsRequired(false);

            builder.Property(e => e.RequestDate).HasColumnName("DT_REQUEST");

            builder.Property(e => e.BillToCode).HasColumnName("CD_BILL_TO");
            builder.Property(e => e.BillToName).HasColumnName("NAME_BILL_TO");
            builder.Property(e => e.BillToCnpj).HasColumnName("CNPJ_BILL_TO");
            builder.Property(e => e.BillToAddress).HasColumnName("ADDRESS_BILL_TO");

            builder.Property(e => e.ShipToCode).HasColumnName("CD_SHIP_TO");
            builder.Property(e => e.ShipToName).HasColumnName("NAME_SHIP_TO");
            builder.Property(e => e.ShipToCnpj).HasColumnName("CNPJ_SHIP_TO");
            builder.Property(e => e.ShipToAddress).HasColumnName("ADRESS_SHIP_TO");

            builder.Property(e => e.TransactionName).HasColumnName("TRANSACTION_NAME");
            builder.Property(e => e.TransactionBillToCode).HasColumnName("TRANSACTION_CD_BILL_TO");
            builder.Property(e => e.TransactionShipToCode).HasColumnName("TRANSACTION_CD_SHIP_TO");
            builder.Property(e => e.TransactionSerie).HasColumnName("TRANSACTION_SERIE");
            builder.Property(e => e.TransactionSpecie).HasColumnName("TRANSACTION_SPECIE");

            builder.Property(e => e.Observation).HasColumnName("OBSERVATION");
            builder.Property(e => e.ObservationTwo).HasColumnName("OBSERVATION2");
            builder.Property(e => e.Instruction).HasColumnName("INSTRUCTION");

            builder.Property(e => e.SynInvoiceNumber).HasColumnName("SYN_INVOICE_NUMBER");
            builder.Property(e => e.SynInvoiceDate).HasColumnName("SYN_INVOICE_DATE");
            builder.Property(e => e.SynInvoiceAmount).HasColumnName("SYN_INVOICE_AMOUNT");
            builder.Property(e => e.SynNfeAccessKey).HasColumnName("SYN_NFE_ACCESS_KEY");

            builder.Property(e => e.ServiceInvoiceNumber).HasColumnName("SYN_SERVICE_INVOICE_NUMBER");
            builder.Property(e => e.RejectDate).HasColumnName("REJECT_DATE");

            builder.Property(e => e.BlDraft).HasColumnName("BL_DRAFT");
            builder.Property(e => e.GenerateNf).HasColumnName("GENERATE_NF");
            builder.Property(e => e.BillToSent).HasColumnName("BILL_TO_SENT");
            builder.Property(e => e.ShipToSent).HasColumnName("SHIP_TO_SENT");
            builder.Property(e => e.UpdatedOnSynchro).HasColumnName("COR_DOF_UPDATED");
            builder.Property(e => e.Cancelled).HasColumnName("BL_CANCELLED");
            builder.Property(e => e.Finalized).HasColumnName("BL_FINALIZED");
            builder.Property(e => e.IsConsolidated).HasColumnName("IS_CONSOLIDATED").IsRequired();

            builder.Property(e => e.Status).HasColumnName("STATUS")
                .HasConversion(new InvoiceStatusConverter());

            builder.Property(e => e.InvoiceType).HasColumnName("INVOICE_TYPE")
                .HasConversion(new InvoiceTypeConverter());

            builder.Property(e => e.SynchroStatus).HasColumnName("SYNCHRO_ORDER_STATUS")
                .HasConversion(new OrderSynchroStatusConverter());

            builder.HasMany(e => e.Items).WithOne(e => e.Invoice).HasForeignKey("INVOICE_ID").OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(e => e.Branch).WithMany().HasForeignKey("BRANCH_ID").IsRequired(false);
            builder.HasOne(e => e.Transaction).WithMany().HasForeignKey("TRANSACTION_ID").IsRequired();
            builder.HasOne(e => e.Reason).WithMany().HasForeignKey("REASON_ID").IsRequired(false);
            builder.HasOne(e => e.RejectReason).WithMany().HasForeignKey(e => e.RejectReasonId).IsRequired(false);
            builder.HasOne(e => e.Carrier).WithMany().HasForeignKey("CARRIER_ID").IsRequired(false);
            builder.HasOne(e => e.User).WithMany().HasForeignKey("SYSTEM_USER_ID").IsRequired();
            builder.HasOne(e => e.RejectUser).WithMany().HasForeignKey(e => e.RejectUserId).IsRequired(false);
        }
    }
}
