using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class ProductTypeConfiguration : IEntityTypeConfiguration<ProductType>
    {
        public void Configure(EntityTypeBuilder<ProductType> builder)
        {
            builder.ToTable("DLB_NFMANUAL_PRODUCT_TYPE");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("PRODUCT_TYPE_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<ProductTypeSequenceGenerator>();

            builder.Property(e => e.Name).HasColumnName("PRODUCT_TYPE_NAME").IsRequired().HasMaxLength(255);
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION").HasMaxLength(255);
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");
            builder.Property(e => e.LineItemType).HasColumnName("LINE_ITEM_TYPE").HasMaxLength(2);
            builder.Property(e => e.IsEditable).HasColumnName("EDITABLE");
        }
    }
}
