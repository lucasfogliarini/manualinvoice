﻿using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class ReasonConfiguration : IEntityTypeConfiguration<Reason>
    {
        public void Configure(EntityTypeBuilder<Reason> builder)
        {
            builder.ToTable("DLB_NFMANUAL_REASON");
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("REASON_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<ReasonSequenceGenerator>();

            builder.Property(c => c.Name).HasColumnName("REASON_NAME").IsRequired();
            builder.Property(c => c.Description).HasColumnName("DESCRIPTION").IsRequired();
            builder.Property(c => c.ReasonType).HasColumnName("REASON_TYPE").IsRequired();
            builder.Property(c => c.IsActive).HasColumnName("IS_ACTIVE").IsRequired();
        }
    }
}
