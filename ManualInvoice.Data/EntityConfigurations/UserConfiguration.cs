using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("DLB_NFMANUAL_SYSTEM_USER");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("SYSTEM_USER_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<UserSequenceGenerator>();

            builder.Property(e => e.UserName).HasColumnName("USER_NAME").IsRequired();
            builder.Property(e => e.Email).HasColumnName("EMAIL").IsRequired();
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");
            builder.Property(e => e.LastLogonDate).HasColumnName("LAST_LOGON_DATE");
            builder.Property(e => e.ProfileId).HasColumnName("PROFILE_ID");
        }
    }
}

