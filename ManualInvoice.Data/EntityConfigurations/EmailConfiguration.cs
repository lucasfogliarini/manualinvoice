using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class EmailConfiguration : IEntityTypeConfiguration<Email>
    {
        public void Configure(EntityTypeBuilder<Email> builder)
        {
            builder.ToTable("DLB_NFMANUAL_EMAIL");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("EMAIL_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<EmailSequenceGenerator>();

            builder.Property(e => e.TypeId).HasColumnName("EMAIL_TYPE_ID").IsRequired();
            builder.Property(e => e.RelatedEntityId).HasColumnName("RELATED_ENTITY_ID").IsRequired();
            builder.Property(e => e.Subject).HasColumnName("SUBJECT").IsRequired().HasMaxLength(255);
            builder.Property(e => e.Body).HasColumnName("BODY").IsRequired().HasMaxLength(1000);
            builder.Property(e => e.CreateDate).HasColumnName("CREATE_DATE").IsRequired();
            builder.Property(e => e.SentDate).HasColumnName("SENT_DATE");
            builder.Property(e => e.IsSent).HasColumnName("BL_SENT");
            builder.Property(e => e.ReceiverEmail).HasColumnName("RECEIVER_EMAIL").IsRequired().HasMaxLength(255);
            builder.Property(e => e.Link).HasColumnName("LINK").HasMaxLength(255);
            builder.Property(e => e.ReceiverId).HasColumnName("RECEIVER_ID").IsRequired();
        }
    }
}
