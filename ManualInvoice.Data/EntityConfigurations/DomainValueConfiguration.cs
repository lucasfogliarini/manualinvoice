using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class DomainValueConfiguration : IEntityTypeConfiguration<DomainValue>
    {
        public void Configure(EntityTypeBuilder<DomainValue> builder)
        {
            builder.ToTable("DLB_NFMANUAL_DOMAIN_VALUE");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("DOMAIN_VALUE_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<DomainValueSequenceGenerator>();

            builder.Property(typeof(int?), "DOMAIN_ID").IsRequired(false);

            builder.Property(e => e.Code).HasColumnName("DOMAIN_VALUE_CODE");
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION");

            builder.HasOne(e => e.Domain).WithMany().HasForeignKey("DOMAIN_ID");
        }
    }
}

