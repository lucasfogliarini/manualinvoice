using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class SystemConfigurationConfiguration : IEntityTypeConfiguration<SystemConfiguration>
    {
        public void Configure(EntityTypeBuilder<SystemConfiguration> builder)
        {
            builder.ToTable("DLB_NFMANUAL_PARAMETERS");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("PARAMETER_ID").IsRequired();                           
            builder.Property(e => e.DaysDraftRequest).HasColumnName("DAYS_DRAFT_REQUEST");
            builder.Property(e => e.EmailRemember).HasColumnName("EMAIL_REMEMBER");
        }
    }
}
