using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class TransactionVariableConfiguration : IEntityTypeConfiguration<TransactionVariable>
    {
        public void Configure(EntityTypeBuilder<TransactionVariable> builder)
        {
            builder.ToTable("DLB_NFMANUAL_VAR_TRANS");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("VAR_TRANS_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<TransactionVariableSequenceGenerator>();

            builder.Property(e => e.IsMandatory).HasColumnName("MANDATORY");
            builder.Property(e => e.TransactionId).HasColumnName("TRANSACTION_ID");
            builder.Property(e => e.VariableId).HasColumnName("VARIABLE_ID");

            builder.HasOne(e => e.Variable).WithMany().HasForeignKey(e => e.VariableId);
            builder.HasOne(e => e.Transaction).WithMany(e => e.TransactionVariables).HasForeignKey(e => e.TransactionId);
        }
    }
}
