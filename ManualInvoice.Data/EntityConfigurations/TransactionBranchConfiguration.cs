using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class TransactionBranchConfiguration : IEntityTypeConfiguration<TransactionBranch>
    {
        public void Configure(EntityTypeBuilder<TransactionBranch> builder)
        {
            builder.ToTable("DLB_NFMANUAL_BRANCH_TRANS");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("BRANCH_TRANS_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<TransactionBranchSequenceGenerator>();

            builder.Property(e => e.TransactionId).HasColumnName("TRANSACTION_ID").IsRequired();
            builder.Property(e => e.BranchId).HasColumnName("BRANCH_ID");

            builder.HasOne(e => e.Branch).WithMany().HasForeignKey(e => e.BranchId);
            builder.HasOne(e => e.Transaction).WithMany(e => e.TransactionBranches).HasForeignKey(e => e.TransactionId);
        }
    }
}
