using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class DelegationUserConfiguration : IEntityTypeConfiguration<DelegationUser>
    {
        public void Configure(EntityTypeBuilder<DelegationUser> builder)
        {
            builder.ToTable("DLB_NFMANUAL_DELEGATION_USER");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("DELEGATION_USER_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<DelegationUserSequenceGenerator>();

            builder.Property(e => e.UserId).HasColumnName("SYSTEM_USER_ID").IsRequired(false);
            builder.Property(e => e.DelegationId).HasColumnName("DELEGATION_ID").IsRequired(false);

            builder.HasOne(e => e.User).WithMany().HasForeignKey(e=>e.UserId).IsRequired();
            builder.HasOne(e => e.Delegation).WithMany(e => e.DelegationUsers).HasForeignKey(e=>e.DelegationId).IsRequired();
        }
    }
}

