using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class LevelConfiguration : IEntityTypeConfiguration<Level>
    {
        public void Configure(EntityTypeBuilder<Level> builder)
        {
            builder.ToTable("DLB_NFMANUAL_LEVEL");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("LEVEL_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<LevelSequenceGenerator>();

            builder.Property(typeof(int?), "PARENT_LEVEL_ID").IsRequired(false);
            builder.Property(e => e.Name).HasColumnName("NAME").IsRequired();
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION").IsRequired();
            builder.Property(e => e.Visible).HasColumnName("VISIBLE");
            builder.Property(e => e.Invoice).HasColumnName("INVOICE");
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");
        }
    }
}

