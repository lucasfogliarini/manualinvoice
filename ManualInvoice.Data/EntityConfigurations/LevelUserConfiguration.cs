using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class LevelUserConfiguration : IEntityTypeConfiguration<LevelUser>
    {
        public void Configure(EntityTypeBuilder<LevelUser> builder)
        {
            builder.ToTable("DLB_NFMANUAL_LEVEL_USER");
            builder.HasKey(e => e.Id );
            builder.Property(e => e.Id).HasColumnName("LEVEL_USER_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<LevelUserSequenceGenerator>();

            builder.Property(e=> e.LevelId).HasColumnName("LEVEL_ID").IsRequired(true);
            builder.Property(e => e.UserId).HasColumnName("SYSTEM_USER_ID").IsRequired(true);

            builder.HasOne(e => e.Level).WithMany().HasForeignKey(e=>e.LevelId);
            builder.HasOne(e => e.User).WithMany(e=>e.LevelUsers).HasForeignKey(e=>e.UserId);
        }
    }
}

