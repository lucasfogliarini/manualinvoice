﻿using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {
        public void Configure(EntityTypeBuilder<Branch> builder)
        {
            builder.ToTable("DLB_NFMANUAL_BRANCH");
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("BRANCH_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<BranchSequenceGenerator>();

            builder.Property(c => c.Name).HasColumnName("BRANCH_NAME").IsRequired();
            builder.Property(c => c.Location).HasColumnName("LOCATION");
            builder.Property(c => c.Description).HasColumnName("DESCRIPTION");
            builder.Property(c => c.IsActive).HasColumnName("IS_ACTIVE");
        }
    }
}
