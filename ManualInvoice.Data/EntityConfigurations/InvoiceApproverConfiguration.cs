using ManualInvoice.Data.Converters;
using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class InvoiceApproverConfiguration : IEntityTypeConfiguration<InvoiceApprover>
    {
        public void Configure(EntityTypeBuilder<InvoiceApprover> builder)
        {
            builder.ToTable("DLB_NFMANUAL_APPROVER");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("APPROVER_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<InvoiceApproverSequenceGenerator>();

            builder.Property(e => e.UserId).HasColumnName("USER_ID").IsRequired(false);
            builder.Property(e => e.EffectiveUserId).HasColumnName("USER_EFFECTIVE_ID").IsRequired(false);
            builder.Property(typeof(long?), "INVOICE_ID").IsRequired(false);
            builder.Property(typeof(int?), "LEVEL_ID").IsRequired(false);

            builder.Property(e => e.Situation).HasColumnName("SITUATION")
                .HasConversion(new ApproverStatusConverter());
            builder.Property(e => e.ApprovalDate).HasColumnName("DT_APPROVAL");
            builder.Property(e => e.LastEmailDate).HasColumnName("DT_LAST_EMAIL");
            builder.Property(e => e.ApprovalSequence).HasColumnName("SEQUENCE_APPROVAL");
            builder.Property(e => e.IsVisible).HasColumnName("VISIBLE");

            builder.HasOne(e => e.User).WithMany().HasForeignKey(e => e.UserId);
            builder.HasOne(e => e.EffectiveUser).WithMany().HasForeignKey(e => e.EffectiveUserId);
            builder.HasOne(e => e.Invoice).WithMany().HasForeignKey("INVOICE_ID");
            builder.HasOne(e => e.Level).WithMany().HasForeignKey("LEVEL_ID");
        }
    }
}

