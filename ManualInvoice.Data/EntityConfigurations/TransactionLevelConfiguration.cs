using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class TransactionLevelConfiguration : IEntityTypeConfiguration<TransactionLevel>
    {
        public void Configure(EntityTypeBuilder<TransactionLevel> builder)
        {
            builder.ToTable("DLB_NFMANUAL_LEVELS_TRANS");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("LEVEL_TRANSACTION_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<TransactionLevelSequenceGenerator>();

            builder.Property(e => e.SequenceApproval).HasColumnName("SEQUENCE_APPROVAL");
            builder.Property(e => e.TransactionId).HasColumnName("TRANSACTION_ID").IsRequired();
            builder.Property(e => e.LevelId).HasColumnName("LEVEL_ID").IsRequired();

            builder.HasOne(e => e.Level).WithMany().HasForeignKey(e=>e.LevelId);
            builder.HasOne(e => e.Transaction).WithMany(e=>e.TransactionLevels).HasForeignKey(e=>e.TransactionId);
        }
    }
}
