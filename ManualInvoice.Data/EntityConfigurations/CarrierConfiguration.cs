using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class CarrierConfiguration : IEntityTypeConfiguration<Carrier>
    {
        public void Configure(EntityTypeBuilder<Carrier> builder)
        {
            builder.ToTable("DLB_NFMANUAL_CARRIER");
            builder.HasKey(e => e.Id);

            builder.Property(c => c.Id).HasColumnName("CARRIER_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<CarrierSequenceGenerator>();

            builder.Property(e => e.Code).HasColumnName("CODE");
            builder.Property(e => e.Name).HasColumnName("NAME");
            builder.Property(e => e.VolumeQuantity).HasColumnName("QTD_VOLUM");
            builder.Property(e => e.VolumeBrand).HasColumnName("BRAND_VOLUM");
            builder.Property(e => e.VolumeNumbering).HasColumnName("NUMBERING_VOLUM");
            builder.Property(e => e.FreightValue).HasColumnName("VALUE_FREIGHT");
            builder.Property(e => e.InsuranceValue).HasColumnName("VALUE_INSURANCE");
            builder.Property(e => e.Weight).HasColumnName("WEIGHT");
            builder.Property(e => e.Specie).HasColumnName("SPECIE");
            builder.Property(e => e.OtherCosts).HasColumnName("OTHERS_COSTS");
        }
    }
}

