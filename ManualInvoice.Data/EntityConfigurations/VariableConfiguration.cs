using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class VariableConfiguration : IEntityTypeConfiguration<Variable>
    {
        public void Configure(EntityTypeBuilder<Variable> builder)
        {
            builder.ToTable("DLB_NFMANUAL_VARIABLE");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("VARIABLE_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<VariableSequenceGenerator>();

            builder.Property(e => e.Length).HasColumnName("LENGTH");
            builder.Property(e => e.Name).HasColumnName("NAME").IsRequired();
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");
            builder.Property(e => e.IsAlphanumeric).HasColumnName("TYPE");
            builder.Property(e => e.IsEditable).HasColumnName("EDITABLE");
            builder.Property(e => e.IsSystem).HasColumnName("IS_SYS_VAR");
            builder.Property(e => e.DefaultValue).HasColumnName("DEFAULT_VALUE");

        }
    }
}

