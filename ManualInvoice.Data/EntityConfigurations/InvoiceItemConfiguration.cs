﻿using ManualInvoice.Data.Converters;
using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class InvoiceItemConfiguration : IEntityTypeConfiguration<InvoiceItem>
    {
        public void Configure(EntityTypeBuilder<InvoiceItem> builder)
        {
            builder.ToTable("DLB_NFMANUAL_INVOICE_ITEM");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnName("INVOICE_ITEM_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<InvoiceItemSequenceGenerator>();

            builder.Property(e => e.ProductCode).HasColumnName("PRODUCT_CODE");
            builder.Property(e => e.ProductName).HasColumnName("PRODUCT_NAME");
            builder.Property(e => e.Price).HasColumnName("PRICE");
            builder.Property(e => e.PriceOriginal).HasColumnName("PRICE_ORIG");
            builder.Property(e => e.Quantity).HasColumnName("QUANTITY");
            builder.Property(e => e.QuantityOriginal).HasColumnName("QUANTITY_ORIG");
            builder.Property(e => e.Ncm).HasColumnName("NCM");
            builder.Property(e => e.RmaCode).HasColumnName("RMA_CODE");
            builder.Property(e => e.RmaName).HasColumnName("RMA_NAME");
            builder.Property(e => e.ItemLine).HasColumnName("ITEM_LINE");
            builder.Property(e => e.OriginalDofId).HasColumnName("ORIGINAL_DOF_ID");
            builder.Property(e => e.OriginalIdfNum).HasColumnName("ORIGINAL_IDF_NUM");
            builder.Property(e => e.DellTieNum).HasColumnName("DELL_TIE_NUM");
            builder.Property(e => e.DellBaseFlag).HasColumnName("DELL_BASE_FLAG")
                .HasConversion(new DellBaseFlagConverter());
        }
    }
}
