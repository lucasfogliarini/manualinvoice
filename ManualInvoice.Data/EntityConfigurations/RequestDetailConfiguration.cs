using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class RequestDetailConfiguration : IEntityTypeConfiguration<Domain.Entities.RequestDetail>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.RequestDetail> builder)
        {
            builder.ToTable("DLB_NFMANUAL_REQUEST_DETAIL_V");
            builder.HasKey(e => e.Id);
            builder.Property(c => c.Id).HasColumnName("INVOICE_ID");
            builder.Property(e => e.TransactionName).HasColumnName("TRANSACTION_NAME");
            builder.Property(e => e.RequestAmount).HasColumnName("REQUEST_AMOUNT");
            builder.Property(e => e.RequestUserId).HasColumnName("REQUEST_USER_ID");
            builder.Property(e => e.RequestUserName).HasColumnName("REQUEST_USERNAME");
            builder.Property(e => e.RequestDate).HasColumnName("REQUEST_DATE");
            builder.Property(e => e.LevelName).HasColumnName("LEVEL_NAME");
            builder.Property(e => e.ApproverId).HasColumnName("APPROVER_ID");
            builder.Property(e => e.ApproverName).HasColumnName("APPROVER_NAME");
            builder.Property(e => e.ApprovalDate).HasColumnName("APPROVAL_DATE");
            builder.Property(e => e.AgingRequestApprovalDate).HasColumnName("AGING_REQUEST_APPROVAL_DATE");
            builder.Property(e => e.InvoiceNumber).HasColumnName("INVOICE_NUMBER");
            builder.Property(e => e.InvoiceDate).HasColumnName("INVOICE_DATE");
            builder.Property(e => e.InvoiceAmount).HasColumnName("INVOICE_AMOUNT");
            builder.Property(e => e.AgingRequestInvoiceDate).HasColumnName("AGING_REQUEST_INVOICE_DATE");
        }
    }
}

