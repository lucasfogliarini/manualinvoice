using ManualInvoice.Data.SequenceGenerators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class DomainConfiguration : IEntityTypeConfiguration<Domain.Entities.Domain>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Domain> builder)
        {
            builder.ToTable("DLB_NFMANUAL_DOMAIN");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("DOMAIN_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<DomainSequenceGenerator>();

            builder.Property(e => e.Code).HasColumnName("DOMAIN_CODE");
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION");

        }
    }
}

