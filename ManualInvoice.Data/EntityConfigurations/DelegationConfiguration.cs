using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class DelegationConfiguration : IEntityTypeConfiguration<Delegation>
    {
        public void Configure(EntityTypeBuilder<Delegation> builder)
        {
            builder.ToTable("DLB_NFMANUAL_DELEGATION");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("DELEGATION_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<DelegationSequenceGenerator>();

            builder.Property(e => e.UserId).HasColumnName("SYSTEM_USER_ID");
            builder.Property(e => e.DelegationDate).HasColumnName("DT_DELEGATION");
            builder.Property(e => e.PeriodFrom).HasColumnName("DT_BEGIN_DELEGATION");
            builder.Property(e => e.PeriodTo).HasColumnName("DT_END_DELEGATION");
            builder.Property(e => e.IsCascade).HasColumnName("BL_CASCADE");
            builder.Property(e => e.IsOldApproval).HasColumnName("BL_OLD_APPROVAL");
            builder.Property(e => e.IsActive).HasColumnName("IS_ACTIVE");

            builder.HasOne(e => e.User).WithMany().HasForeignKey(e=>e.UserId).IsRequired();
        }
    }
}

