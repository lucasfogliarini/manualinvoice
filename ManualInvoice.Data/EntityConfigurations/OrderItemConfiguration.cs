﻿using ManualInvoice.Data.SequenceGenerators;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal sealed class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.ToTable("DLB_NFMANUAL_ORDER_ITEM");
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("ORDER_ITEM_ID")
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<OrderItemSequenceGenerator>();

            builder.Property(typeof(long), "INVOICE_ID").IsRequired();
            builder.Property(typeof(long), "INVOICE_ITEM_ID").IsRequired();

            builder.Property(e => e.DiscountClassCode).HasColumnName("SYN_DISCOUNT_CLASS_CODE").IsRequired(false);
            builder.Property(e => e.ItemUnitSoldPriceOrig).HasColumnName("SYN_ITEM_UNIT_SOLD_PRICE_ORIG").IsRequired(false);
            builder.Property(e => e.ItemUnitSoldPrice).HasColumnName("SYN_ITEM_UNIT_SOLD_PRICE").IsRequired(false);
            builder.Property(e => e.ItemUnitListPriceOrig).HasColumnName("SYN_ITEM_UNIT_LIST_PRICE_ORIG").IsRequired(false);
            builder.Property(e => e.ItemUnitListPrice).HasColumnName("SYN_ITEM_UNIT_LIST_PRICE").IsRequired(false);

            builder.Property(e => e.QtyItemOrig).HasColumnName("SYN_QTY_ITEM_ORIG").IsRequired(false);
            builder.Property(e => e.QtyItem).HasColumnName("SYN_QTY_ITEM").IsRequired(false);
            builder.Property(e => e.ItemLob).HasColumnName("SYN_ITEM_LOB").IsRequired(false);
            builder.Property(e => e.ItemUnitCostAmtOrig).HasColumnName("SYN_ITEM_UNIT_COST_AMT_ORIG").IsRequired(false);
            builder.Property(e => e.ItemUnitCostAmt).HasColumnName("SYN_ITEM_UNIT_COST_AMT").IsRequired(false);

            builder.Property(e => e.ItemUnitCostAmtUs).HasColumnName("SYN_ITEM_UNIT_COST_AMT_US").IsRequired(false);
            builder.Property(e => e.ItemUnitCostAmtUsOrig).HasColumnName("SYN_ITEM_UNIT_COST_AMT_US_ORIG").IsRequired(false);
            builder.Property(e => e.AposStartDate).HasColumnName("SYN_APOS_START_DATE").IsRequired(false);
            builder.Property(e => e.AposEndDate).HasColumnName("SYN_APOS_END_DATE").IsRequired(false);
            builder.Property(e => e.TieNum).HasColumnName("SYN_DELL_TIE_NUM").IsRequired(false);
            builder.Property(e => e.FulFillmentLocId).HasColumnName("SYN_FULFILLMENT_LOC_ID").IsRequired(false);

            builder.HasOne(e => e.Invoice).WithMany().HasForeignKey("INVOICE_ID").IsRequired();
            builder.HasOne(e => e.InvoiceItem).WithMany().HasForeignKey("INVOICE_ITEM_ID").IsRequired();
        }
    }
}
