﻿using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class DelegationViewConfiguration : IEntityTypeConfiguration<DelegationView>
    {
        public void Configure(EntityTypeBuilder<DelegationView> builder)
        {
            builder.ToTable("DLB_NFMANUAL_DELEGATION_V");
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("ID");
            builder.Property(c => c.PeriodFrom).HasColumnName("DT_BEGIN_DELEGATION");
            builder.Property(c => c.PeriodTo).HasColumnName("DT_END_DELEGATION");
            builder.Property(c => c.IsCascade).HasColumnName("BL_CASCADE");
            builder.Property(c => c.IsOldApproval).HasColumnName("BL_OLD_APPROVAL");
            builder.Property(c => c.DelegationByUserId).HasColumnName("BY_USER_ID");
            builder.Property(c => c.DelegationByUserName).HasColumnName("BY_USER_NAME");
            builder.Property(c => c.DelegatedUserId).HasColumnName("DELEGATED_USER_ID");
        }
    }
}