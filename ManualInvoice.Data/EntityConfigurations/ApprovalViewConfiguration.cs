﻿using ManualInvoice.Data.Converters;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class ApprovalViewConfiguration : IEntityTypeConfiguration<ApprovalView>
    {
        public void Configure(EntityTypeBuilder<ApprovalView> builder)
        {
            builder.ToTable("DLB_NFMANUAL_APPROVAL_V");
            builder.HasKey(c => c.Id);
            
            builder.Property(c => c.Id).HasColumnName("ID");
            builder.Property(c => c.InvoiceId).HasColumnName("INVOICE_ID");                
            builder.Property(c => c.DtRequest).HasColumnName("DT_REQUEST");
            builder.Property(c => c.UserId).HasColumnName("SYSTEM_USER_ID");
            builder.Property(c => c.UserName).HasColumnName("USER_NAME");
            builder.Property(c => c.TransactionId).HasColumnName("TRANSACTION_ID");
            builder.Property(c => c.TransactionName).HasColumnName("TRANSACTION_NAME");
            builder.Property(c => c.TransactionType).HasColumnName("TRANSACTION_TYPE")
                .HasConversion(new TransactionTypeConverter());
            builder.Property(c => c.LevelId).HasColumnName("LEVEL_ID");
            builder.Property(c => c.SequenceApproval).HasColumnName("SEQUENCE_APPROVAL");           
            builder.Property(c => c.ApproverUserId).HasColumnName("APPROVER_USER_ID");           
            builder.Property(c => c.ApproverId).HasColumnName("APPROVER_ID");           
        }
    }
}