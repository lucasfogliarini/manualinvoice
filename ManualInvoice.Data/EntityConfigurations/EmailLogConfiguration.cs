using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ManualInvoice.Domain.Entities;
using ManualInvoice.Data.SequenceGenerators;

namespace ManualInvoice.Data.EntityConfigurations
{
    internal class EmailLogConfiguration : IEntityTypeConfiguration<EmailLog>
    {
        public void Configure(EntityTypeBuilder<EmailLog> builder)
        {
            builder.ToTable("DLB_NFMANUAL_EMAIL_LOG");
            builder.HasKey(e => e.Id);

            builder.Property(e=> e.Id).HasColumnName("EMAIL_LOG_ID").IsRequired()
                .ValueGeneratedOnAdd()
                .HasValueGenerator<EmailLogSequenceGenerator>();

            builder.Property(typeof(int), "EMAIL_ID").IsRequired();
            builder.Property(e => e.LogDate).HasColumnName("DT_LOG").IsRequired();
            builder.Property(e => e.Description).HasColumnName("DESCRIPTION").HasMaxLength(2000);

            builder.HasOne(e => e.Email).WithMany().HasForeignKey("EMAIL_ID");
        }
    }
}

