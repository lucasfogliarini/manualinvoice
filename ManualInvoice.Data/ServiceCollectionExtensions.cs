﻿using Dell.Finance.Framework.Core.HttpClientExtensions.Extensions;
using Dell.Finance.Framework.Core.Security;
using Dell.Finance.Framework.Core.Security.Extensions;
using ManualInvoice.Data.Gateways;
using ManualInvoice.Data.Repository;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi;
using ManualInvoice.Domain.Gateways.SkuApi;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;

namespace ManualInvoice.Data
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNFManualUnitOfWork(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<NFManualDbContext>(o => o.UseOracle(connectionString, opt =>
            {
                opt.UseOracleSQLCompatibility("11");
            }));
            services.AddScoped<IUnitOfWork, NFManualUnitOfWork>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<IReasonRepository, ReasonRepository>();
            services.AddScoped<IDelegationRepository, DelegationRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IBranchRepository, BranchRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITransactionBranchRepository, TransactionBranchRepository>();
            services.AddScoped<ILevelRepository, LevelRepository>();
            services.AddScoped<IVariableRepository, VariableRepository>();
            services.AddScoped<ILevelUserRepository, LevelUserRepository>();
            services.AddScoped<ISkuService, SkuService>();
            services.AddScoped<IInvoiceItemRepository, InvoiceItemRepository>();
            services.AddScoped<ICarrierRepository,CarrierRepository>();
            services.AddScoped<ITransactionVariableRepository, TransactionVariableRepository>();
            services.AddScoped<IInvoiceVariableRepository, InvoiceVariableRepository>();
            services.AddScoped<IInvoiceApproverRepository, InvoiceApproverRepository>();
            services.AddScoped<IPaymentCodeRepository, PaymentCodeRepository>();
            services.AddScoped<ITransactionLevelRepository, TransactionLevelRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderItemRepository, OrderItemRepository>();            
            services.AddScoped<IApprovalViewRepository, ApprovalViewRepository>();
            services.AddScoped<ISystemConfigurationRepository, SystemConfigurationRepository>();
            services.AddScoped<IDelegationViewRepository, DelegationViewRepository>();
            services.AddScoped<ISkuFileRepository, SkuFileRepository>();
            services.AddScoped<IEmailRepository, EmailRepository>();

            return services;
        }

        public static IServiceCollection AddGateways(this IServiceCollection services, AppSettings appSettings)
        {
            services.AddMemoryCache();
            services.ConfigureHttpClientExtensions(NullLogger.Instance);

            services.AddSecurityTokenProvider(new SecurityConfiguration
            {
                SecurityUrl = appSettings.SecurityApiUrl,
                SecuritySystem = appSettings.SecurityApiSystem,
                SecurityPassword = appSettings.SecurityApiPassword
            });

            services.AddHttpClient<ICustomerApiGateway, CustomerApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            services.AddHttpClient<IInvoiceApiGateway, InvoiceApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            services.AddHttpClient<ISkuApiGateway, SkuApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            services.AddHttpClient<ICommonApiGateway, CommonApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            services.AddHttpClient<ISecurityApiGateway, SecurityApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            services.AddHttpClient<IDirectApiGateway, DirectApiGateway>().AddSecurityAuthorization().AddPollyRetry();
            return services;
        }
    }
}