﻿using ManualInvoice.Domain.Common;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ManualInvoice.Data.Converters
{
    public class ApproverStatusConverter : ValueConverter<ApproverStatusType?, string>
    {
        public ApproverStatusConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(ApproverStatusType? approverStatus)
        {
            switch (approverStatus)
            {
                case ApproverStatusType.Analyzing:
                    return "W";
                case ApproverStatusType.Rejected:
                    return "R";
                case ApproverStatusType.Approved:
                    return "A";
                case ApproverStatusType.Cancelled:
                    return "C";
                case ApproverStatusType.Pending:
                    return "E";
                default:
                    return null;
            }
        }

        public static ApproverStatusType? Deserialize(string value)
        {
            switch (value)
            {
                case "W":
                    return ApproverStatusType.Analyzing;
                case "R":
                    return ApproverStatusType.Rejected;
                case "A":
                    return ApproverStatusType.Approved;
                case "C":
                    return ApproverStatusType.Cancelled;
                case "E":
                    return ApproverStatusType.Pending;
                default:
                    return null;
            }
        }
    }
}
