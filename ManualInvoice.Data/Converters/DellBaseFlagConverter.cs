﻿using ManualInvoice.Domain.Enum;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ManualInvoice.Data.Converters
{
    public class DellBaseFlagConverter : ValueConverter<DellBaseFlagType?, string>
    {
        public DellBaseFlagConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(DellBaseFlagType? dellBaseFlagType)
        {
            switch (dellBaseFlagType)
            {
                case DellBaseFlagType.Base:
                    return "Y";
                case DellBaseFlagType.NonBase:
                    return "N";
                case DellBaseFlagType.Rollup:
                    return "R";
                default:
                    return null;
            }
        }

        public static DellBaseFlagType? Deserialize(string value)
        {
            switch (value)
            {
                case "Y":
                    return DellBaseFlagType.Base;
                case "N":
                    return DellBaseFlagType.NonBase;
                case "R":
                    return DellBaseFlagType.Rollup;
                default:
                    return DellBaseFlagType.Unknown;
            }
        }
    }
}
