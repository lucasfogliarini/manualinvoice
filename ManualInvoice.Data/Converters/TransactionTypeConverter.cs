﻿using ManualInvoice.Domain.Common;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ManualInvoice.Data.Converters
{
    public class TransactionTypeConverter : ValueConverter<TransactionType?, string>
    {
        public TransactionTypeConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(TransactionType? transactionType)
        {
            switch (transactionType)
            {
                case TransactionType.Inbound:
                    return "E";
                case TransactionType.Outbound:
                    return "S";
                default:
                    return null;
            }
        }

        public static TransactionType? Deserialize(string value)
        {
            switch (value)
            {
                case "E":
                    return TransactionType.Inbound;
                case "S":
                    return TransactionType.Outbound;
                default:
                    return null;
            }
        }
    }
}
