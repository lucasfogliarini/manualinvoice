﻿using ManualInvoice.Domain.Enum;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Data.Converters
{
    public class OrderSynchroStatusConverter : ValueConverter<OrderSynchroStatus?, string>
    {
        public OrderSynchroStatusConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(OrderSynchroStatus? invoiceType)
        {
            switch (invoiceType)
            {
                case OrderSynchroStatus.None:
                    return "NONE";
                case OrderSynchroStatus.StageBillTo:
                    return "STAGEBILLTO";
                case OrderSynchroStatus.StageShipTo:
                    return "STAGESHIPTO";
                case OrderSynchroStatus.StageUpdateOrder:
                    return "STAGEUPDATEORDER";
                case OrderSynchroStatus.StageSalesOrder:
                    return "STAGESALESORDER";
                case OrderSynchroStatus.StageLoadVolume:
                    return "STAGELOADVOLUME";
                case OrderSynchroStatus.Finalized:
                    return "FINALIZED";
                default:
                    return null;
            }
        }

        public static OrderSynchroStatus? Deserialize(string value)
        {
            switch (value)
            {
                case "NONE":
                    return OrderSynchroStatus.None;
                case "STAGEBILLTO":
                    return OrderSynchroStatus.StageBillTo;
                case "STAGESHIPTO":
                    return OrderSynchroStatus.StageShipTo;
                case "STAGEUPDATEORDER":
                    return OrderSynchroStatus.StageUpdateOrder;
                case "STAGESALESORDER":
                    return OrderSynchroStatus.StageSalesOrder;
                case "STAGELOADVOLUME":
                    return OrderSynchroStatus.StageLoadVolume;
                case "FINALIZED":
                    return OrderSynchroStatus.Finalized;
                default:
                    return null;
            }
        }
    }
}
