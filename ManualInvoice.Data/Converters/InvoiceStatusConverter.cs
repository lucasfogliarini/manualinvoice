﻿using ManualInvoice.Domain.Common;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ManualInvoice.Data.Converters
{
    public class InvoiceStatusConverter : ValueConverter<InvoiceStatusType?, string>
    {
        public InvoiceStatusConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(InvoiceStatusType? invoiceStatus)
        {
            switch (invoiceStatus)
            {
                case InvoiceStatusType.Analyzing:
                    return "ANALYZING";
                case InvoiceStatusType.Approved:
                    return "APPROVED";
                case InvoiceStatusType.Cancelled:
                    return "CANCELLED";
                case InvoiceStatusType.Draft:
                    return "DRAFT";
                case InvoiceStatusType.Finalized:
                    return "FINALIZED";
                case InvoiceStatusType.Rejected:
                    return "REJECTED";
                default:
                    return null;
            }
        }

        public static InvoiceStatusType? Deserialize(string value)
        {
            switch (value)
            {
                case "ANALYZING":
                    return InvoiceStatusType.Analyzing;
                case "APPROVED":
                    return InvoiceStatusType.Approved;
                case "CANCELLED":
                    return InvoiceStatusType.Cancelled;
                case "DRAFT":
                    return InvoiceStatusType.Draft;
                case "FINALIZED":
                    return InvoiceStatusType.Finalized;
                case "REJECTED":
                    return InvoiceStatusType.Rejected;
                default:
                    return null;
            }
        }
    }
}
