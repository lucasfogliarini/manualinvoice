﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ManualInvoice.Data.Converters
{
    public class InvoiceTypeConverter : ValueConverter<InvoiceType?, string>
    {
        public InvoiceTypeConverter(ConverterMappingHints mappingHints = null)
            : base(x => Serialize(x), x => Deserialize(x), mappingHints)
        {
        }

        public static string Serialize(InvoiceType? invoiceType)
        {
            switch (invoiceType)
            {
                case InvoiceType.Product:
                    return "PRODUCT";
                case InvoiceType.Service:
                    return "SERVICE";
                default:
                    return null;
            }
        }

        public static InvoiceType? Deserialize(string value)
        {
            switch (value)
            {
                case "PRODUCT":
                    return InvoiceType.Product;
                case "SERVICE":
                    return InvoiceType.Service;
                default:
                    return null;
            }
        }
    }
}