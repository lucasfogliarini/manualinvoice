namespace ManualInvoice.Data.SequenceGenerators
{
    internal class UserSequenceGenerator : SequenceGeneratorBase<int>
    {
        public UserSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_SYSTEM_USER_SEQ")
        {
        }
    }
}

