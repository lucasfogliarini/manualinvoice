﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Data.SequenceGenerators
{
    internal class OrderItemSequenceGenerator : SequenceGeneratorBase<long>
    {
        public OrderItemSequenceGenerator() : 
            base("NFMANUAL.DLB_NFMANUAL_ORDER_ITEM_SEQ")
        {
        }
    }
}
