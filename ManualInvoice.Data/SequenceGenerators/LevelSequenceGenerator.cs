namespace ManualInvoice.Data.SequenceGenerators
{
    internal class LevelSequenceGenerator : SequenceGeneratorBase<int>
    {
        public LevelSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_LEVEL_SEQ")
        {
        }
    }
}

