﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class InvoiceSequenceGenerator : SequenceGeneratorBase<long>
    {
        public InvoiceSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_INVOICE_SEQ")
        {
        }
    }
}
