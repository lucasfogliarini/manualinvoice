﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class ReasonSequenceGenerator : SequenceGeneratorBase<int>
    {
        public ReasonSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_REASON_SEQ")
        {
        }
    }
}
