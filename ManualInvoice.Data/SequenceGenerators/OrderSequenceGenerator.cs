﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Data.SequenceGenerators
{
    internal class OrderSequenceGenerator : SequenceGeneratorBase<long>
    {
        public OrderSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_ORDER_SEQ")
        {
        }
    }
}
