﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class TransactionSequenceGenerator : SequenceGeneratorBase<int>
    {
        public TransactionSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_TRANS_SEQ")
        {
        }
    }
}
