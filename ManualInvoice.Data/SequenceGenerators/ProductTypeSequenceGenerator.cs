namespace ManualInvoice.Data.SequenceGenerators
{
    internal class ProductTypeSequenceGenerator : SequenceGeneratorBase<int>
    {
        public ProductTypeSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_PRODUCT_TYPE_SEQ")
        {
        }
    }
}
