namespace ManualInvoice.Data.SequenceGenerators
{
    internal class VariableSequenceGenerator : SequenceGeneratorBase<int>
    {
        public VariableSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_VARIABLE_SEQ")
        {
        }
    }
}

