namespace ManualInvoice.Data.SequenceGenerators
{
    internal class DomainValueSequenceGenerator : SequenceGeneratorBase<int>
    {
        public DomainValueSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_DOMAIN_VALUE_SEQ")
        {
        }
    }
}

