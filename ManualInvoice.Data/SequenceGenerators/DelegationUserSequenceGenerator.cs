namespace ManualInvoice.Data.SequenceGenerators
{
    internal class DelegationUserSequenceGenerator : SequenceGeneratorBase<int>
    {
        public DelegationUserSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_DELEG_USER_SEQ")
        {
        }
    }
}

