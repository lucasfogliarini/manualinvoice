namespace ManualInvoice.Data.SequenceGenerators
{
    internal class TransactionLevelSequenceGenerator : SequenceGeneratorBase<string>
    {
        public TransactionLevelSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_LEVELS_TRANS_SEQ")
        {
        }
    }
}
