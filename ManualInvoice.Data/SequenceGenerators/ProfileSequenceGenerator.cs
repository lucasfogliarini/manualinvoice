namespace ManualInvoice.Data.SequenceGenerators
{
    internal class ProfileSequenceGenerator : SequenceGeneratorBase<int>
    {
        public ProfileSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_PROFILE_SEQ")
        {
        }
    }
}

