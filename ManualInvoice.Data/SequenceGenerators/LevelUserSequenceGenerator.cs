namespace ManualInvoice.Data.SequenceGenerators
{
    internal class LevelUserSequenceGenerator : SequenceGeneratorBase<int>
    {
        public LevelUserSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_LEVEL_USER_SEQ")
        {
        }
    }
}

