namespace ManualInvoice.Data.SequenceGenerators
{
    internal class InvoiceApproverSequenceGenerator : SequenceGeneratorBase<int>
    {
        public InvoiceApproverSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_APPROVER_SEQ")
        {
        }
    }
}

