﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class BranchSequenceGenerator : SequenceGeneratorBase<int>
    {
        public BranchSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_BRANCH_SEQ")
        {
        }
    }
}
