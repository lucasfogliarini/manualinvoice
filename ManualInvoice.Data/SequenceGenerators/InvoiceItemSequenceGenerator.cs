﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class InvoiceItemSequenceGenerator : SequenceGeneratorBase<long>
    {
        public InvoiceItemSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_PRODUCT_SEQ")
        {
        }
    }
}
