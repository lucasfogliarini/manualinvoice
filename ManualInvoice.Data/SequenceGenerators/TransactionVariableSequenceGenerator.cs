namespace ManualInvoice.Data.SequenceGenerators
{
    internal class TransactionVariableSequenceGenerator : SequenceGeneratorBase<int>
    {
        public TransactionVariableSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_VAR_TRANS_SEQ")
        {
        }
    }
}

