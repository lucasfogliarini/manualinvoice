namespace ManualInvoice.Data.SequenceGenerators
{
    internal class InvoiceVariableSequenceGenerator : SequenceGeneratorBase<int>
    {
        public InvoiceVariableSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_VAR_INVOICE_SEQ")
        {
        }
    }
}

