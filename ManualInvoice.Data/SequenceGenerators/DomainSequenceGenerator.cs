namespace ManualInvoice.Data.SequenceGenerators
{
    internal class DomainSequenceGenerator : SequenceGeneratorBase<int>
    {
        public DomainSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_DOMAIN_SEQ")
        {
        }
    }
}

