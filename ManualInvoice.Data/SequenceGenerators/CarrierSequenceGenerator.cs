﻿namespace ManualInvoice.Data.SequenceGenerators
{
    internal class CarrierSequenceGenerator : SequenceGeneratorBase<long>
    {
        public CarrierSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_CARRIER_SEQ")
        {
        }
    }
}
