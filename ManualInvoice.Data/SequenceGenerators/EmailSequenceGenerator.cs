namespace ManualInvoice.Data.SequenceGenerators
{
    internal class EmailSequenceGenerator : SequenceGeneratorBase<int>
    {
        public EmailSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_EMAIL_SEQ")
        {
        }
    }
}
