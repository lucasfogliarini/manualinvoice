﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Data.Common;

namespace ManualInvoice.Data.SequenceGenerators
{
    internal abstract class SequenceGeneratorBase<TKey> : ValueGenerator<TKey>
    {
        private readonly string m_sequenceName;

        protected SequenceGeneratorBase(string sequenceName)
        {
            m_sequenceName = sequenceName;
        }

        public override bool GeneratesTemporaryValues => false;

        public override TKey Next(EntityEntry entry)
        {
            using (DbCommand command = entry.Context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT {m_sequenceName}.NEXTVAL FROM DUAL";
                entry.Context.Database.OpenConnection();
                return (TKey)Convert.ChangeType(command.ExecuteScalar(), typeof(TKey));
            }
        }
    }
}
