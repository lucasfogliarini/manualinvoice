namespace ManualInvoice.Data.SequenceGenerators
{
    internal class PaymentCodeSequenceGenerator : SequenceGeneratorBase<int>
    {
        public PaymentCodeSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_PAYMENT_CODE_SEQ")
        {
        }
    }
}
