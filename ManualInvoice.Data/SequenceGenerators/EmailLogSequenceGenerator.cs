namespace ManualInvoice.Data.SequenceGenerators
{
    internal class EmailLogSequenceGenerator : SequenceGeneratorBase<int>
    {
        public EmailLogSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_EMAIL_LOG_SEQ")
        {
        }
    }
}

