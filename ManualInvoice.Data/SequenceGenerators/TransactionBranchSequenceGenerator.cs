namespace ManualInvoice.Data.SequenceGenerators
{
    internal class TransactionBranchSequenceGenerator : SequenceGeneratorBase<string>
    {
        public TransactionBranchSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_BRANCH_TRANS_SEQ")
        {
        }
    }
}

