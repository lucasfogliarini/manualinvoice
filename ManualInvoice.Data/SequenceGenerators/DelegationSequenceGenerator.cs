namespace ManualInvoice.Data.SequenceGenerators
{
    internal class DelegationSequenceGenerator : SequenceGeneratorBase<int>
    {
        public DelegationSequenceGenerator()
            : base("NFMANUAL.DLB_NFMANUAL_DELEGATION_SEQ")
        {
        }
    }
}

