﻿using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Data
{
    internal class NFManualUnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly DbContext _dbContext;

        public NFManualUnitOfWork(NFManualDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> Query<TEntity>(bool tracked = false, params Expression<Func<TEntity, object>>[] includes)
            where TEntity : class, IEntity
        {
            IQueryable<TEntity> result = _dbContext.Set<TEntity>();

            foreach (var include in includes)
            {
                result = result.Include(include);
            }

            if (!tracked)
            {
                result = result.AsNoTracking();
            }

            return result;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Add(entity);
        }

        public void AttachUnchanged<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            if (null != entity && _dbContext.Entry(entity).State != EntityState.Detached)
            {
                _dbContext.Entry(entity).State = EntityState.Unchanged;
            }
        }

        public void AttachUnchangedRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class, IEntity
        {
            if (null != entities && entities.Any())
            {
                foreach (var entity in entities)
                {
                    if (_dbContext.Entry(entity).State != EntityState.Detached)
                    {
                        _dbContext.Entry(entity).State = EntityState.Unchanged;
                    }
                }
            }
        }

        public void AttachDeleted<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            if (null != entity)
            {
                _dbContext.Entry(entity).State = EntityState.Deleted;
            }
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) 
            where TEntity : class, IEntity
        {
            _dbContext.RemoveRange(entities);
        }

        public void RemoveRange<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class, IEntity
        {
            var entities = Query<TEntity>().Where(predicate);
            _dbContext.RemoveRange(entities);
        }

        public void Commit()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex) when (ex.InnerException != null)
            {
                throw new ApplicationException(ex.InnerException.Message, ex);
            }
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Update(entity);
        }

        public TEntity Find<TEntity>(params object[] keyValues)
            where TEntity : class, IEntity
        {
            var set = _dbContext.Set<TEntity>();
            var result = set.Find(keyValues);

            return result;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public int ExecuteSqlCommand(string sql, params object[] parameters)
        {
            int rows =  _dbContext.Database.ExecuteSqlCommand(sql, parameters);
            return rows;
        }
    }
}
