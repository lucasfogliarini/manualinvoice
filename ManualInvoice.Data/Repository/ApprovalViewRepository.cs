﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class ApprovalViewRepository : EntityRepositoryBase<ApprovalView>, IApprovalViewRepository
    {
        public ApprovalViewRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public override void Add(ApprovalView entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(ApprovalView entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public override void Update(ApprovalView entity)
        {
            UnitOfWork.Update(entity);
        }
        public IQueryable<ApprovalView> QuerySearch()
        {
            return UnitOfWork.Query<ApprovalView>(false);
        }
    }
}
