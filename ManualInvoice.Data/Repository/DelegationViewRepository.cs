﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class DelegationViewRepository : EntityRepositoryBase<DelegationView>, IDelegationViewRepository
    {
        public DelegationViewRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public override void Add(DelegationView entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(DelegationView entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public override void Update(DelegationView entity)
        {
            UnitOfWork.Update(entity);
        }
        public IQueryable<DelegationView> QuerySearch()
        {
            return UnitOfWork.Query<DelegationView>(false);
        }
    }
}
