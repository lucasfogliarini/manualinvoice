﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class OrderItemRepository : EntityRepositoryBase<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(OrderItem entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Order);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.InvoiceItem);
        }

        public override void Delete(OrderItem entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Order);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.InvoiceItem);
        }

        public override void Update(OrderItem entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Order);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.InvoiceItem);
        }

        public IQueryable<OrderItem> QuerySearch()
        {
            return UnitOfWork.Query<OrderItem>();
        }
        public IQueryable<OrderItem> QueryComplete(bool tracked)
        {
            var query = QueryComplete();
            if (!tracked)
            {
                query = query.AsNoTracking();
            }
            return query;
        }
        public override IQueryable<OrderItem> QueryComplete()
        {
            return UnitOfWork
                .Query<OrderItem>(true, e => e.Order, e => e.Invoice, e => e.InvoiceItem);
        }
    }
}
