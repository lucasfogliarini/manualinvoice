﻿using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Data.Repository
{
    public abstract class RepositoryBase : IRepository
    {
        private readonly IUnitOfWork m_unitOfWork;

        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            m_unitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get { return m_unitOfWork; } }
    }
}
