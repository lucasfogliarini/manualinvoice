﻿using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Entities;
using ManualInvoice.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Data.Repository
{
    public abstract class EntityRepositoryBase<TEntity> : RepositoryBase, IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
    {
        public EntityRepositoryBase(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public abstract void Add(TEntity entity);

        public abstract void Delete(TEntity entity);

        public virtual IQueryable<TEntity> QueryComplete()
        {
            return UnitOfWork.Query<TEntity>(true);
        }

        public abstract void Update(TEntity entity);

        public virtual TEntity Find(params object[] keyValues)
        {
            return UnitOfWork.Find<TEntity>(keyValues);
        }

        public void CheckDuplicated(Expression<Func<TEntity, bool>> predicate, IDictionary<string, object> errors)
        {
            var isduplicated = UnitOfWork.Query<TEntity>().Any(predicate);
            if (isduplicated)
            {
                throw new DuplicatedException(typeof(TEntity).Name, errors);
            }
        }
    }
}
