﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class CarrierRepository : EntityRepositoryBase<Carrier>, ICarrierRepository
    {
        public CarrierRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(Carrier entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Carrier entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public override void Update(Carrier entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}