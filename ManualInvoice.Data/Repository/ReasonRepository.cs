﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class ReasonRepository : EntityRepositoryBase<Reason>, IReasonRepository
    {
        public ReasonRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(Reason entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Reason entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Reason> QuerySearch()
        {
            return UnitOfWork.Query<Reason>(false);
        }

        public override void Update(Reason entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}
