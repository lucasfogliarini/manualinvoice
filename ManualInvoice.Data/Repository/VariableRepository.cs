﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class VariableRepository : EntityRepositoryBase<Variable>, IVariableRepository
    {
        public VariableRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(Variable entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Variable entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Variable> QuerySearch()
        {
            return UnitOfWork.Query<Variable>();
        }

        public override void Update(Variable entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}