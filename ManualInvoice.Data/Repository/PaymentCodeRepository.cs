﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class PaymentCodeRepository : EntityRepositoryBase<PaymentCode>, IPaymentCodeRepository
    {
        public PaymentCodeRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(PaymentCode entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(PaymentCode entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public override void Update(PaymentCode entity)
        {
            UnitOfWork.Update(entity);
        }

        public IQueryable<PaymentCode> QuerySearch()
        {
            return UnitOfWork.Query<PaymentCode>();
        }
    }
}
