﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class TransactionVariableRepository : EntityRepositoryBase<TransactionVariable>, ITransactionVariableRepository
    {
        public TransactionVariableRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(TransactionVariable entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Transaction);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override void Delete(TransactionVariable entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Transaction);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override void Update(TransactionVariable entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Transaction);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override IQueryable<TransactionVariable> QueryComplete()
        {
            return UnitOfWork.Query<TransactionVariable>(false, e => e.Transaction, e => e.Variable);
        }

        public void UpdateTransactionVariables(Transaction transaction)
        {
            UnitOfWork.RemoveRange<TransactionVariable>(tv => tv.TransactionId == transaction.Id);

            foreach (var tv in transaction.TransactionVariables)
            {
                var transactionVariable = new TransactionVariable
                {
                    IsMandatory = tv.IsMandatory,
                    TransactionId = transaction.Id,
                    VariableId = tv.VariableId
                };

                Add(transactionVariable);
            }
        }
    }
}
