﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class LevelRepository : EntityRepositoryBase<Level>, ILevelRepository
    {
        public LevelRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(Level entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Level entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Level> QuerySearch()
        {
            return UnitOfWork.Query<Level>();
        }

        public override void Update(Level entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}