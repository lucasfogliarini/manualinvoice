﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class TransactionLevelRepository : EntityRepositoryBase<TransactionLevel>, ITransactionLevelRepository
    {
        public TransactionLevelRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Add(TransactionLevel entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public override void Delete(TransactionLevel entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public override void Update(TransactionLevel entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public override IQueryable<TransactionLevel> QueryComplete()
        {
            return UnitOfWork.Query<TransactionLevel>(false, e => e.Transaction, e => e.Level);
        }

        public void UpdateTransactionLevels(Transaction transaction)
        {
            UnitOfWork.RemoveRange<TransactionLevel>(tl => tl.TransactionId == transaction.Id);

            foreach (var tl in transaction.TransactionLevels)
            {
                var transactionLevel = new TransactionLevel
                {
                    LevelId = tl.LevelId,
                    SequenceApproval = tl.SequenceApproval,
                    TransactionId = transaction.Id
                };

                Add(transactionLevel);
            }
        }
    }
}
