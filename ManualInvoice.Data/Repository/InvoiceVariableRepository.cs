﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class InvoiceVariableRepository : EntityRepositoryBase<InvoiceVariable>, IInvoiceVariableRepository
    {
        public InvoiceVariableRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(InvoiceVariable entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override void Delete(InvoiceVariable entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override void Update(InvoiceVariable entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Variable);
        }

        public override IQueryable<InvoiceVariable> QueryComplete()
        {
            return UnitOfWork
                .Query<InvoiceVariable>(true, e => e.Invoice, e => e.User, e => e.Variable);
        }

        public IQueryable<InvoiceVariable> QuerySearch()
        {
            return UnitOfWork.Query<InvoiceVariable>(false);
        }
    }
}
