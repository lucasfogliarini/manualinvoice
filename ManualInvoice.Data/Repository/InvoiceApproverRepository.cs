﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class InvoiceApproverRepository : EntityRepositoryBase<InvoiceApprover>, IInvoiceApproverRepository
    {
        public InvoiceApproverRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(InvoiceApprover entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.EffectiveUser);
        }

        public override void Delete(InvoiceApprover entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.EffectiveUser);
        }

        public override void Update(InvoiceApprover entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchanged(entity.Level);
            UnitOfWork.AttachUnchanged(entity.EffectiveUser);
        }

        public override IQueryable<InvoiceApprover> QueryComplete()
        {
            return UnitOfWork
                .Query<InvoiceApprover>(true, e => e.Invoice, e => e.User, e => e.Level, e => e.EffectiveUser, e => e.Invoice.RejectReason);
        }

        public IQueryable<InvoiceApprover> QuerySearch(bool tracked = false)
        {
            return UnitOfWork.Query<InvoiceApprover>(tracked);
        }
    }
}
