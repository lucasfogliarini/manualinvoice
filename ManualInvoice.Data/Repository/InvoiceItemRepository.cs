﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class InvoiceItemRepository : EntityRepositoryBase<InvoiceItem>, IInvoiceItemRepository
    {
        public InvoiceItemRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(InvoiceItem entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public override void Delete(InvoiceItem entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public override void Update(InvoiceItem entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public IQueryable<InvoiceItem> QuerySearch()
        {
            return UnitOfWork.Query<InvoiceItem>();
        }

        public IQueryable<InvoiceItem> QueryComplete(bool tracked)
        {
            var query = QueryComplete();
            if (!tracked)
            {
                query = query.AsNoTracking();
            }
            return query;
        }

        public override IQueryable<InvoiceItem> QueryComplete()
        {
            return UnitOfWork.Query<InvoiceItem>(true, e => e.Invoice);
        }
    }
}
