﻿using ExcelDataReader;
using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ManualInvoice.Data.Repository
{
    public class SkuFileRepository : ISkuFileRepository
    {
        private const int TP_ITEM = 0;
        private const int NR_ITEM = 1;
        private const int QT_ITEM = 2;
        private const int VL_ITEM = 3;
        private const int NR_SVC_TAG = 4;
        private const int NR_RMA = 5;

        public IEnumerable<ImportedSkuDto> ReadFile(Stream stream)
        {
            var importedSkus = new List<ImportedSkuDto>();

            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                // Discard header
                reader.Read();

                while (reader.Read())
                {
                    // Ignore empty row
                    if (IsEmptyRow(reader))
                    {
                        continue;
                    }

                    ImportedSkuDto importedSku = null;

                    try
                    {
                        importedSku = new ImportedSkuDto
                        {
                            Type = GetType(reader),
                            Code = GetCode(reader),
                            Quantity = GetQuantity(reader),
                            Price = GetPrice(reader),
                            ServiceTag = GetServiceTag(reader),
                            RmaCode = GetRmaCode(reader)
                        };
                    }
                    catch (Exception ex)
                    {
                        importedSku = new ImportedSkuDto
                        {
                            Error = ex.Message
                        };
                    }

                    importedSkus.Add(importedSku);
                }
            }

            return importedSkus;
        }

        private bool IsEmptyRow(IExcelDataReader reader)
        {
            return string.IsNullOrWhiteSpace(reader.GetValue(TP_ITEM)?.ToString())
                && string.IsNullOrWhiteSpace(reader.GetValue(NR_ITEM)?.ToString())
                && string.IsNullOrWhiteSpace(reader.GetValue(QT_ITEM)?.ToString())
                && string.IsNullOrWhiteSpace(reader.GetValue(VL_ITEM)?.ToString())
                && string.IsNullOrWhiteSpace(reader.GetValue(NR_SVC_TAG)?.ToString())
                && string.IsNullOrWhiteSpace(reader.GetValue(NR_RMA)?.ToString());
        }

        private string GetType(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= TP_ITEM ? null : reader.GetValue(TP_ITEM)?.ToString(); ;
            return string.IsNullOrWhiteSpace(value) ? string.Empty : value;
        }

        private string GetCode(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= NR_ITEM ? null : reader.GetValue(NR_ITEM)?.ToString().Trim().Replace("\\n", string.Empty);
            return string.IsNullOrWhiteSpace(value) ? string.Empty : value;
        }

        private int GetQuantity(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= QT_ITEM ? null : reader.GetValue(QT_ITEM)?.ToString().Trim();
            return int.TryParse(value, out int qtItem) ? qtItem : 0;
        }

        private decimal GetPrice(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= VL_ITEM ? null : reader.GetValue(VL_ITEM)?.ToString().Trim();
            return decimal.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out decimal vlItem) ? decimal.Round(vlItem, 6) : 0M;
        }

        private string GetServiceTag(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= NR_SVC_TAG ? null : reader.GetValue(NR_SVC_TAG)?.ToString().Trim();
            return string.IsNullOrWhiteSpace(value) ? string.Empty : value;
        }

        private string GetRmaCode(IExcelDataReader reader)
        {
            var value = reader.FieldCount <= NR_RMA ? null : reader.GetValue(NR_RMA)?.ToString().Trim();
            return string.IsNullOrWhiteSpace(value) ? string.Empty : value;
        }
    }
}
