﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class LevelUserRepository : RepositoryBase, ILevelUserRepository
    {
        public LevelUserRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public void UpdateLevelUsers(int userId, IEnumerable<int> levelIds)
        {
            var removeLevelUsers = UnitOfWork.Query<LevelUser>().Where(e => e.UserId == userId);
            UnitOfWork.RemoveRange(removeLevelUsers);

            var levelUsers = levelIds.Select(levelId => new LevelUser() { UserId = userId, LevelId = levelId }).ToList();
            foreach (var levelUser in levelUsers)
            {
                UnitOfWork.Add(levelUser);
            }
            
            UnitOfWork.Commit();
        }
    }
}