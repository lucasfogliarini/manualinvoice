﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class TransactionRepository : EntityRepositoryBase<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(Transaction entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchangedRange(entity.TransactionBranches.Select(tb => tb.Branch));
            UnitOfWork.AttachUnchangedRange(entity.TransactionLevels.Select(tl => tl.Level));
            UnitOfWork.AttachUnchangedRange(entity.TransactionVariables.Select(tv => tv.Variable));
        }

        public override void Delete(Transaction entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Transaction> QuerySearch()
        {
            return UnitOfWork.Query<Transaction>(false);
        }

        public override void Update(Transaction entity)
        {
            UnitOfWork.Update(entity);
        }

        public override IQueryable<Transaction> QueryComplete()
        {
            return UnitOfWork.Query<Transaction>(true);
        }
    }
}
