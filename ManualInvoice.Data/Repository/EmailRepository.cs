﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class EmailRepository : EntityRepositoryBase<Email>, IEmailRepository
    {
        public EmailRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(Email entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Email entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Email> QuerySearch()
        {
            return UnitOfWork.Query<Email>(false);
        }

        public override void Update(Email entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}
