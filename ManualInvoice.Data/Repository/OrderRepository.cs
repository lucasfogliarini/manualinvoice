﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class OrderRepository : EntityRepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(Order entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchangedRange(entity.OrderItems);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public override void Delete(Order entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchangedRange(entity.OrderItems);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public override void Update(Order entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchangedRange(entity.OrderItems);
            UnitOfWork.AttachUnchanged(entity.Invoice);
        }

        public override IQueryable<Order> QueryComplete()
        {
            return UnitOfWork
                .Query<Order>(true, e => e.OrderItems);
        }

        public IQueryable<Order> QuerySearch()
        {
            return UnitOfWork.Query<Order>();
        }
    }
}
