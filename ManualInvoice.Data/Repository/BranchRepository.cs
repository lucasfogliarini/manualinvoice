﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class BranchRepository : EntityRepositoryBase<Branch>, IBranchRepository
    {
        public BranchRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(Branch entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Branch entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Branch> QuerySearch()
        {
            return UnitOfWork.Query<Branch>(false);
        }

        public override void Update(Branch entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}
