﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class SystemConfigurationRepository : EntityRepositoryBase<SystemConfiguration>, ISystemConfigurationRepository
    {
        public SystemConfigurationRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override void Add(SystemConfiguration entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(SystemConfiguration entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<SystemConfiguration> QuerySearch()
        {
            return UnitOfWork.Query<SystemConfiguration>(false);
        }

        public override void Update(SystemConfiguration entity)
        {
            UnitOfWork.Update(entity);
        }
    }
}
