﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class InvoiceRepository : EntityRepositoryBase<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(Invoice entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Branch);
            UnitOfWork.AttachUnchanged(entity.Transaction);
            UnitOfWork.AttachUnchanged(entity.Reason);
            UnitOfWork.AttachUnchanged(entity.RejectReason);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchangedRange(entity.Items);
            UnitOfWork.AttachUnchanged(entity.Carrier);
        }

        public override void Delete(Invoice entity)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Invoice> QueryComplete(bool tracked)
        {
            var query = QueryComplete();

            if (!tracked)
            {
                query = query.AsNoTracking();
            }

            return query;
        }

        public override IQueryable<Invoice> QueryComplete()
        {
            return UnitOfWork
                .Query<Invoice>(true, e => e.Branch, e => e.Transaction, e => e.User, e => e.Reason, e => e.Items, e => e.Carrier, e => e.RejectUser);
        }

        public override void Update(Invoice entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Branch);
            UnitOfWork.AttachUnchanged(entity.Transaction);
            UnitOfWork.AttachUnchanged(entity.Reason);
            UnitOfWork.AttachUnchanged(entity.RejectReason);
            UnitOfWork.AttachUnchanged(entity.User);
            UnitOfWork.AttachUnchangedRange(entity.Items);
            UnitOfWork.AttachUnchanged(entity.Carrier);
        }

        public IQueryable<Invoice> QuerySearch()
        {
            return UnitOfWork.Query<Invoice>();
        }

        public IQueryable<RequestDetail> QueryRequestDetail()
        {
            return UnitOfWork.Query<RequestDetail>();
        }
    }
}
