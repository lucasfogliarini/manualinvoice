﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public sealed class TransactionBranchRepository : EntityRepositoryBase<TransactionBranch>, ITransactionBranchRepository
    {
        public TransactionBranchRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Add(TransactionBranch entity)
        {
            UnitOfWork.Add(entity);
            UnitOfWork.AttachUnchanged(entity.Branch);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public override void Delete(TransactionBranch entity)
        {
            UnitOfWork.AttachDeleted(entity);
            UnitOfWork.AttachUnchanged(entity.Branch);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public override void Update(TransactionBranch entity)
        {
            UnitOfWork.Update(entity);
            UnitOfWork.AttachUnchanged(entity.Branch);
            UnitOfWork.AttachUnchanged(entity.Transaction);
        }

        public IQueryable<TransactionBranch> QuerySearch()
        {
            return UnitOfWork.Query<TransactionBranch>(false);
        }

        public void UpdateTransactionBranches(Transaction transaction)
        {
            UnitOfWork.RemoveRange<TransactionBranch>(tb => tb.TransactionId == transaction.Id);

            foreach (var tb in transaction.TransactionBranches)
            {
                var transactionLevel = new TransactionBranch
                {
                    BranchId = tb.BranchId,
                    TransactionId = transaction.Id
                };

                Add(transactionLevel);
            }
        }
    }
}
