using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using System.Linq;

namespace ManualInvoice.Data.Repository
{
    public class DelegationRepository : EntityRepositoryBase<Delegation>, IDelegationRepository
    {
        public DelegationRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Add(Delegation entity)
        {
            UnitOfWork.Add(entity);
        }

        public override void Delete(Delegation entity)
        {
            UnitOfWork.AttachDeleted(entity);
        }

        public IQueryable<Delegation> QuerySearch()
        {
            return UnitOfWork.Query<Delegation>(false);
        }

        public override void Update(Delegation entity)
        {
            UnitOfWork.Update(entity);
        }

        public int UpdateTransientDelegations(int userId)
        {
            int rowsAffected = UnitOfWork
                .ExecuteSqlCommand("begin NFMANUAL.DLB_NFM_USER_DELEGATIONS_PRC(:p0); end;", new object[] { userId });
            return rowsAffected;
        }

    }
}

