﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Constants;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ManualInvoice.Data.Gateways
{
    public class SecurityApiGateway : HttpGateway, ISecurityApiGateway
    {        
        public SecurityApiGateway(HttpClient httpClient, AppSettings appSettings) : base(httpClient, appSettings.SecurityApiUrl)
        {           
        }

        public async Task<UserAuthorizationDto> GetUserAuthorizationAsync(string userName, string system = "manualinvoice")
        {
            var body = new
            {
                system,
                userName
            };

            string resource = $"users/AuthenticateInto";
            var userAuthorizationString = await PostAsync(resource, body);
            var userAuthorization = JsonConvert.DeserializeObject<UserAuthorizationDto>(userAuthorizationString);    

            return userAuthorization;
        }

        public async Task<ProfilesDto> GetProfilesAsync()
        {
            string resource = "profiles/getall";
            var profilesString = await GetAsync(resource);
            var profiles = JsonConvert.DeserializeObject<ProfilesDto>(profilesString);

            return profiles;
        }

        public async Task<UserResponseDto> GetUsersAsync(UserRequestDto userRequest)
        {
            var queryParams = GetQueryParams(userRequest);
            string resource = $"Users/Search?{queryParams}";
            var usersString = await GetAsync(resource);
            var users = JsonConvert.DeserializeObject<UserResponseDto>(usersString);

            return users;
        }      
    }
}
