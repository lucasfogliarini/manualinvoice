using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ManualInvoice.Data.Gateways
{
    public class InvoiceApiGateway : HttpGateway, IInvoiceApiGateway
    {
        private readonly string _salesOrderDataEndPoint = "SalesOrderData";

        public InvoiceApiGateway(HttpClient httpClient, AppSettings appSettings)
            : base(httpClient, appSettings.FiscalDocumentsApiUrl)
        {
        }

        public async Task<List<FiscalDocumentDto>> GetSynchroInvoices(IEnumerable<long> ids)
        {
            string urlIds = string.Join("&", ids.Select(x => $"ids={x}"));
            string resource = $"FiscalDocuments/GetInvoicesWithSaleOrders?{urlIds}";

            var httpReponse = await GetHttpResponseAsync(resource);

            if (httpReponse.StatusCode is HttpStatusCode.OK)
            {
                var response = await httpReponse.Content.ReadAsAsync<ResponseDto<List<FiscalDocumentDto>>>();

                if (response == null)
                {
                    return new List<FiscalDocumentDto>();
                }

                return response.Data;
            }

            if (httpReponse.StatusCode is HttpStatusCode.NotFound)
            {
                return new List<FiscalDocumentDto>();
            }

            throw new HttpResponseException(httpReponse);
        }

        public async Task<NopDto> GetNopByCode(string nopCode)
        {
            string resource = $"Nop/GetNopByCode?nopCodigo={nopCode}";

            var httpReponse = await GetHttpResponseAsync(resource);
            string stringResponse = await httpReponse.Content.ReadAsStringAsync();
            if (httpReponse.StatusCode is HttpStatusCode.OK)
            {
                var response = JsonConvert.DeserializeObject<ResponseNopData>(stringResponse);

                return response.Data;
            }

            if (httpReponse.StatusCode is HttpStatusCode.NotFound)
            {
                return null;
            }

            throw new HttpResponseException(httpReponse);
        }

        public async Task<bool> CreateLoadVolume(LoadVolumeDto body)
        {
            string resource = "LoadVolume";
            var httpResponse = await PostAsJsonAsync(resource, body);

            if (httpResponse.StatusCode != HttpStatusCode.Created)
            {
                throw new HttpResponseException(httpResponse);
            }

            return true;
        }

        public async Task<CreatedOrderResponseDto> CreateOrder(OrderDto body)
        {
            string resource = "FiscalDocuments/CreateOrder";
            var httpResponse = await PostAsJsonAsync(resource, body);

            if (httpResponse.StatusCode == HttpStatusCode.OK || httpResponse.StatusCode == HttpStatusCode.BadRequest)
            {
                var response = await httpResponse.Content.ReadAsAsync<CreatedOrderResponseDto>();
                return response;
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<bool> IsOrderNumberValidAsync(string orderNumber)
        {
            string resource = $"FiscalDocuments/GetByOrderNumber?orderNumbers={orderNumber}";
            var httpReponse = await GetHttpResponseAsync(resource);
            var response = await httpReponse.Content.ReadAsAsync<ResponseDto<List<FiscalDocumentDto>>>();
            var totalRecords = response.TotalRecords;
            var isOrderNumberValid = totalRecords > 0 ? true : false;
            return isOrderNumberValid;
        }

        public async Task<int> GetMaxItemsPerInvoice()
        {
            string resource = "FiscalDocuments/GetMaxItemsPerInvoice";
            var httpReponse = await GetHttpResponseAsync(resource);

            if (httpReponse.StatusCode is HttpStatusCode.OK)
            {
                return await httpReponse.Content.ReadAsAsync<int>();
            }

            throw new HttpResponseException(httpReponse);
        }

        public async Task<ResponseDto<IList<FiscalDocumentSimpleDto>>> GetOriginalInvoices(OriginalInvoicesRequestDto originalInvoicesRequestDto)
        {
            const string domsSisCode = "02";

            List<GetInvoicesByFilterDto> filterList = new List<GetInvoicesByFilterDto>();

            var take = originalInvoicesRequestDto.Take <= 0 ? 10 : originalInvoicesRequestDto.Take;
            var skip = originalInvoicesRequestDto.Skip <= 0 ? 0 : originalInvoicesRequestDto.Skip;

            var pageNumber = (skip + take) / take;
            var pageSize = take;

            string resource = $"FiscalDocuments/Get?Page.Number={pageNumber}&Page.Size={pageSize}&Include=Items";

            filterList.Add(new GetInvoicesByFilterDto("SisCodigo", domsSisCode, "Equals"));

            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.Id))
                filterList.Add(new GetInvoicesByFilterDto("Id", originalInvoicesRequestDto.Id, "Equals"));
            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.BranchId))
                filterList.Add(new GetInvoicesByFilterDto("InformanteEstCodigo", originalInvoicesRequestDto.BranchId, "Equals"));
            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.Number))
                filterList.Add(new GetInvoicesByFilterDto("Numero", originalInvoicesRequestDto.Number, "Equals"));
            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.Order))
                filterList.Add(new GetInvoicesByFilterDto("DellRecofNumOrder", originalInvoicesRequestDto.Order, "StartsWith"));
            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.DofImportNumber))
                filterList.Add(new GetInvoicesByFilterDto("DofImportNumero", originalInvoicesRequestDto.DofImportNumber, "StartsWith"));

            if (filterList.Count == 0)
                return new ResponseDto<IList<FiscalDocumentSimpleDto>>();

            if (!string.IsNullOrEmpty(originalInvoicesRequestDto.Nop))
                filterList.Add(new GetInvoicesByFilterDto("NopCodigo", originalInvoicesRequestDto.Nop, "StartsWith"));

            var enumType = EnumExtension.GetEnumValue<TransactionType>(originalInvoicesRequestDto.TransactionType);
            var enumDescription = EnumExtension.GetDescription(enumType);

            if (!string.IsNullOrEmpty(enumDescription))
                filterList.Add(new GetInvoicesByFilterDto("IndEntradaSaida", enumDescription, "Equals"));

            var httpReponse = await GetHttpResponseAsync(ListToUrlEncode(resource, filterList));

            if (httpReponse.StatusCode is HttpStatusCode.NotFound)
            {
                return new ResponseDto<IList<FiscalDocumentSimpleDto>>();
            }

            if (httpReponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpReponse);
            }

            var response = await httpReponse.Content.ReadAsAsync<ResponseDto<List<FiscalDocumentSimpleDto>>>();

            if (response == null || response.Data == null)
            {
                return new ResponseDto<IList<FiscalDocumentSimpleDto>>();
            }

            var responseDto = new ResponseDto<IList<FiscalDocumentSimpleDto>>
            {
                TotalRecords = response.TotalRecords,
                Data = response.Data
            };

            return responseDto;
        }

        private static string ListToUrlEncode(string resource, List<GetInvoicesByFilterDto> filterList)
        {
            string urlParam = null;
            int index = 0;
            foreach (GetInvoicesByFilterDto filter in filterList)
            {
                urlParam += $"&Filters[{index}].Operator={filter.Operator}&Filters[{index}].Field={filter.Field}&Filters[{index}].Value={HttpUtility.UrlEncode(filter.Value)}";
                index++;
            }
            resource += urlParam;
            return resource;
        }

        public async Task<bool> CreateOrderOnSynchro(SalesOrderDto salesOrderDto)
        {
            string resource = _salesOrderDataEndPoint;
            var httpResponse = await PostAsJsonAsync(resource, salesOrderDto);

            if (httpResponse.StatusCode != HttpStatusCode.Created)
            {
                throw new HttpResponseException(httpResponse);
            }

            return true;
        }

        public async Task<bool> UpdateOrderOnSynchro(SalesOrderDto salesOrderDto)
        {
            string resource = _salesOrderDataEndPoint;
            var httpResponse = await PutAsJsonAsync(resource, salesOrderDto);

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponse);
            }

            return true;
        }

        public async Task<SalesOrderDto> GetSalesOrderDataByDofSequence(long dofSequence)
        {
            string endpoint = $"{_salesOrderDataEndPoint}/{dofSequence}?include=false";

            var httpResponse = await GetHttpResponseAsync(endpoint);

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                return null;
            }

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponse);
            }

            var response = await httpResponse.Content.ReadAsAsync<SalesOrderDto>();

            return response;
        }

        public async Task<List<long>> UpdateOnSynchro(UpdateOrderDto updateOrderDto)
        {
            string resource = "FiscalDocuments/UpdateOnSynchro";
            var httpResponse = await PostAsJsonAsync(resource, updateOrderDto);

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                return new List<long>();
            }

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponse);
            }

            return await httpResponse.Content.ReadAsAsync<List<long>>();
        }

        public async Task<bool> IssueInvoice(long dofId)
        {
            string resource = "FiscalDocuments/IssueInvoice";

            var httpResponse = await PostAsJsonAsync(resource, dofId);

            return httpResponse.StatusCode == HttpStatusCode.OK;
        }
    }
}
