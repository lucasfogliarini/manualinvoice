﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ManualInvoice.Data.Gateways
{
    public sealed class DirectApiGateway : HttpGateway, IDirectApiGateway
    {
        private readonly string _endpoint = "DofConsolidation";

        public DirectApiGateway(HttpClient httpClient, AppSettings appSettings)
            : base(httpClient, appSettings.DirectApiUrl)
        {
        }

        public async Task<IList<ResponseDofConsolidationCreatedDto>> CreateAsync(IList<CreateDofConsolidationDto> dofConsolidations)
        {
            HttpResponseMessage httpResponse = await PostAsJsonAsync(_endpoint, dofConsolidations);

            if (httpResponse.StatusCode == HttpStatusCode.NotFound)
            {
                throw new NotFoundException<CreateDofConsolidationDto>("Not found dof consolidations");
            }

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponse);
            }

            var result = await httpResponse.Content.ReadAsAsync<ResponseDto<IList<ResponseDofConsolidationCreatedDto>>>();

            return result?.Data;
        }

        public async Task<long[]> GetDofIdsConsolidatedsAsync(long[] dofIds)
        {
            HttpResponseMessage httpResponseMessage = await GetHttpResponseAsync($"{_endpoint}/GetDofIdsConsolidateds", dofIds);

            if (httpResponseMessage.StatusCode == HttpStatusCode.NotFound)
            {
                throw new NotFoundException<CreateDofConsolidationDto>("Not found dof consolidations");
            }

            if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponseMessage);
            }

            var result = await httpResponseMessage.Content.ReadAsAsync<ResponseDto<long[]>>();

            return result?.Data;
        }
    }
}
