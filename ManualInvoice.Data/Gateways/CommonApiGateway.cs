﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.CommonApi;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ManualInvoice.Data.Gateways
{
    public sealed class CommonApiGateway : HttpGateway, ICommonApiGateway
    {
        private readonly string _federativeUnitEndpoint = "FederativeUnit";
        private readonly string _methodGetAll = "GetAll";

        public CommonApiGateway(HttpClient httpClient, AppSettings appSettings)
            : base(httpClient, appSettings.CommonApiUrl)
        {
        }

        public async Task<IList<FederativeUnitDto>> GetAllFederativeUnitsAsync()
        {
            HttpResponseMessage httpResponse = await GetHttpResponseAsync($"{_federativeUnitEndpoint}/{_methodGetAll}");

            if (httpResponse.StatusCode == HttpStatusCode.NotFound)
            {
                throw new NotFoundException<FederativeUnitDto>("Not found federative units");
            }

            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpResponseException(httpResponse);
            }

            var result = await httpResponse.Content.ReadAsAsync<ResponseDto<IList<FederativeUnitDto>>>();

            return result?.Data;
        }
    }
}
