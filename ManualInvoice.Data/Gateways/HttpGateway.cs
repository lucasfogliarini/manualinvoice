﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ManualInvoice.Data.Gateways
{
    public abstract class HttpGateway
    {
        private readonly HttpClient _httpClient;

        public string AppDomain { get; private set; }

        public HttpGateway(HttpClient httpClient, string domain)
        {
            _httpClient = httpClient;
            AppDomain = domain;
        }

        private Uri MountRequestUri(string resource)
        {
            var requestUri = $"{AppDomain}{resource}";
            return new Uri(requestUri);
        }

        protected async Task<string> GetAsync(string resource)
        {
            AddHeaderAcceptJson();

            var requestUri = MountRequestUri(resource);
            var response = await _httpClient.GetAsync(requestUri);
            string stringResponse = await response.Content.ReadAsStringAsync();

            return stringResponse;
        }

        protected async Task<HttpResponseMessage> GetHttpResponseAsync(string resource)
        {
            var requestUri = MountRequestUri(resource);
            var response = await _httpClient.GetAsync(requestUri);

            return response;
        }
        
        protected async Task<HttpResponseMessage> GetHttpResponseAsync(string resource, dynamic stringContent)
        {
            AddHeaderAcceptJson();

            var requestUri = MountRequestUri(resource);

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = requestUri,
                Content = AddStringContent(stringContent)
            };

            var response = await _httpClient.SendAsync(request).ConfigureAwait(false);
            
            return response;
        }

        protected Task<HttpResponseMessage> PostAsJsonAsync<T>(string requestUri, T body)
        {
            var uri = MountRequestUri(requestUri);
            return _httpClient.PostAsJsonAsync(uri, body);
        }

        protected Task<HttpResponseMessage> PutAsJsonAsync<T>(string requestUri, T body)
        {
            var uri = MountRequestUri(requestUri);
            return _httpClient.PutAsJsonAsync(uri, body);
        }

        protected Task<HttpResponseMessage> PostAsync(string resource, HttpContent httpContent)
        {
            var requestUri = MountRequestUri(resource);
            var response = _httpClient.PostAsync(requestUri, httpContent);
            return response;
        }

        protected async Task<string> PostAsync(string resource, dynamic stringContent)
        {
            AddHeaderAcceptJson();

            var _stringContent = AddStringContent(stringContent);
            var response = await PostAsync(resource, _stringContent);
            string stringResponse = await response.Content.ReadAsStringAsync();

            return stringResponse;
        }

        protected void AddAuthentication(string token, string scheme = "Bearer")
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(scheme, token);
        }

        protected void AddHeaderAcceptJson()
        {
            _httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        protected StringContent AddStringContent(dynamic stringContent)
        {
           return new StringContent(JsonConvert.SerializeObject(stringContent), Encoding.UTF8, "application/json");
        }

        protected string GetQueryParams(object queryObject)
        {
            var properties = queryObject.GetType().GetProperties().Where(p => p.GetValue(queryObject, null) != null);
            List<string> queryParams = new List<string>();
            foreach (var property in properties)
            {
                if (property.PropertyType.IsArray)
                {
                    var propertyValue = property.GetValue(queryObject, null) as Array;
                    for (int i = 0; i < propertyValue.Length; i++)
                    {
                        var value = propertyValue.GetValue(i);
                        if (value != null)
                        {
                            queryParams.Add($"{property.Name}[{i}]={value}");
                        }
                    }
                }
                else
                {
                    var propertyValue = HttpUtility.UrlEncode(property.GetValue(queryObject, null).ToString());
                    var queryValue = $"{property.Name}={propertyValue}";
                    queryParams.Add(queryValue);
                }
            }
            return string.Join("&", queryParams);
        }
    }
}
