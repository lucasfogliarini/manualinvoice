﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Dtos.FrameworkFilter;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Extensions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ManualInvoice.Data.Gateways
{
    public class CustomerApiGateway : HttpGateway, ICustomerApiGateway
    {
        public CustomerApiGateway(HttpClient httpClient, AppSettings appSettings) : base(httpClient, appSettings.CustomerApiUrl)
        {
        }

        public async Task<ResponseDto<IEnumerable<CustomerDto>>> GetCustomersAsync(CustomerRequestDto requestDto)
        {
            var parameters = new List<string>
            {
                $"{nameof(requestDto.Skip)}={requestDto.Skip}",
                $"{nameof(requestDto.Take)}={requestDto.Take}",
                $"{nameof(requestDto.OrderBy)}={requestDto.OrderBy}",
                $"{nameof(requestDto.OrderByDesc)}={requestDto.OrderByDesc}",
                $"{nameof(requestDto.ActualOnly)}={requestDto.ActualOnly}"
            };

            if (!string.IsNullOrWhiteSpace(requestDto.CpfCgc))
            {
                parameters.Add($"{nameof(requestDto.CpfCgc)}={requestDto.CpfCgc}");
            }

            if (!string.IsNullOrWhiteSpace(requestDto.Name))
            {
                parameters.Add($"{nameof(requestDto.Name)}={requestDto.Name}");
            }

            var queryString = string.Join("&", parameters);

            var httpResponse = await GetHttpResponseAsync("Customers/GetWithLocation?" + queryString);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<ResponseDto<IEnumerable<CustomerDto>>>(stringResponse);
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                throw new NotFoundException<CustomerDto>($"Customer was not found.");
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<CustomerDto> GetByCodeAsync(string code)
        {
            var paginatedGridSettings = new GridSettings
            {
                Filters = new List<Filter>
                {
                    new Filter(field: "Code", value: code, @operator: Operator.Equals, nullValue: false)
                },
                Include = "Addresses"
            };

            var queryString = paginatedGridSettings.ToQueryString();

            var httpResponse = await GetHttpResponseAsync("Customers/GetAll?" + queryString);

            string stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                var response = JsonConvert.DeserializeObject<ResponseDto<IEnumerable<CustomerDto>>>(stringResponse);
                // Apply OrderByDescending to EndDate property with nulls first to get the actual registry
                return response.Data?.OrderByDescending(p => p.EndDate ?? DateTime.MaxValue).FirstOrDefault();
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                throw new NotFoundException<CustomerDto>($"Customer {code} was not found.");
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<CustomerDto> GetCustomerAndLocationWithValidVigencyAsync(string pfjCode)
        {
            string resource = $"Customers/GetCustomerAndLocationWithValidVigency/{pfjCode}";

            var httpResponse = await GetHttpResponseAsync(resource);
            string stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<CustomerDto>(stringResponse);
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                return new CustomerDto() { ExistingPerson = false };
            }

            throw new HttpResponseException(httpResponse);
        }
    }
}
