﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Dtos.FrameworkFilter;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Extensions;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.SkuApi;
using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ManualInvoice.Data.Gateways
{
    public class SkuApiGateway : HttpGateway, ISkuApiGateway
    {
        public SkuApiGateway(HttpClient httpClient, AppSettings appSettings)
            : base(httpClient, appSettings.SkuApiUrl)
        {
        }

        public async Task<SkuDto> GetSkuByCode(string skuCode, InvoiceType invoiceType)
        {
            var resource = string.Empty;

            if (invoiceType == InvoiceType.Product)
            {
                resource = $"Skus/{skuCode}";
            }
            else
            {
                resource = $"ServiceSku/GetServiceSkuWithCityCode?skuCode={skuCode}";
            }

            var httpResponse = await GetHttpResponseAsync(resource);
            string stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                var response = JsonConvert.DeserializeObject<ResponseSkuDataDto>(stringResponse);

                return response.Data;
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                return null;
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<string> GetCityCodeBySku(string skuCode)
        {
            var resource = $"CityCode/GetCityCodeBySku?skuCode={skuCode}";

            var httpReponse = await GetHttpResponseAsync(resource);

            if (httpReponse.StatusCode is HttpStatusCode.OK)
            {
                var stringResponse = await httpReponse.Content.ReadAsAsync<ResponseDto<string>>();
                return stringResponse.Data;
            }

            if (httpReponse.StatusCode is HttpStatusCode.NotFound)
            {
                return string.Empty;
            }

            throw new HttpResponseException(httpReponse);
        }

        public async Task<ResponseDto<IEnumerable<SkuDto>>> GetAsync(
            int skip, int take, string orderBy, bool orderByDesc, string name, InvoiceType invoiceType)
        {
            var paginatedGridSettings = new PaginatedGridSettings
            {
                Page = new Page(number: (skip + take) / take, size: take),
                Filters = new List<Filter>
                {
                    new Filter(field: "Name", value: name, @operator: Operator.Contains)
                },
                SortExpression = $"{orderBy} " + (orderByDesc ? "desc" : "asc")
            };

            if (invoiceType == InvoiceType.Service)
            {
                paginatedGridSettings.Filters.Add(
                    new Filter(field: "Lc116", value: null, @operator: Operator.NotEquals, nullValue: true));
            }

            var queryString = paginatedGridSettings.ToQueryString();
            var endpoint = invoiceType == InvoiceType.Product ? "Skus" : "ServiceSku";

            var httpResponse = await GetHttpResponseAsync($"{endpoint}/Get?{queryString}");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<ResponseDto<IEnumerable<SkuDto>>>(stringResponse);
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                throw new NotFoundException<SkuDto>($"Item {name} was not found.");
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<SkuDto> GetByCodeAsync(string code, InvoiceType invoiceType)
        {
            var gridSettings = new GridSettings
            {
                Filters = new List<Filter>
                {
                    new Filter(field: nameof(SkuDto.Code), value: code, @operator: Operator.Contains)
                }
            };

            var queryString = gridSettings.ToQueryString();
            var endpoint = invoiceType == InvoiceType.Product ? "Skus" : "ServiceSku";

            var httpResponse = await GetHttpResponseAsync($"{endpoint}/GetAll?{queryString}");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                var response = JsonConvert.DeserializeObject<ResponseDto<IEnumerable<SkuDto>>>(stringResponse);
                return response?.Data.FirstOrDefault();
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                throw new NotFoundException<SkuDto>($"Route was not found.");
            }

            throw new HttpResponseException(httpResponse);
        }

        public async Task<IEnumerable<SkuDto>> GetByCodesAsync(IEnumerable<string> codes, InvoiceType invoiceType)
        {
            var endpoint = invoiceType == InvoiceType.Product ? "Skus" : "ServiceSku";

            var httpResponse = await GetHttpResponseAsync($"{endpoint}/GetByCodes?codes={string.Join("&codes=", codes)}");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode is HttpStatusCode.OK)
            {
                var response = JsonConvert.DeserializeObject<ResponseDto<IEnumerable<SkuDto>>>(stringResponse);
                return response?.Data;
            }

            if (httpResponse.StatusCode is HttpStatusCode.NotFound)
            {
                throw new NotFoundException<SkuDto>($"Route was not found.");
            }

            throw new HttpResponseException(httpResponse);
        }
    }
}
