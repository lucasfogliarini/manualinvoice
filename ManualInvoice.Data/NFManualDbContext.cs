﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace ManualInvoice.Data
{
    internal class NFManualDbContext : DbContext
    {
        public NFManualDbContext(DbContextOptions<NFManualDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("NFMANUAL");

            var thisAssembly = Assembly.GetExecutingAssembly();
            modelBuilder.ApplyConfigurationsFromAssembly(thisAssembly);
        }
    }
}
