﻿using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Infrastructure
{
    public interface IUnitOfWork
    {      
        IQueryable<TEntity> Query<TEntity>(bool tracked = false, params Expression<Func<TEntity, object>>[] includes)
            where TEntity : class, IEntity;

        void Add<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        void Update<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        void Commit();

        void AttachUnchanged<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        void AttachUnchangedRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class, IEntity;

        void AttachDeleted<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        void RemoveRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class, IEntity;

        void RemoveRange<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class, IEntity;

        TEntity Find<TEntity>(params object[] keyValues)
            where TEntity : class, IEntity;

        int ExecuteSqlCommand(string sql, params object[] parameters);
    }
}
