﻿using ManualInvoice.Infrastructure.Entities;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Infrastructure.Services
{
    public abstract class ServiceBase<TRepository> : IService
        where TRepository : IRepository
    {
        protected ServiceBase(IUnitOfWork unitOfWork, TRepository mainRepository)
        {
            UnitOfWork = unitOfWork;
            MainRepository = mainRepository;
        }

        protected IUnitOfWork UnitOfWork { get; }

        protected TRepository MainRepository { get; }
    }
}
