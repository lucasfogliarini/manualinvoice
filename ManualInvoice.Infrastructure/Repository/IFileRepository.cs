﻿using System.Collections.Generic;
using System.IO;

namespace ManualInvoice.Infrastructure.Repository
{
    public interface IFileRepository<T> where T : class
    {
        IEnumerable<T> ReadFile(Stream stream);
    }
}
