﻿using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Infrastructure.Repository
{
    public interface IEntityRepository<TEntity> : IRepository
        where TEntity : class, IEntity, new()
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> QueryComplete();
        TEntity Find(params object[] keyValues);
        void CheckDuplicated(Expression<Func<TEntity, bool>> predicate, IDictionary<string, object> errors);
    }
}
