﻿namespace ManualInvoice.Infrastructure.Entities
{
    public interface IEntityWithId<T> : IEntity
    {
        T Id { get; set; }
    }
}
