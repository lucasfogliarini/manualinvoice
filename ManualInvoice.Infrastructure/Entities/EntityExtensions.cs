﻿using System.Collections.Generic;

namespace ManualInvoice.Infrastructure.Entities
{
    public static class EntityExtensions
    {
        public static bool IsTransient<T>(this IEntityWithId<T> entity)
        {
            if (entity is null)
            {
                throw new System.ArgumentNullException(nameof(entity));
            }

            return EqualityComparer<T>.Default.Equals(default(T), entity.Id);
        }
    }
}