﻿using FluentAssertions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class SecurityApiServiceTests
    {
        private ISecurityApiGateway _securityApiGateway;

        [TestInitialize]
        public void Initialize()
        {
            _securityApiGateway = Substitute.For<ISecurityApiGateway>();
        }

        [TestMethod]
        public void GettingProfilesWithProfilesShouldReturnProfiles()
        {
            // Arrange
            var profilesDto = new ProfilesDto() 
            { 
                Data = new List<ProfileDto>() 
                { 
                    new ProfileDto() { Name = "profile1" }
                } 
            };
            _securityApiGateway.GetProfilesAsync().Returns(profilesDto);
            var service = new SecurityApiService(_securityApiGateway);

            // Act
            var profiles = service.GetProfilesAsync().Result;

            // Assert
            profiles.Should().NotBeNull();
        }

        [TestMethod]
        public void GettingUsersWithProfilesShouldReturnUsers()
        {
            // Arrange
            var userAuthorization = new UserAuthorizationDto() { Token = "token" };
            var profilesDto = new ProfilesDto()
            {
                Data = new List<ProfileDto>()
                {
                    new ProfileDto() { Name = "profile1" }
                }
            };
            _securityApiGateway.GetProfilesAsync().Returns(profilesDto);
            string userName = @"username1";
            _securityApiGateway.GetUserAuthorizationAsync(userName, SecurityApiService.SYSTEM_SECURITY).Returns(userAuthorization);
            var service = new SecurityApiService(_securityApiGateway);

            // Act
            var profiles = service.GetProfilesAsync().Result;

            // Assert
            profiles.Should().NotBeNull();
        }
    }
}
