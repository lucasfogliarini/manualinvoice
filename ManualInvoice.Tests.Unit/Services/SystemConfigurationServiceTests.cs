﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class SystemConfigurationServiceTests
    {
        private IRuleValidator _ruleValidator;
        private ISystemConfigurationRepository _systemConfigurationRepository;
        private IUnitOfWork _unitOfWork;
        private ISystemConfigurationService _service;

        [TestInitialize]
        public void Initialize()
        {
            _ruleValidator = new RuleValidator(Substitute.For<ILogger<RuleValidator>>());
            _systemConfigurationRepository = Substitute.For<ISystemConfigurationRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _service = new SystemConfigurationService(_unitOfWork, _ruleValidator, _systemConfigurationRepository);
        }

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllSystemConfiguration()
        {
            // Arrange
            var systemConfigurationList = new List<SystemConfiguration>();

            systemConfigurationList.Add(new SystemConfiguration(1));

            _systemConfigurationRepository.QuerySearch().Returns(systemConfigurationList.AsQueryable());

            // Act
            var result = _service.Search();

            _systemConfigurationRepository.DidNotReceive().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Count().Should().Be(1);
                result.Should().NotBeNull();
            };
        }       
    }
}
