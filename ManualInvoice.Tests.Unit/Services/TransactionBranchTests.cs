﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class TransactionBranchTests
    {
        private ITransactionBranchRepository _transactionBranchRepository;
        private IUnitOfWork _unitOfWork;
        private ITransactionBranchService _service;

        [TestInitialize]
        public void Initialize()
        {
            _transactionBranchRepository = Substitute.For<ITransactionBranchRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _service = new TransactionBranchService(_unitOfWork, _transactionBranchRepository);
        }

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllTransactionsBranches()
        {
            // Arrange
            var transactionBranchList = new List<TransactionBranch>();

            transactionBranchList.Add(new TransactionBranch("1", 1, 1));
            transactionBranchList.Add(new TransactionBranch("2", 1, 2));
            transactionBranchList.Add(new TransactionBranch("3", 2, 3));
            transactionBranchList.Add(new TransactionBranch("4", 2, 1));

            int transactionBranchCount = transactionBranchList.Count;

            _transactionBranchRepository.QuerySearch().Returns(transactionBranchList.AsQueryable());

            // Act
            var result = _service.Search();

            _transactionBranchRepository.DidNotReceive().QueryComplete();

            // Assert
            result.Should().NotBeNull().And.HaveCount(transactionBranchCount);
        }

        [TestMethod]
        public void GettingByIdWithExistingIdShouldReturnTransactionBranch()
        {
            // Arrange
            var expectedId = Guid.NewGuid().ToString();

            var entity = new TransactionBranch(expectedId, 10, 20);
            var transactionBranchList = new List<TransactionBranch>();
            transactionBranchList.Add(entity);

            _transactionBranchRepository.QueryComplete().Returns(transactionBranchList.AsQueryable());

            // Act
            var result = _service.Get(expectedId);

            _transactionBranchRepository.Received().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Id.Should().NotBeNull().And.Be(expectedId);
            };
        }

        [TestMethod]
        public void GettingByIdWithNotExistingIdShouldThrowNotFoundException()
        {
            // Arrange
            var entity = new TransactionBranch(Guid.NewGuid().ToString(), 10, 20);
            var transactionBranchList = new List<TransactionBranch>();
            transactionBranchList.Add(entity);

            _transactionBranchRepository.QueryComplete().Returns(transactionBranchList.AsQueryable());

            // Act and Assert
            Assert.ThrowsException<NotFoundException<TransactionBranch>>(() =>
            {
                _service.Get(Guid.NewGuid().ToString().Replace("-", string.Empty));
            });

            _transactionBranchRepository.Received().QueryComplete();
            _transactionBranchRepository.DidNotReceive().QuerySearch();
        }

        [TestMethod]
        public void GettingBranchesByTransactionIdNotExistingDataShouldReturnListEmpty()
        {
            // Arrange
            int transactionId = 102030;
            bool onlyActives = false;
            var source = new List<TransactionBranch>();

            _transactionBranchRepository.QueryComplete().Returns(source.AsQueryable());

            // Act
            var result = _service.GetBranchesByTransaction(transactionId, onlyActives);

            // Assert
            _transactionBranchRepository.Received().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingBranchesByTransactionIdWithExistingTransactionIdShouldReturnBranches()
        {
            // Arrange
            int transactionId = 102030;
            bool onlyActives = false;
            int[] branchIds = new int[] { 10, 20 };
            var source = new List<TransactionBranch>();
            source.Add(new TransactionBranch(Guid.NewGuid().ToString(), transactionId, branchIds[0]));
            source.Add(new TransactionBranch(Guid.NewGuid().ToString(), transactionId, branchIds[1]));

            _transactionBranchRepository.QueryComplete().Returns(source.AsQueryable());

            // Act
            var result = _service.GetBranchesByTransaction(transactionId, onlyActives);

            // Assert
            _transactionBranchRepository.Received().QueryComplete();
            result.Should().NotBeNullOrEmpty()
                .And
                .HaveCount(source.Count)
                .And
                .Match(branchesMatch => branchesMatch.All(branch => branchIds.Contains(branch.Id)));
        }

        [TestMethod]
        public void GettingBranchesByTransactionIdWithBranchesActivesAndExistingTransactionIdShouldReturnBranchesActives()
        {
            // Arrange
            int transactionId = 102030;
            bool onlyActives = true;
            var branchActive = new Branch() { Id = 10, IsActive = true };
            var branchDisabled = new Branch() { Id = 20, IsActive = false };
            var source = new List<TransactionBranch>();
            source.Add(new TransactionBranch()
            {
                Id = Guid.NewGuid().ToString(),
                Branch = branchActive,
                Transaction = new Transaction(transactionId, "Transaction", Common.TransactionType.Inbound)
            });
            source.Add(new TransactionBranch()
            {
                Id = Guid.NewGuid().ToString(),
                Branch = branchDisabled,
                Transaction = new Transaction(transactionId, "Transaction", Common.TransactionType.Inbound)
            });

            _transactionBranchRepository.QueryComplete().Returns(source.AsQueryable());

            // Act
            var result = _service.GetBranchesByTransaction(transactionId, onlyActives);

            // Assert
            _transactionBranchRepository.Received().QueryComplete();
            result.Should().NotBeNullOrEmpty()
                .And
                .HaveCount(1)
                .And
                .Match(branchesMatch => branchesMatch.All(branch => branch.Id == branchActive.Id));
        }
    }
}
