using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class DelegationServiceTest
    {
        private IRuleValidator m_ruleValidator;

        [TestInitialize]
        public void Initialize()
        {
            m_ruleValidator = new RuleValidator(Substitute.For<ILogger<RuleValidator>>());
        }

        [TestMethod]
        public void CreatingWithDelegationShouldDelegationCreated()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            string userName = "NTAccout";

            userService.ImportUserAsync(userName).Returns(new User() { Id = 1, UserName = userName });


            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true,
                UserId = 1
            };
            string[] delegationTo = { userName };

            Delegation result = service.Create(delegation, delegationTo).Result;

            delegationRepository.Received().Add(Arg.Is(delegation));
            unitOfWork.Received().Commit();

            Assert.IsNotNull(delegation);
        }

        [TestMethod]
        public void CreatingWithDelegationWithMissingAttributesShouldThrowsException()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            string userName = "NTAccout";

            userService.ImportUserAsync(userName).Returns(new User() { Id = 1, UserName = userName });

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            // DelegateFrom is required
            Delegation delegation = new Delegation
            {
                PeriodFrom = null,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };
            string[] delegationTos = { userName };

            Assert.ThrowsExceptionAsync<ValidationException>(() =>
            {
                return service.Create(delegation, delegationTos);
            });

            // DelegateTo is required
            delegation = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = null,
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };

            Assert.ThrowsExceptionAsync<ValidationException>(() =>
            {
                return service.Create(delegation, delegationTos);
            });
        }

        [TestMethod]
        public void CreatingWithDelegationWithIdShouldThrowsException()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            string userName = "NTAccout";

            userService.ImportUserAsync(userName).Returns(new User() { Id = 1, UserName = userName });

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation
            {
                Id = 1,
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };
            string[] delegationTos = { userName };

            // Create operation should not have an id specified
            Assert.ThrowsExceptionAsync<ValidationException>(() =>
            {
                return service.Create(delegation, delegationTos);
            });
        }

        [TestMethod]
        public void UpdatingWithDelegationShouldDelegationUpdated()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation
            {
                Id = 1,
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };

            Delegation result = service.Update(delegation);

            delegationRepository.Received().Update(Arg.Is(delegation));
            unitOfWork.Received().Commit();

            Assert.IsNotNull(delegation);
        }

        [TestMethod]
        public void UpdatingWithDelegationWithMissingAttributesShouldThrowsException()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation
            {
                PeriodFrom = null,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Delegation result = service.Update(delegation);
            });

            delegation = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = null,
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Delegation result = service.Update(delegation);
            });

            delegation = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = null,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true
            };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Delegation result = service.Update(delegation);
            });
        }

        [TestMethod]
        public void DeletingWithDelegationShouldDelegationDeleted()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation { Id = 1 };

            bool result = service.Delete(delegation);

            delegationRepository.Received().Delete(Arg.Is<Delegation>(x => x.Id == 1));

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GettingWithExistingIdShouldDelegationReturned()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation { Id = 1 };

            delegationRepository.QueryComplete().Returns(new Delegation[] { delegation }.AsQueryable());

            var result = service.Get(1);

            delegationRepository.Received().QueryComplete();
            delegationRepository.DidNotReceive().QuerySearch();

            Assert.IsNotNull(result);

            Assert.AreEqual(delegation, result);
        }

        [TestMethod]
        public void GettingWithUnknownIdShouldThrowNotFoundException()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation = new Delegation { Id = 1 };

            delegationRepository.QueryComplete().Returns(new Delegation[] { delegation }.AsQueryable());

            Assert.ThrowsException<NotFoundException<Delegation>>(() =>
            {
                service.Get(2);
            });

            delegationRepository.Received().QueryComplete();
            delegationRepository.DidNotReceive().QuerySearch();
        }

        [TestMethod]
        public void SearchingWithNoFilterWhouldReturnAllDelegations()
        {
            var delegationRepository = Substitute.For<IDelegationRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();
            var userService = Substitute.For<IUserService>();

            IDelegationService service = new DelegationService(unitOfWork, userService, m_ruleValidator, delegationRepository);

            Delegation delegation2 = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true,
                DelegationUsers = new List<DelegationUser>() { new DelegationUser() { UserId = 10 } }
            };
            Delegation delegation3 = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(5),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = false,
                IsOldApproval = true,
                DelegationUsers = new List<DelegationUser>() { new DelegationUser() { UserId = 10 } }
            };
            Delegation delegation4 = new Delegation
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(10),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = false,
                DelegationUsers = new List<DelegationUser>() { new DelegationUser() { UserId = 10 } }
            };

            var delegations = new Delegation[] { delegation2, delegation3, delegation4 };

            delegationRepository.QuerySearch().Returns(delegations.AsQueryable());

            var result = service.Search();

            delegationRepository.DidNotReceive().QueryComplete();
            delegationRepository.Received().QuerySearch();

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Should().HaveCount(delegations.Count());
                result.Should().Contain(delegation2);
                result.Should().Contain(delegation3);
                result.Should().Contain(delegation4);
            };
        }
    }
}

