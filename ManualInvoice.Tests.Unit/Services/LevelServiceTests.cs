﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class LevelServiceTests
    {
        private IRuleValidator _ruleValidator;
        private ILevelRepository _levelRepository;
        private IUnitOfWork _unitOfWork;

        [TestInitialize]
        public void Initialize()
        {
            _ruleValidator = Substitute.For<IRuleValidator>();
            _levelRepository = Substitute.For<ILevelRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
        }

        [TestMethod]
        public void SearchingWithLevelsShouldReturnLevels()
        {
            // Arrange
            var service = new LevelService(_unitOfWork, _ruleValidator, _levelRepository);

            var levels = new List<Level>()
            {
                new Level(){ Id = 1, Name = "level1", Invoice = true, IsActive = true, Visible = true },
                new Level(){ Id = 2, Name = "level2", Invoice = true, IsActive = true, Visible = false },
                new Level(){ Id = 3, Name = "level3", Invoice = false, IsActive = false, Visible = true },
            };

            _levelRepository.QuerySearch().Returns(levels.AsQueryable());

            // Act
            var returnLevels = service.Search();

            // Assert
            returnLevels.Should().NotBeNull();
        }
    }
}
