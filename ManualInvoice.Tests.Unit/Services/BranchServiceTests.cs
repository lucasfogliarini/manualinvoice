﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class BranchServiceTests
    {
        private IRuleValidator _ruleValidator;
        private IBranchRepository _branchRepository;
        private IUnitOfWork _unitOfWork;
        private ITransactionBranchService _transactionBranchService;
        private IBranchService _service;

        [TestInitialize]
        public void Initialize()
        {
            _ruleValidator = new RuleValidator(Substitute.For<ILogger<RuleValidator>>());
            _branchRepository = Substitute.For<IBranchRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _transactionBranchService = Substitute.For<ITransactionBranchService>();
            _service = new BranchService(_unitOfWork, _ruleValidator, _branchRepository, _transactionBranchService);
        }

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllBranchs()
        {
            // Arrange
            var branchList = new List<Branch>();

            branchList.Add(new Branch(1));
            branchList.Add(new Branch(2));
            branchList.Add(new Branch(3));
            branchList.Add(new Branch(4));

            _branchRepository.QuerySearch().Returns(branchList.AsQueryable());

            // Act
            var result = _service.Search();

            _branchRepository.DidNotReceive().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Count().Should().Be(4);
                result.Should().NotBeNull();
            };
        }

        [TestMethod]
        public void GettingByIdWithExistingIdShouldReturnBranch()
        {
            // Arrange
            var expectedId = 1;

            var branch = new Branch(expectedId);

            _branchRepository.QueryComplete().Returns(new Branch[] { branch }.AsQueryable());

            // Act
            var result = _service.Get(expectedId);

            _branchRepository.Received().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Id.Should().Be(expectedId);
                result.Should().NotBeNull();
            };
        }

        [TestMethod]
        public void GettingByIdWithNotExistingIdShouldThrowNotFoundException()
        {
            // Arrange
            var branch = new Branch(1);
            _branchRepository.QueryComplete().Returns(new Branch[] { branch }.AsQueryable());

            // Act and Assert
            Assert.ThrowsException<NotFoundException<Branch>>(() =>
            {
                _service.Get(2);
            });

            _branchRepository.Received().QueryComplete();
            _branchRepository.DidNotReceive().QuerySearch();
        }

        [TestMethod]
        public void GettingByTransactionWitExistingTransactionShouldReturnBranches()
        {
            // Arrange
            int transactionId = 102030;
            bool onlyActives = false;
            var branches = new List<Branch>();
            branches.Add(new Branch(10));
            _transactionBranchService.GetBranchesByTransaction(transactionId, onlyActives)
                .Returns(branches);

            // Act
            var result = _service.GetBranchesByTransaction(transactionId, onlyActives);

            // Assert
            _transactionBranchService.Received().GetBranchesByTransaction(transactionId, onlyActives);
            result.Should().NotBeNullOrEmpty().And.HaveCount(branches.Count);
        }

        [TestMethod]
        public void GettingByTransactionWithNotExistingTransactionShouldReturnEmptyList()
        {
            // Arrange
            int transactionId = 102030;
            bool onlyActives = false;
            var branches = new List<Branch>();
            _transactionBranchService.GetBranchesByTransaction(transactionId, onlyActives)
                .Returns(branches);

            // Act
            var result = _service.GetBranchesByTransaction(transactionId, onlyActives);

            // Assert
            _transactionBranchService.Received().GetBranchesByTransaction(transactionId, onlyActives);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingByNameWithExistingNameShouldReturnBranch()
        {
            //Arrange
            var branchName = "DELL";

            var branch = new Branch
            {
                Id = 1,
                Name = branchName,
            };

            _branchRepository.QueryComplete().Returns(new Branch[] { branch }.AsQueryable());

            // Act
            var result = _service.GetByName(branchName);

            // Assert
            _branchRepository.Received().QueryComplete();

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Name.Should().Equals(branchName);
            };
        }

        [TestMethod]
        public void GettingByNameWithNotExistingNameShouldThrowNotFoundException()
        {
            // Arrange
            var branchName = "DELL";
            var inexistentBranchName = "ABC";

            var branch = new Branch
            {
                Id = 1,
                Name = branchName,
            };

            _branchRepository.QueryComplete().Returns(new Branch[] { branch }.AsQueryable());

            // Act and Assert
            Assert.ThrowsException<NotFoundException<Branch>>(() =>
            {
                _service.GetByName(inexistentBranchName);
            });

            _branchRepository.Received().QueryComplete();
            _branchRepository.DidNotReceive().QuerySearch();
        }
    }
}
