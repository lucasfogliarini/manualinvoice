﻿using FluentAssertions;
using ManualInvoice.Domain.Gateways.CommonApi;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class FederativeUnitServiceTests
    {
        private ICommonApiGateway _gateway;
        private IFederativeUnitService _service;

        [TestInitialize]
        public void Initialize()
        {
            _gateway = Substitute.For<ICommonApiGateway>();
            _service = new FederativeUnitService(_gateway);
        }

        [TestMethod]
        public async Task GettingAllFederativeUnitsAsyncNoDataFoundShouldReturnAnEmptyList()
        {
            // Arrange
            _gateway.GetAllFederativeUnitsAsync().Returns(new List<FederativeUnitDto>());

            // Act
            var result = await _service.GetAllFederativeUnitsAsync();

            // Assert
            await _gateway.Received().GetAllFederativeUnitsAsync();
            result.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public async Task GettingAllFederativeUnitsAsyncWithTwoDataFoundShouldReturnListWithTwoData()
        {
            // Arrange
            var federativeUnits = new List<FederativeUnitDto>();
            federativeUnits.Add(new FederativeUnitDto());
            federativeUnits.Add(new FederativeUnitDto());
            _gateway.GetAllFederativeUnitsAsync().Returns(federativeUnits);

            // Act
            var result = await _service.GetAllFederativeUnitsAsync();

            // Assert
            await _gateway.Received().GetAllFederativeUnitsAsync();
            result.Should().HaveCount(federativeUnits.Count);
        }
    }
}
