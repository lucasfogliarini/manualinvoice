﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class ApprovalServiceTests
    {

        [TestInitialize]
        public void Initialize()
        { }        

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllApprovals()
        {
            var approvalViewRepository = Substitute.For<IApprovalViewRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();            
            var userService = Substitute.For<IUserService>();
            
            var userName = "ntaccount";
            var userId = 1;
            userService.GetByName(userName).Returns(new User() { Id = userId, UserName = userName });

            IApprovalViewService service = new ApprovalViewService(unitOfWork, approvalViewRepository, userService);

            ApprovalView approval2 = new ApprovalView() { ApproverUserId = userId };
            ApprovalView approval3 = new ApprovalView() { ApproverUserId = userId };
            ApprovalView approval4 = new ApprovalView() { ApproverUserId = userId };

            approvalViewRepository.QuerySearch().Returns(new ApprovalView[] { approval2, approval3, approval4 }.AsQueryable());
            
            var result = service.Search(userName);

            approvalViewRepository.DidNotReceive().QueryComplete();
            approvalViewRepository.Received().QuerySearch();

            Assert.IsNotNull(result);

            Assert.AreEqual(3, result.Count());
            Assert.IsTrue(result.Contains(approval2));
            Assert.IsTrue(result.Contains(approval3));
            Assert.IsTrue(result.Contains(approval4));
        }
    }
}
