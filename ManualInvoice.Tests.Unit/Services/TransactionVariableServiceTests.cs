﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class TransactionVariableServiceTests
    {
        private IUnitOfWork _unitOfWork;
        private ITransactionVariableRepository _repository;
        private ITransactionVariableService _service;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _repository = Substitute.For<ITransactionVariableRepository>();
            _service = new TransactionVariableService(_unitOfWork, _repository);
        }

        [TestMethod]
        public void GettingActivesVariablesByTransactionWithTransactionIdZeroShouldReturnEmptyList()
        {
            // Arrange
            int transactionId = 0;

            // Act
            var result = _service.GetActivesVariablesByTransaction(transactionId);

            // Assert
            _repository.DidNotReceive().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingActivesVariablesByTransactionWithNoMatchByTransactionShouldReturnEmptyList()
        {
            // Arrange
            int transactionId = 102030;
            _repository.QueryComplete().Returns(new List<TransactionVariable>().AsQueryable());

            // Act
            var result = _service.GetActivesVariablesByTransaction(transactionId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettinghActivesVariablesByTransactionWithOneMatchByTransactionShouldReturnListWithOneItem()
        {
            // Arrange
            int transactionId = 102030;

            var transactionVariables = new List<TransactionVariable>();
            transactionVariables.Add(
                new TransactionVariable()
                {
                    Transaction = new Transaction() { Id = transactionId },
                    Variable = new Variable() { IsActive = true }
                });
            _repository.QueryComplete().Returns(transactionVariables.AsQueryable());

            // Act
            var result = _service.GetActivesVariablesByTransaction(transactionId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().HaveCount(transactionVariables.Count);
        }

        [TestMethod]
        public void GettingByVariablesWithoutVariablesIdShouldReturnEmptyList()
        {
            // Arrange
            int[] variablesId = new int[0];

            // Act
            var result = _service.GetByVariables(variablesId);

            // Assert
            _repository.DidNotReceive().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingByVariablesWithNoMatchByVariablesShouldReturnEmptyList()
        {
            // Arrange
            int[] variablesId = new int[] { 10, 20 };
            _repository.QueryComplete().Returns(new List<TransactionVariable>().AsQueryable());

            // Act
            var result = _service.GetByVariables(variablesId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingByVariablesWithOneMatchByVariablesShouldReturnListWithOneItem()
        {
            // Arrange
            int[] variablesId = new int[] { 10, 20 };

            var transactionVariables = new List<TransactionVariable>();
            transactionVariables.Add(new TransactionVariable() { Variable = new Variable() { Id = variablesId.FirstOrDefault() } });
            _repository.QueryComplete().Returns(transactionVariables.AsQueryable());

            // Act
            var result = _service.GetByVariables(variablesId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().HaveCount(transactionVariables.Count);
        }
    }
}
