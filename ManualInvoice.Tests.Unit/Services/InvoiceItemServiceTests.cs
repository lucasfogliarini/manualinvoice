﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class InvoiceItemServiceTests
    {
        private IUnitOfWork _unitOfWork;
        private IInvoiceItemRepository _repository;
        private IInvoiceItemService _service;
        private IOrderItemService _orderItemService;

        [TestInitialize]
        public void Initialize()
        {
            _repository = Substitute.For<IInvoiceItemRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _orderItemService = Substitute.For<IOrderItemService>();
            _service = new InvoiceItemService(_unitOfWork, _repository, _orderItemService);
        }

        [TestMethod]
        public void MergingItemsWithNoTtemsInDatabaseShouldAddAllItems()
        {
            // Arrange
            int invoiceId = 102030;
            IList<InvoiceItem> entities = new List<InvoiceItem>() { CreateEntity(invoiceId), CreateEntity(invoiceId) };

            _repository.QuerySearch().Returns(entities.AsQueryable());

            // Act
            var result = _service.MergeItems(entities, invoiceId);

            // Assert
            entities.ToList().ForEach(x => _repository.Received().Add(x));
            _repository.DidNotReceive().Update(Arg.Any<InvoiceItem>());
            _repository.DidNotReceive().Delete(Arg.Any<InvoiceItem>());
            _unitOfWork.Received(entities.Count).Commit();
            result.Should().HaveCount(entities.Count);
        }

        [TestMethod]
        public void MergingItemsWithItemsFitForAllCrudOerationsShouldDoTheCrudOperationsOnTheTtems()
        {
            // Arrange
            int invoiceId = 102030;
            var newItem = CreateEntity();
            var updateItem = CreateEntity(invoiceId, 102020);
            var updatedItem = CreateEntity(invoiceId, 102020);
            var deleteItem = CreateEntity(invoiceId, 102021);

            updatedItem.ProductCode = "NEW CODE";
            updatedItem.DellBaseFlag = Enum.DellBaseFlagType.NonBase;
            updatedItem.QuantityOriginal = updatedItem.Quantity;
            updatedItem.PriceOriginal = updatedItem.Price;

            IList <InvoiceItem> entitiesInDataBase = new List<InvoiceItem>() { deleteItem, updateItem };
            IList<InvoiceItem> entities = new List<InvoiceItem>() { newItem, updatedItem };

            _repository.Find(updateItem.Id).Returns(updateItem);
            _repository.QueryComplete(false)
                .Returns(entitiesInDataBase.AsQueryable(), entities.AsQueryable());
            _repository.QuerySearch()
                .Returns(entitiesInDataBase.AsQueryable(), entities.AsQueryable());
            
            // Act
            var result = _service.MergeItems(entities, invoiceId);

            // Assert
            _repository.Received().Add(newItem);
            _repository.Received().Update(Arg.Any<InvoiceItem>());
            _repository.Received().Delete(deleteItem);
            _unitOfWork.Received(3).Commit();
            result.Should().HaveCount(entities.Count);
        }

        [TestMethod]
        public void CreatingItemWithInvoiceIdShouldReturnItemCreated()
        {
            // Arrange
            var invoiceId = 123;
            var newItem = CreateEntity(invoiceId);

            _repository.QuerySearch().Returns(new InvoiceItem[] { newItem }.AsQueryable());

            // Act
            var result = _service.Create(newItem, invoiceId);

            // Assert
            _repository.Received().Add(newItem);
            _unitOfWork.Received(1).Commit();
            result.Invoice.Id.Should().Be(invoiceId);
        }

        [TestMethod]
        public void CreatingListItemsWithInvoiceIdShouldReturnListItemsCreated()
        {
            // Arrange
            var invoiceId = 123;
            var firstItem = CreateEntity(invoiceId);
            var secondItem = CreateEntity(invoiceId);

            var itemsList = new List<InvoiceItem> { firstItem, secondItem };

            _repository.QuerySearch().Returns(itemsList.AsQueryable());

            // Act
            var result = _service.Create(itemsList, invoiceId);

            // Assert
            _repository.Received().Add(firstItem);
            _repository.Received().Add(secondItem);
            _unitOfWork.Received(2).Commit();
            result.Count.Should().Equals(itemsList.Count);
        }

        private InvoiceItem CreateEntity(int invoiceId = 0, int invoiceItemId = 0)
        {
            return new InvoiceItem()
            {
                Id = invoiceItemId,
                Ncm = "Ncm",
                Price = 50,
                ProductCode = "ABC",
                ProductName = "Name",
                Quantity = 5,
                RmaCode = "RMA",
                Invoice = invoiceId > 0 ? new Invoice() { Id = invoiceId } : null
            };
        }
    }
}
