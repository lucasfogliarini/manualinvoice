﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class VariableServiceTests
    {
        private IVariableRepository _variableRepository;
        private IUnitOfWork _unitOfWork;
        private VariableService _service;

        [TestInitialize]
        public void Initialize()
        {
            _variableRepository = Substitute.For<IVariableRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            var ruleValidator = Substitute.For<IRuleValidator>();
            _service = new VariableService(_unitOfWork, ruleValidator, _variableRepository);
        }

        [TestMethod]
        public void SearchingWithVariablesShouldReturn()
        {
            // Arrange

            var variables = new List<Variable>()
            {
                new Variable(){ Id = 1, Name = "variable1", DefaultValue = "$count", IsActive = true },
                new Variable(){ Id = 2, Name = "variable2", DefaultValue = "$take", IsActive = true },
                new Variable(){ Id = 3, Name = "variable3", DefaultValue = "$top", IsActive = true },
            };

            _variableRepository.QuerySearch().Returns(variables.AsQueryable());

            // Act
            var returnVariables = _service.Search();

            // Assert
            returnVariables.Should().NotBeNull();
        }

        [TestMethod]
        public void GettingByIdWithExistingIdShouldReturnVariable()
        {
            // Arrange
            var variable = new Variable()
            {
                Id = 1,
                Name = "var1",
                IsActive = true,
            };

            _variableRepository.QuerySearch().Returns(new Variable[] { variable }.AsQueryable());

            // Act
            var variableResult = _service.Get(variable.Id);

            // Assert
            variableResult.Should().NotBeNull();
        }

        [TestMethod]
        public void CreatingWithVariableShouldReturnVariableCreated()
        {
            // Arrange
            var variable = new Variable()
            {
                Id = 1,
                Name = "var1",
                IsActive = true,
            };

            // Act
            var variableResult = _service.Create(variable);

            // Assert
            _variableRepository.Received().Add(Arg.Is(variable));
            _unitOfWork.Received().Commit();
            variableResult.Should().NotBeNull();
        }

        [TestMethod]
        public void UpdatingWithVariableShouldReturnVariableUpdated()
        {
            // Arrange
            var variable = new Variable()
            {
                Id = 1,
                Name = "var1",
                IsActive = true,
                DefaultValue = "DefaultValue1",
                IsAlphanumeric = false,
                IsSystem = true,
                IsEditable = false,
                Length = 12
            };

            // Act
            var variableResult = _service.Update(variable);

            // Assert
            _variableRepository.Received().Update(Arg.Is(variable));
            _unitOfWork.Received().Commit();
            variableResult.Should().NotBeNull();
        }
    }
}
