﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class InvoiceApproverServiceTests
    {
        private IUnitOfWork _unitOfWork;
        private IInvoiceApproverService _service;
        private IInvoiceApproverRepository _repository;
        private ITransactionLevelService _transactionLevelService;
        private IUserService _userService;
        private IApprovalViewService _approvalViewService;
        private IEmailService _emailService;
        private IReasonService _reasonService;
        private AppSettings _appSettings;

        [TestInitialize]
        public void Initialize()
        {
            _repository = Substitute.For<IInvoiceApproverRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _transactionLevelService = Substitute.For<ITransactionLevelService>();
            _userService = Substitute.For<IUserService>();
            _approvalViewService = Substitute.For<IApprovalViewService>();
            _emailService = Substitute.For<IEmailService>();
            _reasonService = Substitute.For<IReasonService>();
            _appSettings = Substitute.For<AppSettings>();
            _service = new InvoiceApproverService(
                _unitOfWork, 
                _repository,
                _transactionLevelService,
                _userService,
                _approvalViewService,
                _emailService,
                _reasonService, 
                _appSettings);
        }

        [TestMethod]
        public void CreatingWithTransactionLevelsVisibleShouldCreate()
        {
            // Arrange
            int invoiceId = 1;
            int transactionId = 1;

            IList<TransactionLevel> transactionLevels = new List<TransactionLevel>();
            transactionLevels.Add(new TransactionLevel
            {
                Level = new Level { Visible = true }
            });

            _transactionLevelService.GetActivesLevelsByTransaction(transactionId).Returns(transactionLevels);

            // Act
            _service.Create(invoiceId, transactionId);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Add(Arg.Any<InvoiceApprover>());
                _userService.DidNotReceiveWithAnyArgs().Search();
                _unitOfWork.Received().Commit();
            }
        }

        [TestMethod]
        public void CreatingWithTransactionLevelsInvisibleShouldCreate()
        {
            // Arrange
            int invoiceId = 1;
            int transactionId = 1;
            int levelId = 1;
            string userId = "1";

            IList<TransactionLevel> transactionLevels = new List<TransactionLevel>();

            transactionLevels.Add(new TransactionLevel
            {
                Level = new Level { Visible = false, Id = levelId }
            });

            var users = new List<UserResponseItemDto>();
            var user = new UserResponseItemDto()
            {
                Id = userId,
                Levels = new List<LevelDto>()
                {
                    new LevelDto()
                    {
                        Id = levelId
                    }
                },
                Profiles = new string[] { "Approver" }
            };
            users.Add(user);
            var userResponseDto = new UserResponseDto()
            {
                Users = users
            };

            _transactionLevelService.GetActivesLevelsByTransaction(transactionId).Returns(transactionLevels);
            _userService.Search(Arg.Any<UserRequestDto>()).Returns(userResponseDto);

            // Act
            _service.Create(invoiceId, transactionId);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Add(Arg.Any<InvoiceApprover>());
                _userService.Received().Search(Arg.Any<UserRequestDto>());
                _unitOfWork.Received().Commit();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithZeroInvoiceIdShouldReturnEmpty()
        {
            // Arrange
            long invoiceId = 0;

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().BeEmpty();
                _repository.DidNotReceiveWithAnyArgs().Add(Arg.Any<InvoiceApprover>());
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithNullInvoiceApproversShouldReturnEmpty()
        {
            // Arrange
            long invoiceId = 1;

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().BeEmpty();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithNoFindInvoiceApproversShouldReturnEmpty()
        {
            // Arrange
            long invoiceId = 1;
            _repository.QueryComplete().Returns(new List<InvoiceApprover>().AsQueryable());
            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().BeEmpty();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithInvoiceLevelNullShouldReturnEmpty()
        {
            // Arrange
            long invoiceId = 1;

            var invoice = new Invoice() { Id = invoiceId };
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover() { Invoice = invoice, IsVisible = true });
            _repository.QueryComplete().Returns(invoiceApprovers.AsQueryable());
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto() { Id = "1" }
                    }
                });

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.First().Level.Should().BeNull();
            }
        }


        [TestMethod]
        public void GettingByInvoiceAsyncWithDraftInvoiceIdShouldReturnDto()
        {
            // Arrange
            long invoiceId = 1;
            int levelId = 1;

            var invoice = new Invoice()
            {
                Id = invoiceId,
                RejectReason = new Reason()
                {
                    Name = "reasonName"
                },
                Status = InvoiceStatusType.Draft
            };
            var level = new Level() { Id = levelId };
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover()
            {
                User = new User()
                {
                    Id = 1,
                    UserName = "ntaccount"
                },
                EffectiveUser = new User()
                {
                    UserName = "ntaccount"
                },
                ApprovalSequence = 1,
                Invoice = invoice,
                IsVisible = true,
                Level = level,
                Situation = ApproverStatusType.Rejected
            });
            _repository.QueryComplete().Returns(invoiceApprovers.AsQueryable());

            var userResponseItemDtos = new List<UserResponseItemDto>();
            var userResponseItemDto = new UserResponseItemDto()
            {
                Id = "1",
                Profiles = new string[] { "Approver" }
            };
            var levelDtos = new List<LevelDto>();
            var levelDto = new LevelDto() { Id = levelId };
            levelDtos.Add(levelDto);
            userResponseItemDto.Levels = levelDtos;
            userResponseItemDtos.Add(userResponseItemDto);
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto() { Users = userResponseItemDtos });

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeEmpty();
                _repository.Received().QueryComplete();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithDraftInvoiceIdAndNoUsersShouldReturnDto()
        {
            // Arrange
            long invoiceId = 1;
            int levelId = 1;

            var invoice = new Invoice()
            {
                Id = invoiceId,
                RejectReason = new Reason()
                {
                    Name = "reasonName"
                },
                Status = InvoiceStatusType.Draft
            };
            var level = new Level() { Id = levelId };
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover()
            {
                ApprovalSequence = 1,
                Invoice = invoice,
                IsVisible = true,
                Level = level,
                Situation = ApproverStatusType.Rejected
            });
            _repository.QueryComplete().Returns(invoiceApprovers.AsQueryable());
            _userService.Search(Arg.Any<UserRequestDto>()).Returns(new UserResponseDto() { Users = null });

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeEmpty();
                _repository.Received().QueryComplete();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithNoDraftInvoiceIdAndApproverInvisibleShouldReturnDto()
        {
            // Arrange
            long invoiceId = 1;
            int levelId = 1;

            var invoice = new Invoice()
            {
                Id = invoiceId,
                RejectReason = new Reason()
                {
                    Name = "reasonName"
                },
                Status = InvoiceStatusType.Draft
            };
            var level = new Level() { Id = levelId };
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover()
            {
                User = new User()
                {
                    Id = 1,
                    UserName = "ntaccount"
                },
                EffectiveUser = new User()
                {
                    UserName = "ntaccount"
                },
                ApprovalSequence = 1,
                Invoice = invoice,
                IsVisible = false,
                Level = level,
                Situation = ApproverStatusType.Rejected
            });
            _repository.QueryComplete().Returns(invoiceApprovers.AsQueryable());
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto()
                        {
                            Id = "1",
                            Levels = new List<LevelDto>()
                            {
                                new LevelDto()
                                {
                                    Id = levelId
                                }
                            }
                        }
                    }
                });

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().BeEmpty();
            }
        }

        [TestMethod]
        public void GettingByInvoiceAsyncWithNoDrafInvoiceIdAndInvisibleLevelShouldReturnDto()
        {
            // Arrange
            long invoiceId = 1;
            int levelId = 1;

            var invoice = new Invoice()
            {
                Id = invoiceId,
                RejectReason = new Reason()
                {
                    Name = "reasonName"
                },
                Status = InvoiceStatusType.Analyzing
            };
            var level = new Level() { Id = levelId, Visible = false };
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover()
            {
                Invoice = invoice,
                IsVisible = false,
                Level = level,
                Situation = ApproverStatusType.Analyzing
            });
            invoiceApprovers.Add(new InvoiceApprover()
            {
                Invoice = invoice,
                IsVisible = false,
                Level = level,
                Situation = ApproverStatusType.Analyzing
            });
            _repository.QueryComplete().Returns(invoiceApprovers.AsQueryable());
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto()
                        {
                            Id = "1",
                            Levels = new List<LevelDto>()
                            {
                                new LevelDto()
                                {
                                    Id = levelId
                                }
                            }
                        }
                    }
                });

            // Act
            var result = _service.GetByInvoiceAsync(invoiceId).Result;

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().HaveCount(1);
                _repository.Received().QueryComplete();
            }
        }

        [TestMethod]
        public void UpdatingWithApproversShouldReturnApproved()
        {
            // Arrange
            IList<InvoiceApprover> approvers = new List<InvoiceApprover>() {
                new InvoiceApprover() { Id = 1 , UserId = 10}
            };
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover() { Id = 1, UserId = 15 });

            _repository.QuerySearch().Returns(entity.AsQueryable());

            // Act
            var result = _service.Update(approvers);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
                _unitOfWork.Received().Commit();
                result.Should().NotBeEmpty();
            }
        }

        [TestMethod]
        public void UpdatingWithSameUserApproverShouldReturnApprover()
        {
            // Arrange
            IList<InvoiceApprover> approvers = new List<InvoiceApprover>() {
                new InvoiceApprover() { Id = 1 , UserId = 10}
            };
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover() { Id = 1, UserId = 10 });

            _repository.QuerySearch().Returns(entity.AsQueryable());

            // Act
            var result = _service.Update(approvers);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.DidNotReceiveWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
                _unitOfWork.DidNotReceive().Commit();
                result.Should().NotBeEmpty();
            }
        }

        [TestMethod]
        public void UpdatingWithoutFindApproverShouldReturnNotFoundException()
        {
            // Arrange
            IList<InvoiceApprover> approvers = new List<InvoiceApprover>() {
                new InvoiceApprover() { Id = 1 , UserId = 10}
            };
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover() { Id = 1, UserId = 15 });

            // Act and Assert
            Assert.ThrowsException<NotFoundException<InvoiceApprover>>(() =>
            {
                _service.Update(approvers);
            });
            
            // Assert
            using (var scope = new AssertionScope())
            {
                _unitOfWork.DidNotReceive().Commit();
                _repository.DidNotReceiveWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
            }
        }

        [TestMethod]
        public void UpdatingWithNullApproversShouldReturnEmpty()
        {
            //Arrange
            List<InvoiceApprover> approvers = null;

            //Act
            var result = _service.Update(approvers);

            // Assert
            result.Should().BeEmpty();
            _repository.DidNotReceive();
        }

        [TestMethod]
        public void UpdatingWithEmptyApproversShouldReturnEmpty()
        {
            //Arrange
            List<InvoiceApprover> approvers = new List<InvoiceApprover>();

            //Act
            var result = _service.Update(approvers);

            // Assert
            result.Should().BeEmpty();
            _repository.DidNotReceive();
        }

        [TestMethod]
        public void ApprovingWithInvoiceIdShouldUpdateApprovers()
        {
            // Arrange
            int invoiceId = 102030;
            int effectiveUser = 10;
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover() {
                Id = invoiceId,
                Invoice =  new Invoice(),
                Level = new Level(),
                ApprovalSequence = 2,
                Situation = ApproverStatusType.Analyzing,
                EffectiveUser = new User() { Id = effectiveUser },
                UserId = 1
            });

            _repository.QueryComplete().Returns(entity.AsQueryable());
            _repository.QuerySearch().Returns(entity.AsQueryable());

            // Act
            _service.Approve(invoiceId, effectiveUser);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
                _unitOfWork.Received().Commit();
            }
        }

        [TestMethod]
        public void AprovingWithoutFindInvoiceShouldThrowNotFoundException()
        {
            // Arrange
            int invoiceId = 102030;
            int effectiveUserId = 1;

            //Act and Assert
            Assert.ThrowsException<NotFoundException<InvoiceApprover>>(() =>
            {
                _service.Approve(invoiceId, effectiveUserId);
            });

            // Assert    
            _unitOfWork.DidNotReceive().Commit();
        }

        [TestMethod]
        public void RejectingWithInvoiceIdShouldUpdateApprovers()
        {
            // Arrange
            int invoiceId = 102030;
            int effectiveUser = 10;
            int reasonId = 1;
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover()
            {
                Id = invoiceId,
                Situation = ApproverStatusType.Rejected,
                EffectiveUser = new User() { Id = effectiveUser },
                UserId = 1
            });
            DateTime rejectDate = DateTime.Now;

            _repository.QueryComplete().Returns(entity.AsQueryable());

            // Act
            _service.Reject(invoiceId, effectiveUser, rejectDate, reasonId);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
                _unitOfWork.Received().Commit();
            }
        }

        [TestMethod]
        public async Task RejectingWithoutFindInvoiceShouldThrowNotFoundException()
        {
            // Arrange
            int invoiceId = 102030;
            int effectiveUserId = 1;
            DateTime rejectDate = DateTime.Now;
            int reasonId = 1;

            //Act and Assert
            await Assert.ThrowsExceptionAsync<NotFoundException<InvoiceApprover>>(async () =>
            {
                await _service.Reject(invoiceId, effectiveUserId, rejectDate, reasonId);
            });

            // Assert
            _unitOfWork.DidNotReceive().Commit();
        }

        [TestMethod]
        public void AnalyzingWithInvoiceIdShouldUpdateApprovers()
        {
            // Arrange
            int invoiceId = 102030;
            var entity = new List<InvoiceApprover>();
            entity.Add(new InvoiceApprover()
            {
                Id = invoiceId,
                Invoice = new Invoice(),
                Level = new Level(),
                ApprovalSequence = 2,
                Situation = ApproverStatusType.Analyzing,
                UserId = 1
            });

            _repository.QueryComplete().Returns(entity.AsQueryable());
            _repository.QuerySearch().Returns(entity.AsQueryable());

            // Act
            _service.Analyze(invoiceId);

            // Assert
            using (var scope = new AssertionScope())
            {
                _repository.ReceivedWithAnyArgs().Update(Arg.Any<InvoiceApprover>());
                _unitOfWork.Received().Commit();
            }
        }

        [TestMethod]
        public async Task AnalyzingWithoutFindInvoiceShouldThrowNotFoundException()
        {
            // Arrange
            int invoiceId = 102030;

            // Act and Assert
            await Assert.ThrowsExceptionAsync<NotFoundException<InvoiceApprover>>(async () =>
            {
                await _service.Analyze(invoiceId);
            });
        }

        [TestMethod]
        public void GettingByInvoiceWithInvoiceIdShouldReturnInvoiceApprovers()
        {
            // Arrange
            int invoiceId = 102030;
            _repository.QuerySearch().Returns((new List<InvoiceApprover>()).AsQueryable());

            // Act and Assert
            var result = _service.GetByInvoice(invoiceId);

            // Assert    
            result.Should().NotBeNull();
        }
    }
}

