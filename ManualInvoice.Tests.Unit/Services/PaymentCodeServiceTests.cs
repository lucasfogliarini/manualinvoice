﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class PaymentCodeServiceTests
    {
        private IPaymentCodeRepository _repository;
        private IUnitOfWork _unitOfWork;
        private IPaymentCodeService _service;
        private IRuleValidator _ruleValidator;

        [TestInitialize]
        public void Initialize()
        {
            _ruleValidator = Substitute.For<IRuleValidator>();
            _repository = Substitute.For<IPaymentCodeRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _service = new PaymentCodeService(_unitOfWork, _ruleValidator, _repository);
        }

        [TestMethod]
        public void GettingAllActivesWithoutActivePaymentCodeShouldReturnEmptyList()
        {
            // Arrange
            _repository.QueryComplete()
                .Returns(new List<PaymentCode>()
                {
                    new PaymentCode(){IsActive = false},
                    new PaymentCode(){IsActive = false}
                }.AsQueryable());

            // Act
            var result = _service.GetAllActives();

            // Assert
            _repository.Received().QueryComplete();
            result.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void GettingAllActivesWithOnePaymentCodeActiveShouldReturnOnlyTheActive()
        {
            // Arrange
            var paymentCodes = new List<PaymentCode>()
            {
                new PaymentCode(){IsActive = true},
                new PaymentCode(){IsActive = false}
            };

            _repository.QueryComplete()
                .Returns(paymentCodes.AsQueryable());

            // Act
            var result = _service.GetAllActives();

            // Assert
            _repository.Received().QueryComplete();
            result.Should().HaveCount(paymentCodes.Count(x => x.IsActive));
        }

        [TestMethod]
        public void CreatingWithPaymentCodeShouldReturnPaymentCodeCreated()
        {
            // Arrange
            var paymentCode = new PaymentCode()
            {
                Id = 1,
                Code = "@",
                IsActive = true,
                Description = "@",
                IsCreditCard =  true
            };

            // Act
            var paymentCodeResult = _service.Create(paymentCode);

            // Assert
            _repository.Received().Add(Arg.Is(paymentCode));
            _unitOfWork.Received().Commit();
            paymentCodeResult.Should().NotBeNull();
        }

        [TestMethod]
        public void UpdatingWithPaymentCodeShouldReturnpaymentCodeUpdated()
        {
            // Arrange
            var paymentCode = new PaymentCode()
            {
                Id = 1,
                Code = "O",
                IsActive = true,
                Description = "Payment O",
                IsCreditCard = false
            };

            // Act
            var paymentCodeResult = _service.Update(paymentCode);

            // Assert
            _repository.Received().Update(Arg.Is(paymentCode));
            _unitOfWork.Received().Commit();
            paymentCodeResult.Should().NotBeNull();
        }
    }
}
