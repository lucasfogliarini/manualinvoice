﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Dtos.InvoiceVariable;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class InvoiceServiceTests
    {
        private IRuleValidator _ruleValidator;
        private IInvoiceRepository _invoiceRepository;
        private IInvoiceApiGateway _invoiceApiGateway;
        private IInvoiceItemService _invoiceItemService;
        private IUnitOfWork _unitOfWork;
        private IInvoiceService _service;
        private ICarrierService _carrierService;
        private IInvoiceVariableService _invoiceVariableService;
        private IInvoiceApproverService _invoiceApproverService;
        private ISkuService _skuService;
        private ICustomerApiGateway _customerApiGateway;
        private ITransactionService _transactionService;
        private IPaymentCodeService _paymentCodeService;
        private IOrderService _orderService;
        private IBranchService _branchService;
        private IIssuerService _issuerService;
        private ILoadVolumeService _loadVolumeService;
        private IApprovalViewService _approverViewService;
        private IUserService _userService;
        private IDofConsolidationService _dofConsolidationService;
        private IOrderItemService _orderItemService;

        [TestInitialize]
        public void Initialize()
        {
            _ruleValidator = Substitute.For<IRuleValidator>();
            _invoiceRepository = Substitute.For<IInvoiceRepository>();
            _invoiceApiGateway = Substitute.For<IInvoiceApiGateway>();
            _invoiceItemService = Substitute.For<IInvoiceItemService>();
            _skuService = Substitute.For<ISkuService>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _carrierService = Substitute.For<ICarrierService>();
            _invoiceVariableService = Substitute.For<IInvoiceVariableService>();
            _invoiceApproverService = Substitute.For<IInvoiceApproverService>();
            _customerApiGateway = Substitute.For<ICustomerApiGateway>();
            _transactionService = Substitute.For<ITransactionService>();
            _paymentCodeService = Substitute.For<IPaymentCodeService>();
            _orderService = Substitute.For<IOrderService>();
            _branchService = Substitute.For<IBranchService>();
            _issuerService = Substitute.For<IIssuerService>();
            _loadVolumeService = Substitute.For<ILoadVolumeService>();
            _approverViewService = Substitute.For<IApprovalViewService>();
            _userService = Substitute.For<IUserService>();
            _dofConsolidationService = Substitute.For<IDofConsolidationService>();
            _orderItemService = Substitute.For<IOrderItemService>();

            _service = new InvoiceService(
                _unitOfWork,
                _ruleValidator,
                _invoiceRepository,
                _invoiceApiGateway,
                _invoiceItemService,
                _carrierService,
                _invoiceVariableService,
                _customerApiGateway,
                _skuService,
                _transactionService,
                _paymentCodeService,
                _orderService,
                _invoiceApproverService,
                _branchService,
                _issuerService,
                _loadVolumeService,
                _approverViewService,
                _userService,
                _dofConsolidationService,
                _orderItemService);
        }

        [TestMethod]
        public async Task CreatingInvoiceWithSuccessShouldReturnCreatedInvoice()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            int userId = 2030;
            NopDto nop = new NopDto { NopCode = "S03", NopType = NopType.Service };
            _ruleValidator.Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());
            string[] originalInvoices = null;

            _invoiceApiGateway.GetNopByCode(nop.NopCode).Returns<NopDto>(nop);
            // Act
            var result = await _service.CreateInvoice(userId, invoice, new Transaction() { BillToCode = nop.NopCode }, null, originalInvoices, false);

            // Assert
            Received.InOrder(() =>
            {
                _ruleValidator.Received().Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true);
                _invoiceRepository.ReceivedWithAnyArgs().Add(Arg.Any<Invoice>());
                _unitOfWork.Received().Commit();
            });

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Response.User.Id.Should().Equals(userId);
            };
        }

        [TestMethod]
        public void CreatingInvoiceWithInvalidInvoiceShouldReturnException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            int userId = 2030;
            var applicationException = new ApplicationException("Teste");
            _ruleValidator.Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(x => { throw applicationException; });
            string[] originalInvoices = null;

            // Act

            Func<Task> result = async () => _ = await _service.CreateInvoice(userId, invoice, new Transaction(), null, originalInvoices, false);

            // Assert
            _invoiceRepository.DidNotReceiveWithAnyArgs().Add(Arg.Any<Invoice>());
            _unitOfWork.DidNotReceive().Commit();
            result.Should().Throw<ApplicationException>().And.Message.Should().Equals(applicationException.Message);

        }

        [TestMethod]
        public void UpdatingInvoiceWithSuccessShouldReturnUpdatedInvoice()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            _ruleValidator.Validate<UpdateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());
            int invoiceId = 1010;

            // Act
            var result = _service.Update(invoiceId, invoice, new List<InvoiceItem>(), new List<InvoiceVariable>(), new List<InvoiceApprover>());

            // Assert
            Received.InOrder(() =>
            {
                _ruleValidator.Received().Validate<UpdateInvoiceValidator>(Arg.Any<Invoice>(), true);
                _invoiceRepository.ReceivedWithAnyArgs().Update(Arg.Any<Invoice>());
                _unitOfWork.Received().Commit();
            });

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Response.Id.Should().Be(invoiceId);
            };
        }

        [TestMethod]
        public void UpdatingInvoiceWithInvalidInvoiceShouldReturnException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            int invoiceId = 1010;
            var applicationException = new ApplicationException("Teste");
            _ruleValidator.Validate<UpdateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(x => { throw applicationException; });

            // Act
            Action result = () => _ = _service.Update(invoiceId, invoice, new List<InvoiceItem>(), new List<InvoiceVariable>(), new List<InvoiceApprover>());

            // Assert
            _invoiceRepository.DidNotReceiveWithAnyArgs().Update(Arg.Any<Invoice>());
            _unitOfWork.DidNotReceive().Commit();
            result.Should().Throw<ApplicationException>().And.Message.Should().Equals(applicationException.Message);
        }

        [TestMethod]
        public async Task GettingSynchroInvoicesWhenPassIdsShouldReturnInvoicesForThem()
        {
            // Arrange
            IEnumerable<long> ids = new List<long>() { 10, 20 };
            List<FiscalDocumentDto> mockResult = new List<FiscalDocumentDto>();
            mockResult.Add(new FiscalDocumentDto());
            _invoiceApiGateway.GetSynchroInvoices(ids).Returns(mockResult);

            // Act
            var result = await _service.GetSynchroInvoices(ids);

            // Assert
            result.Should().NotBeNull().And.HaveCount(mockResult.Count);
        }

        [TestMethod]
        public async Task GettingSynchroInvoicesWithoutPassIdsShouldReturnAnEmptyList()
        {
            // Arrange
            IEnumerable<long> ids = new List<long>();
            List<FiscalDocumentDto> mockResult = new List<FiscalDocumentDto>();
            mockResult.Add(new FiscalDocumentDto());
            _invoiceApiGateway.GetSynchroInvoices(ids).Returns(mockResult);

            // Act
            var result = await _service.GetSynchroInvoices(ids);
            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull().And.HaveCount(1);
                result.First().Items.Should().NotBeNull().And.HaveCount(0);
            };
        }

        public void CancelingInvoiceWithInexistentIdShouldThrowException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            var invoiceId = 0;
            var userName = "John Doe";

            _invoiceRepository.QueryComplete().Returns(new[] { invoice }.AsQueryable());

            // Act
            Action result = () => _service.Cancel(invoiceId, userName);

            // Assert
            using (var scope = new AssertionScope())
            {
                _invoiceRepository.DidNotReceiveWithAnyArgs().Update(Arg.Any<Invoice>());
                _unitOfWork.DidNotReceive().Commit();
                result.Should().Throw<NotFoundException<Invoice>>();
            }
        }

        [TestMethod]
        public void CancelingInvoiceWithoutCreatorUserShouldThrowException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            var invoiceId = invoice.Id;
            var userName = "John Doe";
            invoice.User.UserName = "Fail User";

            _invoiceRepository.QueryComplete().Returns(new[] { invoice }.AsQueryable());

            // Act
            Action result = () => _service.Cancel(invoiceId, userName);

            // Assert
            using (var scope = new AssertionScope())
            {
                _invoiceRepository.DidNotReceiveWithAnyArgs().Update(Arg.Any<Invoice>());
                _unitOfWork.DidNotReceive().Commit();
                result.Should().Throw<ForbiddenException>()
                    .And.Message.Should().Contain("The Invoice can only be cancelled by its creator");
            }
        }

        [TestMethod]
        public void CancelingInvoiceWithValidConditionsShouldPass()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            var invoiceId = invoice.Id;
            var userName = "John Doe";
            invoice.User.UserName = userName;

            _invoiceRepository.QueryComplete().Returns(new[] { invoice }.AsQueryable());

            // Act
            Action result = () => _service.Cancel(invoiceId, userName);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotThrow();
                invoice.Status.Should().Be(InvoiceStatusType.Cancelled);
            };
        }

        [TestMethod]
        public async Task GettingInvoiceVariablesAsyncWithInvoiceIdWithoutVariablesShouldReturnEmptyList()
        {
            // Arrange
            int invoiceId = 1010;
            _invoiceVariableService.GetByInvoiceAsync(invoiceId).Returns(new List<InvoiceVariableResponseDto>());

            // Act
            var result = await _service.GetInvoiceVariablesAsync(invoiceId);

            // Assert
            await _invoiceVariableService.Received().GetByInvoiceAsync(invoiceId);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public async Task GettingInvoiceVariablesAsyncWithInvoiceIdThatHasVariablesShouldReturnListOfVariables()
        {
            // Arrange
            int invoiceId = 1010;
            var invoiceVariables = new List<InvoiceVariableResponseDto>();
            invoiceVariables.Add(new InvoiceVariableResponseDto());
            _invoiceVariableService.GetByInvoiceAsync(invoiceId).Returns(invoiceVariables);

            // Act
            var result = await _service.GetInvoiceVariablesAsync(invoiceId);

            // Assert
            await _invoiceVariableService.Received().GetByInvoiceAsync(invoiceId);
            result.Should().HaveCount(invoiceVariables.Count);
        }

        [TestMethod]
        public async Task CreatingOrderWithInvoiceAlreadySentShouldNotSendAgainAndReturnNull()
        {
            // Arrange
            int invoiceId = 102030;

            Invoice entity = new Invoice()
            {
                Id = invoiceId,
                SynchroStatus = OrderSynchroStatus.Finalized
            };
            _invoiceRepository.QueryComplete().Returns(new List<Invoice>() { entity }.AsQueryable());

            // Act
            var result = await _service.CreateOrder(invoiceId);

            // Assert
            _invoiceRepository.Received().QueryComplete();
            _invoiceVariableService.DidNotReceive().GetInvoiceVariablesByInvoiceId(invoiceId);
            _invoiceRepository.DidNotReceive().Update(Arg.Any<Invoice>());
            _unitOfWork.DidNotReceive().Commit();
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task CreatingOrderWithInvoiceWithBillNotSentShouldNotSendAgainAndReturnStageSalesOrderDto()
        {
            // Arrange
            int invoiceId = 102030;

            Invoice entity = new Invoice()
            {
                Id = invoiceId,
                UpdatedOnSynchro = false,
                BillToSent = false,
                ShipToSent = false,
                BillToCode = "ABC",
                TransactionBillToCode = "QWE",
                Transaction = new Transaction(),
                Branch = new Branch()
            };

            _invoiceApiGateway.UpdateOnSynchro(Arg.Any<UpdateOrderDto>()).Returns(new List<long>() { 987654320001 });
            _invoiceRepository.QueryComplete().Returns(new List<Invoice>() { entity }.AsQueryable());
            _invoiceVariableService.GetInvoiceVariablesByInvoiceId(invoiceId)
                .Returns(new List<InvoiceVariable>());
            _invoiceApiGateway.CreateOrder(Arg.Any<OrderDto>()).Returns(new CreatedOrderResponseDto { Success = true });
            _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(entity.BillToCode)
                .Returns(new CustomerDto());

            // Act
            var result = await _service.CreateOrder(invoiceId);

            // Assert
            _invoiceRepository.Received().QueryComplete();
            _invoiceVariableService.Received().GetInvoiceVariablesByInvoiceId(invoiceId);
            _invoiceRepository.Received().Update(Arg.Any<Invoice>());
            await _invoiceApiGateway.Received().UpdateOnSynchro(Arg.Any<UpdateOrderDto>());
            _unitOfWork.Received().Commit();
            result.Should().NotBeNull();
        }

        [TestMethod]
        public async Task CreatingOrderWithInvoiceWithShipNotSentShouldNotSendAgainAndReturnStageSalesOrderDto()
        {
            // Arrange
            int invoiceId = 102030;

            Invoice entity = new Invoice()
            {
                Id = invoiceId,
                UpdatedOnSynchro = false,
                BillToSent = true,
                ShipToSent = false,
                BillToCode = "ABC",
                ShipToCode = "ABC",
                TransactionBillToCode = "QWE",
                TransactionShipToCode = "ERT",
                Transaction = new Transaction(),
                Branch = new Branch()
            };

            _invoiceApiGateway.UpdateOnSynchro(Arg.Any<UpdateOrderDto>()).Returns(new List<long>() { 987654320001 });
            _invoiceRepository.QueryComplete().Returns(new List<Invoice>() { entity }.AsQueryable());
            _invoiceVariableService.GetInvoiceVariablesByInvoiceId(invoiceId)
                .Returns(new List<InvoiceVariable>());
            _invoiceApiGateway.CreateOrder(Arg.Any<OrderDto>()).Returns(new CreatedOrderResponseDto { Success = true });
            _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(entity.ShipToCode)
                .Returns(new CustomerDto());

            // Act
            var result = await _service.CreateOrder(invoiceId);

            // Assert
            _invoiceRepository.Received().QueryComplete();
            _invoiceVariableService.Received().GetInvoiceVariablesByInvoiceId(invoiceId);
            _invoiceRepository.Received().Update(Arg.Any<Invoice>());
            await _invoiceApiGateway.Received().UpdateOnSynchro(Arg.Any<UpdateOrderDto>());
            _unitOfWork.Received().Commit();
            result.Should().NotBeNull();
        }

        [TestMethod]
        public async Task VerifyingIfIsOrderNumberValidAsyncWhereOrderNumberNotExists()
        {
            // Arrange
            var orderNumber = "1";
            _invoiceApiGateway.IsOrderNumberValidAsync(orderNumber).Returns(false);

            // Act
            var result = await _service.IsOrderNumberValidAsync(orderNumber);

            // Assert
            await _invoiceApiGateway.Received().IsOrderNumberValidAsync(orderNumber);
            result.Should().BeFalse();
        }

        [TestMethod]
        public async Task VerifyingIfIsOrderNumberValidAsyncWhereOrderNumberExists()
        {
            // Arrange
            var orderNumber = "999999999";
            _invoiceApiGateway.IsOrderNumberValidAsync(orderNumber).Returns(true);

            // Act
            var result = await _service.IsOrderNumberValidAsync(orderNumber);

            // Assert
            await _invoiceApiGateway.Received().IsOrderNumberValidAsync(orderNumber);
            result.Should().BeTrue();
        }

        [TestMethod]
        public async Task VerifyingIfIsItemQuantityValidAsyncWhereQuantityIsValid()
        {
            // Arrange
            var itemQuantity = 1;
            var maxItems = 2;
            _invoiceApiGateway.GetMaxItemsPerInvoice().Returns(maxItems);

            // Act
            var result = await _service.IsItemQuantityValid(itemQuantity);

            // Assert
            await _invoiceApiGateway.Received().GetMaxItemsPerInvoice();
            result.Should().BeTrue();
        }

        [TestMethod]
        public async Task VerifyingIfIsItemQuantityValidAsyncWhereQuantityIsInvalid()
        {
            // Arrange
            var itemQuantity = 10;
            var maxItems = 2;
            _invoiceApiGateway.GetMaxItemsPerInvoice().Returns(maxItems);

            // Act
            var result = await _service.IsItemQuantityValid(itemQuantity);

            // Assert
            await _invoiceApiGateway.Received().GetMaxItemsPerInvoice();
            result.Should().BeFalse();
        }

        [TestMethod]
        public async Task CreatingInvoiceWithConsolidatedItemShouldReturnCreatedInvoiceWithOnlyOneItem()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            int userId = 2030;
            NopDto nop = new NopDto { NopCode = "S03", NopType = NopType.Service };
            var consolidatedItem = new InvoiceItem { Quantity = 1, ProductCode = "123", ProductName = "Software Livre", Price = 23M };
            var fiscalDocumentsDto = new List<FiscalDocumentDto>();

            _ruleValidator.Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());
            _invoiceApiGateway.GetNopByCode(nop.NopCode).Returns(nop);

            string[] originalInvoices = { "1234", "4321" };
            long[] convertedOriginalInvoices = originalInvoices.Select(long.Parse).ToArray();

            fiscalDocumentsDto.Add(FiscalDocumentsDtoMock.CreateFiscalDocumentsDto(convertedOriginalInvoices[0]));
            fiscalDocumentsDto.Add(FiscalDocumentsDtoMock.CreateFiscalDocumentsDto(convertedOriginalInvoices[1]));

            _invoiceApiGateway.GetSynchroInvoices(Arg.Any<IEnumerable<long>>()).Returns(fiscalDocumentsDto);

            var branch = new Branch
            {
                Id = 1,
                Name = fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo,
                Description = "ABC",
                IsActive = true,
                Location = "123"
            };

            _branchService.GetByName(fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo).Returns(branch);

            var addressDto = new CustomerAddressDto
            {
                PublicPlace = "A",
                Number = "123",
                CityName = "Porto Alegre",
                State = "Rio Grande do Sul"
            };

            var customerDto = new CustomerDto
            {
                PfjCode = "123",
                Name = "DELL COMPUTADORES",
                CpfCgc = "123456789987",
                Addresses = new List<CustomerAddressDto>()
            };

            customerDto.Addresses.Add(addressDto);

            _issuerService.GetByPfjCodeAsync(fiscalDocumentsDto.FirstOrDefault().DestinatarioPfjCodigo).Returns(customerDto);

            _invoiceItemService.Create(consolidatedItem, invoice.Id).Returns(consolidatedItem);

            // Act
            var result = await _service.CreateInvoice(userId, invoice, new Transaction() { BillToCode = nop.NopCode }, consolidatedItem, originalInvoices, true);

            // Assert
            Received.InOrder(() =>
            {
                _ruleValidator.Received().Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true);
                _branchService.Received().GetByName(fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo);
                _issuerService.Received().GetByPfjCodeAsync(fiscalDocumentsDto.FirstOrDefault().DestinatarioPfjCodigo);
                _invoiceRepository.ReceivedWithAnyArgs().Add(Arg.Any<Invoice>());
                _unitOfWork.Received().Commit();
                _invoiceItemService.Received().Create(Arg.Any<List<InvoiceItem>>(), invoice.Id);
                _orderService.Received().CreateOrders(invoice, Arg.Any<List<FiscalDocumentDto>>());
            });

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Response.IsConsolidated.Should().BeTrue();
                result.Response.BillToCode.Should().Be(customerDto.PfjCode);
                result.Response.BillToName.Should().Be(customerDto.Name);
                result.Response.BillToCnpj.Should().Be(customerDto.CpfCgc);
                result.Response.BillToAddress.Should().Be(customerDto.GetFullAddress());
                result.Response.Branch.Name.Should().Be(branch.Name);
            };
        }

        [TestMethod]
        public async Task CreatingInvoiceBasedOnOriginalInvoicesWithoutConsolidateShouldCreateInvoiceWithAllItems()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            int userId = 2030;
            NopDto nop = new NopDto { NopCode = "S03", NopType = NopType.Service };
            var fiscalDocumentsDto = new List<FiscalDocumentDto>();
            var item = new InvoiceItem { Quantity = 1, ProductCode = "123", ProductName = "Software Livre", Price = 23M };

            _ruleValidator.Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());
            _invoiceApiGateway.GetNopByCode(nop.NopCode).Returns(nop);

            string[] originalInvoices = { "1234", "4321" };
            long[] convertedOriginalInvoices = originalInvoices.Select(long.Parse).ToArray();

            fiscalDocumentsDto.Add(FiscalDocumentsDtoMock.CreateFiscalDocumentsDto(convertedOriginalInvoices[0]));
            fiscalDocumentsDto.Add(FiscalDocumentsDtoMock.CreateFiscalDocumentsDto(convertedOriginalInvoices[1]));

            _invoiceApiGateway.GetSynchroInvoices(Arg.Any<IEnumerable<long>>()).Returns(fiscalDocumentsDto);

            var branch = new Branch
            {
                Id = 1,
                Name = fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo,
                Description = "ABC",
                IsActive = true,
                Location = "123"
            };

            _branchService.GetByName(fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo).Returns(branch);

            var addressDto = new CustomerAddressDto
            {
                PublicPlace = "A",
                Number = "123",
                CityName = "Porto Alegre",
                State = "Rio Grande do Sul"
            };

            var customerDto = new CustomerDto
            {
                PfjCode = "123",
                Name = "DELL COMPUTADORES",
                CpfCgc = "123456789987",
                Addresses = new List<CustomerAddressDto>()
            };

            customerDto.Addresses.Add(addressDto);

            _issuerService.GetByPfjCodeAsync(fiscalDocumentsDto.FirstOrDefault().DestinatarioPfjCodigo).Returns(customerDto);

            _invoiceItemService.Create(item, invoice.Id).Returns(item);

            // Act
            var result = await _service.CreateInvoice(userId, invoice, new Transaction() { BillToCode = nop.NopCode }, null, originalInvoices, false);

            // Assert
            Received.InOrder(() =>
            {
                _ruleValidator.Received().Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true);
                _branchService.Received().GetByName(fiscalDocumentsDto.FirstOrDefault().InformanteEstCodigo);
                _issuerService.Received().GetByPfjCodeAsync(fiscalDocumentsDto.FirstOrDefault().DestinatarioPfjCodigo);
                _invoiceRepository.ReceivedWithAnyArgs().Add(Arg.Any<Invoice>());
                _unitOfWork.Received().Commit();
                _invoiceItemService.Received().Create(Arg.Any<List<InvoiceItem>>(), invoice.Id);
                _orderService.Received().CreateOrders(invoice, Arg.Any<List<FiscalDocumentDto>>());
            });

            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Response.IsConsolidated.Should().BeFalse();
                result.Response.BillToCode.Should().Be(customerDto.PfjCode);
                result.Response.BillToName.Should().Be(customerDto.Name);
                result.Response.BillToCnpj.Should().Be(customerDto.CpfCgc);
                result.Response.BillToAddress.Should().Be(customerDto.GetFullAddress());
                result.Response.Branch.Name.Should().Be(branch.Name);
            };
        }

        [TestMethod]
        public async Task CreatingSalesOrderDataWithInvalidDataShouldReturnFalse()
        {
            // Arrange
            var salesOrderDto = new SalesOrderDto
            {
                SiteCode = 001,
                InvoiceSequence = 1354817308
            };

            _invoiceApiGateway.CreateOrderOnSynchro(salesOrderDto).Returns(false);

            // Act
            var result = await _invoiceApiGateway.CreateOrderOnSynchro(salesOrderDto);

            // Assert
            await _invoiceApiGateway.Received().CreateOrderOnSynchro(salesOrderDto);

            result.Should().BeFalse();
        }

        [TestMethod]
        public async Task CreatingSalesOrderDataWithInvalidDataShouldReturnTrue()
        {
            // Arrange
            var listSalesItemDto = new List<SalesItemDto>();

            var salesItemDto = new SalesItemDto
            {
                SiteCode = 001,
                InvoiceSequence = 1354817308,
                SequenceNumber = "1",
                ItemQuantity = 1
            };
            var salesOrderDto = new SalesOrderDto
            {
                SiteCode = 001,
                InvoiceSequence = 1354817308,
                CustomerClassCode = "OH",
                PaymentAmount1LocalCurrency = 1624.34M,
                ShippingCost = 66.97M,
                OrderRate = 23033,
                Items = listSalesItemDto,
            };

            _invoiceApiGateway.CreateOrderOnSynchro(salesOrderDto).Returns(true);

            // Act
            var result = await _invoiceApiGateway.CreateOrderOnSynchro(salesOrderDto);
            await _invoiceApiGateway.Received().CreateOrderOnSynchro(salesOrderDto);
            
            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public async Task CopingInvoiceWithValidInvoiceIdReturnCopiedInvoice()
        {
            // Arrange
            var user = new User
            {
                Id = 123,
                UserName = "TestUser"
            };

            var invoice = InvoiceMock.CreateInvoice();

            InvoiceItem invoiceItem = new InvoiceItem { Id = 1010, Invoice = new Invoice { Id = invoice.Id } };
            InvoiceItem invoiceItemTwo = new InvoiceItem { Id = 1010, Invoice = new Invoice { Id = invoice.Id} };

            IList<InvoiceItem> invoiceItems = new List<InvoiceItem>()
            {
                invoiceItem,
                invoiceItemTwo
            };

            InvoiceVariable invoiceVariable = new InvoiceVariable { Id = 123};
            InvoiceVariable invoiceVariableTwo = new InvoiceVariable { Id = 321};
            IList<InvoiceVariable> invoiceVariables = new List<InvoiceVariable>()
            {
               invoiceVariable,
               invoiceVariableTwo
            };

            invoice.Items = invoiceItems;

            _invoiceVariableService.GetInvoiceVariablesByInvoiceId(invoice.Id)
                .Returns(invoiceVariables);
            _invoiceRepository.QueryComplete(false).Returns(new List<Invoice> { invoice }.AsQueryable());
            _branchService.GetBranchesByTransaction(invoice.Transaction.Id, true).Returns(new List<Branch>());
            _orderService.GetOrders(invoice.Id).Returns(new List<Order>());
            _invoiceItemService.Create(invoiceItems, invoice.Id).Returns(invoiceItems);
            _invoiceVariableService.CreateWithExistsVaribles(invoice.Id, invoice.Transaction.Id, user.Id, invoiceVariables).Returns(new List<InvoiceVariable>());

            // Act
            var result = await _service.CopyInvoice(invoice.Id, user);

            // Assert
            result.Should().Equals(result.Id != invoice.Id);
        }

        [TestMethod]
        public void ApprovingWithInvoiceIdAndUserNameShouldCommit()
        {
            // Arrange
            long invoiceId = 10;
            string userName = "ntaccount";

            _userService.GetByName(userName).Returns(new User() { Id = 10 });
            _invoiceRepository.QueryComplete().Returns(new List<Invoice>() {
                new Invoice() { Id = invoiceId }
            }.AsQueryable());

            _ruleValidator.Validate<CreateInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());
            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover() { Id = 1 });
            _invoiceApproverService.GetByInvoice(invoiceId).Returns(invoiceApprovers);
            
            // Act
            _service.Approve(invoiceId, userName);
            
            // Assert
            _invoiceRepository.ReceivedWithAnyArgs().Update(Arg.Any<Invoice>());
            _unitOfWork.Received().Commit();
        }

        [TestMethod]
        public void CopingInvoiceWithInvalidInvoiceIdReturnError()
        {
            // Arrange
            var user = new User
            {
                Id = 123,
                UserName = "TestUser"
            };

            //Act
            var invoiceIdWrong = 1818;
            Func<Task> result = async () => await _service.CopyInvoice(invoiceIdWrong, user);

            // Assert
            result.Should().Throw<NotFoundException<Invoice>>().And.Message.Should().Equals($"Invoice with id '{invoiceIdWrong}' was not found."); ;
        }

        public void RejectingWithInvoiceIdAndReasonIdAndUserNameShouldCommit()
        {
            // Arrange
            long invoiceId = 10;
            int reasonId = 15;
            string userName = "ntaccount";

            _userService.GetByName(userName).Returns(new User() { Id = 10 });
            _invoiceRepository.QueryComplete().Returns(new List<Invoice>() {
                new Invoice() { Id = invoiceId }
            }.AsQueryable());

            _ruleValidator.Validate<RejectInvoiceValidator>(Arg.Any<Invoice>(), true).Returns(new FluentValidation.Results.ValidationResult());

            var invoiceApprovers = new List<InvoiceApprover>();
            invoiceApprovers.Add(new InvoiceApprover()
            {
                Id = 1,
                Situation = ApproverStatusType.Analyzing
            });
            _invoiceApproverService.GetByInvoice(invoiceId).Returns(invoiceApprovers);

            User effectiveUser = new User() { Id = 20, UserName = userName };
            _userService.GetByName(userName).Returns(effectiveUser);

            var approvalView = new List<ApprovalView>() { new ApprovalView() { InvoiceId = (int)invoiceId } };
            _approverViewService.Search(userName).Returns(approvalView.AsQueryable());

            // Act
            _service.Reject(invoiceId, reasonId, userName);

            // Assert
            _invoiceRepository.ReceivedWithAnyArgs().Update(Arg.Any<Invoice>());
            _unitOfWork.Received().Commit();
        }
    }
}
