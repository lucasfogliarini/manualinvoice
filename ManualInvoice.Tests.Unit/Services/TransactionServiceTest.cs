﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class TransactionServiceTest
    {
        private IUnitOfWork _unitOfWork;
        private IRuleValidator _ruleValidator;
        private ITransactionRepository _transactionRepository;
        private ITransactionBranchRepository _transactionBranchRepository;
        private ITransactionLevelRepository _transactionLevelRepository;
        private ITransactionVariableRepository _transactionVariableRepository;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _ruleValidator = Substitute.For<IRuleValidator>();
            _transactionRepository = Substitute.For<ITransactionRepository>();
            _transactionBranchRepository = Substitute.For<ITransactionBranchRepository>();
            _transactionLevelRepository = Substitute.For<ITransactionLevelRepository>();
            _transactionVariableRepository = Substitute.For<ITransactionVariableRepository>();
        }

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllTransactions()
        {
            // Arrange
            ITransactionService service = new TransactionService(
                _unitOfWork, _ruleValidator, _transactionRepository, _transactionBranchRepository,
                _transactionLevelRepository, _transactionVariableRepository);

            var transactionList = new List<Transaction>();

            transactionList.Add(new Transaction(1, "ABC", TransactionType.Inbound));
            transactionList.Add(new Transaction(2, "DEF", TransactionType.Inbound));
            transactionList.Add(new Transaction(3, "GHI", TransactionType.Inbound));
            transactionList.Add(new Transaction(4, "JKL", TransactionType.Inbound));

            _transactionRepository.QuerySearch().Returns(transactionList.AsQueryable());

            // Act
            var result = service.Search();

            _transactionRepository.DidNotReceive().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Count().Should().Be(4);
                result.Should().NotBeNull();
            };
        }

        [TestMethod]
        public void GettingByIdWithExistingIdShouldReturnTransaction()
        {
            // Arrange
            var expectedId = 1;

            ITransactionService service = new TransactionService(
                _unitOfWork, _ruleValidator, _transactionRepository, _transactionBranchRepository,
                _transactionLevelRepository, _transactionVariableRepository);

            var transaction = new Transaction(expectedId, "ABC", TransactionType.Inbound);

            _transactionRepository.QueryComplete().Returns(new Transaction[] { transaction }.AsQueryable());

            // Act
            var result = service.Get(expectedId);

            _transactionRepository.Received().QueryComplete();

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Id.Should().Be(expectedId);
                result.Should().NotBeNull();
            };
        }

        [TestMethod]
        public void GettingByIdWithNotExistingIdShouldReturnNull()
        {
            // Arrange
            ITransactionService service = new TransactionService(
                _unitOfWork, _ruleValidator, _transactionRepository, _transactionBranchRepository,
                _transactionLevelRepository, _transactionVariableRepository);

            var transaction = new Transaction(1, "ABC", TransactionType.Inbound);

            _transactionRepository.QueryComplete().Returns(new Transaction[] { transaction }.AsQueryable());

            // Act
            var result = service.Get(12345);

            _transactionRepository.Received().QueryComplete();
            _transactionRepository.DidNotReceive().QuerySearch();

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GettingByIdWithExistingRepeatedIdShouldReturnException()
        {
            // Arrange
            var id = 1;

            ITransactionService service = new TransactionService(
                _unitOfWork, _ruleValidator, _transactionRepository, _transactionBranchRepository,
                _transactionLevelRepository, _transactionVariableRepository);

            var transactionList = new List<Transaction>();

            transactionList.Add(new Transaction(1, "ABC", TransactionType.Inbound));
            transactionList.Add(new Transaction(1, "ABC", TransactionType.Inbound)); // Repeated

            _transactionRepository.QueryComplete().Returns(transactionList.AsQueryable());

            // Assert
            Assert.ThrowsException<InvalidOperationException>(() => service.Get(id));
        }
    }
}
