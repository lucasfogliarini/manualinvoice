﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Unit.Services
{
    [TestClass]
    public class IssuerServiceTests
    {
        private IIssuerService _issuerService;
        private ICustomerApiGateway _customerApiGateway;

        [TestInitialize]
        public void Initialize()
        {
            _customerApiGateway = Substitute.For<ICustomerApiGateway>();
            _issuerService = new IssuerService(_customerApiGateway);
        }

        [TestMethod]
        public async Task GettingByPfjCodeWithSpecificPfjCodeShouldReturnIssuerDto()
        {
            //Arrange
            var pfjCode = "90389610000111";
            var customerDto = CustomerDtoMock.CreateCustomerDto(pfjCode, false, true, true);
            
            _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync("90389610000111").Returns(customerDto);

            //Act
            var result = await _issuerService.GetByPfjCodeAsync(pfjCode);

            //Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.PfjCode.Should().Be(pfjCode);
            };
        }

        [TestMethod]
        public async Task GettingByPfjCodeWithNotExistentPfjCodeShouldReturnIssuerDto()
        {
            //Arrange
            var pfjCode = "90389610000113";
            var notExistingPfjCode = "0000";
            var customerDto = CustomerDtoMock.CreateCustomerDto(pfjCode, false, true, true);

            _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(pfjCode).Returns(customerDto);

            //Act
            var result = await _issuerService.GetByPfjCodeAsync(notExistingPfjCode);

            //Assert
            Assert.IsNull(result);
        }
    }
}
