﻿using FluentAssertions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Unit.Services
{
    [TestClass]
    public class LoadVolumeServiceTests
    {
        private IInvoiceApiGateway _invoiceApiGateway;
        private ILoadVolumeService _service;

        [TestInitialize]
        public void Initialize()
        {
            _invoiceApiGateway = Substitute.For<IInvoiceApiGateway>();
            _service = new LoadVolumeService(_invoiceApiGateway);
        }
        [TestMethod]
        public async Task CreatingLoadVolumeWithInvalidDataShouldReturnFalse()
        {
            // Arrange
            var loadVolume = new LoadVolumeDto
            {
                InvoiceSequence = 02,
                SiteCode = 001222,
                BrandVolumes = "10",
                NumberVolumes = "10",
                QuantiyVolumes = 1,
                EspecieVolumes = "BOX"
            };

            _invoiceApiGateway.CreateLoadVolume(loadVolume).Returns(false);

            // Act
            var result = await _invoiceApiGateway.CreateLoadVolume(loadVolume);

            // Assert
            await _invoiceApiGateway.Received().CreateLoadVolume(loadVolume);
            result.Should().BeFalse();
        }

        [TestMethod]
        public async Task CreatingLoadVolumeWithValidDataShouldReturnTrue()
        {
            // Arrange
            var loadVolume = new LoadVolumeDto
            {
                InvoiceSequence = 123456789,
                SiteCode = 001,
                BrandVolumes = "10",
                NumberVolumes = "10",
                QuantiyVolumes = 1,
                EspecieVolumes = "BOX"
            };

            _invoiceApiGateway.CreateLoadVolume(loadVolume).Returns(true);

            // Act
            var result = await _invoiceApiGateway.CreateLoadVolume(loadVolume);

            // Assert
            await _invoiceApiGateway.Received().CreateLoadVolume(loadVolume);
            result.Should().BeTrue();
        }
    }
}
