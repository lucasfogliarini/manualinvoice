﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class UserServiceTests
    {
        private ISecurityApiService _securityApiService;
        private IUserRepository _userRepository;
        private IUnitOfWork _unitOfWork;
        private IRuleValidator _ruleValidator;
        private IUserService _userService;
        private ILevelUserRepository _levelUserRepository;

        [TestInitialize]
        public void Initialize()
        {
            _securityApiService = Substitute.For<ISecurityApiService>();
            _userRepository = Substitute.For<IUserRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _ruleValidator = Substitute.For<IRuleValidator>();
            _levelUserRepository  = Substitute.For<ILevelUserRepository>();
            _userService = new UserService(_securityApiService, _unitOfWork, _ruleValidator, _userRepository, _levelUserRepository);
        }

        [TestMethod]
        public void SearchingWithUsersShouldReturnUsers()
        {
            //arrange
            var users = new List<User>()
            {
                GetUser("user1", "level1"),
                GetUser("user2", "level2"),
                GetUser("user3", "level1")
            };
            _userRepository.QuerySearch().Returns(users.AsQueryable());

            // Act
            var returnUsers = _userService.Search();

            // Assert
            returnUsers.Should().NotBeNull();
        }

        [TestMethod]
        public void FindUserByExistingNameShouldReturnUser()
        {
            // Arrange
            var users = CreateUsers(5);
            var firstUser = users.FirstOrDefault();

            _userRepository.QuerySearch().Returns(users.AsQueryable());

            // Act
            var result = _userService.GetByName(firstUser.UserName);

            // Assert
            _userRepository.Received().QuerySearch();
            result.Should().NotBeNull().And.Be(firstUser);
        }

        [TestMethod]
        public void FindUserByNonexistentNameShouldReturnNull()
        {
            // Arrange
            var users = CreateUsers(5);
            _userRepository.QuerySearch().Returns(users.AsQueryable());

            // Act
            var result = _userService.GetByName(Guid.NewGuid().ToString());

            // Assert
            _userRepository.Received().QuerySearch();
            result.Should().BeNull();
        }

        [TestMethod]
        public void FindUserByNullValueNameShouldReturnNull()
        {
            // Arrange
            var users = CreateUsers(5);
            _userRepository.QuerySearch().Returns(users.AsQueryable());

            // Act
            var result = _userService.GetByName(null);

            // Assert
            _userRepository.DidNotReceive().QuerySearch();
            result.Should().BeNull();
        }

        private IList<User> CreateUsers(int size) 
        {
            var result = new List<User>();
            int count = 0;

            while (count < size)
            {
                User user = GetUser($"user{count + 1}", $"level{count + 1}");

                result.Add(user);
                count++;
            }

            return result;
        }

        private User GetUser(string userName, string level)
        {
            return new User()
            {
                UserName = userName,
                Email = userName + "@dell.com",
                LevelUsers = new List<LevelUser>()
                {
                    new LevelUser()
                    {
                        Level = new Level()
                        {
                            Name = level
                        }
                    }
                }
            };
        }
    }
}
