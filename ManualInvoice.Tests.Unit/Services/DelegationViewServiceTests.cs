﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class DelegationViewServiceTests
    {
        private IDelegationViewRepository _delegationViewRepository;
        private IUnitOfWork _unitOfWork;
        private IUserService _userService;
        private IDelegationViewService _service;

        [TestInitialize]
        public void Initialize()
        {
            _delegationViewRepository = Substitute.For<IDelegationViewRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();            
            _userService = Substitute.For<IUserService>();
            _service = new DelegationViewService(_unitOfWork, _delegationViewRepository, _userService);
        }        

        [TestMethod]
        public async Task GettingWithAdministratorAndCascadeDelegationShouldReturnDelegationAndCascade()
        {
            // Arrange
            var userName = "ntaccount";
            var userId = 1;
            _userService.GetByName(userName).Returns(new User() { Id = userId, UserName = userName });

            var date = DateTime.Now;
            DelegationView delegation = new DelegationView() { DelegatedUserId = userId, PeriodFrom = date, PeriodTo = date, IsCascade = true };
            _delegationViewRepository.QueryComplete().Returns(new DelegationView[] { delegation }.AsQueryable());
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto()
                        {
                            Id = "1",
                            Profiles = new string[] { "Administrator" }
                        }
                    }
                });

            // Act
            var result = await _service.GetDelegation(userName);

            // Assert
            _delegationViewRepository.Received().QueryComplete();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasDelegation);
            Assert.IsTrue(result.HasCascade);
        }

        [TestMethod]
        public async Task GettingWithApproverUserDelegatedShouldReturnDelegation()
        {
            // Arrange
            var userName = "ntaccount";
            var userId = 1;
            _userService.GetByName(userName).Returns(new User() { Id = userId, UserName = userName });

            var date = DateTime.Now;
            DelegationView delegation = new DelegationView() { DelegatedUserId = userId, PeriodFrom = date, PeriodTo = date };
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto()
                        {
                            Id = "1",
                            Profiles = new string[] { "Approver" }
                        }
                    }
                });
            _delegationViewRepository.QueryComplete().Returns(new DelegationView[] { delegation }.AsQueryable());

            // Act
            var result = await _service.GetDelegation(userName);

            // Assert
            _delegationViewRepository.Received().QueryComplete();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasDelegation);
            Assert.IsFalse(result.HasCascade);
        }

        [TestMethod]
        public async Task GettingWithStandardUserDelegatedShouldNotReturnDelegation()
        {
            // Arrange
            var userName = "ntaccount";
            var userId = 1;
            _userService.GetByName(userName).Returns(new User() { Id = userId, UserName = userName });

            var date = DateTime.Now;
            DelegationView delegation = new DelegationView() { DelegatedUserId = userId, PeriodFrom = date, PeriodTo = date };
            _userService.Search(Arg.Any<UserRequestDto>())
                .Returns(new UserResponseDto()
                {
                    Users = new List<UserResponseItemDto>()
                    {
                        new UserResponseItemDto()
                        {
                            Id = "1",
                            Profiles = new string[] { "Standard" }
                        }
                    }
                });
            _delegationViewRepository.QueryComplete().Returns(new DelegationView[] { delegation }.AsQueryable());

            // Act
            var result = await _service.GetDelegation(userName);

            // Assert
            _delegationViewRepository.Received().QueryComplete();
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasDelegation);
            Assert.IsFalse(result.HasCascade);
        }
    }
}
