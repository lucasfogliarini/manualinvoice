﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.SkuApi;
using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Unit.Services
{
    [TestClass]
    public class SkuServiceTests
    {
        private ISkuApiGateway _skuApiGateway;
        private ISkuFileRepository _skuFileRepository;
        private ISkuService _skuService;

        [TestInitialize]
        public void Initialize()
        {
            _skuApiGateway = Substitute.For<ISkuApiGateway>();
            _skuFileRepository = Substitute.For<ISkuFileRepository>();
            _skuService = new SkuService(_skuApiGateway, _skuFileRepository);
        }

        [TestMethod]
        public async Task GettingBySkuCodeWithExistingProductSkuCodeAndProcutInvoiceTypeShouldReturnSkuDto()
        {
            // Arrange
            var productSku = "313-1087";
            var invoiceType = InvoiceType.Product;

            var skuDto = new SkuDto { Code = productSku, Description = "Unidade de Disco Óptico" };

            _skuApiGateway.GetSkuByCode(productSku, invoiceType).Returns(skuDto);

            var shouldReturnExceptionWhenNull = true;
            // Act
            var result = await _skuService.GetBySkuCodeAsync(productSku, invoiceType, shouldReturnExceptionWhenNull);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Code.Should().Be(productSku);
            }
        }

        [TestMethod]
        public void GettingBySkuCodeWithExistingProductSkuCodeAndServiceInvoiceTypeShouldReturnEmptyDto()
        {
            // Arrange
            var productSku = "313-1087";
            var invoiceType = InvoiceType.Service;

            SkuDto skuDto = null;

            _skuApiGateway.GetSkuByCode(productSku, invoiceType).Returns(skuDto);

            var shouldReturnExceptionWhenNull = true;

            // Assert
            Assert.ThrowsExceptionAsync<NotFoundException<SkuDto>>(async () =>
            {
                await _skuService.GetBySkuCodeAsync(productSku, invoiceType, shouldReturnExceptionWhenNull);
            });
        }


        [TestMethod]
        public async Task GettingBySkuCodeWithExistingServiceSkuCodeAndServiceInvoiceTypeShouldReturnSkuDto()
        {
            // Arrange
            var serviceSku = "915-4265";
            var invoiceType = InvoiceType.Service;

            var skuDto = new SkuDto { Code = serviceSku, Description = "Suporte Técnico Gold" };

            _skuApiGateway.GetSkuByCode(serviceSku, invoiceType).Returns(skuDto);

            var shouldReturnExceptionWhenNull = true;

            // Act
            var result = await _skuService.GetBySkuCodeAsync(serviceSku, invoiceType, shouldReturnExceptionWhenNull);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.Code.Should().Be(serviceSku);
            }
        }

        [TestMethod]
        public void GettingBySkuCodeWithExistingServiceSkuCodeAndProductInvoiceTypeShouldReturnEmptyDto()
        {
            // Arrange
            var serviceSku = "01.07";
            var invoiceType = InvoiceType.Product;

            SkuDto skuDto = null;

            _skuApiGateway.GetSkuByCode(serviceSku, invoiceType).Returns(skuDto);

            var shouldReturnExceptionWhenNull = true;

            // Assert
            Assert.ThrowsExceptionAsync<NotFoundException<SkuDto>>(async () =>
            {
                await _skuService.GetBySkuCodeAsync(serviceSku, invoiceType, shouldReturnExceptionWhenNull);
            });
        }

        [TestMethod]
        public async Task GettingCityCodeBySkuCodeWithSkuCodeShouldReturnCityCode()
        {
            // Arrange
            var skuCode = "01.07";
            var cityCode = "123456789";

            _skuApiGateway.GetCityCodeBySku(skuCode).Returns(cityCode);

            // Act
            var result = await _skuService.GetCityCodeBySkuAsync(skuCode);

            // Assert
            Assert.AreEqual(cityCode, result);
        }

        [TestMethod]
        public void GettingCityCodeBySkuCodeWithSkuCodeShouldThrowExceptionWhenReturnIsNull()
        {
            // Arrange
            var skuCode = "01.07";
            var cityCode = "123456789";

            _skuApiGateway.GetCityCodeBySku(skuCode).Returns(cityCode);

            // Assert
            Assert.ThrowsExceptionAsync<NotFoundException<string>>(async () =>
            {
                await _skuService.GetCityCodeBySkuAsync(skuCode);
            });
        }
    }
}
