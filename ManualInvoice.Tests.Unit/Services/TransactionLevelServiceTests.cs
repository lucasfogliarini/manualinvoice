﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class TransactionLevelServiceTests
    {
        private IUnitOfWork _unitOfWork;
        private ITransactionLevelService _service;
        private ITransactionLevelRepository _repository;

        [TestInitialize]
        public void Initialize()
        {
            _repository = Substitute.For<ITransactionLevelRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _service = new TransactionLevelService(_unitOfWork, _repository);
        }

        [TestMethod]
        public void GettingActiveLevelsByTransactionWithValidTransactionIdShouldReturnAnyTransactionLevel()
        {
            // Arrange
            int transactionId = 10;
            _repository.QueryComplete().Returns(new List<TransactionLevel>() {
                new TransactionLevel() {
                    Transaction = new Transaction() { Id = transactionId},
                    Level = new Level() { IsActive = true}
                }
            }.AsQueryable());

            // Act
            var result = _service.GetActivesLevelsByTransaction(transactionId);

            // Assert
            result.Should().NotBeEmpty();
        }

        [TestMethod]
        public void GettingActiveLevelsByTransactionWithInalidTransactionIdShouldReturnEmptyTransactionLevel()
        {
            // Arrange
            int transactionId = 0;
            _repository.QueryComplete().Returns(new List<TransactionLevel>() {
                new TransactionLevel() {
                    Transaction = new Transaction() { Id = 10},
                    Level = new Level() { IsActive = true}
                }
            }.AsQueryable());

            // Act
            var result = _service.GetActivesLevelsByTransaction(transactionId);
            var result2 = _service.GetActivesLevelsByTransaction(20);

            // Assert
            result.Should().BeEmpty();
            result2.Should().BeEmpty();
        }

        [TestMethod]
        public void GettingByLevelsWithLevelIdsShouldTransactionLevels()
        {
            // Arrange
            int[] levelIds = { 10, 20, 30 };
            _repository.QueryComplete().Returns(new List<TransactionLevel>() {
                new TransactionLevel() { Level = new Level() { Id = levelIds[0] }},
                new TransactionLevel() { Level = new Level() { Id = levelIds[1] }},
                new TransactionLevel() { Level = new Level() { Id = levelIds[2] }}
            }.AsQueryable());

            // Act
            var result = _service.GetByLevels(levelIds);

            // Assert
            result.Should().NotBeEmpty();
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow(new int[] { })]
        public void GettingByLevelsWithoutLevelIdsShouldReturnEmpty(int[] levelIds)
        {
            // Act
            var result = _service.GetByLevels(levelIds);

            // Assert
            result.Should().BeEmpty();
            _repository.DidNotReceive();
        }
    }
}
