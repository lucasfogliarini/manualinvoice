﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class DofConsolidationServiceTests
    {
        private IDirectApiGateway _directApiGateway;
        private IDofConsolidationService _service;

        [TestInitialize]
        public void Initialize()
        {
            _directApiGateway = Substitute.For<IDirectApiGateway>();
            _service = new DofConsolidationService(_directApiGateway);
        }

        [TestMethod]
        public async Task CreattingOnDirectWithoutOrdersShouldReturnEmptyList()
        {
            // Arrange
            IList<Order> orders = new List<Order>();
            IList<long> dofs = new List<long> { 10, 20 };
            long invoiceId = 203040;

            // Act
            var result = await _service.CreateOnDirect(invoiceId, orders, dofs);

            // Assert
            await _directApiGateway.DidNotReceiveWithAnyArgs().CreateAsync(default);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public async Task CreattingOnDirectWithoutDofsShouldReturnEmptyList()
        {
            // Arrange
            IList<Order> orders = new List<Order> { new Order(), new Order() };
            IList<long> dofs = new List<long>();
            long invoiceId = 203040;

            // Act
            var result = await _service.CreateOnDirect(invoiceId, orders, dofs);

            // Assert
            await _directApiGateway.DidNotReceiveWithAnyArgs().CreateAsync(default);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public async Task CreattingOnDirectWithValidValuesShouldReturnDofConsolidationsCreateds()
        {
            // Arrange
            IList<Order> orders = new List<Order>
            {
                new Order{ OriginalDofId = 102030, RecofNumOrder = "ABC" },
                new Order{ OriginalDofId = 405060, RecofNumOrder = "DEF" },
            };
            IList<long> dofs = new List<long>() { 20, 30 };
            IList<ResponseDofConsolidationCreatedDto> dofConsolidationCreatedDtos = new List<ResponseDofConsolidationCreatedDto>
            {
                new ResponseDofConsolidationCreatedDto { Id = Guid.NewGuid() },
                new ResponseDofConsolidationCreatedDto { Id = Guid.NewGuid() },
                new ResponseDofConsolidationCreatedDto { Id = Guid.NewGuid() },
                new ResponseDofConsolidationCreatedDto { Id = Guid.NewGuid() }
            };
            long invoiceId = 203040;
            _directApiGateway.CreateAsync(Arg.Any<IList<CreateDofConsolidationDto>>()).Returns(dofConsolidationCreatedDtos);

            // Act
            var result = await _service.CreateOnDirect(invoiceId, orders, dofs);

            // Assert
            await _directApiGateway.ReceivedWithAnyArgs().CreateAsync(default);
            result.Should().HaveCount(dofConsolidationCreatedDtos.Count);
        }
    }
}
