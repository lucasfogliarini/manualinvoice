﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Unit.Services
{
    [TestClass]
    public class CarrierServiceTests
    {
        private IUnitOfWork _unitOfWork = Substitute.For<IUnitOfWork>();
        [TestInitialize]
        public void Initialize()
        {
        }
        [TestMethod]
        public void CreateCarrierMock()
        {
            //Arrange
            long id = 10;
            Carrier carrier = CarrierMock.CreateFullCarrier(id);
            //Assert
            Assert.IsNotNull(carrier);
        }
        [TestMethod]
        public void ShouldReturnNewCarrier()
        {
            long id = 10;
            Carrier carrier = CarrierMock.CreateCarrierOnlyWithID(id);
            ICarrierRepository _repository = Substitute.For<ICarrierRepository>();
            ICarrierService service = new CarrierService(_unitOfWork, _repository);
            Carrier result = service.Create(carrier);
            _repository.Received().Add(carrier);
            _unitOfWork.Received().Commit();
            // Assert
            using (var scope = new AssertionScope())
            {
                result.Id.Should().Be(id);
                result.Should().NotBeNull();
            };
        }
        [TestMethod]
        public void PuttingCarrierShouldReturnUpdatedCarrier()
        {
            long id = 10;
            Carrier carrier = CarrierMock.CreateFullCarrier(id);
            ICarrierRepository _repository = Substitute.For<ICarrierRepository>();
            ICarrierService service = new CarrierService(_unitOfWork, _repository);
            Carrier result = service.Update(carrier);
            _repository.Received().Update(carrier);
            _unitOfWork.Received().Commit();
            // Assert
            using (var scope = new AssertionScope())
            {
                result.Id.Should().Be(id);
                result.Should().NotBeNull();
            };
        }
    }
}