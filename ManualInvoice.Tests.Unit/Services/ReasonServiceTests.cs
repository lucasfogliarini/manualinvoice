﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public class ReasonServiceTests
    {
        private IRuleValidator m_ruleValidator;

        [TestInitialize]
        public void Initialize()
        {
            m_ruleValidator = new RuleValidator(Substitute.For<ILogger<RuleValidator>>());
        }

        [TestMethod]
        public void CreatingWithReasonShouldReasonCreated()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Name = "a", Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            Reason result = service.Create(reason);

            reasonRepository.Received().Add(Arg.Is(reason));
            unitOfWork.Received().Commit();

            Assert.IsNotNull(reason);
        }

        [TestMethod]
        public void CreatingWithReasonWithMissingAttributesShouldThrowsException()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            // Name is required
            Reason reason = new Reason { Name = null, Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Create(reason);
            });

            // Description is required
            reason = new Reason { Name = "a", Description = null, ReasonType = ReasonType.Request, IsActive = true };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Create(reason);
            });
        }

        [TestMethod]
        public void CreatingWithReasonWithIdShouldThrowsException()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 1, Name = "a", Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            // Create operation should not have an id specified
            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Create(reason);
            });
        }

        [TestMethod]
        public void UpdatingWithReasonShouldReasonUpdated()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 1, Name = "a", Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            Reason result = service.Update(reason);

            reasonRepository.Received().Update(Arg.Is(reason));
            unitOfWork.Received().Commit();

            Assert.IsNotNull(reason);
        }

        [TestMethod]
        public void UpdatingWithReasonWithMissingAttributesShouldThrowsException()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 0, Name = "a", Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Update(reason);
            });

            reason = new Reason { Id = 1, Name = null, Description = "b", ReasonType = ReasonType.Request, IsActive = true };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Update(reason);
            });

            reason = new Reason { Id = 1, Name = "a", Description = null, ReasonType = ReasonType.Request, IsActive = true };

            Assert.ThrowsException<ValidationException>(() =>
            {
                Reason result = service.Update(reason);
            });
        }

        [TestMethod]
        public void DeletingWithReasonShouldReasonDeleted()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 2 };

            bool result = service.Delete(reason);

            reasonRepository.Received().Delete(Arg.Is<Reason>(x => x.Id == 2));

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GettingWithExistingIdShouldReasonReturned()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 2 };

            reasonRepository.QueryComplete().Returns(new Reason[] { reason }.AsQueryable());

            var result = service.Get(2);

            reasonRepository.Received().QueryComplete();
            reasonRepository.DidNotReceive().QuerySearch();

            Assert.IsNotNull(result);

            Assert.AreEqual(reason, result);
        }

        [TestMethod]
        public void GettingWithUnknownIdShouldReturnNull()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason = new Reason { Id = 2 };

            reasonRepository.QueryComplete().Returns(new Reason[] { reason }.AsQueryable());

            // Act and Assert
            Assert.ThrowsException<NotFoundException<Reason>>(() =>
            {
                service.Get(1);
            });

            reasonRepository.Received().QueryComplete();
            reasonRepository.DidNotReceive().QuerySearch();
        }

        [TestMethod]
        public void SearchingWithNoFilterShouldReturnAllReasons()
        {
            var reasonRepository = Substitute.For<IReasonRepository>();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            IReasonService service = new ReasonService(unitOfWork, m_ruleValidator, reasonRepository);

            Reason reason2 = new Reason { Id = 2, Name = "a", Description = "b", ReasonType = ReasonType.Request, IsActive = true };
            Reason reason3 = new Reason { Id = 3, Name = "c", Description = "d", ReasonType = ReasonType.Reject, IsActive = true };
            Reason reason4 = new Reason { Id = 4, Name = "e", Description = "f", ReasonType = ReasonType.Request, IsActive = false };

            reasonRepository.QuerySearch().Returns(new Reason[] { reason2, reason3, reason4 }.AsQueryable());

            var result = service.Search();

            reasonRepository.DidNotReceive().QueryComplete();
            reasonRepository.Received().QuerySearch();

            Assert.IsNotNull(result);

            Assert.AreEqual(3, result.Count());
            Assert.IsTrue(result.Contains(reason2));
            Assert.IsTrue(result.Contains(reason3));
            Assert.IsTrue(result.Contains(reason4));
        }
    }
}
