﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Unit.Services
{
    [TestClass]
    public class NopServiceTests
    {
        private INopService _nopService;
        private IInvoiceApiGateway _invoiceApiGateway;

        [TestInitialize]
        public void Initialize()
        {
            _invoiceApiGateway = Substitute.For<IInvoiceApiGateway>();
            _nopService = new NopService(_invoiceApiGateway);
        }

        [TestMethod]
        public async Task GettingByNopCodeWithSpecificNopCodeShouldReturnNopDto()
        {
            // Arrange
            var nopCode = "E02.91";
            var nopDto = new NopDto { NopCode = nopCode, NopType = NopType.Product };

            _invoiceApiGateway.GetNopByCode(nopCode).Returns(nopDto);

            // Act
            var result = await _invoiceApiGateway.GetNopByCode(nopCode);

            // Assert
            using(var scoper = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.NopCode.Should().Be(nopCode);
            }
        }

        [TestMethod]
        public async Task GettingByNopCodeWithNotExistingNopCodeShouldReturnEmptyNopDto()
        {
            // Arrange
            var nopCode = "E02.91";
            var notExistingNopCode = "A";
            var nopDto = new NopDto { NopCode = nopCode, NopType = NopType.Product };

            _invoiceApiGateway.GetNopByCode(nopCode).Returns(nopDto);

            // Act
            var result = await _invoiceApiGateway.GetNopByCode(notExistingNopCode);

            // Assert
            Assert.IsNull(result);
        }

    }
}
