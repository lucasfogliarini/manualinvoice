﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    [TestClass]
    public sealed class InvoiceVariableServiceTests
    {
        private IUnitOfWork _unitOfWork;
        private IInvoiceVariableService _service;
        private IInvoiceVariableRepository _repository;
        private ITransactionVariableService _transactionVariableService;
        private IFederativeUnitService _federativeUnitService;
        private IPaymentCodeService _paymentCodeService;

        [TestInitialize]
        public void Initialize()
        {
            _repository = Substitute.For<IInvoiceVariableRepository>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _service = Substitute.For<IInvoiceVariableService>();
            _transactionVariableService = Substitute.For<ITransactionVariableService>();
            _federativeUnitService = Substitute.For<IFederativeUnitService>();
            _paymentCodeService = Substitute.For<IPaymentCodeService>();
            _service = new InvoiceVariableService(_unitOfWork, _repository, _transactionVariableService, _federativeUnitService, _paymentCodeService);
        }

        [TestMethod]
        public void CreatingWithTransactionWithoutAnyVariablesShouldReturnEmptyList()
        {
            // Arrange
            int transactionId = 102030;
            int invoiceId = 101010;
            int userId = 3020;
            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>();
            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.Create(invoiceId, transactionId, userId);

            // Assert
            _repository.DidNotReceive().Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.DidNotReceive().Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void CreatingWithTransactionThatHasTwoVariablesShouldReturnTwoInvoiceVariablesCreated()
        {
            // Arrange
            int transactionId = 102030;
            int invoiceId = 101010;
            int userId = 3020;

            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>()
            {
                CreateTransactionVariable(transactionId),
                CreateTransactionVariable(transactionId)
            };

            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.Create(invoiceId, transactionId, userId);

            // Assert 
            _repository.Received(2).Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(2).Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);
            result.Should().HaveCount(transactionVariables.Count());
        }


        [TestMethod]
        public void CreatingWithExistsVariblesThatHasTwoVariablesShouldReturnTwoInvoiceVariablesCreated()
        {
            // Arrange
            int transactionId = 1234567;
            int invoiceId = 7654321;
            int userId = 6789;

            InvoiceVariable invoiceVariable = new InvoiceVariable(invoiceId, 123, false, userId, "1515");
            InvoiceVariable invoiceVariableTwo = new InvoiceVariable(invoiceId, 123, false, userId, "1515");
            IList<InvoiceVariable> invoiceVariables = new List<InvoiceVariable>
            {
                invoiceVariable,
                invoiceVariableTwo
            };

            InvoiceVariable invoiceVariableOld = new InvoiceVariable(1111, 123, false, 12345, "1515");
            InvoiceVariable invoiceVariableOldTwo = new InvoiceVariable(1111, 123, false, 12345, "1515");
            IList<InvoiceVariable> invoiceVariablesOld = new List<InvoiceVariable>
            {
                invoiceVariableOld,
                invoiceVariableOldTwo
            };

            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>()
            {
                CreateTransactionVariable(transactionId),
                CreateTransactionVariable(transactionId)
            };

            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.CreateWithExistsVaribles(invoiceId, transactionId, userId, invoiceVariablesOld);

            // Assert 
            _repository.Received(2).Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(2).Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);

            using (var scope = new AssertionScope())
            {
                result.Should().HaveCount(invoiceVariables.Count());
                result.Should().Equals(result.FirstOrDefault().Invoice.Id == invoiceId);
            }
        }

        [TestMethod]
        public void CreatingWithExistsVariblesThatHasTwoVariablesShouldReturnThreeVariablesCreated()
        {
            // Arrange
            int transactionId = 1234567;
            int invoiceId = 7654321;
            int userId = 6789;

            InvoiceVariable invoiceVariableOld = new InvoiceVariable(1111, 123, false, 12345, "1515");
            InvoiceVariable invoiceVariableOldTwo = new InvoiceVariable(1111, 123, false, 12345, "1515");
            IList<InvoiceVariable> invoiceVariablesOld = new List<InvoiceVariable>
            {
                invoiceVariableOld,
                invoiceVariableOldTwo
            };

            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>()
            {
                CreateTransactionVariable(transactionId),
                CreateTransactionVariable(transactionId),
                CreateTransactionVariable(transactionId)
            };

            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.CreateWithExistsVaribles(invoiceId, transactionId, userId, invoiceVariablesOld);

            // Assert 
            _repository.Received(3).Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(3).Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);
            result.Should().HaveCount(3);
        }

        [TestMethod]
        public void CreatingWithExistsVariblesThatHasTwoVariablesShouldReturnOneVariablesCreated()
        {
            // Arrange
            int transactionId = 1234567;
            int invoiceId = 7654321;
            int userId = 6789;

            InvoiceVariable invoiceVariableOld = new InvoiceVariable(1111, 123, false, 12345, "1515");
            InvoiceVariable invoiceVariableOldTwo = new InvoiceVariable(1111, 123, false, 12345, "1515");
            IList<InvoiceVariable> invoiceVariablesOld = new List<InvoiceVariable>
            {
                invoiceVariableOld,
                invoiceVariableOldTwo
            };

            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>()
            {
                CreateTransactionVariable(transactionId)
            };

            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.CreateWithExistsVaribles(invoiceId, transactionId, userId, invoiceVariablesOld);

            // Assert 
            _repository.Received(1).Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(1).Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);
            result.Should().HaveCount(1);
        }

        [TestMethod]
        public void CreatingWithExistsVariblesThatHasTwoVariablesShouldNotReturnVariablesCreated()
        {
            // Arrange
            int transactionId = 1234567;
            int invoiceId = 7654321;
            int userId = 6789;

            InvoiceVariable invoiceVariableOld = new InvoiceVariable(1111, 123, false, 12345, "1515");
            InvoiceVariable invoiceVariableOldTwo = new InvoiceVariable(1111, 123, false, 12345, "1515");
            IList<InvoiceVariable> invoiceVariablesOld = new List<InvoiceVariable>
            {
                invoiceVariableOld,
                invoiceVariableOldTwo
            };

            IEnumerable<TransactionVariable> transactionVariables = new List<TransactionVariable>();

            _transactionVariableService.GetActivesVariablesByTransaction(transactionId)
                .Returns(transactionVariables);

            // Act
            var result = _service.CreateWithExistsVaribles(invoiceId, transactionId, userId, invoiceVariablesOld);

            // Assert 
            _repository.Received(0).Add(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(0).Commit();
            _transactionVariableService.Received().GetActivesVariablesByTransaction(transactionId);
            result.Should().HaveCount(0);
        }

        [TestMethod]
        public async Task GettingByInvoiceAsyncWithInvoiceIdEqualsZeroShouldReturnEmptyList()
        {
            // Arrange
            int invoiceId = 0;

            // Act
            var result = await _service.GetByInvoiceAsync(invoiceId);

            // Assert
            _repository.DidNotReceive().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public async Task GettingByInvoiceAsyncWithoutFindingVariableForInvoiceShouldReturnEmptyList()
        {
            // Arrange
            int invoiceId = 102030;
            _repository.QueryComplete().Returns(new List<InvoiceVariable>().AsQueryable());

            // Act
            var result = await _service.GetByInvoiceAsync(invoiceId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public async Task GettingByInvoiceAsyncWithInvoiceIdContainingTwoVariablesShouldReturnTwoInvoiceVariables()
        {
            // Arrange
            int invoiceId = 102030;
            var invoiceVariables = new List<InvoiceVariable>();
            invoiceVariables.Add(
                new InvoiceVariable()
                {
                    Invoice = new Invoice { Id = invoiceId },
                    Variable = new Variable()
                });
            invoiceVariables.Add(
                new InvoiceVariable()
                {
                    Invoice = new Invoice { Id = invoiceId },
                    Variable = new Variable()
                });
            _repository.QueryComplete().Returns(invoiceVariables.AsQueryable());

            // Act
            var result = await _service.GetByInvoiceAsync(invoiceId);

            // Assert
            _repository.Received().QueryComplete();
            result.Should().HaveCount(invoiceVariables.Count);
        }

        [TestMethod]
        public void UpdatingWithoutVariablesShouldEmptyList()
        {
            // Arrange
            var variables = new List<InvoiceVariable>();
            _repository.QuerySearch().Returns(new List<InvoiceVariable>().AsQueryable());

            // Act
            var result = _service.Update(variables);

            // Assert
            _repository.DidNotReceive().QuerySearch();
            _repository.DidNotReceive().Update(Arg.Any<InvoiceVariable>());
            _unitOfWork.DidNotReceive().Commit();
            result.Should().BeNullOrEmpty();

        }

        [TestMethod]
        public void UpdatingWithOneVariableShouldReturnOneVariableUpdated()
        {
            // Arrange
            var variableUpdate = new InvoiceVariable() { Id = 102030, Value = "newValue" };
            var variable = new InvoiceVariable() { Id = 102030, Value = "Value" };
            var variables = new List<InvoiceVariable>() { variableUpdate };
            _repository.QuerySearch().Returns(new List<InvoiceVariable>() { variable }.AsQueryable());

            // Act
            var result = _service.Update(variables);

            // Assert
            _repository.Received(variables.Count).Update(Arg.Any<InvoiceVariable>());
            _unitOfWork.Received(variables.Count).Commit();
            result.Should().HaveCount(variables.Count);
        }

        private TransactionVariable CreateTransactionVariable(int transactionId)
        {
            var result = new TransactionVariable();

            result.IsMandatory = true;
            result.Transaction = new Transaction() { Id = transactionId };
            result.Variable = new Variable()
            {
                IsActive = true,
                IsAlphanumeric = true,
                IsEditable = true,
                Length = 10,
                Name = "Teste"
            };

            return result;
        }
    }
}
