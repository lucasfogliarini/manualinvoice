﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Tests.Unit.Mocks
{
    public static class InvoiceMock
    {
        private const int _defaultId = 2030;

        public static Invoice CreateInvoice()
        {
            return CreateInvoice(_defaultId, _defaultId, _defaultId, _defaultId, _defaultId, _defaultId.ToString());
        }

        public static Invoice CreateInvoice(int id, int userId, int transactionId, int branchId, int reasonId, string billToCode, string billToNop = "NOP-Test")
        {
            var result = new Invoice();
            result.Id = id;
            result.User = new User(userId);
            result.Transaction = new Transaction() { Id = transactionId };
            result.Branch = new Branch() { Id = branchId };
            result.Reason = new Reason() { Id = reasonId };
            result.BillToCode = billToCode;
            result.TransactionBillToCode = billToNop;

            return result;
        }
    }
}
