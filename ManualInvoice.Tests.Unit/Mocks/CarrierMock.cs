﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;

namespace ManualInvoice.Tests.Unit.Mocks
{
    public static class CarrierMock
    {
        public static Carrier CreateFullCarrier(long id)
        {
            Carrier carrier = new Carrier()
            {
                Id = id,
                Code = "TATA",
                Name = "Transportadora TATA",
                VolumeQuantity = 1,
                VolumeBrand = "XX",
                VolumeNumbering = "YY",
                FreightValue = 2,
                InsuranceValue = 3,
                Weight = 10,
                Specie = "CX",
                OtherCosts = 100
            };
            return carrier;
        }
        public static Carrier CreateCarrierOnlyWithID(long id)
        {
            Carrier carrier = new Carrier()
            {
                Id = id
            };
            return carrier;
        }
    }
}
