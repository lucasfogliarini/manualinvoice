﻿using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;

namespace ManualInvoice.Tests.Unit.Mocks
{
    public static class FiscalDocumentsDtoMock
    {
        public static FiscalDocumentDto CreateFiscalDocumentsDto(long id)
        {
            FiscalDocumentDto fiscalDocumentsDto = CreateFiscalDocument(id);

            fiscalDocumentsDto.Items.Add(CreateFiscalItem(id));
            fiscalDocumentsDto.SalesOrder = CreateSalesOrder();
            fiscalDocumentsDto.SalesOrder.Items = new List<SalesItemDto>();
            fiscalDocumentsDto.SalesOrder.Items.Add(CreateSalesItem());

            return fiscalDocumentsDto;
        }

        private static FiscalDocumentDto CreateFiscalDocument(long id)
        {
            return new FiscalDocumentDto
            {
                Id = id,
                DellRecofNumOrder = "123",
                DestinatarioPfjCodigo = "321",
                InformanteEstCodigo = "DELL",
                SiteCode = 01,
                DofSequence = 123654,
                ValorTotalContabil = 123M,
                PesoBrutoKg = 123M,
                PesoLiquidoKg = 654M,
                ValorTotalFaturado = 987M,
                ValorTotalICMS = 456M,
                ValorTotalIPI = 159M,
                ValorTotalISS = 951M,
                ValorTotalCofins = 45M,
                ValorTotalSTF = 12M,
                ValorTotalSTT = 13M,
                GoodsFiscalAmount = 11M,
                OthersFiscalAmount = 19M,
                ProvisionOfServicesFiscalAmount = 11M,
                ServicesFiscalAmount = 10M
            };
        }

        private static SalesItemDto CreateSalesItem()
        {
            return new SalesItemDto
            {
                InvoiceSequence = 123456,
                ItemNumber = 1,
                SiteCode = 123
            };
        }

        private static SalesOrderDto CreateSalesOrder()
        {
            return new SalesOrderDto
            {
                InvoiceSequence = 123456
            };
        }

        private static FiscalDocumentItemDto CreateFiscalItem(long id)
        {
            var fiscalDocumentItemDto = new FiscalDocumentItemDto
            {
                Id = 321654,
                DofId = id,
                SiteCode = 01,
                DofSequence = 123789654,
                IdfNum = 1,
                BaseFlag = DellBaseFlagType.Base,
                Qty = 1,
                UnitPrice = 1234,
                NbmCodigo = "abc",
                MercCodigo = "123",
                ValorFiscal = 123M,
                ValorFaturado = 321M,
                ValorBaseIpi = 789M,
                ValorBaseStf = 987M,
                AliqStf = 159M,
                ValorBaseStt = 954M,
                AliqStt = 852M,
                ValorStt = 987M,
                ValorTributavelIcms = 3547M,
                ValorTributavelStf = 961M,
                ValorTributavelStt = 753M,
                ValorTributavelIpi = 147963M,
                ValorContabil = 456159M,
                TotalPrice = 123987
            };

            return fiscalDocumentItemDto;
        }
    }
}
