﻿using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;

namespace ManualInvoice.Tests.Unit.Mocks
{
    public static class CustomerDtoMock
    {

        public static CustomerDto CreateCustomerDto(string pfjCodigo, bool withAddress, bool hasCustomerValidVigency, bool hasLocationValidVigency)
        {
            CustomerDto customerDto = new CustomerDto()
            {
                Id = 1,
                Name = "Leonardo",
                Addresses = withAddress ? CreateCustomerAddressesList() : new List<CustomerAddressDto>(),
                CpfCgc = "123456789",
                PfjCode = pfjCodigo,
                HasCustomerValidVigency = hasCustomerValidVigency,
                HasLocationValidVigency = hasLocationValidVigency
            };

            return customerDto;
        }

        public static List<CustomerAddressDto> CreateCustomerAddressesList()
        {
            List<CustomerAddressDto> addressesList = new List<CustomerAddressDto>();

            CustomerAddressDto customerAddressDto = new CustomerAddressDto()
            {
                Id = 1,
                PublicPlace = "Av. Protasio Alves",
                Number = "123",
                CityName = "Porto Alegre",
                State = "RS"
            };

            addressesList.Add(customerAddressDto);
            return addressesList;
        }
    }
}
