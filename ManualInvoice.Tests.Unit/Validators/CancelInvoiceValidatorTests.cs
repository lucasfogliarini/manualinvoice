﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;

namespace ManualInvoice.Domain.Validators
{
    [TestClass]
    public class CancelInvoiceValidatorTests
    {
        private IRuleValidator _ruleValidator;
        private ILogger<RuleValidator> _logger;

        [TestInitialize]
        public void Initialize()
        {
            _logger = Substitute.For<ILogger<RuleValidator>>();
            _ruleValidator = new RuleValidator(_logger);
        }

        [TestMethod]
        public void ValidatingInvoiceWithCancelledStatusShouldThrowException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            invoice.Status = Common.InvoiceStatusType.Cancelled;

            string propertyName = "Status";
            string errorCode = "NotEqualValidator";

            // Act
            Action result = () => _ruleValidator.Validate<CancelInvoiceValidator>(invoice, true);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().Throw<ValidationException>().And.Errors.Should()
                    .Contain(x => x.PropertyName == propertyName && x.ErrorCode == errorCode
                               && x.ErrorMessage.Contains("The Invoice was already cancelled"));
            };
        }

        [TestMethod]
        public void ValidatingInvoiceWithoutAnalyzingStatusShouldThrowException()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            invoice.Status = Common.InvoiceStatusType.Draft;

            string propertyName = "Status";
            string errorCode = "EqualValidator";

            // Act
            Action result = () => _ruleValidator.Validate<CancelInvoiceValidator>(invoice, true);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().Throw<ValidationException>().And.Errors.Should()
                    .Contain(x => x.PropertyName == propertyName && x.ErrorCode == errorCode
                               && x.ErrorMessage.Contains("The Invoice should be in 'Analyzing' Status to be cancelled"));
            };
        }

        [TestMethod]
        public void ValidatingInvoiceWithValidConditionsShouldPass()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice();
            invoice.Status = Common.InvoiceStatusType.Analyzing;

            // Act
            Action result = () => _ruleValidator.Validate<CancelInvoiceValidator>(invoice, true);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotThrow();
            };
        }
    }
}
