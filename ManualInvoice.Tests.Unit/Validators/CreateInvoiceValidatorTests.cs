﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace ManualInvoice.Domain.Validators
{
    [TestClass]
    public class CreateInvoiceValidatorTests
    {
        private IRuleValidator _ruleValidator;
        private ILogger<RuleValidator> _logger;

        private const int _defaultId = 2030;
        private const string _notEmptyValidator = "NotEmptyValidator";
        private const string _emptyValidator = "EmptyValidator";

        [TestInitialize]
        public void Initialize()
        {
            _logger = Substitute.For<ILogger<RuleValidator>>();
            _ruleValidator = new RuleValidator(_logger);
        }

        [TestMethod]
        public void CreatingInvoiceWithIdShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(_defaultId, _defaultId, _defaultId, _defaultId, _defaultId, _defaultId.ToString());
            string propError = "Id";

            // Act
            var result = _ruleValidator.Validate<CreateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_emptyValidator));
            };
        }

        [TestMethod]
        public void CreatingInvoiceWithoutEnteringTransactionShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(0, _defaultId, 0, _defaultId, _defaultId, _defaultId.ToString());
            string propError = "Transaction.Id";

            // Act
            var result = _ruleValidator.Validate<CreateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }

        [TestMethod]
        public void CreatingInvoiceWithoutEnteringUserShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(0, 0, _defaultId, _defaultId, _defaultId, _defaultId.ToString());
            string propError = "User.Id";

            // Act
            var result = _ruleValidator.Validate<CreateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }
    }
}
