﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Tests.Unit.Mocks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace ManualInvoice.Domain.Validators
{
    [TestClass]
    public class UpdateInvoiceValidatorTests
    {
        private IRuleValidator _ruleValidator;
        private ILogger<RuleValidator> _logger;

        private const int _defaultId = 2030;
        private const string _notEmptyValidator = "NotEmptyValidator";

        [TestInitialize]
        public void Initialize()
        {
            _logger = Substitute.For<ILogger<RuleValidator>>();
            _ruleValidator = new RuleValidator(_logger);
        }

        [TestMethod]
        public void UpdatingInvoiceWithoutEnteringIdShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(0, _defaultId, _defaultId, _defaultId, _defaultId, _defaultId.ToString());
            string propError = "Id";

            // Act
            var result = _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }

        [TestMethod]
        public void UpdatingInvoiceWithoutEnteringBranchShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(_defaultId, _defaultId, _defaultId, 0, _defaultId, _defaultId.ToString());
            string propError = "Branch.Id";

            // Act
            var result = _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }

        [TestMethod]
        public void UpdatingInvoiceWithoutEnteringReasonShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(_defaultId, _defaultId, _defaultId, _defaultId, 0, _defaultId.ToString());
            string propError = "Reason.Id";

            // Act
            var result = _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }

        [TestMethod]
        public void UpdatingInvoiceWithoutEnteringBillToCodeShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(_defaultId, _defaultId, _defaultId, _defaultId, _defaultId, string.Empty);
            string propError = "BillToCode";

            // Act
            var result = _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }

        [TestMethod]
        public void UpdatingInvoiceWithoutEnteringBillToNopeShouldReturnError()
        {
            // Arrange
            var invoice = InvoiceMock.CreateInvoice(_defaultId, _defaultId, _defaultId, _defaultId, _defaultId, _defaultId.ToString(), string.Empty);
            string propError = "TransactionBillToCode";

            // Act
            var result = _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, false);

            // Assert
            using (var scope = new AssertionScope())
            {
                result.Should().NotBeNull();
                result.IsValid.Should().BeFalse();
                result.Errors.Should().NotBeNull().And.NotBeEmpty().And.Contain(x => x.PropertyName.Equals(propError) && x.ErrorCode.Equals(_notEmptyValidator));
            };
        }
    }
}
