﻿using FluentAssertions;
using FluentAssertions.Execution;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class SecurityApiGatewayTests : GatewayTestBase
    {
        private readonly ISecurityApiGateway _securityApiGateway;

        public SecurityApiGatewayTests()
        {
            _securityApiGateway = ServiceProvider.GetService<ISecurityApiGateway>();
        }

        [TestMethod]
        public async Task GettingUserAuthorizationAsyncWithSystemAndUserNameShouldReturnUserAuthorization()
        {
            //Arrange
            string system = "manualinvoice";
            string userName = $"{Environment.UserDomainName}\\{Environment.UserName}";

            //Act
            var userAuthorization = await _securityApiGateway.GetUserAuthorizationAsync(userName, system);

            //Assert
            using (var scope = new AssertionScope())
            {
                userAuthorization.Token.Should().NotBeNullOrEmpty();
                userAuthorization.Permissions.Should().NotBeNull();
            }
        }

        [TestMethod]
        public async Task GettingProfilesAsyncWithExistingProfilesShouldReturnProfiles()
        {
            //Act
            var profiles = await _securityApiGateway.GetProfilesAsync();

            //Assert
            profiles.Should().NotBeNull();
        }

        [TestMethod]
        public async Task GettingUsersAsyncWithExistingUsersShouldReturnUsers()
        {
            //arrange
            var userRequest = new UserRequestDto()
            {
                NTAccount = "Luc",
                OrderBy = "",
                ProfileName = "ManualInvoice - Administrator",
                Skip = 1,
                Take = 10
            };

            //Act
            var users = await _securityApiGateway.GetUsersAsync(userRequest);

            //Assert
            users.Should().NotBeNull();
        }
    }
}
