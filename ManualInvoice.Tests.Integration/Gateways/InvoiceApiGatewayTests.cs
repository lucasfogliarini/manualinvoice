﻿using FluentAssertions;
using ManualInvoice.Domain.Gateways;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class InvoiceApiGatewayTests : GatewayTestBase
    {
        private readonly IInvoiceApiGateway _invoiceApiGateway;

        public InvoiceApiGatewayTests()
        {
            _invoiceApiGateway = ServiceProvider.GetService<IInvoiceApiGateway>();
        }

        [TestMethod]
        public async Task GettingNopByCodeExistingNopCodeShouldReturnNop()
        {
            //Arrange
            string nopCode = "S01.01";

            //Act
            var nopDto = await _invoiceApiGateway.GetNopByCode(nopCode);

            //Assert
            nopDto.Should().NotBeNull();
        }
    }
}
