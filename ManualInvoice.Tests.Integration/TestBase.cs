﻿using ManualInvoice.Data;
using ManualInvoice.Domain.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace ManualInvoice.Tests.Integration
{
    public abstract class TestBase
    {
        protected ServiceProvider ServiceProvider { get; set; }

        protected AppSettings AddAppSettings(ServiceCollection services) 
        {
            var configurationRoot = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json", optional: true)
                .Build();

            var appSettings = configurationRoot.GetSection(nameof(AppSettings)).Get<AppSettings>();
            services.AddSingleton(appSettings);
            return appSettings;
        }
    }
}
