﻿using FluentAssertions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class DelegationServiceTests : ServiceTestBase
    {
        private readonly IDelegationService _deletationService;

        public DelegationServiceTests()
        {
            _deletationService = ServiceProvider.GetService<IDelegationService>();
        }

        [TestMethod]
        public void CreatingWithDelegationShouldDelegationCreated()
        {
            //Arrange
            var delegation = new Delegation()
            {
                PeriodFrom = DateTime.Now,
                PeriodTo = DateTime.Now.AddDays(1),
                DelegationDate = DateTime.Now,
                IsActive = true,
                IsCascade = true,
                IsOldApproval = true,
            };
            string[] delegationTo = { "lucas_pedroso1" };

            //Act
            var delegationCreated = _deletationService.Create(delegation, delegationTo);

            //Assert
            delegationCreated.Should().NotBeNull();
        }
    }
}
