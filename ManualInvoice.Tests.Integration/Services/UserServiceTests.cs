﻿using ManualInvoice.Domain.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class UserServiceTests : ServiceTestBase
    {
        private readonly IUserService _userService;

        public UserServiceTests()
        {
            _userService = ServiceProvider.GetService<IUserService>();
        }

        [TestMethod]
        public async Task ConfiguringLevelsWithLevelIdsShouldConfigure()
        {
            //Arrange
            string ntAccount = "americas\\lucas_pedroso1";
            int[] levelIds = { 2, 6, 8  };

            //Act
            await _userService.ConfigureLevelsAsync(ntAccount, levelIds);

            //Assert
            Assert.IsTrue(true);
        }
    }
}
