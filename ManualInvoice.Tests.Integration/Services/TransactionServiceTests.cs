﻿using FluentAssertions;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class TransactionServiceTests : ServiceTestBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionServiceTests()
        {
            _transactionService = ServiceProvider.GetService<ITransactionService>();
        }

        [TestMethod]
        public void SearchingWithFiltersShouldReturnTransactions()
        {
            //Arrange
            string transactionName = "BRSP1 - SERVICOS SAO PAULO";
            string billToCode = "S03.02";
            string level = "Faturamento-Serv-SP";
            string branch = "BRSP1";
            var transactionType = TransactionType.Outbound;
            bool isActive = true;


            //Act
            Expression<Func<Transaction, bool>> predicate = e =>
            e.Name.Contains(transactionName) &&
            e.BillToCode == billToCode &&
            e.TransactionType == transactionType &&
            e.IsActive == isActive &&
            e.TransactionLevels.Any(t => t.Level.Name == level) &&
            e.TransactionBranches.Any(t => t.Branch.Name == branch);

            var transactions = _transactionService.Search().Where(predicate)
            .Select(e => new Transaction()
            {
                Name = e.Name,
                TransactionLevels = e.TransactionLevels,
                TransactionBranches = e.TransactionBranches
            }).ToList();

            //Assert
            transactions.Should().HaveCount(1);
        }

        [TestMethod]
        public void CreatingWithSuccessShouldReturnCreatedTransaction()
        {
            //Arrange
            var transaction = new Transaction()
            {
                Name = "transaction1",
                BillToCode = "BillToCode",
                Observation = "Observation1",
                Serie = "Serie",
                ShipToCode = "ShipToCode",
                Specie = "Specie",
                TransactionType = TransactionType.Inbound,
                IsActive = true,
                TransactionBranches = new []
                {
                    new TransactionBranch()
                },
                TransactionLevels = new []
                {
                    new TransactionLevel()
                },
                TransactionVariables = new []
                {
                    new TransactionVariable()
                }
            };

            var transactionCreated = _transactionService.Create(transaction);

            //Assert
            transactionCreated.Should().NotBeNull();
        }

        [TestMethod]
        public void UpdatingWithSuccessShouldReturnUpdatedTransaction()
        {
            //Arrange
            var transaction = new Transaction()
            {
                Id = 1489,
                Name = "transaction1",
                BillToCode = "BillToCode",
                Observation = "Observation1",
                Serie = "Serie",
                ShipToCode = "ShipToCode",
                Specie = "Specie",
                TransactionType = TransactionType.Inbound,
                IsActive = true,
                TransactionBranches = new[]
                {
                    new TransactionBranch()
                },
                TransactionLevels = new[]
                {
                    new TransactionLevel()
                },
                TransactionVariables = new[]
                {
                    new TransactionVariable()
                }
            };

            var transactionUpdated = _transactionService.Update(transaction);

            //Assert
            transactionUpdated.Should().NotBeNull();
        }
    }
}
