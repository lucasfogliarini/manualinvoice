﻿using FluentAssertions;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace ManualInvoice.Tests.Integration
{
    [TestClass]
    public class NopServiceTests : ServiceTestBase
    {
        private readonly INopService _nopService;

        public NopServiceTests()
        {
            _nopService = ServiceProvider.GetService<INopService>();
        }

        [TestMethod]
        public async Task GettingByNopCodeExistingShouldReturnNop()
        {
            //Arrange
            string nopCode = "S01.01";

            //Act
            var nopDto = await _nopService.GetByNopCodeAsync(nopCode);

            //Assert
            nopDto.Should().NotBeNull();
        }
    }
}
