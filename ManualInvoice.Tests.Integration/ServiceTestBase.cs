﻿using ManualInvoice.Data;
using ManualInvoice.Domain;
using ManualInvoice.Domain.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace ManualInvoice.Tests.Integration
{
    public abstract class ServiceTestBase : TestBase
    {
        public ServiceTestBase()
        {
            ConfigureServiceCollection();
        }

        private void ConfigureServiceCollection()
        {
            var services = new ServiceCollection();
            var appSettings = AddAppSettings(services);
            services.AddServices();
            services.AddValidators();
            services.AddRepositories();
            services.AddGateways(appSettings);
            services.AddNFManualUnitOfWork(appSettings.NFManualConnectionString);
            ServiceProvider = services.BuildServiceProvider();
        }
    }
}
