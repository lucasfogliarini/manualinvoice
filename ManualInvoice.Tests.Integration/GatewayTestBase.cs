﻿using ManualInvoice.Data;
using Microsoft.Extensions.DependencyInjection;

namespace ManualInvoice.Tests.Integration
{
    public abstract class GatewayTestBase : TestBase
    {
        public GatewayTestBase()
        {
            ConfigureServiceCollection();
        }

        private void ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            var appSettings = AddAppSettings(serviceCollection);
            serviceCollection.AddGateways(appSettings);
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
    }
}
