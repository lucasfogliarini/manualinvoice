## Design
- API Framework _using_ [AspNetCore](https://www.nuget.org/packages/Microsoft.AspNetCore.App/)
- Dependency Injection Pattern _using_ [Microsoft.Extensions.DependencyInjection](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection.Abstractions/)
- ORM Pattern with Fluent _using_ [EntityFrameworkCore](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore/) and [Oracle.EntityFrameworkCore](https://www.nuget.org/packages/Oracle.EntityFrameworkCore/)
- [UnitOfWork Pattern](https://martinfowler.com/eaaCatalog/unitOfWork.html)
- Unit Tests _using_ [MSTest](https://www.nuget.org/packages/MSTest.TestFramework)
- Validation Pattern with Fluent _using_ [FluentValidation](https://www.nuget.org/packages/FluentValidation/)
- Logging _using_ [Microsoft.Extensions.Logging](https://www.nuget.org/packages/Microsoft.Extensions.Logging/) and [Seq.Extensions.Logging](https://www.nuget.org/packages/Seq.Extensions.Logging)
- PCF integration for configuration _using_ [Steeltoe.Extensions.Configuration.CloudFoundryCore](https://www.nuget.org/packages/Steeltoe.Extensions.Configuration.CloudFoundryCore/)
- API tool and documentation _using_ [Swashbuckle.AspNetCore](https://www.nuget.org/packages/Swashbuckle.AspNetCore/)

## Environments and Deployment
The __environments__ and __deployment configuration__ can be analyzed on 
[gitlab-ci.yml](https://gitlab.dell.com/finance/latam-finance-it/applications/synchro/manualinvoice/manualinvoiceapi/blob/master/.gitlab-ci.yml)
and [manifest.yml](https://gitlab.dell.com/finance/latam-finance-it/applications/synchro/manualinvoice/manualinvoiceapi/blob/master/src/manifest.yml)

- [dev1](https://manualinvoiceapi-dev1.ausvdc02.pcf.dell.com),
  [dev2](https://manualinvoiceapi-dev2.ausvdc02.pcf.dell.com)
- [dit1](https://manualinvoiceapi-dit1.ausvdc02.pcf.dell.com),
  [dit2](https://manualinvoiceapi-dit2.ausvdc02.pcf.dell.com)
- [ge1](https://manualinvoiceapi-ge1.ausvdc02.pcf.dell.com),
  [ge2](https://manualinvoiceapi-ge2.ausvdc02.pcf.dell.com),
  [ge3](https://manualinvoiceapi-ge3.ausvdc02.pcf.dell.com),
  [ge4](https://manualinvoiceapi-ge4.ausvdc02.pcf.dell.com)
- [perf](https://manualinvoiceapi-perf.ausvdc02.pcf.dell.com)

## Endpoints
The endpoints can be analyzed in the Swagger page.

e.g.  
https://manualinvoiceapi-dit1.ausvdc02.pcf.dell.com/swagger

## SonarQube
[Issues](http://ausdwsynapp02.aus.amer.dell.com:9000/project/issues?id=ManualInvoiceApi&resolved=false)  
[Coverage](http://ausdwsynapp02.aus.amer.dell.com:9000/component_measures?id=ManualInvoiceApi&metric=coverage)

## Fortify
[Fortify Pipeline Schedule](https://gitlab.dell.com/finance/latam-finance-it/applications/synchro/manualinvoice/manualinvoiceapi/pipeline_schedules)  
[Fortify App](https://fortify.dell.com/ssc/html/ssc/version/19054/overview/null/?filterSet=a243b195-0a59-3f8b-1403-d55b7a7d78e6) TODO MUST CHECK

