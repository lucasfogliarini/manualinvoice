﻿using Dell.Finance.Framework.Core.Security.Contracts;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.Security.Extensions
{
    public static class SecurityServiceCollectionExtensions
    {
        public static void AddSecurityTokenProvider(this IServiceCollection serviceCollection, SecurityConfiguration securityConfiguration)
        {
            serviceCollection.AddSingleton<ISecurityTokenProvider>((serviceProvider) =>
            {
                var logger = serviceProvider.GetService<ILogger<SecurityApiService>>();

                var securityApiService = new SecurityApiService(logger, securityConfiguration);
                var memoryCache = serviceProvider.GetService<IMemoryCache>();

                return new SecurityTokenProvider(securityApiService, memoryCache);
            });
        }
    }
}
