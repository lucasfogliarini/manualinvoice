﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.Security
{
    public class SecurityConfiguration
    {
        public string SecurityUrl { get; set; }

        public string SecuritySystem { get; set; }

        public string SecurityPassword { get; set; }
    }
}
