﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.Security.Dtos
{
    public class SecurityTokenDto
    {
        public SecurityTokenDto() { }

        public SecurityTokenDto(string prefix, string token)
        {
            Prefix = prefix;
            Token = token;
        }

        public string Prefix { get; set; }

        public string Token { get; set; }

        public string FormatAsString()
        {
            return $"{Prefix} {Token}";
        }
    }
}
