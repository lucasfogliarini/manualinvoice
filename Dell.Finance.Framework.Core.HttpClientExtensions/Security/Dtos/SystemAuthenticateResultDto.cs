﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.Security.Dtos
{
    public class SystemAuthenticateResultDto
    {
        public string Token { get; set; }

        public string System { get; set; }

        public bool IsActive { get; set; }
    }
}
