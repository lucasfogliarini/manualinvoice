﻿using Dell.Finance.Framework.Core.Security.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dell.Finance.Framework.Core.Security.Contracts
{
    public interface ISecurityApiService
    {
        Task<SystemAuthenticateResultDto> SystemAuthenticateAsync();
    }
}
