﻿using Dell.Finance.Framework.Core.Security.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.Security.Contracts
{
    public interface ISecurityTokenProvider
    {
        SecurityTokenDto GetSecurityToken();
    }
}
