﻿using Dell.Finance.Framework.Core.Security.Contracts;
using Dell.Finance.Framework.Core.Security.Dtos;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IdentityModel.Tokens.Jwt;

namespace Dell.Finance.Framework.Core.Security
{
    public class SecurityTokenProvider : ISecurityTokenProvider
    {
        private const string SECURITY_TOKEN_KEY = "SECURITY_TOKEN";
        private const string SECURITY_TOKEN_PREFIX = "Bearer";
        
        private readonly ISecurityApiService _securityApiService;
        private readonly IMemoryCache _memoryCache;

        public SecurityTokenProvider(ISecurityApiService securityApiService, IMemoryCache memoryCache)
        {
            _securityApiService = securityApiService;
            _memoryCache = memoryCache;
        }

        public SecurityTokenDto GetSecurityToken()
        {
            SecurityTokenDto securityTokenDto;

            if (!_memoryCache.TryGetValue(SECURITY_TOKEN_KEY, out securityTokenDto))
            {
                var systemAuthenticateResult = _securityApiService.SystemAuthenticateAsync().GetAwaiter().GetResult();
                securityTokenDto = new SecurityTokenDto(SECURITY_TOKEN_PREFIX, systemAuthenticateResult.Token);

                var expiration = ReadExpirationTimeFromJwtToken(systemAuthenticateResult.Token)
                    .AddMinutes(-5).TimeOfDay;

                _memoryCache.Set(SECURITY_TOKEN_KEY, securityTokenDto, expiration);
            }

            return securityTokenDto;
        }

        private DateTime ReadExpirationTimeFromJwtToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtToken = handler.ReadJwtToken(token);

            return jwtToken.ValidTo;
        }
    }
}
