﻿using Dell.Finance.Framework.Core.Security.Contracts;
using Dell.Finance.Framework.Core.Security.Dtos;
using Flurl;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Dell.Finance.Framework.Core.Security
{
    public class SecurityApiService : ISecurityApiService
    {
        private readonly ILogger<SecurityApiService> _logger;
        private readonly SecurityConfiguration _securityConfiguration;

        public SecurityApiService(ILogger<SecurityApiService> logger, SecurityConfiguration securityConfiguration)
        {
            _logger = logger;
            _securityConfiguration = securityConfiguration;
        }

        public async Task<SystemAuthenticateResultDto> SystemAuthenticateAsync()
        {
            var url = GetAuthenticateUri();
            var body = JsonConvert.SerializeObject(new { System = _securityConfiguration.SecuritySystem, Password = _securityConfiguration.SecurityPassword });

            using (var httpClient = new HttpClient())
            {
                _logger.LogInformation($"Calling {url} with system = {_securityConfiguration.SecuritySystem}.");
                var httpResponse = await httpClient.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));

                httpResponse.EnsureSuccessStatusCode();

                var responseContent = await httpResponse.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<SystemAuthenticateResultDto>(responseContent);
            }
        }

        public Uri GetAuthenticateUri()
        {
            return new Url(_securityConfiguration.SecurityUrl)
                .AppendPathSegment("Systems")
                .AppendPathSegment("Authenticate")
                .ToUri();
        }
    }
}
