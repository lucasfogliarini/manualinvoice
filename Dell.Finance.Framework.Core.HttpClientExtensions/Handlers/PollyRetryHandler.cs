﻿using Polly;
using Polly.Registry;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dell.Finance.Framework.Core.HttpClientExtensions.Handlers
{
    public class PollyRetryHandler : DelegatingHandler
    {
        private const string POLICY_NAME = "Retry";

        private readonly IPolicyRegistry<string> _policyRegistry;

        public PollyRetryHandler(IPolicyRegistry<string> policyRegistry)
        {
            _policyRegistry = policyRegistry;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!_policyRegistry.ContainsKey(POLICY_NAME))
                throw new InvalidOperationException("Policy not configured. Use services.ConfigureHttpClientExtensions method to configure polly handler.");

            return _policyRegistry
                .Get<Policy<HttpResponseMessage>>(POLICY_NAME)
                .ExecuteAsync(() => base.SendAsync(request, cancellationToken));
        }
    }
}
