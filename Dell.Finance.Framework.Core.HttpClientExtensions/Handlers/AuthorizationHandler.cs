﻿using Dell.Finance.Framework.Core.Security.Contracts;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dell.Finance.Framework.Core.HttpClientExtensions.Handlers
{
    public class AuthorizationHandler : DelegatingHandler
    {
        private const string AUTHORIZATION_HEADER = "Authorization";

        private readonly ISecurityTokenProvider _securityTokenProvider;
        
        public AuthorizationHandler(ISecurityTokenProvider securityTokenProvider)
        {
            _securityTokenProvider = securityTokenProvider;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if(request.Headers.Contains(AUTHORIZATION_HEADER))
                request.Headers.Remove(AUTHORIZATION_HEADER);

            request.Headers.Add(AUTHORIZATION_HEADER, _securityTokenProvider.GetSecurityToken().FormatAsString());

            return base.SendAsync(request, cancellationToken);
        }
    }
}
