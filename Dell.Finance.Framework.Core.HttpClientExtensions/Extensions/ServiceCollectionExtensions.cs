﻿using Dell.Finance.Framework.Core.HttpClientExtensions.Handlers;
using Dell.Finance.Framework.Core.Security;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Dell.Finance.Framework.Core.HttpClientExtensions.Extensions
{
    public static class ServiceCollectionExtensions
    {
        private const string POLICY_NAME = "Retry";

        private static readonly HttpStatusCode[] _handleStatusCodes = {
            HttpStatusCode.NotFound,
            HttpStatusCode.RequestTimeout,
            HttpStatusCode.ServiceUnavailable,
            HttpStatusCode.GatewayTimeout
        };

        public static IServiceCollection ConfigureHttpClientExtensions(this IServiceCollection services, ILogger logger, int retryCount = 3, int backOffPower = 3)
        {
            services.AddTransient<AuthorizationHandler>();

            services.AddTransient<PollyRetryHandler>();

            var policyRegistry = services.AddPolicyRegistry();
            policyRegistry.Add(
                POLICY_NAME,
                Policy<HttpResponseMessage>
                    .HandleResult(x => _handleStatusCodes.Contains(x.StatusCode))
                    .WaitAndRetryAsync(retryCount, retryAttempt => TimeSpan.FromSeconds(backOffPower * retryAttempt),
                        onRetry: (response, calculatedWaitDuration, context) =>
                        {
                            logger.LogWarning($"Error {response.Result.StatusCode} when executing request to {response.Result.RequestMessage.RequestUri.ToString()}, retrying after {calculatedWaitDuration.TotalSeconds} seconds.");
                        })
            );

            return services;
        }
    }
}
