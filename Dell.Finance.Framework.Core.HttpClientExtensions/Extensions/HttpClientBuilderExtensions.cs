﻿using Dell.Finance.Framework.Core.HttpClientExtensions.Handlers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dell.Finance.Framework.Core.HttpClientExtensions.Extensions
{
    public static class HttpClientBuilderExtensions
    {
        public static IHttpClientBuilder AddSecurityAuthorization(this IHttpClientBuilder httpClient)
        {
            httpClient.AddHttpMessageHandler<AuthorizationHandler>();

            return httpClient;
        }

        public static IHttpClientBuilder AddPollyRetry(this IHttpClientBuilder httpClient)
        {
            httpClient.AddHttpMessageHandler<PollyRetryHandler>();

            return httpClient;
        }
    }
}
