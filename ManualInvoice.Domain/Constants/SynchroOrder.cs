﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Constants
{
    public class SynchroOrder
    {
        public const string MANUAL_INVOICE_PREFIX = "04-";
        public const string DOF_IMPORT_NUMBER_SHIPPING_SUFIX = "002";
        public const string DIRECT_OMEGA_ORDER_TYPE = "BR Order";
        public const string SERVICE_INVOICE_SUFIX = ".01";
    }
}
