﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Constants
{
    public class DelegationPermissions
    {
        public static readonly string[] DELEGATED_PERMISSIONS = { "MANUALINVOICE.Approvals.Create", "MANUALINVOICE.Approvals.Delete", "MANUALINVOICE.Approvals.Get",
                                              "MANUALINVOICE.Approvals.Post", "MANUALINVOICE.Approvals.Put", "MANUALINVOICE.Approvals.Screen",
                                              "MANUALINVOICE.Approvals.Update", "MANUALINVOICE.Approvals.View" };

        public static readonly string[] DELEGATED_CASCADE_PERMISSIONS = { "MANUALINVOICE.Management.Screen", "MANUALINVOICE.Delegations.Screen", "MANUALINVOICE.Delegations.Create",
                                                     "MANUALINVOICE.Delegations.Delete", "MANUALINVOICE.Delegations.Export", "MANUALINVOICE.Delegations.Get",
                                                     "MANUALINVOICE.Delegations.Post", "MANUALINVOICE.Delegations.Put", "MANUALINVOICE.Delegations.Screen",
                                                     "MANUALINVOICE.Delegations.Update" };
    }
}
