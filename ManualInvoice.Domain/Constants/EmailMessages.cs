﻿namespace ManualInvoice.Domain.Constants
{
    public class EmailMessages
    {
        public const string APPROVAL_REQUEST_SUBJECT = "Approval of the Request {0}.";
        public const string DANF_FILE_FOR_APPROVED_REQUESTS_BODY_MESSAGE = "The request {0} was approved and the DANFE pdf was attached to this email.";
        public const string PENDING_LEVEL_APPROVAL_BODY_MESSAGE = "The request {0} was sent to \"{1}\" level, responsible for the authorization is \"{2}\"";
        public const string PENDING_LEVEL_APPROVAL_INVOICE_USER_BODY_MESSAGE = "The request {0} was sent to \"{1}\" level, responsible for the authorization: {2}.";
        public const string PENDING_LEVEL_APPROVAL_OPEN_LINK_MESSAGE = "Open the screen in: {0}";
        public const string REJECT_LEVEL_BODY_MESSAGE = "Rejected by \"{0}\". Rejection reason: {1}";
        public const string REJECT_REQUEST_SUBJECT = "Request {0} was rejected.";
        public const string REMINDER_FAILURE_MESSAGE = "Failure on sending email reminder. Error: {0}";
        public const string REMINDER_SUCCESS_MESSAGE = "Email reminder message was successfully sent.";
    }
}
