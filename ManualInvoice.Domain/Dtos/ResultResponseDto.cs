﻿using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Dtos
{
    public class ResultResponseDto<T>
    {
        private readonly List<string> _errors;

        public ResultResponseDto()
        {
            _errors = new List<string>();
        }

        public T Response { get; private set; }

        public bool Success => !_errors.Any();

        public IReadOnlyCollection<string> Errors => _errors.ToList();

        public void AddError(string error)
        {
            if (string.IsNullOrEmpty(error) || _errors.Contains(error))
            {
                return;
            }

            _errors.Add(error);
        }

        public void AddError(string[] errors)
        {
            _errors.AddRange(errors);
        }

        public void SetResponse(T response)
        {
            Response = response;
        }
    }
}
