﻿namespace ManualInvoice.Domain.Dtos
{
    public sealed class SelectItemDto
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public bool IsCreditCard { get; set; }
    }
}
