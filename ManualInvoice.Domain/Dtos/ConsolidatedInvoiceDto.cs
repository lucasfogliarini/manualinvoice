﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos
{
    public class ConsolidatedInvoiceDto
    {
        public string BaseOriginalInvoiceId { get; set; }
        public InvoiceItem ConsolidatedItem { get; set; }
        public IList<FiscalDocumentDto> OriginalInvoices { get; set; }
    }
}
