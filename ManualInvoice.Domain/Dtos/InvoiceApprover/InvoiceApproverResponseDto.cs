﻿using ManualInvoice.Domain.Common;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos.InvoiceApprover
{
    public sealed class InvoiceApproverResponseDto
    {
        public InvoiceApproverResponseDto()
        {
            Users = new List<SelectItemDto>();
        }

        public int Id { get; set; }
        public int? UserId { get; set; }
        public string User { get; set; }
        public int LevelId { get; set; }
        public string Level { get; set; }
        public IList<SelectItemDto> Users { get; set; }
        public ApproverStatusType? Situation { get; set; }
        public string EffectiveUser { get; set; }
        public long? ApprovalSequence { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string RejectReason { get; set; }
    }
}
