﻿namespace ManualInvoice.Domain.Dtos.Delegation
{
    public sealed class HasDelegationDto
    {
        public bool HasDelegation { get; set; }
        public bool HasCascade { get; set; }
    }
}
