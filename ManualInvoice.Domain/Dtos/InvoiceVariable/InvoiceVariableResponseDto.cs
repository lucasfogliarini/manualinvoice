﻿using ManualInvoice.Domain.Enum;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos.InvoiceVariable
{
    public sealed class InvoiceVariableResponseDto
    {
        public InvoiceVariableResponseDto()
        {
            Options = new List<SelectItemDto>();
        }

        public string Value { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public bool Required { get; set; }
        public bool IsAlfaNumeric { get; set; }
        public int Order { get; set; }
        public ControlType ControlType { get; set; }
        public long Length { get; set; }
        public IList<SelectItemDto> Options { get; set; }
        public int VariableId { get; set; }
    }
}
