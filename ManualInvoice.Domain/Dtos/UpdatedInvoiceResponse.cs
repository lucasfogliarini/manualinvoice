﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Dtos
{
    public sealed class UpdatedInvoiceResponse : ResultResponseDto<Invoice>
    {
    }
}
