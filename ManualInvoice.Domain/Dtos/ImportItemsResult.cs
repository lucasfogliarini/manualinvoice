﻿using ManualInvoice.Domain.Entities;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos
{
    public class ImportItemsResult
    {
        public ImportItemsResult()
        {
            ImportedItems = new List<InvoiceItem>();
            Errors = new List<string>();
        }

        public IList<InvoiceItem> ImportedItems { get; set; }
        public IList<string> Errors { get; set; }
    }
}
