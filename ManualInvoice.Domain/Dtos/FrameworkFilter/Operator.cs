﻿namespace ManualInvoice.Domain.Dtos.FrameworkFilter
{
    public enum Operator
    {
        Equals = 0,
        NotEquals = 1,
        Contains = 2,
        StartsWith = 3,
        EndsWith = 4,
        In = 5,
        NotIn = 6,
        Any = 7,
        NotAny = 8,
        All = 9,
        NotAll = 10,
        LessThanOrEqual = 11,
        GreaterThanOrEqual = 12
    }
}
