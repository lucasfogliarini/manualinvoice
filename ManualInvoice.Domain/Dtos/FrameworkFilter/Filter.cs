﻿namespace ManualInvoice.Domain.Dtos.FrameworkFilter
{
    public class Filter
    {
        public Filter(string field, string value, Operator @operator, Operator? subOperator = null, bool? nullValue = null)
        {
            Field = field;
            Value = value;
            Operator = @operator;
            SubOperator = subOperator;
            NullValue = nullValue;
        }

        public string Field { get; set; }
        public string Value { get; set; }
        public Operator Operator { get; set; }
        public Operator? SubOperator { get; set; }
        public bool? NullValue { get; set; }
    }
}
