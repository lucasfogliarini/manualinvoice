﻿namespace ManualInvoice.Domain.Dtos.FrameworkFilter
{
    public class Page
    {
        public Page(int number, int size)
        {
            Number = number;
            Size = size;
        }

        public int Number { get; private set; }
        public int Size { get; private set; }
    }
}
