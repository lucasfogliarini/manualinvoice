﻿namespace ManualInvoice.Domain.Dtos.FrameworkFilter
{
    public class PaginatedGridSettings : GridSettings
    {
        public Page Page { get; set; }
    }
}
