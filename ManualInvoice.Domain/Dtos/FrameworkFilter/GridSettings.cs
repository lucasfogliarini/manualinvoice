﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos.FrameworkFilter
{
    public class GridSettings
    {
        public string SortExpression { get; set; }
        public string Include { get; set; }
        public List<Filter> Filters { get; set; }
    }
}
