﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Dtos
{
    public class ImportedSkuDto
    {
        public string Type { get; set; }
        public string Code { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string ServiceTag { get; set; }
        public string RmaCode { get; set; }
        public string Error { get; set; }
    }
}
