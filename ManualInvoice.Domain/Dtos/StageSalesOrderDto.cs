﻿using ManualInvoice.Domain.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Dtos
{
    public sealed class StageSalesOrderDto
    {
        [JsonIgnore]
        public Invoice Invoice { get; set; }

        public IList<long> DofImports { get; set; }
        [JsonIgnore]
        
        public IList<Order> Orders { get; set; }
        
        public IList<string> Errors { get; set; }
        
        public IList<long> IssuedDofs { get; set; }
        
        public IList<long> NotIssuedDofs { get; set; }

        public StageSalesOrderDto()
        {
            DofImports = new List<long>();
            Orders = new List<Order>();
            Errors = new List<string>();
            IssuedDofs = new List<long>();
            NotIssuedDofs = new List<long>();
        }

        public StageSalesOrderDto(string error)
            : this()
        {
            Errors.Add(error);
        }

        public StageSalesOrderDto(Invoice invoice, List<long> dofImports, IList<Order> orders)
            : this()
        {
            Invoice = invoice;
            DofImports = dofImports;
            Orders = orders;
        }
    }
}
