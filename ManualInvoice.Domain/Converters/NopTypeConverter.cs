﻿using ManualInvoice.Domain.Enum;
using Newtonsoft.Json;
using System;

public sealed class NopTypeConverter : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(string);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var value = (string)reader.Value;

        switch (value)
        {
            case "M":
                return NopType.Product;
            case "S":
                return NopType.Service;
            case "P":
                return NopType.Installment;
            default:
                return NopType.Unknown;
        }
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var _value = (NopType) value;

        switch (_value)
        {
            case NopType.Product:
                writer.WriteValue("M");
                break;
            case NopType.Service:
                writer.WriteValue("S");
                break;
            case NopType.Installment:
                writer.WriteValue("P");
                break;
            default:
                writer.WriteValue("null");
                break;
        }
    }
}