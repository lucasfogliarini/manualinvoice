﻿using ManualInvoice.Domain.Enum;
using Newtonsoft.Json;
using System;

namespace ManualInvoice.Domain.Converters
{
    public class DellBaseFlagTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "Y":
                    return DellBaseFlagType.Base;
                case "N":
                    return DellBaseFlagType.NonBase;
                case "R":
                    return DellBaseFlagType.Rollup;
                default:
                    return DellBaseFlagType.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var _value = (DellBaseFlagType)value;

            switch (_value)
            {
                case DellBaseFlagType.Base:
                    writer.WriteValue("Y");
                    break;
                case DellBaseFlagType.NonBase:
                    writer.WriteValue("N");
                    break;
                case DellBaseFlagType.Rollup:
                    writer.WriteValue("R");
                    break;
                default:
                    writer.WriteValue("null");
                    break;
            }
        }
    }
}
