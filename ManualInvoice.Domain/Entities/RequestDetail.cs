using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public class RequestDetail : IEntityWithId<int>
    {
        public int Id { get; set; }
        public string TransactionName { get; set; }
        public decimal RequestAmount { get; set; }
        public int? RequestUserId { get; set; }
        public string RequestUserName { get; set; }
        public DateTime RequestDate { get; set; }
        public string LevelName { get; set; }
        public int? ApproverId { get; set; }
        public string ApproverName { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public TimeSpan? AgingRequestApprovalDate { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public TimeSpan? AgingRequestInvoiceDate { get; set; }
    }
}
