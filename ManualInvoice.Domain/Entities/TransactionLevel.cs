using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class TransactionLevel : IEntityWithId<string>
    {
        public string Id { get; set; }
        public long? SequenceApproval { get; set; }

        public int TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
    }
}
