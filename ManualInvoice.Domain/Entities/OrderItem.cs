﻿using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public sealed class OrderItem : IEntityWithId<long>
    {
        public OrderItem()
        {
        }

        public OrderItem(long orderId, long invoiceId, long invoiceItemId)
        {
            Order = new Order() { Id = orderId };
            Invoice = new Invoice() { Id = invoiceId };
            InvoiceItem = new InvoiceItem() { Id = invoiceItemId };
        }

        public long Id { get; set; }
        public string DiscountClassCode { get; set; }
        public decimal? ItemUnitSoldPriceOrig { get; set; }
        public decimal? ItemUnitSoldPrice { get; set; }
        public decimal? ItemUnitListPriceOrig { get; set; }
        public decimal? ItemUnitListPrice { get; set; }
        public long? QtyItemOrig { get; set; }
        public long? QtyItem { get; set; }
        public string ItemLob { get; set; }
        public decimal? ItemUnitCostAmtOrig { get; set; }
        public decimal? ItemUnitCostAmt { get; set; }
        public decimal? ItemUnitCostAmtUs { get; set; }
        public decimal? ItemUnitCostAmtUsOrig { get; set; }
        public DateTime? AposStartDate { get; set; }
        public DateTime? AposEndDate { get; set; }
        public string TieNum { get; set; }
        public string FulFillmentLocId { get; set; }
        public Order Order { get; set; }
        public Invoice Invoice { get; set; }
        public InvoiceItem InvoiceItem { get; set; }

        internal void Update(OrderItem orderItem)
        {
            DiscountClassCode = orderItem.DiscountClassCode;
            ItemUnitSoldPriceOrig = orderItem.ItemUnitSoldPriceOrig;
            ItemUnitSoldPrice = orderItem.ItemUnitSoldPrice;
            ItemUnitListPriceOrig = orderItem.ItemUnitListPriceOrig;
            ItemUnitListPrice = orderItem.ItemUnitListPrice;
            QtyItemOrig = orderItem.QtyItemOrig;
            QtyItem = orderItem.QtyItem;
            ItemLob = orderItem.ItemLob;
            ItemUnitCostAmtOrig = orderItem.ItemUnitCostAmtOrig;
            ItemUnitCostAmt = orderItem.ItemUnitCostAmt;
            ItemUnitCostAmtUs = orderItem.ItemUnitCostAmtUs;
            ItemUnitCostAmtUsOrig = orderItem.ItemUnitCostAmtUsOrig;
            AposStartDate = orderItem.AposStartDate;
            AposEndDate = orderItem.AposEndDate;
            TieNum = orderItem.TieNum;
            FulFillmentLocId = orderItem.FulFillmentLocId;
        }
    }
}
