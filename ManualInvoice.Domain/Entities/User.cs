using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Entities
{
    public class User : IEntityWithId<int>
    {
        public User()
        {
        }

        public User(int id)
            : this()
        {
            Id = id;
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastLogonDate { get; set; }
        public int ProfileId { get; set; }
        public List<LevelUser> LevelUsers { get; set; }
    }
}

