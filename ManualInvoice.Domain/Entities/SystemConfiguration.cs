﻿using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class SystemConfiguration : IEntityWithId<int>
    {

        public SystemConfiguration()
        { }

        public SystemConfiguration(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public int DaysDraftRequest { get; set; }
        public int EmailRemember { get; set; }  
    }
}
