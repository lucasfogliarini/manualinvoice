﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public class ApprovalView : IEntityWithId<int>
    {

        public ApprovalView()
        { }

        public ApprovalView(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public long InvoiceId { get; set; }
        public DateTime? DtRequest { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int TransactionId { get; set; }
        public string TransactionName { get; set; }
        public TransactionType? TransactionType { get; set; }
        public int LevelId{ get; set; }
        public int SequenceApproval { get; set; }
        public int ApproverUserId { get; set; }
        public int ApproverId { get; set; }
    }
}
       