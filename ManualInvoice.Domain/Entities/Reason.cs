﻿using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Reason : IEntityWithId<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ReasonType ReasonType { get; set; }
        public bool IsActive { get; set; }
    }
}
