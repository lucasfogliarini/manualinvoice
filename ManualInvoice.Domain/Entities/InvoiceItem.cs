﻿using ManualInvoice.Domain.Enum;
using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class InvoiceItem : IEntityWithId<long>
    {
        public long Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public int? QuantityOriginal { get; set; }
        public decimal? PriceOriginal { get; set; }
        public string Ncm { get; set; }
        public string RmaCode { get; set; }
        public string RmaName { get; set; }
        public long? ItemLine { get; set; }
        public DellBaseFlagType? DellBaseFlag { get; set; }
        public string DellTieNum { get; set; }
        public long? OriginalDofId { get; set; }
        public long? OriginalIdfNum { get; set; }
        public Invoice Invoice { get; set; }

        internal void Update(InvoiceItem invoiceItem)
        {
            ProductCode = invoiceItem.ProductCode;
            ProductName = invoiceItem.ProductName;
            Quantity = invoiceItem.Quantity;
            Price = invoiceItem.Price;
            QuantityOriginal = invoiceItem.QuantityOriginal;
            PriceOriginal = invoiceItem.PriceOriginal;
            Ncm = invoiceItem.Ncm;
            RmaCode = invoiceItem.RmaCode;
            RmaName = invoiceItem.RmaName;
            ItemLine = invoiceItem.ItemLine;
            DellBaseFlag = invoiceItem.DellBaseFlag;
            DellTieNum = invoiceItem.DellTieNum;
            OriginalDofId = invoiceItem.OriginalDofId;
            OriginalIdfNum = invoiceItem.OriginalIdfNum;
        }
    }
}
