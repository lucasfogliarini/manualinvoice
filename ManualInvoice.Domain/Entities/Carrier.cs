using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Carrier : IEntityWithId<long>
    {
        public Carrier()
        {
        }

        public Carrier(long id) : this()
        {
            Id = id;
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? VolumeQuantity { get; set; }
        public string VolumeBrand { get; set; }
        public string VolumeNumbering { get; set; }
        public decimal? FreightValue { get; set; }
        public decimal? InsuranceValue { get; set; }
        public decimal? Weight { get; set; }
        public string Specie { get; set; }
        public decimal? OtherCosts { get; set; }
        public Carrier Copy()
        {
            var result = new Carrier
            {
                Code = Code,
                Name = Name,
                VolumeQuantity = VolumeQuantity,
                VolumeBrand = VolumeBrand,
                VolumeNumbering = VolumeNumbering,
                FreightValue = FreightValue,
                InsuranceValue = InsuranceValue,
                Weight = Weight,
                Specie = Specie,
                OtherCosts = OtherCosts
            };

            return result;
        }
    }
}

