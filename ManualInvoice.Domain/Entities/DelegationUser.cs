using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class DelegationUser : IEntityWithId<int>
    {
        public int Id { get; set; }

        public User User { get; set; }
        public int? UserId { get; set; }
        public Delegation Delegation { get; set; }
        public int? DelegationId { get; set; }
    }
}

