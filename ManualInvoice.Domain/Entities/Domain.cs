using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Domain : IEntityWithId<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}

