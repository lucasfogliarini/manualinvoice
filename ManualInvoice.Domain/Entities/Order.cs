﻿using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Entities
{
    public class Order : IEntityWithId<long>
    {
        public Order() 
        { 
        }

        public Order(long invoiceId, long originalDofId, string originalRecofNumOrder) 
        {
            Invoice = new Invoice { Id = invoiceId };
            OriginalDofId = originalDofId;
            RecofNumOrder = originalRecofNumOrder;
        }

        public long Id { get; set; }
        public long OriginalDofId { get; set; }
        public string RecofNumOrder { get; set; }
        public string CustClassCode { get; set; }
        public DateTime? OrderCreationDate { get; set; }
        public string DpsType { get; set; }
        public decimal? OrderRate { get; set; }
        public decimal? InfoShipping { get; set; }
        public decimal? InfoDuty { get; set; }
        public string DirectOrderType { get; set; }
        public decimal? PmtAmt1Lc { get; set; }
        public decimal? PmtAmt2Lc { get; set; }
        public decimal? PmtAmt3Lc { get; set; }
        public string MnAuditReason { get; set; }
        public string SpecialInstructions { get; set; }
        public string PaymentTermCode { get; set; }
        public string Promo1 { get; set; }
        public string Promo2 { get; set; }
        public string Promo3 { get; set; }
        public DateTime? ShipDate { get; set; }
        public string SourceTypeCode { get; set; }
        public string PaymentProvider1 { get; set; }
        public string PaymentProvider2 { get; set; }
        public string PaymentProvider3 { get; set; }
        public string PaymentNsu1 { get; set; }
        public string PaymentNsu2 { get; set; }
        public string PaymentNsu3 { get; set; }
        public Invoice Invoice { get; set; }
        public IList<OrderItem> OrderItems { get; set; }
    }
}
