using System;
using System.Collections.Generic;
using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class EmailLog : IEntityWithId<int>
    {
        public int Id { get; set; }
        public long EmailId { get; set; }
        public DateTime LogDate { get; set; }
        public string Description { get; set; }

        public Email Email { get; set; }
    }
}
