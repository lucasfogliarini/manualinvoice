﻿using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Branch : IEntityWithId<int>
    {

        public Branch()
        { }

        public Branch(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}
