﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Entities
{
    public class Invoice : IEntityWithId<long>
    {
        public Invoice()
        {
            Status = InvoiceStatusType.Draft;
            RequestDate = DateTime.Now;
            BlDraft = true;
            GenerateNf = false;
            BillToSent = false;
            ShipToSent = false;
            UpdatedOnSynchro = false;
            Cancelled = false;
            Finalized = false;
            SynchroStatus = OrderSynchroStatus.None;
        }

        #region Table Columns

        public long Id { get; set; }
        public DateTime? RequestDate { get; set; }
        public string BillToCode { get; set; }
        public string BillToName { get; set; }
        public string BillToCnpj { get; set; }
        public string BillToAddress { get; set; }
        public string ShipToCode { get; set; }
        public string ShipToName { get; set; }
        public string ShipToCnpj { get; set; }
        public string ShipToAddress { get; set; }
        public virtual string TransactionName { get; set; }
        public virtual string TransactionBillToCode { get; set; }
        public virtual string TransactionShipToCode { get; set; }
        public virtual string TransactionSerie { get; set; }
        public virtual string TransactionSpecie { get; set; }
        public string Observation { get; set; }
        public string ObservationTwo { get; set; }
        public string Instruction { get; set; }
        public string SynInvoiceNumber { get; set; }
        public DateTime? SynInvoiceDate { get; set; }
        public decimal? SynInvoiceAmount { get; set; }
        public string SynNfeAccessKey { get; set; }
        public string ServiceInvoiceNumber { get; set; }
        public DateTime? RejectDate { get; set; }
        public bool? BlDraft { get; set; }
        public bool? GenerateNf { get; set; }
        public bool? BillToSent { get; set; }
        public bool? ShipToSent { get; set; }
        public bool? UpdatedOnSynchro { get; set; }
        public bool? Cancelled { get; set; }
        public bool? Finalized { get; set; }
        public bool IsConsolidated { get; set; }
        public InvoiceStatusType? Status { get; set; }
        public InvoiceType? InvoiceType { get; set; }
        public OrderSynchroStatus? SynchroStatus { get; set; }
        #endregion
        #region Navigation Properties
        public IList<InvoiceItem> Items { get; set; }
        public Branch Branch { get; set; }
        public Transaction Transaction { get; set; }
        public Reason Reason { get; set; }
        public Reason RejectReason { get; set; }
        public int? RejectReasonId { get; set; }
        public Carrier Carrier { get; set; }
        public User User { get; set; }
        public User RejectUser { get; set; }
        public int? RejectUserId { get; set; }
        #endregion

        public void SetTransactionValues(Transaction transaction)
        {
            if (transaction == null)
            {
                return;
            }

            TransactionBillToCode = transaction.BillToCode;
            TransactionShipToCode = transaction.ShipToCode;
            TransactionName = transaction.Name;
            TransactionSerie = transaction.Serie;
            TransactionSpecie = transaction.Specie;
        }

        public void SetInvoiceBillTo(string pfjCode, string name, string cpfCgc, string address)
        {
            BillToCode = pfjCode;
            BillToName = name;
            BillToCnpj = cpfCgc;
            BillToAddress = address;
        }

        public Invoice Copy(User user, long? carrierId)
        {
            var result = new Invoice
            {
                User = new User() { Id = user.Id },
                BillToAddress = BillToAddress,
                Transaction = Transaction,
                Reason = Reason
            };

            if (carrierId.HasValue)
            {
                result.Carrier = new Carrier() { Id = carrierId.Value };
            }

            result.User = user;
            result.RejectUser = RejectUser;
            result.BillToCode = BillToCode;
            result.BillToName = BillToName;
            result.BillToCnpj = BillToCnpj;
            result.BillToAddress = BillToAddress;

            if (Transaction != null  && !string.IsNullOrEmpty(Transaction.ShipToCode))
            {
                result.ShipToCode = ShipToCode;
                result.ShipToName = ShipToName;
                result.ShipToCnpj = ShipToCnpj;
                result.ShipToAddress = ShipToAddress;
            }

            result.TransactionBillToCode = Transaction.BillToCode;
            result.TransactionShipToCode = Transaction.ShipToCode;
            result.TransactionName = Transaction.Name;
            result.TransactionSerie = Transaction.Serie;
            result.TransactionSpecie = Transaction.Specie;
            result.Observation = Observation;
            result.ObservationTwo = ObservationTwo;
            result.Instruction = Instruction;
            result.SynInvoiceNumber = SynInvoiceNumber;
            result.SynInvoiceDate = SynInvoiceDate;
            result.SynInvoiceAmount = SynInvoiceAmount;
            result.SynNfeAccessKey = SynNfeAccessKey;
            result.ServiceInvoiceNumber = ServiceInvoiceNumber;
            result.IsConsolidated = IsConsolidated;

            return result;
        }

        public void UpdateValues(bool isConsolidated, OrderSynchroStatus? synchroStatus)
        {
            IsConsolidated = isConsolidated;
            SynchroStatus = synchroStatus;
        }
    }
}
