using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class DomainValue : IEntityWithId<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public Domain Domain { get; set; }
    }
}

