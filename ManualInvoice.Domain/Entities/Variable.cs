using ManualInvoice.Domain.Enum;
using ManualInvoice.Infrastructure.Entities;
using System.Linq;

namespace ManualInvoice.Domain.Entities
{
    public class Variable : IEntityWithId<int>
    {
        private readonly int[] _dropdownPaymentCode = new[] { 24, 29, 30 };
        private readonly int[] _dropdownShipmentState = new[] { 33 };

        public int Id { get; set; }
        public long? Length { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsAlphanumeric { get; set; }
        public bool IsEditable { get; set; }
        public bool? IsSystem { get; set; }
        public string DefaultValue { get; set; }
        public ResourceType Resource 
        { 
            get 
            {
                if (_dropdownPaymentCode.Contains(Id))
                {
                    return ResourceType.PaymentCode;
                }

                if (_dropdownShipmentState.Contains(Id))
                {
                    return ResourceType.FederativeUnit;
                }

                return ResourceType.NoResource;
            } 
        }
        public bool IsDropdown { get { return Resource != ResourceType.NoResource; } }
    }
}

