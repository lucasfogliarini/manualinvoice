using ManualInvoice.Infrastructure.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Entities
{
    public class Delegation : IEntityWithId<int>
    {
        public int Id { get; set; }
        public DateTime? DelegationDate { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public bool? IsCascade { get; set; }
        public bool? IsOldApproval { get; set; }
        public bool? IsActive { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public IList<DelegationUser> DelegationUsers { get; set; }
    }
}

