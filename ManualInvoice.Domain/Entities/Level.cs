using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Level : IEntityWithId<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Visible { get; set; }
        public bool? Invoice { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}

