﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Infrastructure.Entities;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Entities
{
    public class Transaction : IEntityWithId<int>
    {

        public Transaction()
        {

        }

        public Transaction(int id, string name, TransactionType transactionType) : this()
        {
            Id = id;
            Name = name;
            TransactionType = transactionType;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Observation { get; set; }
        public bool? IsActive { get; set; }
        public string BillToCode { get; set; }
        public string ShipToCode { get; set; }
        public string Serie { get; set; }
        public string Specie { get; set; }
        public TransactionType? TransactionType { get; set; }
        public IList<TransactionLevel> TransactionLevels { get; set; }
        public IList<TransactionBranch> TransactionBranches { get; set; }
        public IList<TransactionVariable> TransactionVariables { get; set; }
    }
}
