using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public sealed class InvoiceVariable : IEntityWithId<int>
    {
        public InvoiceVariable()
        {
        }

        public InvoiceVariable(long invoiceId, int variableId, bool? mandatory, int userId, string value)
            : this()
        {
            Invoice = new Invoice() { Id = invoiceId };
            Variable = new Variable() { Id = variableId };
            IsMandatory = mandatory;
            User = new User() { Id = userId };
            DateCreate = DateTime.Now;
            Value = value;
        }

        public int Id { get; set; }
        public string Value { get; set; }
        public DateTime? DateCreate { get; set; }
        public bool? IsMandatory { get; set; }

        public Variable Variable { get; set; }
        public Invoice Invoice { get; set; }
        public User User { get; set; }
    }
}

