using ManualInvoice.Domain.Common;
using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public class InvoiceApprover : IEntityWithId<int>
    {
        public InvoiceApprover()
        {
        }

        public InvoiceApprover(long invoiceId, int levelId, long? approvalSequence, bool? isVisible, int userId)
            : this(invoiceId, levelId, approvalSequence, isVisible)
        {
            User = new User() { Id = userId };
        }

        public InvoiceApprover(long invoiceId, int levelId, long? approvalSequence, bool? isVisible)
            : this()
        {
            Invoice = new Invoice() { Id = invoiceId };
            Level = new Level() { Id = levelId };
            ApprovalSequence = approvalSequence;
            IsVisible = isVisible;
        }
        public int Id { get; set; }
        public ApproverStatusType? Situation { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? LastEmailDate { get; set; }
        public long? ApprovalSequence { get; set; }
        public bool? IsVisible { get; set; }

        public Invoice Invoice { get; set; }
        public User User { get; set; }
        public int? UserId { get; set; }
        public User EffectiveUser { get; set; }
        public int? EffectiveUserId { get; set; }
        public Level Level { get; set; }
    }
}

