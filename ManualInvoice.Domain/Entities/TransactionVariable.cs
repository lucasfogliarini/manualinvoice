using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class TransactionVariable : IEntityWithId<int>
    {
        public int Id { get; set; }
        public bool? IsMandatory { get; set; }
        public int? TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public int? VariableId { get; set; }
        public Variable Variable { get; set; }
    }
}

