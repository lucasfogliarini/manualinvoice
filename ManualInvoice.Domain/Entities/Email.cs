using System;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class Email : IEntityWithId<int>
    {
        public int Id { get; set; }
        public EmailType TypeId { get; set; }
        public long RelatedEntityId { get; set; }
        public int ReceiverId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? SentDate { get; set; }
        public bool? IsSent { get; set; }
        public string ReceiverEmail { get; set; }
        public string Link { get; set; }
    }
}
