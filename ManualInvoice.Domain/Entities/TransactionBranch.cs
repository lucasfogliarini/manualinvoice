using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class TransactionBranch : IEntityWithId<string>
    {
        public TransactionBranch() { }

        public TransactionBranch(string id, int transactionId, int branchId)
            : this()
        {
            Id = id;
            Transaction = new Transaction() { Id = transactionId };
            Branch = new Branch() { Id = branchId };
        }

        public string Id { get; set; }
        public int TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
    }
}
