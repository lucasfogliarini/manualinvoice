﻿using ManualInvoice.Infrastructure.Entities;
using System;

namespace ManualInvoice.Domain.Entities
{
    public class DelegationView : IEntityWithId<int>
    {

        public DelegationView()
        { }

        public DelegationView(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public bool IsCascade { get; set; }
        public bool IsOldApproval { get; set; }
        public int DelegationByUserId { get; set; }
        public string DelegationByUserName { get; set; }
        public int DelegatedUserId { get; set; }
    }
}