using ManualInvoice.Infrastructure.Entities;

namespace ManualInvoice.Domain.Entities
{
    public class LevelUser : IEntityWithId<int>
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int LevelId { get; set; }
        public User User { get; set; }
        public Level Level { get; set; }
    }
}

