﻿using ManualInvoice.Domain.Services;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace ManualInvoice.Domain
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IReasonService, ReasonService>();
            services.AddScoped<IDelegationService, DelegationService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<IIssuerService, IssuerService>();
            services.AddScoped<ITransactionBranchService, TransactionBranchService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ILevelService, LevelService>();
            services.AddScoped<IVariableService, VariableService>();
            services.AddScoped<ISecurityApiService, SecurityApiService>();
            services.AddScoped<INopService, NopService>();
            services.AddScoped<IInvoiceItemService, InvoiceItemService>();
            services.AddScoped<ICarrierService, CarrierService>();
            services.AddScoped<ITransactionVariableService, TransactionVariableService>();
            services.AddScoped<IInvoiceVariableService, InvoiceVariableService>();
            services.AddScoped<ITransactionLevelService, TransactionLevelService>();
            services.AddScoped<IInvoiceApproverService, InvoiceApproverService>();
            services.AddScoped<IPaymentCodeService, PaymentCodeService>();
            services.AddScoped<IFederativeUnitService, FederativeUnitService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderItemService, OrderItemService>();
            services.AddScoped<IPeopleService, PeopleService>();
            services.AddScoped<IApprovalViewService, ApprovalViewService>();
            services.AddScoped<ISystemConfigurationService, SystemConfigurationService>();
            services.AddScoped<ILoadVolumeService, LoadVolumeService>();
            services.AddScoped<IDofConsolidationService, DofConsolidationService>();
            services.AddScoped<IDelegationViewService, DelegationViewService>();
            services.AddScoped<IEmailService, EmailService>();

            return services;
        }

        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddSingleton<IRuleValidator, RuleValidator>();
            return services;
        }
    }
}