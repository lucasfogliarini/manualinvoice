﻿namespace System
{
    public static class StringExtensions
    {
        public static decimal ToDecimal(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return decimal.Zero;
            }

            decimal result = decimal.Zero;

            decimal.TryParse(value, out result);

            return result;
        }

        public static string PadLeft(this int value, int totalWidth, char paddingChar)
        {
            string valueToString = value.ToString();

            if (valueToString.Length >= totalWidth)
            {
                return valueToString;
            }

            return valueToString.PadLeft(totalWidth, paddingChar);
        }
    }
}
