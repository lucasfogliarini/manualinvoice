﻿using ManualInvoice.Domain.Dtos.FrameworkFilter;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Extensions
{
    public static class GridSettingsExtensions
    {
        public static string ToQueryString(this GridSettings gridSettings)
        {
            var filters = gridSettings.Filters;
            var sortExpression = gridSettings.SortExpression;
            var include = gridSettings.Include;

            var parameters = new List<string>();

            parameters.Add(GetFilters(filters));

            if (!string.IsNullOrWhiteSpace(sortExpression))
            {
                parameters.Add($"SortExpression={sortExpression}");
            }

            if (!string.IsNullOrWhiteSpace(include))
            {
                parameters.Add($"Include={string.Join(',', include)}");
            }

            var request = string.Join('&', parameters);

            return request;
        }

        public static string ToQueryString(this PaginatedGridSettings paginatedGridSettings)
        {
            var page = paginatedGridSettings.Page;

            var parameters = new List<string>();

            parameters.Add($"Page.Number={page.Number}");
            parameters.Add($"Page.Size={page.Size}");
            parameters.Add((paginatedGridSettings as GridSettings).ToQueryString());

            var request = string.Join('&', parameters);

            return request;
        }

        private static string GetFilters(IList<Filter> filters)
        {
            var parameters = new List<string>();
            var i = 0;

            foreach (var filter in filters)
            {
                // Discard filter when value is null and null value is not allowed
                if (string.IsNullOrWhiteSpace(filter.Value) && filter.NullValue != true)
                {
                    continue;
                }

                parameters.Add($"Filters[{i}].Field={filter.Field}");
                parameters.Add($"Filters[{i}].Value={filter.Value}");
                parameters.Add($"Filters[{i}].Operator={filter.Operator}");

                if (filter.SubOperator.HasValue)
                {
                    parameters.Add($"Filters[{i}].SubOperator={filter.SubOperator.Value}");
                }

                if (filter.NullValue.HasValue)
                {
                    parameters.Add($"Filters[{i}].NullValue={filter.NullValue.Value}");
                }

                i++;
            }

            var request = string.Join('&', parameters);

            return request;
        }
    }
}
