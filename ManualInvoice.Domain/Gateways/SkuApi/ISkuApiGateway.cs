﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways.SkuApi
{
    public interface ISkuApiGateway
    {
        Task<ResponseDto<IEnumerable<SkuDto>>> GetAsync(
            int skip, int take, string orderBy, bool orderByDesc, string name, InvoiceType invoiceType);
        Task<SkuDto> GetSkuByCode(string skuCode, InvoiceType invoiceType);
        Task<string> GetCityCodeBySku(string skuCode);
        Task<SkuDto> GetByCodeAsync(string code, InvoiceType invoiceType);
        Task<IEnumerable<SkuDto>> GetByCodesAsync(IEnumerable<string> codes, InvoiceType invoiceType);
    }
}
