﻿namespace ManualInvoice.Domain.Gateways.SkuApi.Dtos
{
    public class ResponseSkuDataDto
    {
        public string Status { get; set; }
        public SkuDto Data { get; set; }
        public int TotalRecords { get; set; }
    }
}
