﻿using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.SkuApi.Dtos
{
    [JsonObject]
    public class SkuDto
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "lc116")]
        public string Lc116 { get; set; }

        [JsonProperty(PropertyName = "ncmCode")]
        public string NcmCode { get; set; }

        [JsonProperty(PropertyName = "cityCode")]
        public string CityCode { get; set; }
    }
}
