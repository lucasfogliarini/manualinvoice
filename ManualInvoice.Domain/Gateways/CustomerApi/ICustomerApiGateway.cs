﻿using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways
{
    public interface ICustomerApiGateway
    {
        Task<CustomerDto> GetByCodeAsync(string code);
        Task<ResponseDto<IEnumerable<CustomerDto>>> GetCustomersAsync(CustomerRequestDto requestDto);
        Task<CustomerDto> GetCustomerAndLocationWithValidVigencyAsync(string pfjCode);
    }
}
