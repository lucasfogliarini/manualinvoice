﻿using Newtonsoft.Json;
using System;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public sealed class CustomerAddressDto
    {
        public int Id { get; set; }

        public string Code { get; set; }

        [JsonProperty(PropertyName = "cityName")]
        public string CityName { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string PublicPlace { get; set; }

        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "startdate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "enddate")]
        public DateTime? EndDate { get; set; }
    }
}
