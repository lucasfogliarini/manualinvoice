﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    [JsonObject]
    public sealed class CustomerDto
    {
        public CustomerDto()
        {
            Addresses = new List<CustomerAddressDto>();
        }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string PfjCode { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "cpfcgc")]
        public string CpfCgc { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [JsonProperty(PropertyName = "addresses")]
        public IList<CustomerAddressDto> Addresses { get; set; }

        [JsonProperty(PropertyName = "hasCustomerValidVigency")]
        public bool HasCustomerValidVigency { get; set; }

        [JsonProperty(PropertyName = "hasLocationValidVigency")]
        public bool HasLocationValidVigency { get; set; }

        public bool ExistingPerson { get; set; } = true;

        public string GetFullAddress()
        {
            if (!Addresses.Any())
            {
                return string.Empty;
            }

            var address = Addresses.FirstOrDefault();
            return string.Format("{0}, {1}, {2}, {3}", address.PublicPlace, address.Number, address.CityName, address.State);
        }
    }
}
