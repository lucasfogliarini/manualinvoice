﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public class UserAuthorizationDto
    {
        public string Token { get; set; }
        public IList<string> Permissions { get; set; }
    }
}
