﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public class UserRequestDto
    {
        public string Id { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public string OrderBy { get; set; }
        public bool OrderDesc { get; set; }
        public string NTAccount { get; set; }
        public string ProfileName { get; set; }
        public string[] ProfileNames 
        { 
            get
            {
                return ProfileName == null ? new[] { "ManualInvoice - Approver", "ManualInvoice - Administrator", "ManualInvoice - Standard" } : new[] { ProfileName };
            }
        }
        public bool? IsActive { get; set; }
    }
}
