﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public class ProfilesDto
    {
        public IList<ProfileDto> Data { get; set; }
    }

    public class ProfileDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class UserProfilesDto
    {
        public ProfileDto Profile { get; set; }
    }
}
