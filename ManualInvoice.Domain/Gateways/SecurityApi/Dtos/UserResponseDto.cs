﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public class UserResponseDto
    {
        public UserResponseDto()
        {
            Users = new List<UserResponseItemDto>();
        }

        public int Total { get; set; }
        public IEnumerable<UserResponseItemDto> Users { get; set; }
    }

    public class UserResponseItemDto
    {
        public UserResponseItemDto()
        {
            Profiles = new List<string>();
            Levels = new List<LevelDto>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string NtAccount { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<string> Profiles { get; set; }
        public IEnumerable<LevelDto> Levels { get; set; }
    }
}
