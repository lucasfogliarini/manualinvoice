﻿using ManualInvoice.Domain.Gateways.Dtos;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways
{
    public interface ISecurityApiGateway
    {
        Task<UserAuthorizationDto> GetUserAuthorizationAsync(string userName, string system = "manualinvoice");
        Task<ProfilesDto> GetProfilesAsync();
        Task<UserResponseDto> GetUsersAsync(UserRequestDto userRequest);
    }
}
