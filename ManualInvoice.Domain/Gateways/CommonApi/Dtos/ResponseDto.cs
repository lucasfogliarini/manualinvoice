﻿namespace ManualInvoice.Domain.Gateways.CommonApi.Dtos
{
    public sealed class ResponseDto<T> where T : class
    {
        public string Status { get; set; }
        public T Data { get; set; }
        public int TotalRecords { get; set; }
    }
}
