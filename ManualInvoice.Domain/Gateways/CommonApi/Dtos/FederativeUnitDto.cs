﻿namespace ManualInvoice.Domain.Gateways.CommonApi.Dtos
{
    public sealed class FederativeUnitDto
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
