﻿using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways.CommonApi
{
    public interface ICommonApiGateway
    {
        Task<IList<FederativeUnitDto>> GetAllFederativeUnitsAsync();
    }
}
