﻿namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public class ResponseNopData
    {
        public string Status { get; set; }
        public NopDto Data { get; set; }
        public int TotalRecords { get; set; }
    }
}
