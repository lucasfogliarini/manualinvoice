﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    [JsonObject]
    public class FiscalDocumentDto
    {
        public long Id { get; set; }

        public short SiteCode { get; set; }

        public long DofSequence { get; set; }

        public string DellRecofNumOrder { get; set; }

        public string SerieSubserie { get; set; }

        public string ModoEmissao { get; set; }

        public DateTime? DhEmissao { get; set; }

        public string DestinatarioPfjCodigo { get; set; }

        public string RemetentePfjCodigo { get; set; }

        public decimal ValorTotalContabil { get; set; }

        public string Numero { get; set; }

        public string NopCodigo { get; set; }

        public string CfopCodigo { get; set; }

        public string SisCodigo { get; set; }

        public string DofImportNumero { get; set; }

        public string CtrlSituacaoDof { get; set; }

        public string IndEntradaSaida { get; set; }

        public string DestinatarioLocCodigo { get; set; }

        public decimal PesoBrutoKg { get; set; }

        public decimal PesoLiquidoKg { get; set; }

        public long? DestinatarioPFJVigente { get; set; }

        public long? RemetentePFJVigente { get; set; }

        public string EmitentePFJCodigo { get; set; }

        public string EmitenteLocCodigo { get; set; }

        public string InformanteEstCodigo { get; set; }

        public DateTime? CtrlDtInclusao { get; set; }

        public DateTime? CtrlDtUltAlteracao { get; set; }

        public DateTime? DtFatoGeradorImposto { get; set; }

        public string EDOFCodigo { get; set; }

        public string MDOFCodigo { get; set; }

        public string DellNfType { get; set; }

        public string OrderType { get; set; }

        public decimal ValorTotalFaturado { get; set; }

        public decimal ValorTotalICMS { get; set; }

        public decimal ValorTotalIPI { get; set; }

        public decimal ValorTotalISS { get; set; }

        public decimal ValorTotalCofins { get; set; }

        public decimal ValorTotalSTF { get; set; }

        public decimal ValorTotalSTT { get; set; }

        public string CtrlConteudo { get; set; }

        public string RemetenteLocCodigo { get; set; }

        public decimal? ValorTotalBaseIcms { get; set; }

        public decimal? ValorTotalBaseStf { get; set; }

        public decimal? ValorTotalBaseIpi { get; set; }

        public decimal? ValorTotalBaseStt { get; set; }

        public decimal? ValorTotalBasePisPasep { get; set; }

        public decimal? ValorTotalBaseCofins { get; set; }

        public decimal? ValorTotalPisPasep { get; set; }

        public DateTime? DhProcessManager { get; set; }

        public string AnoMesEmissao { get; set; }

        public string NfeCodSituacao { get; set; }

        public string NfeCodSituacaoSyn { get; set; }

        public string NfeDescSituacao { get; set; }

        public string IndEix { get; set; }

        public string Tipo { get; set; }

        public decimal? VlTotalBaseIcmsPartRem { get; set; }

        public decimal? VlTotalIcmsPartRem { get; set; }

        public decimal? VlTotalBaseIcmsPartDest { get; set; }

        public decimal? VlTotalIcmsPartDest { get; set; }

        public string TxtObservacaoLivre { get; set; }

        public string UfEmbarque { get; set; }

        public string LocalEmbarque { get; set; }

        public string DellUsOrder { get; set; }

        public string IndEscrituracao { get; set; }

        public string NfeLocalizador { get; set; }

        public string TransportadorPfjCodigo { get; set; }
        
        public string TransportadorLocCodigo { get; set; }

        public string IndRespFrete { get; set; }

        public decimal GoodsFiscalAmount { get; set; }

        public decimal OthersFiscalAmount { get; set; }

        public decimal ProvisionOfServicesFiscalAmount { get; set; }

        public decimal ServicesFiscalAmount { get; set; }

        public decimal? VlTotalFcp { get; set; }

        public decimal? VlTotalBaseIcmsFcp { get; set; }

        public decimal? VlTotalFcpSt { get; set; }

        public decimal? VlTotalBaseIcmsFcpSt { get; set; }

        public string UfCodigoDestino { get; set; }

        public List<FiscalDocumentItemDto> Items { get; set; }

        public FiscalDocumentDto()
        {
            Items = new List<FiscalDocumentItemDto>();
        }

        [JsonProperty(PropertyName = "SalesData")]
        public SalesOrderDto SalesOrder { get; set; }

    }
}
