﻿using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    [JsonObject]
    public class FiscalDocumentProductInfoDto
    {
        public string MercCodigo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
