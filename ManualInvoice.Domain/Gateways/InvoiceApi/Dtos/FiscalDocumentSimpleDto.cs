﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public class FiscalDocumentSimpleDto
    {
        public long Id { get; set; }
        public string OrderNumber { get; set; }
        public string Serie { get; set; }
        public string NfOrRpsNumber { get; set; }
        public string ShipFrom { get; set; }
        public string ShipTo { get; set; }
        public List<FiscalDocumentItemSimpleDto> Items { get; set; }
        public string NopCodigo { get; set; }
        public string SissCodigo { get; set; }
        public string DofImportNumber { get; set; }
        [JsonProperty(PropertyName = "ValorTotalFaturado")]
        public decimal TotalInvoicedAmount { get; set; }
        [JsonProperty(PropertyName = "InformanteEstCodigo")]
        public string Branch { get; set; }
    }
}
