﻿using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    [JsonObject]
    public class FiscalDocumentServiceInfoDto
    {
        public string SissCodigo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
