﻿using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public sealed class LoadVolumeDto
    {
        public short SiteCode { get; set; }
        public long InvoiceSequence { get; set; }
        public string EspecieVolumes { get; set; }
        [JsonProperty(PropertyName = "qtyVolumes")]
        public long? QuantiyVolumes { get; set; }
        public string BrandVolumes { get; set; }
        public string NumberVolumes { get; set; }
        public long RepackageWeight { get; set; }
        public decimal? GrossWeightKg { get; set; }
        public decimal? NetWeightKg { get; set; }
    }
}