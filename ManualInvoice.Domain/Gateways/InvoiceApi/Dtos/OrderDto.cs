﻿using ManualInvoice.Domain.Entities;


namespace ManualInvoice.Domain.Gateways.Dtos
{
    public sealed class OrderDto
    {
        private readonly string _manualInvoiceSystemId = "04";
        private readonly char _systemFlag = ' ';
        private readonly int _indRespFreight = 1;
        private readonly char _issuerMode = 'C';

        public OrderDto()
        {
            SourceSystemID = _manualInvoiceSystemId;
            SalesRepNumber = string.Empty;
            SalesRepName = string.Empty;
            TotalAmountLC = decimal.Zero;
            TotalAmountUSD = decimal.Zero;
            SystemFlag = _systemFlag;
            NfType = string.Empty;
            OrderType = string.Empty;
            PriceAdjustValue = decimal.Zero;
            IndRespFreight = _indRespFreight;
            CustomerClassCode = string.Empty;
            MnAuditReason = string.Empty;
            RefOrderNumber = string.Empty;
            InfoShipping = decimal.Zero;
            SourceTypeCode = null;
            IssuerMode = _issuerMode;
        }

        public string SourceSystemID { get; set; }
        public string OrderID { get; set; }
        public string OrderSeries { get; set; }
        public string OrderNopCode { get; set; }
        public string DofType { get; set; }
        public string ShipToCustID { get; set; }
        public string ShipToCustLocID { get; set; }
        public string InformantID { get; set; }
        public string IssuerID { get; set; }
        public string IssuerLocID { get; set; }
        public char IssuerMode { get; set; }
        public decimal Shipping { get; set; }
        public decimal Duties { get; set; }
        public char IndInOut { get; set; }
        public string PurchaseNum { get; set; }
        public string Instructions { get; set; }
        public string Paymnt1 { get; set; }
        public string Paymnt2 { get; set; }
        public string Paymnt3 { get; set; }
        public string Comments { get; set; }
        public string SalesRepNumber { get; set; }
        public string SalesRepName { get; set; }
        public decimal TotalAmountLC { get; set; }
        public decimal TotalAmountUSD { get; set; }
        public string NfCustID { get; set; }
        public string NfCustLocID { get; set; }
        public char SystemFlag { get; set; }
        public string NfType { get; set; }
        public string OrderType { get; set; }
        public decimal PriceAdjustValue { get; set; }
        public int? IndRespFreight { get; set; }
        public string CustomerClassCode { get; set; }
        public decimal PaymentAmount1 { get; set; }
        public decimal PaymentAmount2 { get; set; }
        public decimal PaymentAmount3 { get; set; }
        public string MnAuditReason { get; set; }
        public string RefOrderNumber { get; set; }
        public decimal InfoShipping { get; set; }
        public decimal InfoDuties { get; set; }
        public decimal ExchangeRate { get; set; }
        public string SourceTypeCode { get; set; }
        public string NsuPmt1 { get; set; }
        public string NsuPmt2 { get; set; }
        public string NsuPmt3 { get; set; }
        public string ProviderPmt1 { get; set; }
        public string ProviderPmt2 { get; set; }
        public string ProviderPmt3 { get; set; }
        public OrderLineItemDto[] Items { get; set; }
    }
}
