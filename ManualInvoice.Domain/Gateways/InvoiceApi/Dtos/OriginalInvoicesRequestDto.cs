﻿namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public class OriginalInvoicesRequestDto
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public string OrderBy { get; set; }
        public bool OrderDesc { get; set; }
        public string Id { get; set; }
        public string BranchId { get; set; }
        public string Number { get; set; }
        public string Order { get; set; }
        public string DofImportNumber { get; set; }
        public string Nop { get; set; }
        public string TransactionType { get; set; }
    }
}
