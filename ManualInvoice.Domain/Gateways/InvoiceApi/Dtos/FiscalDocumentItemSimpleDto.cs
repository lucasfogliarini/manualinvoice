﻿namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public class FiscalDocumentItemSimpleDto
    {
        public long Id { get; set; }
        public long IdfNum { get; set; }
        public string Lc116Code { get; set; }
        public string SissCodigo { get; set; }
    }
}
