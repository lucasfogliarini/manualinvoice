﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    namespace ManualInvoice.Domain.Gateways.Dtos
    {
        public class ResponseDto<T> where T : class
        {
            public string Status { get; set; }
            public T Data { get; set; }
            public int TotalRecords { get; set; }
        }
    }
}
