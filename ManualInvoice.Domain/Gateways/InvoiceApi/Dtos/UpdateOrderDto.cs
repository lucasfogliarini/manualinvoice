﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    public sealed class UpdateOrderDto
    {
        public string[] DofImportNumbers { get; set; }

        public string CarrierCode { get; set; }

        public string CarrierLocCode { get; set; }

        public decimal CarrierWeight { get; set; }

        public string OrderNumber { get; set; }

        public string ServiceTag { get; set; }

        public string ShipmentPlace { get; set; }

        public string ShipmentState { get; set; }
    }
}
