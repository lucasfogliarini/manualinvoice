﻿namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public class GetInvoicesByFilterDto
    {
        public GetInvoicesByFilterDto(string field, string value, string filter)
        {
            Field = field;
            Value = value;
            Operator = filter;
        }

        public string Field { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }
}
