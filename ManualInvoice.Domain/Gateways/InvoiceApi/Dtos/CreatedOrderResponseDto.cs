﻿namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    public sealed class CreatedOrderResponseDto
    {
        public bool Success { get; set; }

        public string Error { get; set; }
    }
}
