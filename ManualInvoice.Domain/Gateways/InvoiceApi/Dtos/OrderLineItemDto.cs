﻿using System;
using ManualInvoice.Domain.Enum;


namespace ManualInvoice.Domain.Entities
{
    public sealed class OrderLineItemDto
    {
        private readonly int _maxLengthItemCode = 20;
        private readonly char _isItemBase = 'Y';
        private readonly char _hasIcmsIncluded = 'S';
        private readonly string _tieNumber = "001";

        public OrderLineItemDto()
        {
            BaseFlag = _isItemBase;
            TieNumber = _tieNumber;
            IcmsIncluded = _hasIcmsIncluded;
            LOB = string.Empty;
            Source = string.Empty;
            NfId = string.Empty;
            CFOPCode = string.Empty;
            STPCode = string.Empty;
            STCCode = string.Empty;
        }

        public OrderLineItemDto(long itemLine, string itemCode, string nopCode, decimal price, int quantity, LineItemType itemType, char? itemBase, string dellTieNum)
            : this()
        {
            Id = itemLine.ToString();

            if (!string.IsNullOrEmpty(itemCode))
            {
                ItemCode = itemCode.Length > _maxLengthItemCode ? itemCode.Substring(0, _maxLengthItemCode) : itemCode;
            }

            ItemType = (int)itemType;

            NOPCode = !string.IsNullOrEmpty(nopCode) ? nopCode.ToUpperInvariant() : string.Empty;
            UnitPrice = price;
            Quantity = quantity;
            BaseFlag = itemBase.HasValue ? itemBase.Value : _isItemBase;
            TieNumber = !string.IsNullOrEmpty(dellTieNum) ? dellTieNum : _tieNumber;
        }

        public string Id { get; set; }
        public string ItemCode { get; set; }
        public int ItemType { get; set; }
        public string NOPCode { get; set; }
        public char BaseFlag { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string TieNumber { get; set; }
        public char IcmsIncluded { get; set; }
        public string LOB { get; set; }
        public string Source { get; set; }
        public string NfId { get; set; }
        public string CFOPCode { get; set; }
        public string STPCode { get; set; }
        public string STCCode { get; set; }
    }
}
