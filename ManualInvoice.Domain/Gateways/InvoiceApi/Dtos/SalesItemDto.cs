﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    [JsonObject]
    public class SalesItemDto
    {
        public short SiteCode { get; set; }

        public long InvoiceSequence { get; set; }

        public long ItemNumber { get; set; }

        public string ItemLineOfBusiness { get; set; }

        public string SequenceNumber { get; set; }

        public decimal? ItemUnitAsSoldPrice { get; set; }

        public decimal? ItemUnitListPrice { get; set; }

        public decimal? ItemUnitCostAmount { get; set; }

        public decimal? ItemUnitCostAmountUsd { get; set; }

        public DateTime? ItemCostDate { get; set; }

        public string DellTieNumber { get; set; }

        public string DiscountClassCode { get; set; }

        public DateTime? AposStartDate { get; set; }

        public DateTime? AposEndDate { get; set; }

        public long? ItemQuantity { get; set; }

        public string FulfillmentLocationId { get; set; }

        public decimal? SourceUnitPrice { get; set; }

        public string DirectOrigRefOrder { get; set; }

        public long? DirectOrigRefItem { get; set; }
    }
}
