﻿using ManualInvoice.Domain.Enum;
using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.InvoiceApi.Dtos
{
    [JsonObject]
    public class NopDto
    {
        [JsonProperty(PropertyName = "nopCodigo")]
        public string NopCode { get; set; }

        [JsonProperty(PropertyName = "indMercPresServ")]
        [JsonConverter(typeof(NopTypeConverter))]
        public NopType NopType { get; set; }

        [JsonProperty(PropertyName = "indEntradaSaida")]
        public string TransactionType { get; set; }

    }
}
