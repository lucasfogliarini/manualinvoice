﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    [JsonObject]
    public class SalesOrderDto
    {
        public long InvoiceSequence { get; set; }

        public short SiteCode { get; set; }

        public string RefOrderNumber { get; set; }

        public string CustomerClassCode { get; set; }

        public decimal? PaymentAmount1LocalCurrency { get; set; }

        [JsonProperty("PaymoutAmount2LocalCurrency")]
        public decimal? PaymentAmount2LocalCurrency { get; set; }

        [JsonProperty("PaymoutAmount3LocalCurrency")]
        public decimal? PaymentAmount3LocalCurrency { get; set; }

        public string ManufacturingAuditReason { get; set; }

        public decimal? ShippingCost { get; set; }

        public decimal? DutyCost { get; set; }

        public decimal? OrderRate { get; set; }

        public string SpecialInstructions { get; set; }

        public string PaymentTermCode { get; set; }

        public string Promotion1 { get; set; }

        public string Promotion2 { get; set; }

        public string Promotion3 { get; set; }

        public DateTime? ShipDate { get; set; }

        public string SourceTypeCode { get; set; }

        public string PaymentProvider1 { get; set; }

        public string PaymentProvider2 { get; set; }

        public string PaymentProvider3 { get; set; }

        public string PaymentUniqueSequenceNumber1 { get; set; }

        public string PaymentUniqueSequenceNumber2 { get; set; }

        public string PaymentUniqueSequenceNumber3 { get; set; }

        public DateTime? OrderCreationDate { get; set; }

        public string DellProfessionalServicesType { get; set; }

        public string DirectOrderType { get; set; }

        public IList<SalesItemDto> Items { get; set; }
    }
}
