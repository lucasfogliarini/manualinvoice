﻿using ManualInvoice.Domain.Converters;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using Newtonsoft.Json;

namespace ManualInvoice.Domain.Gateways.Dtos
{
    [JsonObject]
    public class FiscalDocumentItemDto
    {
        public long Id { get; set; }
        public long DofId { get; set; }
        public short SiteCode { get; set; }
        public long DofSequence { get; set; }
        public long IdfNum { get; set; }
        [JsonConverter(typeof(DellBaseFlagTypeConverter))]
        public DellBaseFlagType BaseFlag { get; set; }
        public string MercCodigo { get; set; }
        public string PresCodigo { get; set; }
        public decimal Qty { get; set; }
        public decimal AidomsQuantity { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public decimal? ValorIcms { get; set; }
        public decimal? ValorIpi { get; set; }
        public decimal? ValorIss { get; set; }
        public decimal? ValorIcmsSt { get; set; }
        public string NbmCodigo { get; set; }
        public string CfopCodigo { get; set; }
        public string NopCodigo { get; set; }
        public string SissCodigo { get; set; }
        public decimal ValorContabil { get; set; }
        public decimal? AliqIpi { get; set; }
        public decimal? AliqIcms { get; set; }
        public decimal? AliqCofins { get; set; }
        public decimal ValorBaseStf { get; set; }
        public decimal AliqStf { get; set; }
        public decimal ValorBaseStt { get; set; }
        public decimal AliqStt { get; set; }
        public decimal ValorStt { get; set; }
        public decimal ValorTributavelIcms { get; set; }
        public decimal ValorTributavelStf { get; set; }
        public decimal ValorTributavelStt { get; set; }
        public decimal ValorTributavelIpi { get; set; }
        public decimal? ValorBaseIcms { get; set; }
        public decimal ValorBaseIpi { get; set; }
        public decimal? ValorAliqPis { get; set; }
        public decimal? ValorAliqCofins { get; set; }
        public decimal? PercentualIsentoIcms { get; set; }
        public decimal? PercentualIsentoIpi { get; set; }
        public decimal? PercentualTributavelIcms { get; set; }
        public decimal? PercentualTrubutavelIpi { get; set; }
        public decimal? PercentualTrubutavelStf { get; set; }
        public decimal? PercentualTrubutavelStt { get; set; }
        public decimal? PercentualTrubutavelPis { get; set; }
        public decimal? PercentualTrubutavelCofins { get; set; }
        public decimal? ValorIsentoIcms { get; set; }
        public decimal? ValorIsentoIpi { get; set; }
        public decimal? ValorBasePis { get; set; }
        public decimal? ValorBaseCofins { get; set; }
        public decimal? ValorImpostoPis { get; set; }
        public decimal? ValorImpostoCofins { get; set; }
        public decimal ValorFiscal { get; set; }
        public decimal ValorFaturado { get; set; }
        public decimal? ValorBaseIcmsDescL { get; set; }
        public decimal? ValorTributavelIcmsDescL { get; set; }
        public decimal? ValorIcmsDescL { get; set; }
        public decimal? AliqIcmsDescL { get; set; }
        public string ComplementType { get; set; }
        public decimal? PercIcmsPartRem { get; set; }
        public decimal? VlBaseIcmsPartRem { get; set; }
        public decimal? VlIcmsPartRem { get; set; }
        public decimal? AliqIcmsPartRem { get; set; }
        public decimal? PercIcmsPartDest { get; set; }
        public decimal? VlBaseIcmsPartDest { get; set; }
        public decimal? VlIcmsPartDest { get; set; }
        public decimal? AliqIcmsPartDest { get; set; }
        public string NumeroOrdemCompra { get; set; }
        public string NumeroLinhaOrdemCompra { get; set; }
        public decimal? AliqPis { get; set; }
        public string DellTieNum { get; set; }
        public decimal? AliqIcmsFcp { get; set; }
        public decimal? AliqIcmsFcpSt { get; set; }
        public decimal? VlIcmsFcp { get; set; }
        public decimal? VlIcmsFcpSt { get; set; }
        public decimal? VlBaseIcmsFcp { get; set; }
        public decimal? VlBaseIcmsFcpSt { get; set; }
        public decimal? VlBaseStf60 { get; set; }
        public decimal? AliqIcmsStf60 { get; set; }
        public decimal? VlStf60 { get; set; }
        public decimal? VlStf60Proprio { get; set; }
        [JsonProperty(PropertyName = "SalesData")]
        public SalesItemDto SalesItem { get; set; }
        [JsonProperty(PropertyName = "Product")]
        public FiscalDocumentProductInfoDto ProductInfo { get; set; }
        [JsonProperty(PropertyName = "ServiceIss")]
        public FiscalDocumentServiceInfoDto ServiceInfo { get; set; }
    }
}
