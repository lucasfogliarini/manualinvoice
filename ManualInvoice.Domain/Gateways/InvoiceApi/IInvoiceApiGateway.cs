﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways
{
    public interface IInvoiceApiGateway
    {
        Task<List<FiscalDocumentDto>> GetSynchroInvoices(IEnumerable<long> ids);
        Task<NopDto> GetNopByCode(string nopCode);
        Task<ResponseDto<IList<FiscalDocumentSimpleDto>>> GetOriginalInvoices(OriginalInvoicesRequestDto originalInvoicesRequestDto);
        Task<CreatedOrderResponseDto> CreateOrder(OrderDto body);
        Task<bool> IsOrderNumberValidAsync(string orderNumber);
        Task<int> GetMaxItemsPerInvoice();
        Task<bool> CreateOrderOnSynchro(SalesOrderDto salesOrderDto);
        Task<List<long>> UpdateOnSynchro(UpdateOrderDto updateOrderDto);
        Task<bool> CreateLoadVolume(LoadVolumeDto body);
        Task<bool> UpdateOrderOnSynchro(SalesOrderDto salesOrderDto);
        Task<SalesOrderDto> GetSalesOrderDataByDofSequence(long dofSequence);
        Task<bool> IssueInvoice(long dofId);
    }
}
