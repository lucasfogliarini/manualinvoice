﻿namespace ManualInvoice.Domain.Gateways.DirectApi.Dtos
{
    public sealed class CreateDofConsolidationDto
    {
        public CreateDofConsolidationDto()
        {
        }

        public CreateDofConsolidationDto(long invoiceId, long consolidatedInvoiceId, string laOrderNumber, int sourceId, string consolidationId)
            : this()
        {
            InvoiceId = invoiceId;
            ConsolidatedInvoiceId = consolidatedInvoiceId;
            LaOrderNumber = laOrderNumber;
            SourceId = sourceId;
            ConsolidationId = consolidationId;
        }

        public long InvoiceId { get; set; }

        public long ConsolidatedInvoiceId { get; set; }

        public string LaOrderNumber { get; set; }

        public int SourceId { get; set; }

        public string ConsolidationId { get; set; }
    }
}
