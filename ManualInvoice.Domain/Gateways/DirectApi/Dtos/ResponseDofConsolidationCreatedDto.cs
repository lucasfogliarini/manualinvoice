﻿using System;

namespace ManualInvoice.Domain.Gateways.DirectApi.Dtos
{
    public sealed class ResponseDofConsolidationCreatedDto
    {
        public Guid Id { get; set; }
        public long InvoiceId { get; set; }
        public long ConsolidatedInvoiceId { get; set; }
        public string LaOrderNumber { get; set; }
        public int SourceId { get; set; }
        public string ConsolidationId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
