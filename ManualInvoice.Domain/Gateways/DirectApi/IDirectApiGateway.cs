﻿using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Gateways
{
    public interface IDirectApiGateway
    {
        Task<IList<ResponseDofConsolidationCreatedDto>> CreateAsync(IList<CreateDofConsolidationDto> dofConsolidations);
        Task<long[]> GetDofIdsConsolidatedsAsync(long[] dofIds);
    }
}
