﻿namespace ManualInvoice.Domain.Exceptions
{
    public interface IForbiddenException : IDomainException
    {
    }
}
