﻿namespace ManualInvoice.Domain.Exceptions
{
    public interface IDuplicatedException : IDomainException
    {
    }
}
