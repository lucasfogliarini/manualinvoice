﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ManualInvoice.Domain.Exceptions
{
    public class ForbiddenException : Exception, IForbiddenException
    {
        public string Title { get; }
        public string UserName { get; }
        
        public ForbiddenException(string userName)
            : base(BuildErrorMessage(userName))
        {
            Title = $"Forbidden";
        }

        public ForbiddenException(string userName, string additionalMessage)
            : base(BuildErrorMessage(userName, additionalMessage))
        {
            Title = $"Forbidden";
        }

        protected ForbiddenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Title = info.GetString(nameof(Title));
            UserName = info.GetString(nameof(UserName));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(nameof(Title), Title);
            info.AddValue(nameof(UserName), UserName);
            base.GetObjectData(info, context);
        }

        private static string BuildErrorMessage(string userName, string additionalMessage = null)
        {
            var message = $"{userName} cannot perform this operation";

            if (!string.IsNullOrWhiteSpace(additionalMessage))
            {
                message += $"{Environment.NewLine}{additionalMessage}";
            }

            return message;
        }
    }
}
