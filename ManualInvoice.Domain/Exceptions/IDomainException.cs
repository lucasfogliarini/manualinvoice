﻿using System.Collections.Generic;

namespace ManualInvoice.Domain.Exceptions
{
    public interface IDomainException
    {
        string Title { get; }
    }
}
