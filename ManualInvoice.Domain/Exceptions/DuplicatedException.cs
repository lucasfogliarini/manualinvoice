﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ManualInvoice.Domain.Exceptions
{
    public class DuplicatedException : Exception, IDuplicatedException
    {
        public string Title { get; }
        public string ResourceName { get; }
        public IDictionary<string, object> Errors { get; }

        public DuplicatedException(string resourceName, IDictionary<string, object> errors)
            : base(BuildErrorMessage(resourceName, errors))
        {
            ResourceName = resourceName;
            Title = $"{resourceName} was duplicated";
        }

        public DuplicatedException(string resourceName, string title, IDictionary<string, object> errors)
            : base(BuildErrorMessage(resourceName, errors))
        {
            ResourceName = resourceName;
            Title = title;
        }

        protected DuplicatedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Title = info.GetString(nameof(Title));
            ResourceName = info.GetString(nameof(ResourceName));
            Errors = info.GetValue(nameof(Errors), typeof(IDictionary<string, object>)) as IDictionary<string, object>;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(nameof(Title), Title);
            info.AddValue(nameof(ResourceName), ResourceName);
            info.AddValue(nameof(Errors), Errors);
            base.GetObjectData(info, context);
        }

        private static string BuildErrorMessage(string resourceName, IEnumerable<KeyValuePair<string, object>> errors)
        {
            var properties = errors.Select(x => $"{Environment.NewLine}{x.Key}: \"{x.Value}\"");
            return $"There is already a {resourceName} with these properties: {string.Join(string.Empty, properties)}";
        }
    }
}
