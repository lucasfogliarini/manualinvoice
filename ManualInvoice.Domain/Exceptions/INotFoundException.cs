﻿namespace ManualInvoice.Domain.Exceptions
{
    public interface INotFoundException : IDomainException
    {
    }
}
