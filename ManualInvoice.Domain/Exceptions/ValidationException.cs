﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ManualInvoice.Domain.Exceptions
{
    [Serializable]
    public class ValidationException : Exception, IValidationException
    {
        public string Title { get; set; } = "Validation Error";
        public IList<ValidationFailure> Errors { get; set; } = new List<ValidationFailure>();

        public ValidationException(IList<ValidationFailure> validationFailures)
            : base(BuildErrorMessage(validationFailures))
        {
        }

        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Title = info.GetString(nameof(Title));
            Errors = info.GetValue(nameof(Errors), typeof(IList<ValidationFailure>)) as IList<ValidationFailure>;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(nameof(Title), Title);
            info.AddValue(nameof(Errors), Errors, typeof(IList<ValidationFailure>));
            base.GetObjectData(info, context);
        }

        private static string BuildErrorMessage(IList<ValidationFailure> validationFailures)
        {
            var properties = validationFailures.Select(x => $"{Environment.NewLine}{x.ErrorMessage}");
            return $"The following validation errors occured: {string.Join(string.Empty, properties)}";
        }
    }
}
