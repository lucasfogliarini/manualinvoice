﻿namespace ManualInvoice.Domain.Exceptions
{
    public interface IValidationException : IDomainException
    {
    }
}
