﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ManualInvoice.Domain.Exceptions
{
    public class NotFoundException<T> : Exception, INotFoundException
    {
        public string Title { get; set; } = "Resource Not Found";
        public Type ResourceType { get; set; } = typeof(T);

        public NotFoundException() : this($"Could not load a resource of {typeof(T).Name} type.")
        {
        }

        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Title = info.GetString(nameof(Title));
            ResourceType = info.GetValue(nameof(ResourceType), typeof(Type)) as Type;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(nameof(Title), Title);
            info.AddValue(nameof(ResourceType), ResourceType, typeof(Type));
            base.GetObjectData(info, context);
        }
    }
}
