﻿namespace ManualInvoice.Domain.Common
{
    public class AppSettings
    {
        public string SecurityKey { get; set; }
        public string NFManualConnectionString { get; set; }
        public string LoggingConnectionString { get; set; }
        public string LoggingProvider { get; set; }
        public string LoggingServerUrl { get; set; }
        public string LoggingServerName { get; set; }
        public string SecurityApiSystem { get; set; }
        public string SecurityApiPassword { get; set; }
        public string SecurityApiUrl { get; set; }
        public string CustomerApiUrl { get; set; }
        public string FiscalDocumentsApiUrl { get; set; }
        public string SkuApiUrl { get; set; }
        public string CommonApiUrl { get; set; }
        public string DirectApiUrl { get; set; }
        public string ManualInvoiceWebUrl { get; set; }

        public bool UseSeq()
        {
            return null != LoggingProvider && LoggingProvider.Contains("seq");
        }
        public bool UseOracleProvider()
        {
            return null != LoggingProvider && LoggingProvider.Contains("oracle_provider");
        }
    }
}