﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class TransactionBranchService : ServiceBase<ITransactionBranchRepository>, ITransactionBranchService
    {
        public TransactionBranchService(IUnitOfWork unitOfWork, ITransactionBranchRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
        }

        public TransactionBranch Get(string id)
        {
            var transactionBranch = MainRepository.QueryComplete().SingleOrDefault(e => e.Id.Equals(id));

            if (transactionBranch == null)
            {
                var message = $"TransactionBranch with id '{id}' was not found.";
                throw new NotFoundException<TransactionBranch>(message);
            }

            return transactionBranch;
        }

        public IEnumerable<Branch> GetBranchesByTransaction(int transactionId, bool onlyActives)
        {
            var query = MainRepository.QueryComplete()
                .Where(x => x.Transaction.Id == transactionId);

            if (onlyActives)
            {
                query = query.Where(x => x.Branch.IsActive == true);
            }

            IList<Branch> result = query
                .Select(x => x.Branch)
                .ToList();

            return result;
        }

        public IQueryable<TransactionBranch> Search()
        {
            return MainRepository.QuerySearch();
        }
    }
}
