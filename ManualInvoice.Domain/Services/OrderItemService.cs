﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class OrderItemService : ServiceBase<IOrderItemRepository>, IOrderItemService
    {
        public OrderItemService(IUnitOfWork unitOfWork, IOrderItemRepository repository)
            : base(unitOfWork, repository)
        {
        }

        public OrderItem Create(Order order, Invoice invoice, InvoiceItem consolidatedItem, FiscalDocumentItemDto originalItem)
        {
            if (order == null)
            {
                return null;
            }

            var entity = new OrderItem(order.Id, invoice.Id, consolidatedItem.Id);

            if (originalItem.SalesItem != null)
            {
                entity.AposEndDate = originalItem.SalesItem.AposEndDate;
                entity.DiscountClassCode = originalItem.SalesItem.DiscountClassCode;
                entity.ItemUnitSoldPriceOrig = originalItem.SalesItem.ItemUnitAsSoldPrice;
                entity.ItemUnitSoldPrice = originalItem.SalesItem.ItemUnitAsSoldPrice;
                entity.ItemUnitListPriceOrig = originalItem.SalesItem.ItemUnitListPrice;
                entity.ItemUnitListPrice = originalItem.SalesItem.ItemUnitListPrice;
                entity.QtyItemOrig = originalItem.SalesItem.ItemQuantity;
                entity.QtyItem = originalItem.AidomsQuantity == 0 ? originalItem.SalesItem.ItemQuantity : (long) originalItem.AidomsQuantity;
                entity.ItemLob = originalItem.SalesItem.ItemLineOfBusiness;
                entity.ItemUnitCostAmtOrig = originalItem.SalesItem.ItemUnitCostAmount;
                entity.ItemUnitCostAmt = originalItem.SalesItem.ItemUnitCostAmount;
                entity.ItemUnitCostAmtUs = originalItem.SalesItem.ItemUnitCostAmountUsd;
                entity.ItemUnitCostAmtUsOrig = originalItem.SalesItem.ItemUnitCostAmountUsd;
                entity.AposStartDate = originalItem.SalesItem.AposStartDate;
                entity.AposEndDate = originalItem.SalesItem.AposEndDate;
                entity.TieNum = originalItem.SalesItem.DellTieNumber;
                entity.FulFillmentLocId = originalItem.SalesItem.FulfillmentLocationId;
            }

            MainRepository.Add(entity);
            UnitOfWork.Commit();

            return entity;
        }

        public IList<OrderItem> Get(long invoiceId)
        {
            if (invoiceId == 0)
            {
                return new List<OrderItem>();
            }

            IList<OrderItem> orderItems = MainRepository.
                QueryComplete(false).
                Where(x => x.Invoice.Id == invoiceId).
                ToList();

            return orderItems;
        }

        public OrderItem Update(OrderItem entity)
        {
            OrderItem orderItem = MainRepository.Find(entity.Id);

            orderItem.Update(entity);
            MainRepository.Update(orderItem);
            UnitOfWork.Commit();

            return orderItem;
        }

        public void Delete(OrderItem entity)
        {
            MainRepository.Delete(entity);
            UnitOfWork.Commit();
        }
    }
}
