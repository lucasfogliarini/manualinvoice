﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Constants;
using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Dtos.InvoiceApprover;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public sealed class InvoiceApproverService : ServiceBase<IInvoiceApproverRepository>, IInvoiceApproverService
    {
        private readonly ITransactionLevelService _transactionLevelService;
        private readonly IUserService _userService;
        private readonly IApprovalViewService _approvalViewService;
        private readonly IEmailService _emailService;
        private readonly IReasonService _reasonService;
        private readonly AppSettings _appSettings;

        public InvoiceApproverService(
            IUnitOfWork unitOfWork,
            IInvoiceApproverRepository mainRepository,
            ITransactionLevelService transactionLevelService,
            IUserService userService,
            IApprovalViewService approvalViewService,
            IEmailService emailService,
            IReasonService reasonService,
            AppSettings appSettings)
            : base(unitOfWork, mainRepository)
        {
            _transactionLevelService = transactionLevelService;
            _userService = userService;
            _approvalViewService = approvalViewService;
            _emailService = emailService;
            _reasonService = reasonService;
            _appSettings = appSettings;
        }

        public async Task Create(long invoiceId, int transactionId)
        {
            IEnumerable<TransactionLevel> transactionLevels = _transactionLevelService.GetActivesLevelsByTransaction(transactionId);

            if (transactionLevels == null || !transactionLevels.Any())
            {
                return;
            }

            UserResponseDto users = new UserResponseDto();
            if (transactionLevels.Any(x => x.Level.Visible != true))
            {
                UserRequestDto userRequestDto = new UserRequestDto()
                {
                    IsActive = true
                };
                users = await _userService.Search(userRequestDto);
            }

            transactionLevels
                .ToList()
                .ForEach(transactionLevel =>
                {
                    Create(invoiceId, transactionLevel, users.Users);
                });

        }

        private void Create(long invoiceId, TransactionLevel transactionLevel, IEnumerable<UserResponseItemDto> users)
        {
            if (transactionLevel.Level.Visible != true)
            {
                var levelUsers = users
                    .Where(u =>
                        u.Profiles != null && (u.Profiles.Contains("Administrator") || u.Profiles.Contains("Approver")) &&
                        u.Levels != null && u.Levels.Any(l => l.Id == transactionLevel.Level.Id));

                foreach (var levelUser in levelUsers)
                {
                    CreateApprover(new InvoiceApprover(invoiceId, transactionLevel.Level.Id, transactionLevel.SequenceApproval, transactionLevel.Level.Visible, int.Parse(levelUser.Id)));
                }
            }
            else
            {
                CreateApprover(new InvoiceApprover(invoiceId, transactionLevel.Level.Id, transactionLevel.SequenceApproval, transactionLevel.Level.Visible));
            }
        }

        private void CreateApprover(InvoiceApprover entity)
        {
            MainRepository.Add(entity);
            UnitOfWork.Commit();
        }

        public async Task<IList<InvoiceApproverResponseDto>> GetByInvoiceAsync(long invoiceId)
        {
            if (invoiceId == 0)
            {
                return new List<InvoiceApproverResponseDto>();
            }

            IList<InvoiceApprover> invoiceApprovers = MainRepository.QueryComplete()
                .Where(x => x.Invoice.Id == invoiceId)
                .OrderBy(i => i.ApprovalSequence)
                .ToList();

            IList<InvoiceApproverResponseDto> result = await CreateApproversResponseAsync(invoiceApprovers);

            return result;
        }

        private async Task<IList<InvoiceApproverResponseDto>> CreateApproversResponseAsync(IList<InvoiceApprover> invoiceApprovers)
        {
            bool isDraft = false;

            if (invoiceApprovers == null || !invoiceApprovers.Any())
            {
                return new List<InvoiceApproverResponseDto>();
            }

            var result = new List<InvoiceApproverResponseDto>();
            IEnumerable<UserResponseItemDto> users = new List<UserResponseItemDto>();

            if (invoiceApprovers.Any(x => x.Invoice.Status == InvoiceStatusType.Draft))
            {
                isDraft = true;
            }

            if (isDraft)
            {
                UserRequestDto userRequestDto = new UserRequestDto()
                {
                    IsActive = true
                };
                var userSearch = await _userService.Search(userRequestDto);
                users = userSearch.Users;
            }

            invoiceApprovers.ToList()
                .ForEach(invoiceApprover =>
                {
                    if ((isDraft && invoiceApprover.IsVisible == true) || (!isDraft && !result.Any(a => a.LevelId == invoiceApprover.Level.Id && invoiceApprover.IsVisible != true)))
                    {
                        result.Add(CreateApproverResponse(invoiceApprover, users));
                    }
                });

            return result;
        }

        private InvoiceApproverResponseDto CreateApproverResponse(InvoiceApprover invoiceApprover, IEnumerable<UserResponseItemDto> users)
        {
            var result = new InvoiceApproverResponseDto();

            if (invoiceApprover.Level == null)
            {
                return result;
            }

            result.LevelId = invoiceApprover.Level.Id;
            result.Level = invoiceApprover.Level.Name;
            result.Id = invoiceApprover.Id;
            result.Situation = invoiceApprover.Situation;
            result.ApprovalSequence = invoiceApprover.ApprovalSequence;
            result.EffectiveUser = invoiceApprover.EffectiveUser?.UserName;
            result.ApprovalDate = invoiceApprover.ApprovalDate;

            if (invoiceApprover.Situation == ApproverStatusType.Rejected)
            {
                result.RejectReason = invoiceApprover.Invoice.RejectReason.Name;
            }

            if (invoiceApprover.IsVisible == true)
            {
                result.UserId = invoiceApprover.User?.Id;
                result.User = invoiceApprover.User?.UserName;

                if (users != null && users.Any())
                {
                    result.Users = users
                        .Where(l =>
                            (l.Profiles.Contains("Administrator") || l.Profiles.Contains("Approver")) &&
                            (l.Levels != null && l.Levels.Any(ln => ln.Id == result.LevelId)))
                        .Select(u => new SelectItemDto() { Id = u.Id, Name = u.NtAccount })
                        .ToList();
                }
            }

            return result;
        }

        public IList<InvoiceApprover> Update(IList<InvoiceApprover> approvers)
        {
            var approversUpdated = new List<InvoiceApprover>();

            if (approvers == null || !approvers.Any())
            {
                return approversUpdated;
            }

            approvers.ToList().ForEach(approver => approversUpdated.Add(Update(approver)));

            return approversUpdated;
        }

        private InvoiceApprover Update(InvoiceApprover approver)
        {
            var entity = MainRepository.QuerySearch()
                .Where(x => x.Id == approver.Id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new NotFoundException<InvoiceApprover>($"InvoiceApprover {approver.Id} not found.");
            }

            if (entity.UserId == approver.UserId)
            {
                return entity;
            }

            entity.UserId = approver.UserId;

            MainRepository.Update(entity);
            UnitOfWork.Commit();

            return entity;
        }

        public void Approve(int id, int effectiveUserId)
        {
            var entity = MainRepository.QueryComplete()
                .Where(x => x.Id == id && x.UserId != null)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new NotFoundException<InvoiceApprover>($"Invoice Approver {id} not found.");
            }

            DateTime approvalDate = DateTime.Now;

            var invisibleApprovers = GetInvisibles(entity);

            entity.Situation = ApproverStatusType.Approved;
            entity.ApprovalDate = approvalDate;
            entity.EffectiveUserId = effectiveUserId;

            MainRepository.Update(entity);

            foreach (var approver in invisibleApprovers)
            {
                approver.Situation = ApproverStatusType.Approved;
                approver.ApprovalDate = approvalDate;
                approver.EffectiveUserId = effectiveUserId;

                MainRepository.Update(approver);
            }
            UnitOfWork.Commit();
        }

        public async Task Analyze(int id)
        {
            var entity = MainRepository.QueryComplete()
                .Where(x => x.Id == id && x.UserId != null)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new NotFoundException<InvoiceApprover>($"Invoice Approver {id} not found.");
            }

            var invoice = entity.Invoice;
            var level = entity.Level;

            var invisibleApprovers = GetInvisibles(entity);

            entity.Situation = ApproverStatusType.Analyzing;

            MainRepository.Update(entity);

            foreach (var approver in invisibleApprovers)
            {
                approver.Situation = ApproverStatusType.Analyzing;

                MainRepository.Update(approver);
            }
            UnitOfWork.Commit();

            IEnumerable<UserResponseItemDto> securityUsers = new List<UserResponseItemDto>();
            IList<UserResponseItemDto> secApproversAndDelegates = new List<UserResponseItemDto>();
            UserResponseItemDto secUserInvoiceCreator = null;

            UserRequestDto userRequestDto = new UserRequestDto()
            {
                IsActive = true
            };
            var requestUsers = await _userService.Search(userRequestDto);
            securityUsers = requestUsers.Users;

            var approversAndDelegates = _approvalViewService.Search(invoice.Id).ToList();

            foreach(var secUser in securityUsers)
            {
                if (secUser.Id != null && approversAndDelegates != null && approversAndDelegates.Any(x => secUser.Id.Equals(x.ApproverUserId.ToString())))
                {
                    secApproversAndDelegates.Add(secUser);
                }

                if (secUser.Id != null && secUser.Id.Equals(invoice.User.Id.ToString()))
                {
                    secUserInvoiceCreator = secUser;
                }
            }

            SendApprovalEmailToApprover(invoice, level, secApproversAndDelegates, approversAndDelegates);
            SendApprovalEmailToCreator(invoice, level, secUserInvoiceCreator, secApproversAndDelegates);
        }

        private void SendApprovalEmailToApprover(Invoice invoice, Level level, IEnumerable<UserResponseItemDto> secApprovers, IEnumerable<ApprovalView> approvers)
        {
            var currentDate = DateTime.Now;

            foreach (var secApprover in secApprovers)
            {
                var approver = approvers.Where(x => secApprover.Id != null && secApprover.Id.Equals(x.ApproverUserId.ToString())).FirstOrDefault();
                if (approver == null)
                {
                    continue;
                }

                int approverId = approver.ApproverId;
                string message = string.Format(EmailMessages.PENDING_LEVEL_APPROVAL_BODY_MESSAGE, invoice.Id, level.Name, secApprover.NtAccount.ToLower());
                string subject = string.Format(EmailMessages.APPROVAL_REQUEST_SUBJECT, invoice.Id);
                string link = $"{_appSettings.ManualInvoiceWebUrl}requestmanualinvoice/requestInvoice/{invoice.Id}/View";

                Email email = new Email()
                {
                    CreateDate = currentDate,
                    Body = message,
                    Link = link,
                    ReceiverEmail = secApprover.Email,
                    ReceiverId = int.Parse(secApprover.Id),
                    RelatedEntityId = approverId,
                    Subject = subject,
                    TypeId = EmailType.PendingLevel,
                    IsSent = false
                };

                _emailService.Create(email);
            }
        }

        private void SendApprovalEmailToCreator(Invoice invoice, Level level, UserResponseItemDto creator, IEnumerable<UserResponseItemDto> approvers)
        {
            if (creator == null)
            {
                return;
            }

            string ntAccounts = string.Join(", ", approvers.Select(x => $"\"{x.NtAccount.ToLower()}\"").ToList());

            string message = string.Format(EmailMessages.PENDING_LEVEL_APPROVAL_INVOICE_USER_BODY_MESSAGE, invoice.Id, level.Name, ntAccounts);
            string subject = string.Format(EmailMessages.APPROVAL_REQUEST_SUBJECT, invoice.Id);
            string link = $"{_appSettings.ManualInvoiceWebUrl}requestmanualinvoice/requestInvoice/{invoice.Id}/View";
        
            Email email = new Email()
            {
                CreateDate = DateTime.Now,
                Body = message,
                Link = link,
                ReceiverEmail = creator.Email,
                ReceiverId = int.Parse(creator.Id),
                RelatedEntityId = invoice.Id,
                Subject = subject,
                TypeId = EmailType.PendingLevel,
                IsSent = false
            };

            _emailService.Create(email);
        }

        public async Task Reject(int id, int effectiveUserId, DateTime rejectDate, int reasonId)
        {
            var entity = MainRepository.QueryComplete()
                .Where(x => x.Id == id && x.UserId != null)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new NotFoundException<InvoiceApprover>($"Invoice Approver {id} not found.");
            }
            var invoice = entity.Invoice;

            var invisibleApprovers = GetInvisibles(entity);

            entity.Situation = ApproverStatusType.Rejected;
            entity.ApprovalDate = rejectDate;
            entity.EffectiveUserId = effectiveUserId;

            MainRepository.Update(entity);

            foreach (var approver in invisibleApprovers)
            {
                approver.Situation = ApproverStatusType.Rejected;
                approver.ApprovalDate = rejectDate;
                approver.EffectiveUserId = effectiveUserId;

                MainRepository.Update(approver);
            }
            UnitOfWork.Commit();

            IEnumerable<UserResponseItemDto> securityUsers = new List<UserResponseItemDto>();
            UserResponseItemDto secUserInvoiceCreator = null;

            UserRequestDto userRequestDto = new UserRequestDto()
            {
                IsActive = true,
                NTAccount = invoice.User.UserName.ToLower()
            };
            var requestCreator = await _userService.Search(userRequestDto);
            secUserInvoiceCreator = requestCreator.Users.FirstOrDefault();

            Reason reason = _reasonService.Get(reasonId);
            User rejector = _userService.Search().Where(x => x.Id == effectiveUserId).FirstOrDefault();

            SendRejectEmailToCreator(invoice, reason, secUserInvoiceCreator, rejector);
        }

        private void SendRejectEmailToCreator(Invoice invoice, Reason reason, UserResponseItemDto creator, User rejector)
        {
            if (creator == null)
            {
                return;
            }

            string message = string.Format(EmailMessages.REJECT_LEVEL_BODY_MESSAGE, rejector.UserName.ToLower(), reason.Name);
            string subject = string.Format(EmailMessages.REJECT_REQUEST_SUBJECT, invoice.Id);
            string link = $"{_appSettings.ManualInvoiceWebUrl}requestmanualinvoice/requestInvoice/{invoice.Id}/View";

            Email email = new Email()
            {
                CreateDate = DateTime.Now,
                Body = message,
                Link = link,
                ReceiverEmail = creator.Email,
                ReceiverId = int.Parse(creator.Id),
                RelatedEntityId = invoice.Id,
                Subject = subject,
                TypeId = EmailType.RejectedInvoice,
                IsSent = false
            };

            _emailService.Create(email);
        }

        private List<InvoiceApprover> GetInvisibles(InvoiceApprover entity)
        {
            return MainRepository.QuerySearch(true)
                .Where(x => x.Invoice.Id == entity.Invoice.Id
                    && x.Level.Id == entity.Level.Id
                    && x.ApprovalSequence == entity.ApprovalSequence
                    && x.Situation == entity.Situation
                    && x.IsVisible != true
                    && x.Id != entity.Id).ToList();
        }

        public IEnumerable<InvoiceApprover> GetByInvoice(long invoiceId)
        {
            return MainRepository.QuerySearch()
                .Where(x => x.Invoice.Id == invoiceId);
        }
    }
}
