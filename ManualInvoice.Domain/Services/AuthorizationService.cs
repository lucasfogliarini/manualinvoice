﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManualInvoice.Domain.Constants;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;

namespace ManualInvoice.Domain.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly ISecurityApiGateway _securityApiGateway;
        private readonly IDelegationViewService _delegationViewService;

        public AuthorizationService(ISecurityApiGateway securityApiGateway, IDelegationViewService delegationViewService)
        {
            _securityApiGateway = securityApiGateway;
            _delegationViewService = delegationViewService;
        }

        public async Task<UserAuthorizationDto> GetUserAuthorizationAsync(string userName, string system = "manualinvoice")
        {
            var userAuthorization = await _securityApiGateway.GetUserAuthorizationAsync(userName, system);
            userAuthorization = await AddPermissionsIfIsDelegated(userAuthorization, userName);
            return userAuthorization;
        }

        private async Task<UserAuthorizationDto> AddPermissionsIfIsDelegated(UserAuthorizationDto userAuthorization, string userName)
        {
            var permissions = new List<string>();
            bool alreadyPermited = false;

            foreach(var delegatedPermission in DelegationPermissions.DELEGATED_PERMISSIONS)
            {
                if (alreadyPermited)
                {
                    break;
                }
                alreadyPermited = userAuthorization.Permissions.Contains(delegatedPermission);
            }

            foreach (var cascadePermission in DelegationPermissions.DELEGATED_CASCADE_PERMISSIONS)
            {
                if (alreadyPermited)
                {
                    break;
                }
                alreadyPermited = userAuthorization.Permissions.Contains(cascadePermission);
            }

            if (!alreadyPermited)
            {
                var hasDelegation = await _delegationViewService.GetDelegation(userName);

                if (hasDelegation.HasDelegation)
                {
                    permissions.AddRange(DelegationPermissions.DELEGATED_PERMISSIONS);

                    if (hasDelegation.HasCascade)
                    {
                        permissions.AddRange(DelegationPermissions.DELEGATED_CASCADE_PERMISSIONS);
                    }
                }

                foreach (var permission in permissions)
                {
                    userAuthorization.Permissions.Add(permission);
                }
            }

            return userAuthorization;
        }
    }
}
