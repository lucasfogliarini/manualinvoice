﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Dtos.InvoiceVariable;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public sealed class InvoiceVariableService : ServiceBase<IInvoiceVariableRepository>, IInvoiceVariableService
    {
        private readonly ITransactionVariableService _transactionVariableService;
        private readonly IFederativeUnitService _federativeUnitService;
        private readonly IPaymentCodeService _paymentCodeService;

        public InvoiceVariableService(
            IUnitOfWork unitOfWork,
            IInvoiceVariableRepository mainRepository,
            ITransactionVariableService transactionVariableService,
            IFederativeUnitService federativeUnitService,
            IPaymentCodeService paymentCodeService)
            : base(unitOfWork, mainRepository)
        {
            _transactionVariableService = transactionVariableService;
            _federativeUnitService = federativeUnitService;
            _paymentCodeService = paymentCodeService;
        }
        
        public IList<InvoiceVariable> Create(long invoiceId, int transactionId, int userId)
        {
            var result = new List<InvoiceVariable>();
            IEnumerable<TransactionVariable> transactionVariables = _transactionVariableService.GetActivesVariablesByTransaction(transactionId);

            if (transactionVariables == null || !transactionVariables.Any())
            {
                return result;
            }

            transactionVariables
                .ToList()
                .ForEach(transactionVariable =>
                {
                    InvoiceVariable invoiceVariableCreated = Create(invoiceId, userId, transactionVariable);
                    result.Add(invoiceVariableCreated);
                });

            return result;
        }
        
        public IList<InvoiceVariable> CreateWithExistsVaribles(long invoiceId, int transactionId, int userId, IList<InvoiceVariable> invoiceVariablesOld)
        {
            IList<InvoiceVariable> invoiceVariableNew = Create(invoiceId, transactionId, userId);

            if (invoiceVariableNew == null || !invoiceVariableNew.Any())
            {
                return new List<InvoiceVariable>();
            }

            if (invoiceVariablesOld == null || !invoiceVariablesOld.Any())
            {
                return invoiceVariableNew;
            }

            invoiceVariableNew.ToList().ForEach(entity => 
            {
                string newValue = invoiceVariablesOld
                    .Where(x => x.Variable.Id == entity.Variable.Id)
                    .Select(x => x.Value)
                    .FirstOrDefault();

                if (!string.IsNullOrEmpty(newValue))
                {
                    entity.Value = newValue;
                    MainRepository.Update(entity);
                    UnitOfWork.Commit();
                }
            });

            return invoiceVariableNew;
        }
        
        public async Task<IList<InvoiceVariableResponseDto>> GetByInvoiceAsync(long invoiceId)
        {
            if (invoiceId == 0)
            {
                return new List<InvoiceVariableResponseDto>();
            }

            IList<InvoiceVariable> invoiceVariables = MainRepository.QueryComplete()
                .Where(x => x.Invoice.Id == invoiceId)
                .ToList();

            IList<InvoiceVariableResponseDto> result = await CreateVariablesResponseAsync(invoiceVariables);

            return result;
        }
        
        public IList<InvoiceVariable> Update(IList<InvoiceVariable> variables)
        {
            var variablesUpdated = new List<InvoiceVariable>();

            if (variables == null || !variables.Any())
            {
                return variablesUpdated;
            }

            variables.ToList().ForEach(variable => variablesUpdated.Add(Update(variable)));

            return variablesUpdated;
        }
        
        public InvoiceVariable Update(InvoiceVariable variable)
        {
            MainRepository.Update(variable);
            UnitOfWork.Commit();

            return variable;
        }
        
        private InvoiceVariable Create(long invoiceId, int userId, TransactionVariable transactionVariable)
        {
            var entity = new InvoiceVariable(invoiceId, transactionVariable.Variable.Id, transactionVariable.IsMandatory, userId, transactionVariable.Variable.DefaultValue);

            MainRepository.Add(entity);
            UnitOfWork.Commit();

            return entity;
        }
        
        private async Task<IList<InvoiceVariableResponseDto>> CreateVariablesResponseAsync(IList<InvoiceVariable> invoiceVariables)
        {
            if (invoiceVariables == null || !invoiceVariables.Any())
            {
                return new List<InvoiceVariableResponseDto>();
            }

            IList<FederativeUnitDto> federativeUnits = new List<FederativeUnitDto>();
            IList<PaymentCode> paymentCodes = new List<PaymentCode>();
            int[] variablesId = invoiceVariables.Where(x => x.Variable != null).Select(x => x.Variable.Id).ToArray();
            IEnumerable<TransactionVariable> transactionVariables = _transactionVariableService.GetByVariables(variablesId);

            if (invoiceVariables.Any(x => x.Variable != null && x.Variable.Resource == ResourceType.FederativeUnit))
            {
                federativeUnits = await _federativeUnitService.GetAllFederativeUnitsAsync();
            }

            if (invoiceVariables.Any(x => x.Variable != null && x.Variable.Resource == ResourceType.PaymentCode))
            {
                paymentCodes = _paymentCodeService.GetAllActives();
            }

            var result = new List<InvoiceVariableResponseDto>();

            invoiceVariables.ToList()
                .ForEach(invoiceVariable =>
                {
                    int order = 0;

                    if (transactionVariables != null && transactionVariables.Any())
                    {
                        TransactionVariable transactionVariable = transactionVariables
                            .FirstOrDefault(x => x.Variable?.Id == invoiceVariable.Variable?.Id);

                        order = transactionVariable != null ? transactionVariable.Id : 0;
                    }

                    result.Add(CreateVariableResponse(invoiceVariable, federativeUnits, paymentCodes, order));
                });

            return result;
        }
        
        private InvoiceVariableResponseDto CreateVariableResponse(InvoiceVariable invoiceVariable, IList<FederativeUnitDto> federativeUnits, IList<PaymentCode> paymentCodes, int order)
        {
            var result = new InvoiceVariableResponseDto();
            result.VariableId = invoiceVariable.Variable.Id;
            result.Value = invoiceVariable.Value;
            result.Required = invoiceVariable.IsMandatory ?? false;
            result.Order = order;

            if (invoiceVariable.Variable == null)
            {
                return result;
            }

            result.IsAlfaNumeric = invoiceVariable.Variable.IsAlphanumeric;
            result.ControlType = invoiceVariable.Variable.IsDropdown ? ControlType.Dropdown : ControlType.Textbox;
            result.Label = invoiceVariable.Variable.Name;
            result.Key = invoiceVariable.Id.ToString();
            result.Length = invoiceVariable.Variable.Length ?? 0;

            if (invoiceVariable.Variable.IsDropdown)
            {
                result.Options = CreateVariableOptions(invoiceVariable.Variable, federativeUnits, paymentCodes);
            }

            return result;
        }
        
        private IList<SelectItemDto> CreateVariableOptions(Variable variable, IList<FederativeUnitDto> federativeUnits, IList<PaymentCode> paymentCodes)
        {
            switch (variable.Resource)
            {
                case ResourceType.PaymentCode:
                    return CreateVariableOptions(paymentCodes);
                case ResourceType.FederativeUnit:
                    return CreateVariableOptions(federativeUnits);
                case ResourceType.NoResource:
                    return new List<SelectItemDto>();
                default:
                    return new List<SelectItemDto>();
            }
        }
        
        private IList<SelectItemDto> CreateVariableOptions(IList<FederativeUnitDto> federativeUnits)
        {
            if (federativeUnits == null || !federativeUnits.Any())
            {
                return new List<SelectItemDto>();
            }

            IList<SelectItemDto> result = federativeUnits
                .Select(federativeUnit => new SelectItemDto()
                {
                    Id = federativeUnit.Code,
                    Name = federativeUnit.Name
                }).ToList();

            return result;
        }
        
        private IList<SelectItemDto> CreateVariableOptions(IList<PaymentCode> paymentCodes)
        {
            if (paymentCodes == null || !paymentCodes.Any())
            {
                return new List<SelectItemDto>();
            }
            IList<SelectItemDto> result = paymentCodes
                .Select(paymentCode =>
                {
                    bool isCredit = false;
                    if (paymentCode.IsCreditCard.HasValue)
                    {
                        isCredit = paymentCode.IsCreditCard.GetValueOrDefault();
                    }

                    var select = new SelectItemDto()
                    {
                        Id = paymentCode.Code,
                        Name = paymentCode.Description,
                        IsCreditCard = isCredit
                    };
                    return select;
                }).ToList();

            return result;
        }

        public IList<InvoiceVariable> GetInvoiceVariablesByInvoiceId(long invoiceId)
        {
            return MainRepository.QueryComplete().Where(x => x.Invoice.Id == invoiceId).ToList();
        }

        public IList<InvoiceVariable> UpdateVariableValue(IList<InvoiceVariable> variableList, VariableType variableType, string newValue)
        {
            variableList.Where(x => x.Variable.Id == (int)variableType).ToList().ForEach(v => v.Value = newValue);
            
            return variableList;
        }

        public InvoiceVariable GetVariable(IList<InvoiceVariable> variableList, VariableType variableType)
        {
            return variableList.FirstOrDefault(item => item.Variable.Id == (int)variableType);
        }
    }
}
