﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class OrderService : ServiceBase<IOrderRepository>, IOrderService
    {
        private readonly IOrderItemService _orderItemService;

        public OrderService(IUnitOfWork unitOfWork, IOrderRepository repository, IOrderItemService orderItemService)
            : base(unitOfWork, repository)
        {
            _orderItemService = orderItemService;
        }

        public Order GetFirstOrder(long invoiceId)
        {
            return MainRepository.QueryComplete().Where(e => e.Invoice.Id == invoiceId).OrderBy(x => x.Id).FirstOrDefault();
        }

        public IList<Order> GetOrders(long invoiceId)
        {
            var orders = MainRepository.QueryComplete().Where(e => e.Invoice.Id == invoiceId).OrderBy(x => x.Id).ToList();

            return orders;
        }

        public Order Get(long orderId)
        {
            return MainRepository.QueryComplete().Where(e => e.Id == orderId).FirstOrDefault();
        }

        public IList<Order> CreateOrders(Invoice invoice, IList<FiscalDocumentDto> originalInvoices)
        {
            var orders = new List<Order>();

            originalInvoices.ToList().ForEach(originalInvoice =>
            {
                Order entity = Create(invoice.Id, originalInvoice);

                InvoiceItem consolidatedItem = invoice.Items.FirstOrDefault();

                originalInvoice.Items.ForEach(item =>
                {
                    if (!invoice.IsConsolidated)
                    {
                        consolidatedItem = invoice.Items.FirstOrDefault(i => i.OriginalDofId == entity.OriginalDofId && i.OriginalIdfNum == item.IdfNum);
                    }

                    OrderItem orderItem = _orderItemService.Create(entity, invoice, consolidatedItem, item);

                    entity.OrderItems.Add(orderItem);
                });

                orders.Add(entity);
            });

            return orders;
        }

        public Order Create(long invoiceId, FiscalDocumentDto fiscalDocument)
        {
            Order entity = new Order(invoiceId, fiscalDocument.Id, fiscalDocument.DellRecofNumOrder);

            if (fiscalDocument.SalesOrder != null)
            {
                entity.CustClassCode = fiscalDocument.SalesOrder.CustomerClassCode;
                entity.OrderCreationDate = fiscalDocument.SalesOrder.OrderCreationDate;
                entity.DpsType = fiscalDocument.SalesOrder.DellProfessionalServicesType;
                entity.OrderRate = fiscalDocument.SalesOrder.OrderRate;
                entity.InfoShipping = fiscalDocument.SalesOrder.ShippingCost;
                entity.InfoDuty = fiscalDocument.SalesOrder.DutyCost;
                entity.DirectOrderType = fiscalDocument.SalesOrder.DirectOrderType;
                entity.PmtAmt1Lc = fiscalDocument.SalesOrder.PaymentAmount1LocalCurrency;
                entity.PmtAmt2Lc = fiscalDocument.SalesOrder.PaymentAmount2LocalCurrency;
                entity.PmtAmt3Lc = fiscalDocument.SalesOrder.PaymentAmount3LocalCurrency;
                entity.MnAuditReason = fiscalDocument.SalesOrder.ManufacturingAuditReason;
                entity.SpecialInstructions = fiscalDocument.SalesOrder.SpecialInstructions;
                entity.PaymentTermCode = fiscalDocument.SalesOrder.PaymentTermCode;
                entity.Promo1 = fiscalDocument.SalesOrder.Promotion1;
                entity.Promo2 = fiscalDocument.SalesOrder.Promotion2;
                entity.Promo3 = fiscalDocument.SalesOrder.Promotion3;
                entity.ShipDate = fiscalDocument.SalesOrder.ShipDate;
                entity.SourceTypeCode = fiscalDocument.SalesOrder.SourceTypeCode;
                entity.PaymentProvider1 = fiscalDocument.SalesOrder.PaymentProvider1;
                entity.PaymentProvider2 = fiscalDocument.SalesOrder.PaymentProvider2;
                entity.PaymentProvider3 = fiscalDocument.SalesOrder.PaymentProvider3;
                entity.PaymentNsu1 = fiscalDocument.SalesOrder.PaymentUniqueSequenceNumber1;
                entity.PaymentNsu2 = fiscalDocument.SalesOrder.PaymentUniqueSequenceNumber2;
                entity.PaymentNsu3 = fiscalDocument.SalesOrder.PaymentUniqueSequenceNumber3;
            }

            MainRepository.Add(entity);
            UnitOfWork.Commit();
            return entity;
        }

        public Order Update(Order entity)
        {
            MainRepository.Update(entity);
            UnitOfWork.Commit();
            return entity;
        }

        public void Delete(Order entity)
        {
            MainRepository.Delete(entity);
            UnitOfWork.Commit();
        }

        public void RemoveOrdersWithoutItems(long InvoiceId) 
        {
            var orders = GetOrders(InvoiceId);

            orders.Where(order => order.OrderItems == null || !order.OrderItems.Any())
                .ToList()
                .ForEach(order => Delete(order));
        }

        public decimal GetShippmentCost(long InvoiceId)
        {
            var orders = GetOrders(InvoiceId);

            decimal shippmentCost = 0;

            if (orders != null && orders.Any())
            {
                shippmentCost = orders.Sum(x => x.InfoShipping ?? 0);
            }

            return shippmentCost;
        }

        public decimal GetDutiesCost(long InvoiceId)
        {
            var orders = GetOrders(InvoiceId);
            
            decimal dutiesCost = 0;

            if (orders != null && orders.Any())
            {
                dutiesCost = orders.Sum(x => x.InfoDuty ?? 0);    
            }

            return dutiesCost;
        }
    }
}