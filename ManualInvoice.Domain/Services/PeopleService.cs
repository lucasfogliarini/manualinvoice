﻿using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly ICustomerApiGateway _customerApiGateway;

        public PeopleService(ICustomerApiGateway customerApiGateway)
        {
            _customerApiGateway = customerApiGateway;
        }

        public async Task<CustomerDto> GetByCodeAsync(string code)
        {
            return await _customerApiGateway.GetByCodeAsync(code);
        }

        public async Task<ResponseDto<IEnumerable<CustomerDto>>> SearchAsync(CustomerRequestDto requestDto)
        {
            return await _customerApiGateway.GetCustomersAsync(requestDto);
        }
    }
}
