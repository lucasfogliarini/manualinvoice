﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class PaymentCodeService : ServiceBase<IPaymentCodeRepository>, IPaymentCodeService
    {
        readonly IRuleValidator _ruleValidator;

        public PaymentCodeService(IUnitOfWork unitOfWork,
              IRuleValidator ruleValidator,
              IPaymentCodeRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
            _ruleValidator = ruleValidator;
        }

        public PaymentCode Get(int id)
        {
            var paymentCode = MainRepository.QuerySearch().SingleOrDefault(e => e.Id == id);

            if (paymentCode == null)
            {
                var message = $"Payment Code with id '{id}' was not found.";
                throw new NotFoundException<Variable>(message);
            }

            return paymentCode;
        }

        public PaymentCode Create(PaymentCode paymentCode)
        {
            _ruleValidator.Validate<PaymentCodeValidator>(paymentCode, true);

            ExistsSameCode(paymentCode);
            ExistsSameDescription(paymentCode);

            MainRepository.Add(paymentCode);

            UnitOfWork.Commit();

            return paymentCode;
        }

        public IList<PaymentCode> GetAllActives()
        {
            var result = MainRepository.QueryComplete()
                .Where(x => x.IsActive)
                .OrderBy(x => x.Description)
                .ToList();

            return result;
        }

        public IQueryable<PaymentCode> Search()
        {
            return MainRepository.QuerySearch();
        }

        public PaymentCode Update(PaymentCode paymentCode)
        {
            _ruleValidator.Validate<PaymentCodeValidator>(paymentCode, true);

            ExistsSameCode(paymentCode);
            ExistsSameDescription(paymentCode);

            MainRepository.Update(paymentCode);

            UnitOfWork.Commit();

            return paymentCode;
        }
        private void ExistsSameCode(PaymentCode paymentcode)
        {
            var errors = new Dictionary<string, object>()
            {
                [nameof(paymentcode.Code)] = paymentcode.Code
            };

            MainRepository.CheckDuplicated(b => b.Id != paymentcode.Id && b.Code == paymentcode.Code, errors);
        }

        private void ExistsSameDescription(PaymentCode paymentcode)
        {
            var errors = new Dictionary<string, object>()
            {
                [nameof(paymentcode.Description)] = paymentcode.Description
            };

            MainRepository.CheckDuplicated(b => b.Id != paymentcode.Id && b.Description == paymentcode.Description, errors);
        }

        public bool IsCreditCard(string code)
        {
            var result = MainRepository.QuerySearch()
                .Where(x => x.Code.Equals(code)).Select(x => x.IsCreditCard.HasValue ? x.IsCreditCard.Value : false).FirstOrDefault();

            return result;
        }
    }
}
