using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class DelegationService : ServiceBase<IDelegationRepository>, IDelegationService
    {
        readonly IRuleValidator _ruleValidator;
        readonly IUserService _userService;

        public DelegationService(IUnitOfWork unitOfWork, IUserService userService, IRuleValidator ruleValidator, IDelegationRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
            _ruleValidator = ruleValidator;
            _userService = userService;
        }

        public async Task<Delegation> Create(Delegation delegation, string[] delegationTo)
        {
            _ruleValidator.Validate<CreateDelegationValidator>(delegation, true);
            delegation.PeriodFrom = delegation.PeriodFrom.Value.Date;
            delegation.PeriodTo = delegation.PeriodTo.Value.Date;
            delegation.DelegationUsers = new List<DelegationUser>();
            foreach (var delTo in delegationTo)
            {
                var user = await _userService.ImportUserAsync(delTo);
                var delegationUser = new DelegationUser()
                {
                    UserId = user.Id
                };
                delegation.DelegationUsers.Add(delegationUser);
            }

            MainRepository.Add(delegation);

            UnitOfWork.Commit();

            return delegation;
        }

        public bool Delete(Delegation delegation)
        {
            MainRepository.Delete(delegation);

            return true;
        }

        public Delegation Get(int id)
        {
            var delegation = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (delegation == null)
            {
                var message = $"Delegation with id '{id}' was not found.";
                throw new NotFoundException<Delegation>(message);
            }

            return delegation;
        }

        public IQueryable<Delegation> Search()
        {
            return MainRepository.QuerySearch().Where(x => x.DelegationUsers.Any(du => du.UserId.HasValue));
        }

        public Delegation Update(Delegation delegation)
        {
            _ruleValidator.Validate<UpdateDelegationValidator>(delegation, true);

            MainRepository.Update(delegation);

            UnitOfWork.Commit();

            return delegation;
        }
    }
}
