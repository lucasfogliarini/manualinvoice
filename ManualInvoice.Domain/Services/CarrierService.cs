﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;

namespace ManualInvoice.Domain.Services
{
    public class CarrierService : ServiceBase<ICarrierRepository>, ICarrierService
    {
        public CarrierService(IUnitOfWork unitOfWork, ICarrierRepository repository)
            : base(unitOfWork, repository)
        {
        }

        public Carrier Create(Carrier carrier)
        {
            MainRepository.Add(carrier);
            UnitOfWork.Commit();

            return carrier;
        }

        public Carrier Update(Carrier carrier)
        {
            MainRepository.Update(carrier);
            UnitOfWork.Commit();

            return carrier;
        }
    }
}
