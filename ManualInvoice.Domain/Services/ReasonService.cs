﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public class ReasonService : ServiceBase<IReasonRepository>, IReasonService
    {
        readonly IRuleValidator _ruleValidator;

        public ReasonService(IUnitOfWork unitOfWork, IRuleValidator ruleValidator, IReasonRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
            _ruleValidator = ruleValidator;
        }

        public Reason Create(Reason reason)
        {
            if (Exists(reason))
            {
                var duplicatedProperties = new Dictionary<string, object>
                {
                    { "Name", reason.Name },
                    { "Type", reason.ReasonType }
                };
                throw new DuplicatedException("Reason", "Can't create Reason", duplicatedProperties);
            }

            _ruleValidator.Validate<CreateReasonValidator>(reason, true);

            MainRepository.Add(reason);

            UnitOfWork.Commit();

            return reason;
        }

        public bool Delete(Reason reason)
        {
            MainRepository.Delete(reason);

            UnitOfWork.Commit();

            return true;
        }

        public Reason Get(int id)
        {
            var reason = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (reason == null)
            {
                var message = $"Reason with id '{id}' was not found.";
                throw new NotFoundException<Reason>(message);
            }

            return reason;
        }

        public IQueryable<Reason> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Reason Update(Reason reason)
        {
            if (Exists(reason))
            {
                var duplicatedProperties = new Dictionary<string, object>
                {
                    { "Name", reason.Name },
                    { "Type", reason.ReasonType }
                };
                throw new DuplicatedException("Reason", "Can't update Reason", duplicatedProperties);
            }

            _ruleValidator.Validate<UpdateReasonValidator>(reason, true);

            MainRepository.Update(reason);

            UnitOfWork.Commit();

            return reason;
        }

        private bool Exists(Reason reason)
        {
            return MainRepository
                .QuerySearch()
                .Any(r => r.Id != reason.Id
                       && r.Name == reason.Name
                       && r.ReasonType == reason.ReasonType);
        }
    }
}
