﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public class ApprovalViewService : ServiceBase<IApprovalViewRepository>, IApprovalViewService
    {
        readonly IUserService _userService;

        public ApprovalViewService(IUnitOfWork unitOfWork,
              IApprovalViewRepository approvalViewRepository,
              IUserService userService)
            : base(unitOfWork, approvalViewRepository)
        {
            _userService = userService;
        }

        public IQueryable<ApprovalView> Search(string userName)
        {
            User user = _userService.GetByName(userName);
            return MainRepository.QuerySearch()
                .Where(x => x.ApproverUserId == user.Id);
        }

        public IQueryable<ApprovalView> Search(long invoiceId)
        {
            return MainRepository.QuerySearch()
                .Where(x => x.InvoiceId == invoiceId);
        }
    }
}
