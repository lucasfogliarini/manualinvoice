﻿using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class NopService : INopService
    {
        private readonly IInvoiceApiGateway _invoiceApiGateway;

        public NopService(IInvoiceApiGateway invoiceApiGateway)
        {
            _invoiceApiGateway = invoiceApiGateway;
        }

        public async Task<NopDto> GetByNopCodeAsync(string nopCode)
        {
            var nop = await _invoiceApiGateway.GetNopByCode(nopCode);
            if (nop == null)
            {
                throw new NotFoundException<NopDto>("NOP not found in Synchro system.");
            }
            return nop;
        }
    }
}
