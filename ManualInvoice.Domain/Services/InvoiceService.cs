﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Dtos.InvoiceApprover;
using ManualInvoice.Domain.Dtos.InvoiceVariable;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static ManualInvoice.Domain.Constants.SynchroOrder;

namespace ManualInvoice.Domain.Services
{
    public sealed class InvoiceService : ServiceBase<IInvoiceRepository>, IInvoiceService
    {
        private readonly IRuleValidator _ruleValidator;
        private readonly IInvoiceApiGateway _invoiceApiGateway;
        private readonly IInvoiceItemService _invoiceItemService;
        private readonly ICarrierService _carrierService;
        private readonly IInvoiceVariableService _invoiceVariableService;
        private readonly ICustomerApiGateway _customerApiGateway;
        private readonly ISkuService _skuService;
        private readonly ITransactionService _transactionService;
        private readonly IPaymentCodeService _paymentCodeService;
        private readonly IOrderService _orderService;
        private readonly IInvoiceApproverService _invoiceApproverService;
        private readonly IBranchService _branchService;
        private readonly IIssuerService _issuerService;
        private readonly ILoadVolumeService _loadVolumeService;
        private readonly IApprovalViewService _approvalViewService;
        private readonly IUserService _userService;
        private readonly IDofConsolidationService _dofConsolidationService;
        private readonly IOrderItemService _orderItemService;

        public InvoiceService(
            IUnitOfWork unitOfWork,
            IRuleValidator ruleValidator,
            IInvoiceRepository repository,
            IInvoiceApiGateway invoiceApiGateway,
            IInvoiceItemService invoiceItemService,
            ICarrierService carrierService,
            IInvoiceVariableService invoiceVariableService,
            ICustomerApiGateway customerApiGateway,
            ISkuService skuService,
            ITransactionService transactionService,
            IPaymentCodeService paymentCodeService,
            IOrderService orderService,
            IInvoiceApproverService invoiceApproverService,
            IBranchService branchService,
            IIssuerService issuerService,
            ILoadVolumeService loadVolumeService,
            IApprovalViewService approvalViewService,
            IUserService userService,
            IDofConsolidationService dofConsolidationService,
            IOrderItemService orderItemService)
            : base(unitOfWork, repository)
        {
            _ruleValidator = ruleValidator;
            _invoiceApiGateway = invoiceApiGateway;
            _invoiceItemService = invoiceItemService;
            _carrierService = carrierService;
            _invoiceVariableService = invoiceVariableService;
            _customerApiGateway = customerApiGateway;
            _skuService = skuService;
            _transactionService = transactionService;
            _paymentCodeService = paymentCodeService;
            _orderService = orderService;
            _invoiceApproverService = invoiceApproverService;
            _branchService = branchService;
            _issuerService = issuerService;
            _loadVolumeService = loadVolumeService;
            _approvalViewService = approvalViewService;
            _userService = userService;
            _dofConsolidationService = dofConsolidationService;
            _orderItemService = orderItemService;
        }

        public async Task<CreatedInvoiceDto> CreateInvoice(int userId, Invoice invoice, Transaction transaction, InvoiceItem consolidatedItem, string[] originalInvoiceIdSelecteds, bool isConsolidation)
        {
            CreatedInvoiceDto result = new CreatedInvoiceDto();

            invoice.User = new User(userId);
            invoice.SetTransactionValues(transaction);

            _ruleValidator.Validate<CreateInvoiceValidator>(invoice, true);

            var nop = await _invoiceApiGateway.GetNopByCode(transaction.BillToCode);

            invoice.IsConsolidated = consolidatedItem != null;
            invoice.InvoiceType = InvoiceType.Service;

            if (nop.NopType == NopType.Product)
            {
                invoice.InvoiceType = InvoiceType.Product;
                invoice.Carrier = _carrierService.Create(new Carrier());
            }

            bool hasOriginalInvoices = originalInvoiceIdSelecteds != null && originalInvoiceIdSelecteds.Any();
            var consolidatedInvoiceDto = new ConsolidatedInvoiceDto();
            var firstInvoiceSelected = new FiscalDocumentDto();

            if (hasOriginalInvoices)
            {
                var originalInvoiceIds = originalInvoiceIdSelecteds.Select(long.Parse).ToArray();
                consolidatedInvoiceDto.OriginalInvoices = await GetSynchroInvoices(originalInvoiceIds);
                consolidatedInvoiceDto.BaseOriginalInvoiceId = originalInvoiceIdSelecteds.FirstOrDefault();

                if (isConsolidation)
                {
                    consolidatedInvoiceDto.ConsolidatedItem = consolidatedItem;

                    string errorDofConsolidated = await ValidateDofConsolidated(isConsolidation, originalInvoiceIds);
                    result.AddError(errorDofConsolidated);

                    if (!result.Success)
                    {
                        return result;
                    }
                }

                firstInvoiceSelected = consolidatedInvoiceDto.OriginalInvoices.FirstOrDefault(x => x.Id.ToString() == consolidatedInvoiceDto.BaseOriginalInvoiceId);

                invoice.Branch = _branchService.GetByName(firstInvoiceSelected.InformanteEstCodigo);
                if (invoice.Branch == null || invoice.Branch.IsActive != true)
                {
                    invoice.Branch = null;
                }

                var customerDto = await _issuerService.GetByPfjCodeAsync(firstInvoiceSelected.DestinatarioPfjCodigo);

                if (customerDto != null && (customerDto.EndDate == null || customerDto.EndDate > DateTime.Now))
                {
                    bool hasAddress = false;

                    if (customerDto.Addresses.Any())
                    {
                        hasAddress = customerDto.Addresses.ToList().Where(x => x.EndDate == null || x.EndDate > DateTime.Now).FirstOrDefault() != null;
                    }

                    if (hasAddress)
                    {
                        invoice.SetInvoiceBillTo(customerDto.PfjCode, customerDto.Name, customerDto.CpfCgc, customerDto.GetFullAddress());
                    }
                }
            }

            invoice = Create(invoice);

            IList<InvoiceItem> originalInvoiceItems = GetItemsFromOriginalInvoices(hasOriginalInvoices, invoice.IsConsolidated, consolidatedInvoiceDto);

            originalInvoiceItems = RenumberDellTieNumbers(originalInvoiceItems);

            _invoiceItemService.Create(originalInvoiceItems, invoice.Id);

            var variables = _invoiceVariableService.Create(invoice.Id, transaction.Id, userId);

            if (hasOriginalInvoices)
            {
                _orderService.CreateOrders(invoice, consolidatedInvoiceDto.OriginalInvoices);

                var orderNumVariable = variables.FirstOrDefault(x => x.Variable.Id == (int)VariableType.OrderNumber);

                if (orderNumVariable != null)
                {
                    var recofNumOrder = firstInvoiceSelected.DellRecofNumOrder?.Substring(0, 9);
                    if (!string.IsNullOrEmpty(recofNumOrder))
                    {
                        orderNumVariable.Value = recofNumOrder;
                        _invoiceVariableService.Update(orderNumVariable);
                    }
                }
            }

            await _invoiceApproverService.Create(invoice.Id, transaction.Id);

            result.SetResponse(invoice);

            return result;
        }

        private IList<InvoiceItem> GetItemsFromOriginalInvoices(bool hasOriginalInvoices, bool isConsolidated, ConsolidatedInvoiceDto consolidatedInvoices)
        {
            if (!hasOriginalInvoices)
            {
                return new List<InvoiceItem>();
            }

            IList<InvoiceItem> result = new List<InvoiceItem>();

            if (isConsolidated)
            {
                result.Add(consolidatedInvoices.ConsolidatedItem);
                return result;
            }

            consolidatedInvoices.OriginalInvoices.ToList()
                .ForEach(originalInvoice =>
                {
                    originalInvoice.Items.OrderBy(i => i.IdfNum).ToList().ForEach(originalInvoiceItem =>
                    {
                        InvoiceItem newItem = new InvoiceItem();

                        newItem.Price = (decimal)originalInvoiceItem.ValorContabil;
                        newItem.Quantity = decimal.ToInt32(originalInvoiceItem.Qty);
                        newItem.PriceOriginal = (decimal)originalInvoiceItem.ValorContabil;
                        newItem.QuantityOriginal = decimal.ToInt32(originalInvoiceItem.Qty);
                        newItem.DellBaseFlag = originalInvoiceItem.BaseFlag;
                        newItem.Ncm = originalInvoiceItem.NbmCodigo;
                        newItem.OriginalDofId = originalInvoice.Id;
                        newItem.OriginalIdfNum = originalInvoiceItem.IdfNum;
                        newItem.DellTieNum = originalInvoiceItem.DellTieNum;
                        newItem.ProductCode = originalInvoiceItem.PresCodigo;

                        if (originalInvoiceItem.MercCodigo != null)
                        {
                            newItem.ProductCode = originalInvoiceItem.MercCodigo;
                            newItem.ProductName = originalInvoiceItem.ProductInfo?.Name;
                        }

                        if (originalInvoiceItem.SissCodigo != null)
                        {
                            newItem.ProductCode = originalInvoiceItem.SissCodigo;
                            newItem.ProductName = originalInvoiceItem.ServiceInfo?.Name;
                        }

                        result.Add(newItem);
                    });
                });

            return result;
        }

        public Invoice Get(long id)
        {
            var invoice = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (invoice == null)
            {
                var message = $"Invoice with id '{id}' was not found.";
                throw new NotFoundException<Invoice>(message);
            }

            return invoice;
        }

        public Invoice Create(Invoice entity)
        {
            MainRepository.Add(entity);
            UnitOfWork.Commit();

            return entity;
        }

        public IQueryable<Invoice> Search()
        {
            return MainRepository.QuerySearch();
        }

        public UpdatedInvoiceResponse Update(long id, Invoice invoice, IList<InvoiceItem> items, IList<InvoiceVariable> variables, IList<InvoiceApprover> approvers)
        {
            var updatedInvoiceResponse = new UpdatedInvoiceResponse();

            var validations = InvoiceValidate(variables, items, invoice.InvoiceType, id);

            if (validations.Any())
            {
                updatedInvoiceResponse.AddError(validations.ToArray());
                return updatedInvoiceResponse;
            }

            var invoiceInDataBase = MainRepository.QuerySearch().FirstOrDefault(x => x.Id == id);

            if (invoiceInDataBase != null)
            {
                invoice.UpdateValues(invoiceInDataBase.IsConsolidated, invoiceInDataBase.SynchroStatus);
            }

            invoice.Id = id;

            _ruleValidator.Validate<UpdateInvoiceValidator>(invoice, true);

            if (invoice.Transaction != null)
            {
                Transaction transaction = _transactionService.Search().Where(x => x.Id == invoice.Transaction.Id).FirstOrDefault();
                invoice.SetTransactionValues(transaction);
            }

            MainRepository.Update(invoice);
            UnitOfWork.Commit();

            var itemsMerged = _invoiceItemService.MergeItems(items, id);

            var carrierUpdated = invoice.Carrier != null ? _carrierService.Update(invoice.Carrier) : null;

            _orderService.RemoveOrdersWithoutItems(invoice.Id);

            var firstOrder = _orderService.GetFirstOrder(invoice.Id);

            if (firstOrder != null)
            {
                var recofNumOrder = firstOrder.RecofNumOrder?.Substring(0, 9);
                var variableOrderNumber = _invoiceVariableService.GetVariable(variables, VariableType.OrderNumber);
                if (variableOrderNumber != null && variableOrderNumber.Value != recofNumOrder && !string.IsNullOrEmpty(recofNumOrder))
                {
                    variables = _invoiceVariableService.UpdateVariableValue(variables, VariableType.OrderNumber, recofNumOrder);
                }
            }

            _invoiceVariableService.Update(variables);
            _invoiceApproverService.Update(approvers);

            invoice.Carrier = carrierUpdated;
            invoice.Items = itemsMerged;

            updatedInvoiceResponse.SetResponse(invoice);

            return updatedInvoiceResponse;
        }

        public async Task<List<FiscalDocumentDto>> GetSynchroInvoices(IEnumerable<long> ids)
        {
            var invoiceDto = await _invoiceApiGateway.GetSynchroInvoices(ids);
            var orderAsReceived = invoiceDto.OrderBy(x => ids.ToList().FindIndex(y => y == x.Id)).ToList();

            return orderAsReceived;
        }

        public async Task<ResponseDto<IList<FiscalDocumentSimpleDto>>> GetOriginalInvoices(OriginalInvoicesRequestDto originalInvoicesRequestDto)
        {
            var originalInvoices = await _invoiceApiGateway.GetOriginalInvoices(originalInvoicesRequestDto);

            foreach (var invoice in originalInvoices.Data)
            {
                var firstItem = invoice.Items.OrderBy(x => x.IdfNum).FirstOrDefault();
                if (firstItem.SissCodigo != null)
                {
                    var shouldReturnException = false;
                    var skuDto = await _skuService.GetBySkuCodeAsync(firstItem.SissCodigo, InvoiceType.Service, shouldReturnException);
                    invoice.Items.OrderBy(x => x.IdfNum).FirstOrDefault().Lc116Code = skuDto?.Lc116;
                }
            }

            return originalInvoices;
        }

        public void Cancel(long id, string userName)
        {
            var invoice = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (invoice == null)
            {
                throw new NotFoundException<Invoice>();
            }

            var isInvoiceCreator = invoice?.User?.UserName
                .Equals(userName, System.StringComparison.InvariantCultureIgnoreCase) == true;

            if (!isInvoiceCreator)
            {
                throw new ForbiddenException(userName, "The Invoice can only be cancelled by its creator");
            }

            _ruleValidator.Validate<CancelInvoiceValidator>(invoice, true);

            invoice.Status = Common.InvoiceStatusType.Cancelled;

            MainRepository.Update(invoice);

            UnitOfWork.Commit();
        }

        public Task<IList<InvoiceVariableResponseDto>> GetInvoiceVariablesAsync(long invoiceId)
        {
            var result = _invoiceVariableService.GetByInvoiceAsync(invoiceId);

            return result;
        }

        public async Task<IList<InvoiceApproverResponseDto>> GetInvoiceApproversAsync(long invoiceId)
        {
            var result = await _invoiceApproverService.GetByInvoiceAsync(invoiceId);

            return result;
        }

        public async Task<StageSalesOrderDto> CreateOrder(long id)
        {
            Invoice invoice = Get(id);

            if (invoice.SynchroStatus.Value == OrderSynchroStatus.Finalized)
            {
                return new StageSalesOrderDto("Cannot create an order for an invoice with Finalized status");
            }

            IList<Order> orders = _orderService.GetOrders(invoice.Id);

            if (invoice.IsConsolidated)
            {
                var dofIds = orders != null && orders.Any() ?
                    orders.Select(x => x.OriginalDofId).ToArray() :
                    new long[0];

                string validationDofConsolidated = await ValidateDofConsolidated(invoice.IsConsolidated, dofIds);

                if (!string.IsNullOrEmpty(validationDofConsolidated))
                {
                    return new StageSalesOrderDto(validationDofConsolidated);
                }
            }

            IList<InvoiceVariable> invoiceVariables = _invoiceVariableService.GetInvoiceVariablesByInvoiceId(id);
            LineItemType itemType = invoice.InvoiceType == InvoiceType.Service ? LineItemType.Service : LineItemType.Part;

            var orderId = invoice.Id.ToString().PadLeft(7, '0');
            var dofImports = GenerateDofImports(invoice, orderId);

            if (invoice.SynchroStatus.Value == OrderSynchroStatus.None)
            {
                var createdOrderResponse = await CreateOrder(orderId, invoice, invoiceVariables, itemType, invoice.BillToCode, invoice.TransactionBillToCode, invoice.BillToCode, false);

                if (!createdOrderResponse.Success)
                {
                    return new StageSalesOrderDto(createdOrderResponse.Error);
                }

                UpdateSynchroStatus(OrderSynchroStatus.StageBillTo, invoice);
            }

            if (!string.IsNullOrEmpty(invoice.ShipToCode) && invoice.InvoiceType.HasValue && invoice.InvoiceType.Value == InvoiceType.Product)
            {
                var createdOrderResponse = await CreateOrder(string.Concat(orderId, DOF_IMPORT_NUMBER_SHIPPING_SUFIX), invoice, invoiceVariables, itemType, invoice.ShipToCode, invoice.TransactionShipToCode, invoice.BillToCode, true);

                if (!createdOrderResponse.Success)
                {
                    return new StageSalesOrderDto(createdOrderResponse.Error);
                }

                UpdateSynchroStatus(OrderSynchroStatus.StageShipTo, invoice);
            }

            UpdateSynchroStatus(OrderSynchroStatus.StageUpdateOrder, invoice);
            var dofs = new List<long>();
            if (invoice.SynchroStatus.Value == OrderSynchroStatus.StageUpdateOrder)
            {
                dofs = await ExecuteStageUpdateOrder(invoice, dofImports, invoiceVariables);
                if (dofs.Any())
                {
                    UpdateSynchroStatus(OrderSynchroStatus.StageLoadVolume, invoice);
                }
                else
                {
                    throw new Exception("Error updating data in Synchro");
                }
            }


            if (invoice.InvoiceType.HasValue && invoice.InvoiceType.Value == InvoiceType.Product)
            {
                await ExecuteStageLoadVolume(invoice, dofs);
            }

            UpdateSynchroStatus(OrderSynchroStatus.StageSalesOrder, invoice);

            await CreateDofConsolidationOnDirect(invoice, orders, dofs);

            return new StageSalesOrderDto(invoice, dofs, orders);
        }

        public async Task ExecuteStageSalesOrder(StageSalesOrderDto stageSalesOrderDto)
        {
            if (stageSalesOrderDto == null || stageSalesOrderDto.Invoice.SynchroStatus.Value != OrderSynchroStatus.StageSalesOrder)
            {
                return;
            }

            Order firstOrder = stageSalesOrderDto.Orders != null && stageSalesOrderDto.Orders.Any() ? stageSalesOrderDto.Orders.OrderBy(x => x.Id).FirstOrDefault() : null;
            IList<InvoiceVariable> invoiceVariables = _invoiceVariableService.GetInvoiceVariablesByInvoiceId(stageSalesOrderDto.Invoice.Id);

            foreach (var dofIdCreated in stageSalesOrderDto.DofImports)
            {
                SalesOrderDto salesOrderDto = CreateSalesOrderDto(firstOrder, stageSalesOrderDto.Invoice, dofIdCreated, invoiceVariables);

                var salesOrderExisting = await _invoiceApiGateway.GetSalesOrderDataByDofSequence(salesOrderDto.InvoiceSequence);

                if (salesOrderExisting != null)
                {
                    await _invoiceApiGateway.UpdateOrderOnSynchro(salesOrderDto);
                }
                else
                {
                    await _invoiceApiGateway.CreateOrderOnSynchro(salesOrderDto);
                }
            }

            UpdateSynchroStatus(OrderSynchroStatus.Finalized, stageSalesOrderDto.Invoice);
        }

        public async Task IssueInvoiceImmediately(StageSalesOrderDto stageSalesOrderDto)
        {
            foreach (var dofId in stageSalesOrderDto.DofImports)
            {
                if (await _invoiceApiGateway.IssueInvoice(dofId))
                {
                    stageSalesOrderDto.IssuedDofs.Add(dofId);
                }
                else
                {
                    stageSalesOrderDto.NotIssuedDofs.Add(dofId);
                }
            }
        }

        private async Task<IList<ResponseDofConsolidationCreatedDto>> CreateDofConsolidationOnDirect(Invoice invoice, IList<Order> orders, IList<long> dofs)
        {
            if (!invoice.IsConsolidated)
            {
                return new List<ResponseDofConsolidationCreatedDto>();
            }

            var result = await _dofConsolidationService.CreateOnDirect(invoice.Id, orders, dofs);

            return result;
        }

        private List<string> GenerateDofImports(Invoice invoice, string orderId)
        {
            var dofImports = new List<string>();
            var dofImportNumber = MANUAL_INVOICE_PREFIX + orderId;
            dofImports.Add(dofImportNumber);
            dofImports.Add(dofImportNumber + DOF_IMPORT_NUMBER_SHIPPING_SUFIX);
            dofImports.Add(dofImportNumber + SERVICE_INVOICE_SUFIX);

            return dofImports;
        }

        private async Task ExecuteStageLoadVolume(Invoice invoice, List<long> dofs)
        {
            if (invoice.SynchroStatus.Value != OrderSynchroStatus.StageLoadVolume)
            {
                return;
            }

            foreach (var dofIdCreated in dofs)
            {
                var dofSequence = GetDofSequence(dofIdCreated.ToString());
                var siteCode = GetSiteCode(dofIdCreated.ToString());
                await _loadVolumeService.CreateLoadVolume(invoice, dofSequence, siteCode);
            }
        }

        private async Task<List<long>> ExecuteStageUpdateOrder(Invoice invoice, List<string> dofImports, IList<InvoiceVariable> invoiceVariables)
        {
            var updateOrderDto = new UpdateOrderDto();
            updateOrderDto.DofImportNumbers = dofImports.ToArray();
            if (invoice.Carrier != null && invoice.InvoiceType.HasValue && invoice.InvoiceType.Value == InvoiceType.Product)
            {
                updateOrderDto.CarrierCode = invoice.Carrier.Code.ToUpper();
                updateOrderDto.CarrierWeight = invoice.Carrier.Weight.HasValue ? invoice.Carrier.Weight.Value : decimal.Zero;
                var customerDto = await _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(invoice.Carrier.Code);
                if (customerDto != null)
                {
                    updateOrderDto.CarrierLocCode = customerDto.Addresses.FirstOrDefault().Code;
                }
                updateOrderDto.ShipmentPlace = GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ShipmentPlace);
                updateOrderDto.ShipmentState = GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ShipmentState);
            }
            updateOrderDto.OrderNumber = GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.OrderNumber);
            updateOrderDto.ServiceTag = GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ServiceTag);

            return await _invoiceApiGateway.UpdateOnSynchro(updateOrderDto);
        }

        private void UpdateSynchroStatus(OrderSynchroStatus status, Invoice invoice)
        {
            invoice.SynchroStatus = status;
            MainRepository.Update(invoice);
            UnitOfWork.Commit();
        }

        private SalesOrderDto CreateSalesOrderDto(Order order, Invoice invoice, long dofIdCreated, IList<InvoiceVariable> invoiceVariables)
        {
            string variablePaymentValue1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue1);
            string variablePaymentValue2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue2);
            string variablePaymentValue3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue3);
            string variableProviderCode1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode1);
            string variableProviderCode2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode2);
            string variableProviderCode3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode3);
            string variableNsuCode1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode1);
            string variableNsuCode2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode2);
            string variableNsuCode3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode3);

            SalesOrderDto dto = new SalesOrderDto();

            dto.InvoiceSequence = GetDofSequence(dofIdCreated.ToString());
            dto.SiteCode = GetSiteCode(dofIdCreated.ToString());
            dto.Items = new List<SalesItemDto>();
            dto.PaymentAmount1LocalCurrency = variablePaymentValue1.ToDecimal();
            dto.PaymentAmount2LocalCurrency = variablePaymentValue2.ToDecimal();
            dto.PaymentAmount3LocalCurrency = variablePaymentValue3.ToDecimal();
            dto.PaymentProvider1 = variableProviderCode1;
            dto.PaymentProvider2 = variableProviderCode2;
            dto.PaymentProvider3 = variableProviderCode3;
            dto.PaymentUniqueSequenceNumber1 = variableNsuCode1;
            dto.PaymentUniqueSequenceNumber2 = variableNsuCode2;
            dto.PaymentUniqueSequenceNumber3 = variableNsuCode3;

            if (order != null)
            {
                dto.CustomerClassCode = order.CustClassCode;
                dto.OrderCreationDate = order.OrderCreationDate;
                dto.OrderRate = order.OrderRate;
                dto.DirectOrderType = DIRECT_OMEGA_ORDER_TYPE;
                dto.DellProfessionalServicesType = order.DpsType;
                dto.RefOrderNumber = order.RecofNumOrder;
            }

            if (invoiceVariables.Any())
            {
                var orderNumberVariableValue = invoiceVariables.Where(variable => variable.Variable.Id == (int)VariableType.OrderNumber).Select(variable => variable.Value).FirstOrDefault();
                if (!string.IsNullOrEmpty(orderNumberVariableValue))
                {
                    dto.RefOrderNumber = orderNumberVariableValue;
                }
            }

            if (invoice.Carrier != null)
            {
                dto.DutyCost = _orderService.GetDutiesCost(invoice.Id);
                dto.ShippingCost = _orderService.GetShippmentCost(invoice.Id);
            }

            if (invoice.Items == null || !invoice.Items.Any())
            {
                return dto;
            }

            var ordersItems = _orderItemService.Get(invoice.Id);
            foreach (var invoiceItem in invoice.Items)
            {
                OrderItem orderItem = null;

                if (ordersItems.Any())
                {
                    orderItem = ordersItems.Where(item => item.Invoice.Id == invoiceItem.Invoice.Id && invoiceItem.Id == item.InvoiceItem.Id).FirstOrDefault();
                }

                dto.Items.Add(CreateSalesItemDto(invoiceItem, orderItem, dto.SiteCode, dto.InvoiceSequence, invoiceVariables));
            }

            return dto;
        }

        private SalesItemDto CreateSalesItemDto(InvoiceItem invoiceItem, OrderItem orderItem, short siteCode, long dofSequence, IList<InvoiceVariable> invoiceVariables)
        {
            SalesItemDto dto = new SalesItemDto();
            dto.InvoiceSequence = dofSequence;
            dto.SiteCode = siteCode;
            dto.ItemNumber = invoiceItem.ItemLine.Value;

            if (orderItem != null)
            {
                dto.AposEndDate = orderItem.AposEndDate;
                dto.AposStartDate = orderItem.AposStartDate;
                dto.DellTieNumber = orderItem.TieNum;
                dto.DiscountClassCode = orderItem.DiscountClassCode;
                dto.FulfillmentLocationId = orderItem.FulFillmentLocId;
                dto.ItemLineOfBusiness = orderItem.ItemLob;
                dto.SequenceNumber = dto.ItemNumber.ToString();
                dto.ItemQuantity = orderItem.QtyItem;
                dto.ItemUnitAsSoldPrice = orderItem.ItemUnitSoldPrice;
                dto.ItemUnitCostAmount = orderItem.ItemUnitCostAmt;
                dto.ItemUnitCostAmountUsd = orderItem.ItemUnitCostAmtUs;
                dto.ItemUnitListPrice = orderItem.ItemUnitListPrice;
                dto.DirectOrigRefOrder = orderItem.Order?.RecofNumOrder;
            }
            dto.DirectOrigRefItem = invoiceItem.OriginalIdfNum;

            return dto;
        }

        private long GetDofSequence(string value)
        {
            return long.Parse(value.Substring(0, value.Length - 3));
        }

        private short GetSiteCode(string value)
        {
            return short.Parse(value.Substring(value.Length - 3));
        }

        private async Task<CreatedOrderResponseDto> CreateOrder(string orderId, Invoice invoice, IList<InvoiceVariable> invoiceVariables, LineItemType itemType, string pfjCode, string nopCode, string pfjCodeBillTo, bool isShipTo)
        {
            string valueFreight = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.Freight);
            string valueInsurance = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.Insurance);
            string valueOtherCosts = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.OtherCosts);
            string variablePONumber = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PONumber);
            string variablePaymentCode1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentCode1);
            string variablePaymentValue1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue1);
            string variableNsuCode1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode1);
            string variableProviderCode1 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode1);
            string variablePaymentCode2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentCode2);
            string variablePaymentValue2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue2);
            string variableNsuCode2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode2);
            string variableProviderCode2 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode2);
            string variablePaymentCode3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentCode3);
            string variablePaymentValue3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.PaymentValue3);
            string variableNsuCode3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.NSUCode3);
            string variableProviderCode3 = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ProviderCode3);
            string variableExchangeRate = this.GetInvoiceVariableValueByInvoiceId(invoiceVariables, VariableType.ExchangeRate);

            string shipToCustLocId = await GetShipToCustLocIdAsync(pfjCode);
            string shipToCustLocIdBillTo = !isShipTo ? shipToCustLocId : await GetShipToCustLocIdAsync(pfjCodeBillTo);

            List<OrderLineItemDto> items = new List<OrderLineItemDto>();

            if (invoice.Items != null && invoice.Items.Any())
            {
                var sortedItems = invoice.Items.OrderBy(item => item.Id).ToList();
                for (int index = 0; index < sortedItems.Count; index++)
                {
                    sortedItems[index].ItemLine = index + 1;

                    _invoiceItemService.Update(sortedItems[index]);
                }

                sortedItems.ToList()
                    .ForEach(item => items.Add(new OrderLineItemDto(item.ItemLine.Value, item.ProductCode, nopCode, item.Price ?? 0, item.Quantity ?? 0, itemType, item.DellBaseFlag == DellBaseFlagType.Unknown ? DellBaseFlagType.Base.GetCharDescription() : item.DellBaseFlag.GetCharDescription(), item.DellTieNum)));
            }

            var orderDto = new OrderDto();

            orderDto.OrderID = orderId;
            orderDto.OrderSeries = invoice.TransactionSerie;
            orderDto.OrderNopCode = nopCode.ToUpperInvariant();
            orderDto.DofType = invoice.TransactionSpecie;
            orderDto.InformantID = invoice.Branch.Name;
            orderDto.IssuerID = invoice.Branch.Name;
            orderDto.IssuerLocID = invoice.Branch.Location;
            orderDto.Shipping = valueFreight.ToDecimal();
            orderDto.Duties = valueInsurance.ToDecimal();
            orderDto.PurchaseNum = variablePONumber;
            orderDto.Instructions = invoice.Instruction;
            orderDto.Paymnt1 = variablePaymentCode1;
            orderDto.Paymnt2 = variablePaymentCode2;
            orderDto.Paymnt3 = variablePaymentCode3;
            orderDto.Comments = invoice.Observation;
            orderDto.PaymentAmount1 = variablePaymentValue1.ToDecimal();
            orderDto.PaymentAmount2 = variablePaymentValue2.ToDecimal();
            orderDto.PaymentAmount3 = variablePaymentValue3.ToDecimal();
            orderDto.InfoDuties = valueOtherCosts.ToDecimal();
            orderDto.ExchangeRate = variableExchangeRate.ToDecimal();
            orderDto.NsuPmt1 = variableNsuCode1;
            orderDto.NsuPmt2 = variableNsuCode2;
            orderDto.NsuPmt3 = variableNsuCode3;
            orderDto.ProviderPmt1 = variableProviderCode1;
            orderDto.ProviderPmt2 = variableProviderCode2;
            orderDto.ProviderPmt3 = variableProviderCode3;
            orderDto.Items = items.ToArray();

            if (invoice.Transaction.TransactionType == TransactionType.Inbound)
            {
                orderDto.ShipToCustID = isShipTo ? pfjCode.ToUpper() : invoice.Branch.Name.ToUpper();
                orderDto.ShipToCustLocID = isShipTo ? shipToCustLocId : invoice.Branch.Location.ToUpper();
                orderDto.IndInOut = 'E';
                orderDto.NfCustID = pfjCodeBillTo;
                orderDto.NfCustLocID = shipToCustLocIdBillTo;
            }
            else
            {
                orderDto.ShipToCustID = pfjCode.ToUpper();
                orderDto.ShipToCustLocID = shipToCustLocId;
                orderDto.IndInOut = 'S';
                orderDto.NfCustID = string.Empty;
                orderDto.NfCustLocID = string.Empty;
            }

            var result = await _invoiceApiGateway.CreateOrder(orderDto);

            return result;
        }

        private string GetInvoiceVariableValueByInvoiceId(IList<InvoiceVariable> variables, VariableType variableType)
        {
            if (variables == null || !variables.Any())
            {
                return string.Empty;
            }

            int variableId = variableType.GetHashCode();

            InvoiceVariable invoiceVariable = variables.FirstOrDefault(v => v.Variable.Id == variableId);

            return invoiceVariable != null ? invoiceVariable.Value : string.Empty;
        }

        private async Task<string> GetShipToCustLocIdAsync(string pfjCode)
        {
            string shipToCustLocId = string.Empty;

            CustomerDto customerDto = await _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(pfjCode);

            if (customerDto != null && customerDto.Addresses.Any())
            {
                shipToCustLocId = customerDto.Addresses.Select(x => x.Code).OrderBy(x => x).FirstOrDefault();
            }

            return shipToCustLocId;
        }

        public Task<bool> IsOrderNumberValidAsync(string orderNumber)
        {
            return _invoiceApiGateway.IsOrderNumberValidAsync(orderNumber);
        }

        public List<string> InvoiceValidate(IList<InvoiceVariable> invoiceVariables, IList<InvoiceItem> invoiceItems, InvoiceType? invoiceType, long invoiceId)
        {
            List<string> errors = new List<string>();

            if (invoiceVariables == null || invoiceVariables.Count == 0)
            {
                return errors;
            }

            errors.AddRange(ValidateNsuCode(invoiceVariables));
            errors.AddRange(ValidateProviderCode(invoiceVariables));
            errors.AddRange(ValidatePaymentValue(invoiceVariables));
            errors.AddRange(ValidatePaymentCode(invoiceVariables));
            errors.Add(ValidateOrderNumber(invoiceVariables, invoiceId));
            errors.Add(ValidateSumTotalInvoice(invoiceVariables, invoiceItems));
            errors.Add(ValidateMaxItemsInInvoice(invoiceItems, invoiceType));
            errors.AddRange(ValidatePaymentCodeNsuCode(invoiceVariables));
            errors.AddRange(ValidatePaymentCodeProviderCode(invoiceVariables));

            return errors.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();
        }

        public async Task Approve(long id, string userName)
        {
            var invoice = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (invoice == null)
            {
                throw new NotFoundException<Invoice>();
            }

            _ruleValidator.Validate<ApproveInvoiceValidator>(invoice, true);

            if (invoice.Status == InvoiceStatusType.Draft)
            {
                await ApproveDraft(invoice);
            }
            else if (invoice.Status == InvoiceStatusType.Analyzing)
            {
                await ApproveAnalyzing(invoice, userName);
            }

            MainRepository.Update(invoice);

            UnitOfWork.Commit();
        }

        private async Task<string> ValidateDofConsolidated(bool isConsolidated, long[] dofIds)
        {
            if (!isConsolidated || (dofIds == null || !dofIds.Any()))
            {
                return string.Empty;
            }

            var dofIdConsolidateds = await _dofConsolidationService.GetDofIdsConsolidatedsAsync(dofIds);

            if (dofIdConsolidateds == null || !dofIdConsolidateds.Any())
            {
                return string.Empty;
            }

            return $"There are original invoices already used in a consolidation to one item. Invoice Ids: { string.Join(" ,", dofIdConsolidateds)}";
        }

        private async Task ApproveAnalyzing(Invoice invoice, string userName)
        {
            var isInvoiceApprover = _approvalViewService.Search(userName).Where(i => i.InvoiceId == invoice.Id).Any();

            if (!isInvoiceApprover)
            {
                throw new ForbiddenException(userName, "The Invoice can only be approved by the approver user");
            }
            User effectiveUser = _userService.GetByName(userName);

            var approvers = _invoiceApproverService.GetByInvoice(invoice.Id);
            var currentApprover = approvers.Where(a => a.Situation == ApproverStatusType.Analyzing).OrderBy(l => l.ApprovalSequence).First();
            _invoiceApproverService.Approve(currentApprover.Id, effectiveUser.Id);

            var nextApprover = approvers.Where(a => a.Situation == null).OrderBy(l => l.ApprovalSequence).FirstOrDefault();

            if (nextApprover != null)
            {
                await _invoiceApproverService.Analyze(nextApprover.Id);
            }
            else
            {
                invoice.Status = InvoiceStatusType.Approved;
            }
        }

        private async Task ApproveDraft(Invoice invoice)
        {
            var approvers = _invoiceApproverService.GetByInvoice(invoice.Id);
            var firstApprover = approvers.Where(a => !a.Situation.HasValue).OrderBy(l => l.ApprovalSequence).First();
            
            invoice.Status = InvoiceStatusType.Analyzing;
            MainRepository.Update(invoice);
            UnitOfWork.Commit();

            await _invoiceApproverService.Analyze(firstApprover.Id);

        }

        public async Task Reject(long invoiceId, int reasonId, string userName)
        {
            var invoice = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == invoiceId);

            if (invoice == null)
            {
                throw new NotFoundException<Invoice>();
            }

            _ruleValidator.Validate<RejectInvoiceValidator>(invoice, true);

            User effectiveUser = _userService.GetByName(userName);

            DateTime rejectDate = DateTime.Now;

            await RejectApprovers(invoice, effectiveUser, rejectDate, reasonId);

            invoice.Status = InvoiceStatusType.Rejected;
            invoice.RejectUserId = effectiveUser.Id;
            invoice.RejectDate = rejectDate;
            invoice.RejectReasonId = reasonId;

            MainRepository.Update(invoice);

            UnitOfWork.Commit();
        }

        private async Task RejectApprovers(Invoice invoice, User effectiveUser, DateTime rejectDate, int reasonId)
        {
            var isInvoiceApprover = _approvalViewService.Search(effectiveUser.UserName).Where(i => i.InvoiceId == invoice.Id).Any();

            if (!isInvoiceApprover)
            {
                throw new ForbiddenException(effectiveUser.UserName, "The Invoice can only be rejected by the approver user");
            }

            var approvers = _invoiceApproverService.GetByInvoice(invoice.Id);
            var rejectApprover = approvers.Where(a => a.Situation == ApproverStatusType.Analyzing).OrderBy(l => l.ApprovalSequence).First();

            await _invoiceApproverService.Reject(rejectApprover.Id, effectiveUser.Id, rejectDate, reasonId);
        }

        private string ValidateSumTotalInvoice(IList<InvoiceVariable> invoiceVariables, IList<InvoiceItem> invoiceItems)
        {
            CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            int[] variableTypes = new int[] { (int)VariableType.PaymentValue1, (int)VariableType.PaymentValue2, (int)VariableType.PaymentValue3 };
            var existsVariables = invoiceVariables
                .Where(x => variableTypes.Contains(x.Variable.Id));

            if (!existsVariables.Any())
            {
                return string.Empty;
            }

            decimal sum = decimal.Round(invoiceVariables
                .Where(x => variableTypes.Contains(x.Variable.Id))
                .Select(x =>
                {
                    decimal value = decimal.Zero;

                    decimal.TryParse(x.Value, NumberStyles.AllowDecimalPoint, cultureInfo, out value);

                    return value;
                }).Sum(), 2);

            var sumTotalValueInvoice = decimal.Round(invoiceItems.Select(x =>
                {
                    decimal price = decimal.Zero;

                    decimal.TryParse(x.Price?.ToString(), NumberStyles.AllowDecimalPoint, cultureInfo, out price);

                    decimal quantity = decimal.Zero;

                    decimal.TryParse(x.Quantity?.ToString(), NumberStyles.AllowDecimalPoint, cultureInfo, out quantity);

                    return price * quantity;
                }).Sum(), 2);

            return sum != sumTotalValueInvoice ? "The sum of the Payment Values must be equal to the Total Invoice" : string.Empty;
        }

        private IList<string> ValidatePaymentCode(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            var paymentValue1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue1 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode1, paymentValue1, false));

            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            var paymentValue2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue2 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode2, paymentValue2, false));

            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            var paymentValue3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue3 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode3, paymentValue3, false));

            return errors;
        }

        private IList<string> ValidatePaymentValue(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();

            var paymentValue1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue1 select item).FirstOrDefault();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentValue1, paymentCode1, false));

            var paymentValue2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue2 select item).FirstOrDefault();
            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentValue2, paymentCode2, false));

            var paymentValue3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentValue3 select item).FirstOrDefault();
            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentValue3, paymentCode3, false));

            return errors;
        }

        private string GenericValidate(InvoiceVariable invoiceVariable, InvoiceVariable invoiceVariablePair, bool validateCreditCard)
        {
            if (invoiceVariable == null || string.IsNullOrEmpty(invoiceVariable.Value) || invoiceVariablePair == null)
            {
                return string.Empty;
            }
            var invoiceVariableType = System.Enum.GetValues(typeof(VariableType)).Cast<VariableType>().FirstOrDefault(x => invoiceVariable.Variable.Id == (int)x);
            var invoicePairVariableType = System.Enum.GetValues(typeof(VariableType)).Cast<VariableType>().FirstOrDefault(x => invoiceVariablePair.Variable.Id == (int)x);
            if (invoiceVariablePair == null || string.IsNullOrEmpty(invoiceVariablePair.Value))
            {
                if (validateCreditCard)
                {
                    bool isCreditCard = IsCreditCard(invoiceVariable.Value);

                    if (!isCreditCard) return string.Empty;
                }

                return $"{invoicePairVariableType.GetDescription()} is required";
            }
            else if (validateCreditCard)
            {
                bool isCreditCard = IsCreditCard(invoiceVariable.Value);
                if (!isCreditCard)
                {
                    return $"The variable {invoicePairVariableType.GetDescription()} can only be filled if the {invoiceVariableType.GetDescription()} is related to a credit card";
                }
            }
            return string.Empty;
        }

        private bool IsCreditCard(string value)
        {
            return _paymentCodeService.IsCreditCard(value);
        }

        private IList<string> ValidateNsuCode(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();
            var nsuCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode1 select item).FirstOrDefault();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            errors.Add(GenericValidate(nsuCode1, paymentCode1, false));

            var nsuCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode2 select item).FirstOrDefault();
            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            errors.Add(GenericValidate(nsuCode2, paymentCode2, false));


            var nsuCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode3 select item).FirstOrDefault();
            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            errors.Add(GenericValidate(nsuCode3, paymentCode3, false));

            return errors;
        }

        private IList<string> ValidatePaymentCodeNsuCode(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            var nsuCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode1 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode1, nsuCode1, true));

            var nsuCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode2 select item).FirstOrDefault();
            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode2, nsuCode2, true));


            var nsuCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.NSUCode3 select item).FirstOrDefault();
            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode3, nsuCode3, true));

            return errors;
        }

        private IList<string> ValidatePaymentCodeProviderCode(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();
            var providerCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode1 select item).FirstOrDefault();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode1, providerCode1, true));

            var providerCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode2 select item).FirstOrDefault();
            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode2, providerCode2, true));

            var providerCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode3 select item).FirstOrDefault();
            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            errors.Add(GenericValidate(paymentCode3, providerCode3, true));

            return errors;
        }

        private IList<string> ValidateProviderCode(IList<InvoiceVariable> invoiceVariables)
        {
            IList<string> errors = new List<string>();
            var providerCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode1 select item).FirstOrDefault();
            var paymentCode1 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode1 select item).FirstOrDefault();
            errors.Add(GenericValidate(providerCode1, paymentCode1, false));

            var providerCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode2 select item).FirstOrDefault();
            var paymentCode2 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode2 select item).FirstOrDefault();
            errors.Add(GenericValidate(providerCode2, paymentCode2, false));

            var providerCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.ProviderCode3 select item).FirstOrDefault();
            var paymentCode3 = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.PaymentCode3 select item).FirstOrDefault();
            errors.Add(GenericValidate(providerCode3, paymentCode3, false));

            return errors;
        }

        private string ValidateOrderNumber(IList<InvoiceVariable> invoiceVariables, long invoiceId)
        {
            var orderNumber = (from item in invoiceVariables where item.Variable.Id == (int)VariableType.OrderNumber select item).FirstOrDefault();
            if (orderNumber == null || string.IsNullOrEmpty(orderNumber.Value))
            {
                return string.Empty;
            }
            var isOrderNumberValid = IsOrderNumberValidAsync(orderNumber.Value);

            if (!isOrderNumberValid.Result)
            {
                return "There is no invoice with this Order Number in Synchro";
            }

            var originalInvoices = _orderService.GetOrders(invoiceId);
            if (originalInvoices.Any())
            {
                var existsOrderWithOrderNumber = originalInvoices.Any(x => x.RecofNumOrder != null);
                
                var isOrderNumberInOriginalInvoices = originalInvoices.Where(x => x.RecofNumOrder != null).Any(x => x.RecofNumOrder.Substring(0, 9) == orderNumber.Value);

                if (existsOrderWithOrderNumber && !isOrderNumberInOriginalInvoices)
                {
                    return "This order number doesn’t belong to any original invoice";
                }
            }

            return string.Empty;
        }

        public async Task<bool> IsItemQuantityValid(int itemQuantity)
        {
            var permitedItems = await _invoiceApiGateway.GetMaxItemsPerInvoice();
            return itemQuantity <= permitedItems;
        }

        private string ValidateMaxItemsInInvoice(IList<InvoiceItem> invoiceItems, InvoiceType? invoiceType)
        {
            if (invoiceType == null || invoiceType == InvoiceType.Product)
            {
                return string.Empty;
            }

            var isItemQuantityValid = IsItemQuantityValid(invoiceItems.Count());
            if (!isItemQuantityValid.Result)
            {
                var maxItems = _invoiceApiGateway.GetMaxItemsPerInvoice();
                return $"According to the NFSe split rules, the max number of SKUs allowed in an invoice is {maxItems.Result}";
            }
            return string.Empty;
        }

        public async Task<Invoice> CopyInvoice(long invoiceId, User user)
        {
            var invoice = MainRepository.QueryComplete(false).FirstOrDefault(e => e.Id == invoiceId);

            if (invoice == null)
            {
                var message = $"Invoice with id '{invoiceId}' was not found.";
                throw new NotFoundException<Invoice>(message);
            }

            var invoiceVariables = _invoiceVariableService.GetInvoiceVariablesByInvoiceId(invoiceId);
            Invoice copyedInvoice = invoice.Copy(user, null);

            if (invoice.InvoiceType == InvoiceType.Product)
            {
                var copyedCarrier = invoice.Carrier.Copy();
                var carrierCreated = _carrierService.Create(copyedCarrier);
                copyedInvoice = invoice.Copy(user, carrierCreated.Id);
            }

            var invoiceBranchs = _branchService.GetBranchesByTransaction(invoice.Transaction.Id, true);

            if (invoice.Branch != null && invoiceBranchs?.FirstOrDefault(x => x.Id == invoice.Branch.Id) != null)
            {
                copyedInvoice.Branch = invoice.Branch;
            }

            var ordersInvoice = _orderService.GetOrders(invoice.Id);
            var copyedInvoiceCreated = Create(copyedInvoice);
            invoice.Items.ToList().ForEach(item => item.Id = 0);
            var copyedinvoiceItems = _invoiceItemService.Create(invoice.Items, copyedInvoiceCreated.Id);
            await _invoiceApproverService.Create(copyedInvoice.Id, copyedInvoiceCreated.Transaction.Id);
            _invoiceVariableService.CreateWithExistsVaribles(copyedInvoiceCreated.Id, copyedInvoiceCreated.Transaction.Id, user.Id, invoiceVariables);

            return copyedInvoiceCreated;
        }

        public IQueryable<RequestDetail> GetRequestDetails()
        {
            return MainRepository.QueryRequestDetail();
        }
        private IList<InvoiceItem> RenumberDellTieNumbers(IList<InvoiceItem> originalInvoiceItems)
        {
            var tieNumCount = 1;

            List<InvoiceItem> itemsBase = new List<InvoiceItem>();

            List<(string OriginalDellTieNum, string NewDellTieNum, long? DofId)> tieNumlist = new List<(string, string, long?)>();

            var baseitems = originalInvoiceItems.Where(x => x.DellBaseFlag == DellBaseFlagType.Base).ToList();

            foreach (InvoiceItem item in baseitems)
            {
                var newDellTieNum = tieNumCount.PadLeft(3, '0');
                tieNumlist.Add((item.DellTieNum, newDellTieNum, item.OriginalDofId));
                tieNumCount++;
            }

            foreach (InvoiceItem item in originalInvoiceItems)
            {
                if (item.DellBaseFlag == DellBaseFlagType.Base || item.DellBaseFlag == DellBaseFlagType.Rollup)
                {
                    item.DellTieNum = tieNumlist.Where(x => x.DofId == item.OriginalDofId && x.OriginalDellTieNum == item.DellTieNum)
                                          .Select(x => x.NewDellTieNum)
                                          .FirstOrDefault();
                }
                else
                {
                    item.DellTieNum = null;
                }
            }

            return originalInvoiceItems;
        }
    }
}