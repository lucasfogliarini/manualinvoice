﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class InvoiceItemService : ServiceBase<IInvoiceItemRepository>, IInvoiceItemService
    {
        private readonly IOrderItemService _orderItemService;
        public InvoiceItemService(IUnitOfWork unitOfWork, IInvoiceItemRepository mainRepository, IOrderItemService orderItemService)
            : base(unitOfWork, mainRepository)
        {
            _orderItemService = orderItemService;
        }

        public IList<InvoiceItem> MergeItems(IList<InvoiceItem> entities, long invoiceId)
        {
            List<InvoiceItem> itemsToCreate = new List<InvoiceItem>();
            List<InvoiceItem> itemsToUpdate = new List<InvoiceItem>();
            List<InvoiceItem> itemsToDelete = new List<InvoiceItem>();
            List<OrderItem> orderItemsToUpdate = new List<OrderItem>();
            List<OrderItem> orderItemsToDelete = new List<OrderItem>();

            List<InvoiceItem> itemsInDataBase = GetItems(invoiceId, true);

            if (itemsInDataBase == null || !itemsInDataBase.Any())
            {
                itemsToCreate = entities.ToList();
                itemsToCreate = SetNullableFieldsWithDefaultValue(itemsToCreate);
            }
            else
            {
                itemsInDataBase = SetNullableFieldsWithDefaultValue(itemsInDataBase);
                itemsToCreate = entities.Where(item => item.Id == 0).ToList();
                itemsToCreate = SetNullableFieldsWithDefaultValue(itemsToCreate);

                // Items to be Update
                itemsToUpdate = entities
                    .Where(item =>
                        itemsInDataBase
                            .Any(itemDataBase =>
                                itemDataBase.Id == item.Id &&
                                (!itemDataBase.ProductCode.Equals(item.ProductCode)
                                || itemDataBase.Price != item.Price
                                || itemDataBase.Quantity != item.Quantity
                                || itemDataBase.RmaCode != item.RmaCode
                                || itemDataBase.ItemLine != item.ItemLine)))
                    .ToList();

                itemsToUpdate = AdjustNullableFieldsWithDatabaseValues(itemsToUpdate, itemsInDataBase);
                itemsToUpdate = GetRollupItemsForBaseItems(itemsToUpdate, itemsInDataBase);
                orderItemsToUpdate = GetOrderItems(itemsToUpdate, invoiceId);
                orderItemsToUpdate = UpdateDiferencesInOrderItems(itemsToUpdate, orderItemsToUpdate);

                // Items to be Delete 
                itemsToDelete = itemsInDataBase.Where(itemDataBase => !entities.Any(item => itemDataBase.Id == item.Id) && itemDataBase.DellBaseFlag != DellBaseFlagType.Rollup)
                    .ToList();
                // Include Rollup Items 
                itemsToDelete = GetRollupItemsForBaseItems(itemsToDelete, itemsInDataBase);
                // Get Order Items 
                orderItemsToDelete = GetOrderItems(itemsToDelete, invoiceId);

            }

            // Create Items
            itemsToCreate.ForEach(itemToCreate => Create(itemToCreate, invoiceId));
            // Update Items
            orderItemsToUpdate.ForEach(orderItemToUpdate => _orderItemService.Update(orderItemToUpdate));
            itemsToUpdate.ForEach(itemToUpdate => Update(itemToUpdate));
            // Remove Items
            orderItemsToDelete.ForEach(orderItemToDelete => _orderItemService.Delete(orderItemToDelete));
            itemsToDelete.ForEach(itemToDelete => Delete(itemToDelete));

            IList<InvoiceItem> result = GetItems(invoiceId, false);

            return result;
        }

        private List<InvoiceItem> AdjustNullableFieldsWithDatabaseValues(List<InvoiceItem> itemsToUpdate, List<InvoiceItem> itemsInDatabase)
        {
            foreach (InvoiceItem item in itemsToUpdate)
            {
                var itemDB = itemsInDatabase.Where(x => x.Id == item.Id).FirstOrDefault();

                if (itemDB == null)
                {
                    continue;
                }

                item.OriginalDofId = itemDB.OriginalDofId;
                item.OriginalIdfNum = itemDB.OriginalIdfNum;
                item.PriceOriginal = itemDB.PriceOriginal;
                item.QuantityOriginal = itemDB.QuantityOriginal;
                item.DellBaseFlag = itemDB.DellBaseFlag;
                item.DellTieNum = itemDB.DellTieNum;
                item.Invoice = itemDB.Invoice;
            }

            return itemsToUpdate;
        }

        private List<OrderItem> UpdateDiferencesInOrderItems(List<InvoiceItem> itemsToUpdate, List<OrderItem> orderItemsToUpdate)
        {
            if (itemsToUpdate == null || !itemsToUpdate.Any() || orderItemsToUpdate == null || !orderItemsToUpdate.Any())
            {
                return orderItemsToUpdate;
            }

            List<OrderItem> newOrderItemsToUpdate = new List<OrderItem>();

            foreach (InvoiceItem item in itemsToUpdate)
            {
                var orderItem = orderItemsToUpdate.Where(x => x.Invoice.Id == item.Invoice.Id && x.InvoiceItem.Id == item.Id).First();

                if (item.DellBaseFlag == DellBaseFlagType.Rollup)
                {
                    var baseItem = GetBaseItemOfRollupItem(item, itemsToUpdate);
                    var baseOrderItem = orderItemsToUpdate.FirstOrDefault(x => x.InvoiceItem.Id == baseItem.Id);

                    var percent = CalculatePercentDiffence(baseItem.Price, baseItem.PriceOriginal);
                    orderItem.ItemUnitSoldPrice = CalculateValueBasedOnPercent(percent, orderItem?.ItemUnitSoldPriceOrig);

                    if (baseItem != null && baseOrderItem != null)
                    {
                        orderItem.QtyItem = CalculateRollupDifferenceQuantity(baseItem, orderItem, baseOrderItem);
                    }
                }
                else
                {
                    var percent = CalculatePercentDiffence(item.Price, item.PriceOriginal);
                    orderItem.ItemUnitSoldPrice = CalculateValueBasedOnPercent(percent, orderItem?.ItemUnitSoldPriceOrig);
                    orderItem.QtyItem = item.Quantity;
                }

                newOrderItemsToUpdate.Add(orderItem);
            }

            return newOrderItemsToUpdate;
        }


        private decimal CalculatePercentDiffence(decimal? price, decimal? priceOrigem)
        {
            if (priceOrigem <= decimal.Zero)
            {
                return decimal.Zero;
            }

            decimal percentualDiference = 100 - (price.Value * 100) / (priceOrigem.Value);
            return percentualDiference;
        }

        private decimal? CalculateValueBasedOnPercent(decimal percent, decimal? value)
        {
            if (value is null || value.Value <= decimal.Zero)
            {
                return decimal.Zero;
            }

            return Math.Round(value.Value - (value.Value * (percent / 100)), 4);
        }

        private long? CalculateRollupDifferenceQuantity(InvoiceItem item, OrderItem orderRollupItem, OrderItem baseOrderItem)
        {
            if (orderRollupItem?.QtyItemOrig == null || !(baseOrderItem?.QtyItemOrig > 0) || item.Quantity == baseOrderItem.QtyItemOrig)
            {
                return orderRollupItem?.QtyItemOrig;
            }

            return (orderRollupItem.QtyItemOrig / baseOrderItem.QtyItemOrig) * item.Quantity;
        }

        private InvoiceItem GetBaseItemOfRollupItem(InvoiceItem rollupItem, List<InvoiceItem> invoiceItems)
        {
            InvoiceItem baseItem;

            if (rollupItem.DellBaseFlag != DellBaseFlagType.Rollup)
            {
                return rollupItem;
            }

            baseItem = invoiceItems.Where(
                           x => x.Invoice.Id == rollupItem.Invoice.Id &&
                            x.DellBaseFlag == DellBaseFlagType.Base &&
                            x.OriginalDofId == rollupItem.OriginalDofId &&
                            x.DellTieNum == rollupItem.DellTieNum).FirstOrDefault();

            return baseItem;
        }

        public List<InvoiceItem> GetItems(long invoiceId, bool includes)
        {
            if (includes)
            {
                return MainRepository.QueryComplete(false)
                       .Where(item => item.Invoice.Id == invoiceId)
                       .OrderBy(item => item.Id)
                       .ToList();
            }

            return MainRepository.QuerySearch()
                .Where(item => item.Invoice.Id == invoiceId)
                .ToList();
        }

        private List<OrderItem> GetOrderItems(List<InvoiceItem> invoiceItems, long invoiceId)
        {
            List<OrderItem> orderItemsInDataBase = _orderItemService.Get(invoiceId).ToList();

            var items = orderItemsInDataBase.Where(x => invoiceItems.Any(i => x.InvoiceItem.Id == i.Id)).OrderBy(x => x.Id).ToList();

            return items;
        }

        private List<InvoiceItem> SetNullableFieldsWithDefaultValue(List<InvoiceItem> entities)
        {
            if (entities == null || !entities.Any())
            {
                return entities;
            }

            foreach (InvoiceItem item in entities)
            {
                if (!item.DellBaseFlag.HasValue)
                {
                    item.DellBaseFlag = DellBaseFlagType.Unknown;
                }

                if (item.DellBaseFlag != DellBaseFlagType.Rollup && item.QuantityOriginal == 0 && item.Quantity != 0)
                {
                    item.QuantityOriginal = item.Quantity;
                }

                if (item.DellBaseFlag != DellBaseFlagType.Rollup && item.PriceOriginal == 0 && item.Price != 0)
                {
                    item.PriceOriginal = item.Price;
                }
            }

            return entities;
        }

        private List<InvoiceItem> GetRollupItemsForBaseItems(List<InvoiceItem> items, IList<InvoiceItem> itemsInDataBase)
        {
            if (itemsInDataBase == null || !itemsInDataBase.Any())
            {
                return items;
            }

            List<InvoiceItem> newItemsRollupToAdd = new List<InvoiceItem>();

            var baseItems = items.Where(x => x.DellBaseFlag == DellBaseFlagType.Base);

            foreach (InvoiceItem item in baseItems)
            {
                var itemBaseOrderCount = itemsInDataBase.Where(x => x.OriginalDofId == item.OriginalDofId && x.DellBaseFlag == DellBaseFlagType.Base)
                    .Count();

                if (itemBaseOrderCount == 0)
                {
                    break;
                }

                List<InvoiceItem> rollupItems = new List<InvoiceItem>();

                if (itemBaseOrderCount == 1)
                {
                    rollupItems = itemsInDataBase.Where(x => x.OriginalDofId == item.OriginalDofId && x.DellBaseFlag == DellBaseFlagType.Rollup)
                        .ToList();
                }
                else
                {
                    rollupItems = itemsInDataBase.
                        Where(x => x.OriginalDofId == item.OriginalDofId &&
                              x.DellBaseFlag == DellBaseFlagType.Rollup &&
                              x.DellTieNum == item.DellTieNum)
                     .ToList();
                }

                if (rollupItems != null && rollupItems.Any())
                {
                    newItemsRollupToAdd.AddRange(rollupItems);
                }
            }

            if (newItemsRollupToAdd != null && newItemsRollupToAdd.Any())
            {
                items.AddRange(newItemsRollupToAdd);
            }

            return items;
        }

        public InvoiceItem Create(InvoiceItem entity, long invoiceId)
        {
            entity.Invoice = new Invoice() { Id = invoiceId };
            MainRepository.Add(entity);
            UnitOfWork.Commit();

            return entity;
        }

        public IList<InvoiceItem> Create(IList<InvoiceItem> invoiceItems, long invoiceId)
        {
            var result = new List<InvoiceItem>();

            if (invoiceItems == null || !invoiceItems.Any())
            {
                return result;
            }

            invoiceItems.ToList().ForEach(i => result.Add(Create(i, invoiceId)));

            return result;
        }

        public InvoiceItem Update(InvoiceItem invoiceItem)
        {
            InvoiceItem entity = MainRepository.Find(invoiceItem.Id);

            entity.Update(invoiceItem);
            MainRepository.Update(entity);
            UnitOfWork.Commit();

            return entity;
        }

        private void Delete(InvoiceItem entity)
        {
            MainRepository.Delete(entity);
            UnitOfWork.Commit();
        }
    }
}
