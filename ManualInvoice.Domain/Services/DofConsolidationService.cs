﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public sealed class DofConsolidationService : IDofConsolidationService
    {
        private readonly int _sourceIdManualInvoice = 2;
        private readonly IDirectApiGateway _directApiGateway;

        public DofConsolidationService(IDirectApiGateway directApiGateway)
        {
            _directApiGateway = directApiGateway;
        }

        public async Task<IList<ResponseDofConsolidationCreatedDto>> CreateOnDirect(long invoiceId, IList<Order> orders, IList<long> dofs)
        {
            if (orders == null || !orders.Any() || dofs == null || !dofs.Any())
            {
                return new List<ResponseDofConsolidationCreatedDto>();
            }

            var createDofConsolidationDtos = new List<CreateDofConsolidationDto>();

            dofs.ToList().ForEach(dof =>
            {
                var dofConsolidations = orders
                    .Select(order => new CreateDofConsolidationDto(order.OriginalDofId, dof, order.RecofNumOrder, _sourceIdManualInvoice, invoiceId.ToString()))
                    .ToList();

                createDofConsolidationDtos.AddRange(dofConsolidations);
            });

            var result = await _directApiGateway.CreateAsync(createDofConsolidationDtos);

            return result;
        }

        public async Task<long[]> GetDofIdsConsolidatedsAsync(long[] dofIds)
        {
            var result = await _directApiGateway.GetDofIdsConsolidatedsAsync(dofIds);

            return result;
        }
    }
}
