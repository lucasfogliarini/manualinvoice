﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public sealed class LoadVolumeService : ILoadVolumeService
    {
        private readonly IInvoiceApiGateway _invoiceApiGateway;

        public LoadVolumeService(IInvoiceApiGateway invoiceApiGateway)
        {
            _invoiceApiGateway = invoiceApiGateway;
        }
        public async Task<bool> CreateLoadVolume(Invoice invoice, long dofSequence, short siteCode)
        {
            var loadVolume = new LoadVolumeDto
            {
                InvoiceSequence = dofSequence,
                SiteCode = siteCode,
                BrandVolumes = invoice.Carrier?.VolumeBrand,
                NumberVolumes = invoice.Carrier?.VolumeNumbering,
                QuantiyVolumes = invoice.Carrier?.VolumeQuantity,
                EspecieVolumes = invoice.Carrier?.Specie,
                RepackageWeight = 0,
                GrossWeightKg = invoice.Carrier?.Weight
            };

            return await _invoiceApiGateway.CreateLoadVolume(loadVolume);
        }
    }
}
