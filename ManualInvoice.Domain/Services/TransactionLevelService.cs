﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class TransactionLevelService : ServiceBase<ITransactionLevelRepository>, ITransactionLevelService
    {
        public TransactionLevelService(IUnitOfWork unitOfWork, ITransactionLevelRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
        }

        public IEnumerable<TransactionLevel> GetActivesLevelsByTransaction(int transactionId)
        {
            if (transactionId == 0)
            {
                return new List<TransactionLevel>();
            }

            var result = MainRepository.QueryComplete()
                .Where(x => x.Transaction.Id == transactionId && x.Level.IsActive.Value==true)
                .ToList();

            return result;
        }

        public IEnumerable<TransactionLevel> GetByLevels(params int[] levelsId)
        {
            if (levelsId == null || !levelsId.Any())
            {
                return new List<TransactionLevel>();
            }

            var result = MainRepository.QueryComplete()
                .Where(x => levelsId.Contains(x.Level.Id))
                .ToList();

            return result;
        }
    }
}
