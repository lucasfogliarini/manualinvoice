﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.SkuApi;
using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class SkuService : ISkuService
    {
        private readonly ISkuApiGateway _skuApiGateway;
        private readonly ISkuFileRepository _skuFileRepository;

        public SkuService(ISkuApiGateway skuApiGateway, ISkuFileRepository skuFileRepository)
        {
            _skuApiGateway = skuApiGateway;
            _skuFileRepository = skuFileRepository;
        }

        public async Task<SkuDto> GetBySkuCodeAsync(string skuCode, InvoiceType invoiceType, bool throwException)
        {
            var skuDto = await _skuApiGateway.GetSkuByCode(skuCode, invoiceType);

            if(skuDto is null && throwException)
            {
                throw new NotFoundException<SkuDto>("Sku not found");
            }

            return skuDto;
        }

        public async Task<string> GetCityCodeBySkuAsync(string skuCode)
        {
            var cityCode = await _skuApiGateway.GetCityCodeBySku(skuCode);

            if (cityCode is null)
            {
                throw new NotFoundException<string>("City Code not found");
            }

            return cityCode;
        }

        public async Task<ResponseDto<IEnumerable<SkuDto>>> SearchAsync(
            int skip, int take, string orderBy, bool orderByDesc, string name, InvoiceType invoiceType)
        {
            return await _skuApiGateway.GetAsync(skip, take, orderBy, orderByDesc, name, invoiceType);
        }

        public async Task<ImportItemsResult> ReadExcelFileAsync(Stream fileStream, InvoiceType invoiceType)
        {
            var result = new ImportItemsResult();

            var invoiceTypeCode = invoiceType == InvoiceType.Product ? "M" : "S";

            var importedSkus = _skuFileRepository.ReadFile(fileStream);
            var importedCodes = importedSkus.Where(s => s.Type == invoiceTypeCode).Select(s => s.Code).Distinct();

            var firstLC116 = string.Empty;
            var emptyCodes = 0;

            foreach (var importedSku in importedSkus)
            {
                if (string.IsNullOrWhiteSpace(importedSku.Code))
                {
                    emptyCodes++;
                    continue;
                }

                if (importedSku.Type != invoiceTypeCode)
                {
                    result.Errors.Add($"The item {importedSku.Code} has a different type from the invoice and will not be inserted.");
                    continue;
                }

                if (importedSku.Type == "M")
                {
                    var sku = await _skuApiGateway.GetByCodeAsync(importedSku.Code, invoiceType);
                    if (sku != null)
                    {
                        var item = new InvoiceItem
                        {
                            ProductCode = sku.Code,
                            ProductName = sku.Name,
                            Ncm = sku.NcmCode,
                            Quantity = importedSku.Quantity,
                            Price = importedSku.Price,
                            RmaCode = importedSku.RmaCode
                        };

                        if (item.Quantity <= 0)
                        {
                            result.Errors.Add($"The Product {importedSku.Code} should have quantity higher than zero.");
                            continue;
                        }

                        if (item.Price < 0)
                        {
                            result.Errors.Add($"The Product {importedSku.Code} should have price higher or equal zero.");
                            continue;
                        }

                        result.ImportedItems.Add(item);
                    }
                    else
                    {
                        result.Errors.Add($"The Product {importedSku.Code} was not found in Synchro database.");
                    }
                }
                else if (importedSku.Type == "S")
                {
                    var sku = await _skuApiGateway.GetByCodeAsync(importedSku.Code, invoiceType);
                    if (sku != null)
                    {
                        var item = new InvoiceItem
                        {
                            ProductCode = sku.Code,
                            ProductName = sku.Name,
                            Ncm = sku.NcmCode,
                            Quantity = importedSku.Quantity,
                            Price = importedSku.Price,
                            RmaCode = importedSku.RmaCode,
                        };

                        if (item.Quantity <= 0)
                        {
                            result.Errors.Add($"The Service {importedSku.Code} should have quantity higher than zero.");
                            continue;
                        }

                        if (item.Price < 0)
                        {
                            result.Errors.Add($"The Service {importedSku.Code} should have price higher or equal zero.");
                            continue;
                        }

                        if (string.IsNullOrWhiteSpace(sku.Lc116))
                        {
                            result.Errors.Add($"The Service {importedSku.Code} has no Federal supplementary law 116/03 code" +
                                $"and will not be inserted.");
                            continue;
                        }
                        
                        if (string.IsNullOrWhiteSpace(firstLC116))
                        {
                            firstLC116 = sku.Lc116;
                        }

                        if (sku.Lc116 != firstLC116)
                        {
                            result.Errors.Add($"In accord to Federal supplementary law 116/03 by annex I, the system cannot " +
                                $"add more than one service to same request with the different coding. Code {importedSku.Code}.");
                        }
                        else
                        {
                            result.ImportedItems.Add(item);
                        }
                    }
                    else
                    {
                        result.Errors.Add($"The Service {importedSku.Code} was not found in Synchro database.");
                    }
                }
            }

            if (emptyCodes > 0)
            {
                result.Errors.Add($"At least {emptyCodes} items were not imported due to empty SKU codes.");
            }

            return result;
        }
    }
}
