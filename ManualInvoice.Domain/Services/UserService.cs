﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace ManualInvoice.Domain.Services
{
    public class UserService : ServiceBase<IUserRepository>, IUserService
    {
        private readonly ILevelUserRepository _levelUserRepository;
        private readonly ISecurityApiService _securityApiService;
        private readonly IRuleValidator _ruleValidator;

        public UserService(ISecurityApiService securityApiService, 
                          IUnitOfWork unitOfWork,
                          IRuleValidator ruleValidator,
                          IUserRepository userRepository,
                          ILevelUserRepository levelUserRepository)
            : base(unitOfWork, userRepository)
        {
            _levelUserRepository = levelUserRepository;
            _securityApiService = securityApiService;
            _ruleValidator = ruleValidator;
        }

        public IQueryable<User> Search()
        {
            return MainRepository.QuerySearch();
        }

        public async Task<UserResponseDto> Search(UserRequestDto userRequest)
        {
            var userResponseDto = await _securityApiService.GetUsersAsync(userRequest);
            var ntAccounts = userResponseDto.Users.Select(su => su.NtAccount.ToLower()).ToList();
            var manualInvoiceUsers = GetManualInvoiceUsers(ntAccounts);

            userResponseDto.Users = MergeUsers(userResponseDto.Users, manualInvoiceUsers);

            return userResponseDto;
        }

        private IEnumerable<UserResponseItemDto> GetManualInvoiceUsers(IEnumerable<string> ntAccounts)
        {
            var manualInvoiceUsers = MainRepository
                .QuerySearch()
                .Where(e => ntAccounts.Contains(e.UserName))
                .Select(e => new UserResponseItemDto
                {
                    Id = e.Id.ToString(),
                    NtAccount = e.UserName,
                    Levels = e.LevelUsers.Select(lu => new LevelDto
                    {
                        Id = lu.Level.Id,
                        Name = lu.Level.Name
                    }).ToList()
                })
                .ToList();
            return manualInvoiceUsers;
        }

        private IEnumerable<UserResponseItemDto> MergeUsers(IEnumerable<UserResponseItemDto> securityUsers, IEnumerable<UserResponseItemDto> manualInvoiceUsers)
        {
            const string profilePrefix = "ManualInvoice - ";
            foreach (var securityUser in securityUsers)
            {
                var manualInvoiceUser = manualInvoiceUsers.FirstOrDefault(u => u.NtAccount.ToLower() == securityUser.NtAccount.ToLower());
                securityUser.Profiles = securityUser.Profiles.Where(l => l.Contains(profilePrefix)).Select(l => l.Replace(profilePrefix, "")).ToArray();
                securityUser.Levels = manualInvoiceUser?.Levels;
                securityUser.Id = manualInvoiceUser?.Id;
            }

            return securityUsers;
        }

        public User GetByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var result = MainRepository.QuerySearch().FirstOrDefault(e => name.Equals(e.UserName, StringComparison.OrdinalIgnoreCase));

            return result;
        }

        public async Task ConfigureLevelsAsync(string ntAccount, IEnumerable<int> levelIds)
        {
            var user = await ImportUserAsync(ntAccount);            
            _ruleValidator.Validate<ConfigureLevelsValidator>(user, true);
            _levelUserRepository.UpdateLevelUsers(user.Id, levelIds);
        }

        public async Task<User> ImportUserAsync(string ntAccount)
        {
            var securityUser = await _securityApiService.GetUserAsync(ntAccount);
            var user = MainRepository.QuerySearch().FirstOrDefault(e => e.UserName.ToLower() == securityUser.NtAccount.ToLower());
            if (user == null)
            {
                user = Create(securityUser.NtAccount, securityUser.Email, securityUser.IsActive);
            };
            return user;
        }
        public User Create(string ntAccount, string email, bool isActive = true)
        {
            var user = new User()
            {
                UserName = ntAccount.ToLower(),
                IsActive = isActive,
                Email = email,
                LastLogonDate = DateTime.Now,
                ProfileId = 1
            };
            UnitOfWork.Add(user);
            UnitOfWork.Commit();
            return user;
        }
    }
}
