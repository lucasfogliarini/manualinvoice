﻿using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class IssuerService : IIssuerService
    {
        private readonly ICustomerApiGateway _customerApiGateway;

        public IssuerService(ICustomerApiGateway customerApiGateway)
        {
            _customerApiGateway = customerApiGateway;
        }

        public Task<CustomerDto> GetByPfjCodeAsync(string pfjCode)
        {
            return _customerApiGateway.GetCustomerAndLocationWithValidVigencyAsync(pfjCode);
        }
    }
}
