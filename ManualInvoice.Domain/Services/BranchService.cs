﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Domain.Services
{
    public class BranchService : ServiceBase<IBranchRepository>, IBranchService
    {
        readonly IRuleValidator _ruleValidator;
        private readonly ITransactionBranchService _transactionBranchService;

        public BranchService(IUnitOfWork unitOfWork,
                             IRuleValidator ruleValidator,
                             IBranchRepository branchRepository,
                             ITransactionBranchService transactionBranchService)
            : base(unitOfWork, branchRepository)
        {
            _ruleValidator = ruleValidator;
            _transactionBranchService = transactionBranchService;
        }

        public IEnumerable<Branch> GetBranchesByTransaction(int transactionId, bool onlyActives)
        {
            var result = _transactionBranchService.GetBranchesByTransaction(transactionId, onlyActives);

            return result;
        }

        public IQueryable<Branch> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Branch Get(int id)
        {
            var branch = MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);

            if (branch == null)
            {
                var message = $"Branch with id '{id}' was not found.";
                throw new NotFoundException<Branch>(message);
            }

            return branch;
        }

        public Branch GetByName(string name)
        {
            var branch = MainRepository.QueryComplete().OrderByDescending(x => x.IsActive).FirstOrDefault(e => e.Name == name);

            if (branch == null)
            {
                var message = $"Branch with name '{name}' was not found.";
                throw new NotFoundException<Branch>(message);
            }

            return branch;
        }

        public Branch Create(Branch branch)
        {
            Validate(branch);

            MainRepository.Add(branch);

            UnitOfWork.Commit();

            return branch;
        }

        public Branch Update(Branch branch)
        {
            Validate(branch);

            MainRepository.Update(branch);

            UnitOfWork.Commit();

            return branch;
        }

        private void Validate(Branch branch)
        {
            _ruleValidator.Validate<BranchValidator>(branch, true);
            var errors = new Dictionary<string, object>()
            {
                [nameof(branch.Name)] = branch.Name,
                [nameof(branch.Location)] = branch.Location
            };
            Expression<Func<Branch, bool>> predicate;
            if (branch.Id == 0)
            {
                predicate = b => b.Name == branch.Name && b.Location == branch.Location;
            }
            else
            {
                predicate = b => b.Id != branch.Id && b.Name == branch.Name && b.Location == branch.Location;
            }

            MainRepository.CheckDuplicated(predicate, errors);
        }
    }
}
