﻿using ManualInvoice.Domain.Gateways.CommonApi;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public sealed class FederativeUnitService : IFederativeUnitService
    {
        private readonly ICommonApiGateway _commonApiGateway;

        public FederativeUnitService(ICommonApiGateway commonApiGateway)
        {
            _commonApiGateway = commonApiGateway;
        }

        public async Task<IList<FederativeUnitDto>> GetAllFederativeUnitsAsync()
        {
            var result = await _commonApiGateway.GetAllFederativeUnitsAsync();

            return result;
        }
    }
}
