﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Domain.Services
{
    public class VariableService : ServiceBase<IVariableRepository>, IVariableService
    {
        readonly IRuleValidator _ruleValidator;

        public VariableService(IUnitOfWork unitOfWork,
            IRuleValidator ruleValidator,
            IVariableRepository variableRepository)
            : base(unitOfWork, variableRepository)
        {
            _ruleValidator = ruleValidator;
        }

        public IQueryable<Variable> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Variable Get(int id)
        {
            var variable = MainRepository.QuerySearch().SingleOrDefault(e => e.Id == id);

            if (variable == null)
            {
                var message = $"Variable with id '{id}' was not found.";
                throw new NotFoundException<Variable>(message);
            }

            return variable;
        }

        public Variable Create(Variable variable)
        {
            variable.IsEditable = true;
            variable.IsSystem = false;
            Validate(variable);

            MainRepository.Add(variable);

            UnitOfWork.Commit();

            return variable;
        }

        public Variable Update(Variable variable)
        {
            variable.IsSystem = false;
            Validate(variable);

            MainRepository.Update(variable);

            UnitOfWork.Commit();

            return variable;
        }

        private void Validate(Variable variable)
        {
            _ruleValidator.Validate<VariableValidator>(variable, true);
            var errors = new Dictionary<string, object>()
            {
                [nameof(variable.Name)] = variable.Name
            };

            MainRepository.CheckDuplicated(b => b.Id != variable.Id && b.Name == variable.Name, errors);
        }
    }
}
