﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public class EmailService : ServiceBase<IEmailRepository>, IEmailService
    {
        public EmailService(IUnitOfWork unitOfWork,
                            IEmailRepository emailRepository)
            : base(unitOfWork, emailRepository)
        {
        }

        public IQueryable<Email> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Email Create(Email email)
        {
            MainRepository.Add(email);

            UnitOfWork.Commit();

            return email;
        }

        public Email Update(Email email)
        {
            MainRepository.Update(email);

            UnitOfWork.Commit();

            return email;
        }
    }
}
