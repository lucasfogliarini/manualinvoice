﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public class SystemConfigurationService : ServiceBase<ISystemConfigurationRepository>, ISystemConfigurationService
    {
        readonly IRuleValidator _ruleValidator;

        public SystemConfigurationService(IUnitOfWork unitOfWork,
                             IRuleValidator ruleValidator,
                             ISystemConfigurationRepository systemConfigurationRepository)
            : base(unitOfWork, systemConfigurationRepository)
        {
            _ruleValidator = ruleValidator;
        }

        public IQueryable<SystemConfiguration> Search()
        {
            return MainRepository.QuerySearch();
        }

        public SystemConfiguration Update(SystemConfiguration systemConfiguration)
        {
            Validate(systemConfiguration);

            MainRepository.Update(systemConfiguration);

            UnitOfWork.Commit();

            return systemConfiguration;
        }

        private void Validate(SystemConfiguration systemConfiguration)
        {
            _ruleValidator.Validate<SystemConfigurationValidator>(systemConfiguration, true);
        }
    }
}
