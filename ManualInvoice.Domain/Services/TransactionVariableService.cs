﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public sealed class TransactionVariableService : ServiceBase<ITransactionVariableRepository>, ITransactionVariableService
    {
        public TransactionVariableService(IUnitOfWork unitOfWork, ITransactionVariableRepository mainRepository)
            : base(unitOfWork, mainRepository)
        {
        }

        public IEnumerable<TransactionVariable> GetActivesVariablesByTransaction(int transactionId)
        {
            if (transactionId == 0)
            {
                return new List<TransactionVariable>();
            }

            var result = MainRepository.QueryComplete()
                .Where(x => x.Transaction.Id == transactionId && x.Variable.IsActive)
                .ToList();

            return result;
        }

        public IEnumerable<TransactionVariable> GetByVariables(params int[] variablesId)
        {
            if (variablesId == null || !variablesId.Any())
            {
                return new List<TransactionVariable>();
            }

            var result = MainRepository.QueryComplete()
                .Where(x => variablesId.Contains(x.Variable.Id))
                .ToList();

            return result;
        }
    }
}
