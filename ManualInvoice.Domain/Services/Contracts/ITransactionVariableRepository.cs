﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionVariableRepository : IEntityRepository<TransactionVariable>
    {
        void UpdateTransactionVariables(Transaction transaction);
    }
}
