﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ILevelUserRepository : IRepository
    {
        void UpdateLevelUsers(int userId, IEnumerable<int> levelIds);
    }
}
