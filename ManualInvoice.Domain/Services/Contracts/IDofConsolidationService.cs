﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.DirectApi.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IDofConsolidationService : IService
    {
        Task<IList<ResponseDofConsolidationCreatedDto>> CreateOnDirect(long invoiceId, IList<Order> orders, IList<long> dofs);
        Task<long[]> GetDofIdsConsolidatedsAsync(long[] dofIds);
    }
}
