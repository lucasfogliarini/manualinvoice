﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IReasonService : IService
    {
        IQueryable<Reason> Search();
        Reason Get(int id);
        Reason Create(Reason reason);
        Reason Update(Reason reason);
        bool Delete(Reason reason);
    }
}
