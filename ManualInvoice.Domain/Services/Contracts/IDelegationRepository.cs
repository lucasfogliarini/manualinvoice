using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IDelegationRepository : IEntityRepository<Delegation>
    {
        IQueryable<Delegation> QuerySearch();
        int UpdateTransientDelegations(int userId);
    }
}

