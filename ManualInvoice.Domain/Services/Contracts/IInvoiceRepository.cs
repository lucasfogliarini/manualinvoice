﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceRepository : IEntityRepository<Invoice>
    {
        IQueryable<Invoice> QuerySearch();
        IQueryable<Invoice> QueryComplete(bool tracked);
        IQueryable<RequestDetail> QueryRequestDetail();
    }
}
