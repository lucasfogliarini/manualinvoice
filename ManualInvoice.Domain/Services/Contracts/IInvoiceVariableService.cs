﻿using ManualInvoice.Domain.Dtos.InvoiceVariable;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceVariableService : IService
    {
        IList<InvoiceVariable> Create(long invoiceId, int transactionId, int userId);
        Task<IList<InvoiceVariableResponseDto>> GetByInvoiceAsync(long invoiceId);
        IList<InvoiceVariable> Update(IList<InvoiceVariable> variables);
        InvoiceVariable Update(InvoiceVariable variable);
        IList<InvoiceVariable> GetInvoiceVariablesByInvoiceId(long invoiceId);
        IList<InvoiceVariable> CreateWithExistsVaribles(long invoiceId, int transactionId, int userId, IList<InvoiceVariable> invoiceVariablesOld);
        IList<InvoiceVariable> UpdateVariableValue(IList<InvoiceVariable> variableList, VariableType variableType, string newValue);
        InvoiceVariable GetVariable(IList<InvoiceVariable> variableList, VariableType variableType);
    }
}
