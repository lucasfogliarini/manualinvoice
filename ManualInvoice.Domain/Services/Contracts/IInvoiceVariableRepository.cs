﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceVariableRepository : IEntityRepository<InvoiceVariable>
    {
        IQueryable<InvoiceVariable> QuerySearch();
    }
}
