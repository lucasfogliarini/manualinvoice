﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionService : IService
    {
        IQueryable<Transaction> Search();
        Transaction Get(int id);
        Transaction Create(Transaction transaction);
        Transaction Update(Transaction transaction);
    }
}
