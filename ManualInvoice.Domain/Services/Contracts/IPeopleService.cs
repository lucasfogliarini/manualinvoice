﻿using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IPeopleService : IService
    {
        Task<CustomerDto> GetByCodeAsync(string code);

        Task<ResponseDto<IEnumerable<CustomerDto>>> SearchAsync(CustomerRequestDto requestDto);
    }
}
