﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IApprovalViewService : IService
    {        
        IQueryable<ApprovalView> Search(string userName);
        IQueryable<ApprovalView> Search(long invoiceId);
    }
}
