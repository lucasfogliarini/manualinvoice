﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IBranchService : IService
    {
        IQueryable<Branch> Search();
        Branch Get(int id);
        IEnumerable<Branch> GetBranchesByTransaction(int transactionId, bool onlyActives);
        Branch Create(Branch branch);
        Branch Update(Branch branch);
        Branch GetByName(string name);
    }
}
