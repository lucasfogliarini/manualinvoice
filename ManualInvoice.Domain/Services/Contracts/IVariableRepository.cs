﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IVariableRepository : IEntityRepository<Variable>
    {
        IQueryable<Variable> QuerySearch();
    }
}
