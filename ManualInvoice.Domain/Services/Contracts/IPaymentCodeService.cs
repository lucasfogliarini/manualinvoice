﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IPaymentCodeService : IService
    {
        IList<PaymentCode> GetAllActives();
        IQueryable<PaymentCode> Search();
        PaymentCode Get(int id);
        PaymentCode Create(PaymentCode paymentCode);
        bool IsCreditCard(string code);
        PaymentCode Update(PaymentCode paymentCode);
    }
}
