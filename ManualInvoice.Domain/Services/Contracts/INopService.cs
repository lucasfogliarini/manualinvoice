﻿using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface INopService :  IService
    {
        Task<NopDto> GetByNopCodeAsync(string nopCode);
    }
}
