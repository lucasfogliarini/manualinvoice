﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionLevelService : IService
    {
        IEnumerable<TransactionLevel> GetActivesLevelsByTransaction(int transactionId);

        IEnumerable<TransactionLevel> GetByLevels(params int[] levelsId);
    }
}
