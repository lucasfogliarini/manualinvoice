﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceItemService : IService
    {
        IList<InvoiceItem> MergeItems(IList<InvoiceItem> entities, long invoiceId);
        InvoiceItem Create(InvoiceItem entity, long invoiceId);
        IList<InvoiceItem> Create(IList<InvoiceItem> invoiceItems, long invoiceId);
        List<InvoiceItem> GetItems(long invoiceId, bool includes);
        InvoiceItem Update(InvoiceItem invoiceItem);
    }
}
