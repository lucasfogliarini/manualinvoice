﻿using System.Linq;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IDelegationViewRepository : IEntityRepository<DelegationView>
    {
        IQueryable<DelegationView> QuerySearch();
    }
}
