﻿using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IIssuerService : IService
    {
        Task<CustomerDto> GetByPfjCodeAsync(string pfjCode);
    }
}
