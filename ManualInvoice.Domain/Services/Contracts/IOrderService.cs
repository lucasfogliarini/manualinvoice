﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IOrderService : IService
    {
        Order Get(long orderId);
        IList<Order> GetOrders(long invoiceId);
        IList<Order> CreateOrders(Invoice invoice, IList<FiscalDocumentDto> originalInvoices);
        Order GetFirstOrder(long invoiceId);
        decimal GetShippmentCost(long InvoiceId);
        decimal GetDutiesCost(long InvoiceId);
        void RemoveOrdersWithoutItems(long InvoiceId);
    }
}