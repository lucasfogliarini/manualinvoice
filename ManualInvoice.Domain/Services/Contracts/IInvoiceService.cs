﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Dtos.InvoiceApprover;
using ManualInvoice.Domain.Dtos.InvoiceVariable;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceService : IService
    {
        IQueryable<Invoice> Search();
        Task<CreatedInvoiceDto> CreateInvoice(int userId, Invoice invoice, Transaction transaction, InvoiceItem consolidatedItem, string[] originalInvoiceIdSelecteds, bool isConsolidation);
        IQueryable<RequestDetail> GetRequestDetails();
        Invoice Get(long id);
        UpdatedInvoiceResponse Update(long id, Invoice invoice, IList<InvoiceItem> items, IList<InvoiceVariable> variables, IList<InvoiceApprover> approvers);
        Task<List<FiscalDocumentDto>> GetSynchroInvoices(IEnumerable<long> ids);
        Task<ResponseDto<IList<FiscalDocumentSimpleDto>>> GetOriginalInvoices(OriginalInvoicesRequestDto originalInvoicesRequestDto);
        void Cancel(long id, string userName);
        Task<IList<InvoiceVariableResponseDto>> GetInvoiceVariablesAsync(long invoiceId);
        Task<StageSalesOrderDto> CreateOrder(long id);
        Task<bool> IsOrderNumberValidAsync(string orderNumber);
        List<string> InvoiceValidate(IList<InvoiceVariable> invoiceVariables, IList<InvoiceItem> invoiceItems, InvoiceType? invoiceType, long invoiceId);
        Task<bool> IsItemQuantityValid(int itemQuantity);
        Task<IList<InvoiceApproverResponseDto>> GetInvoiceApproversAsync(long invoiceId);
        Task Approve(long id, string userName);
        Task Reject(long invoiceId, int reasonId, string userName);
        Task<Invoice> CopyInvoice(long invoiceId, User user);
        Task ExecuteStageSalesOrder(StageSalesOrderDto stageSalesOrderDto);
        Task IssueInvoiceImmediately(StageSalesOrderDto stageSalesOrderDto);
    }
}
