﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionBranchRepository : IEntityRepository<TransactionBranch>
    {
        IQueryable<TransactionBranch> QuerySearch();
        void UpdateTransactionBranches(Transaction transaction);
    }
}
