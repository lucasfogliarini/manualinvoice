﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IOrderItemRepository : IEntityRepository<OrderItem>
    {
        IQueryable<OrderItem> QuerySearch();
        IQueryable<OrderItem> QueryComplete(bool tracked);
    }
}
