﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionVariableService : IService
    {
        IEnumerable<TransactionVariable> GetActivesVariablesByTransaction(int transactionId);

        IEnumerable<TransactionVariable> GetByVariables(params int[] variablesId);
    }
}
