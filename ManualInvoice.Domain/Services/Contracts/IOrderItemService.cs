﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IOrderItemService
    {
        OrderItem Create(Order order, Invoice invoice, InvoiceItem consolidatedItem, FiscalDocumentItemDto originalItem);
        OrderItem Update(OrderItem entity);
        void Delete(OrderItem entity);
        IList<OrderItem> Get(long invoiceId);
    }
}
