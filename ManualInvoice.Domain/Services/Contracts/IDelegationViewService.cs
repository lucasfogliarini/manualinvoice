﻿using ManualInvoice.Domain.Dtos.Delegation;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IDelegationViewService : IService
    {        
        IQueryable<DelegationView> Search();
        Task<HasDelegationDto> GetDelegation(string userName);
    }
}
