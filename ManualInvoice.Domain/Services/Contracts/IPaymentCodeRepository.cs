﻿using System.Linq;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IPaymentCodeRepository : IEntityRepository<PaymentCode>
    {
        IQueryable<PaymentCode> QuerySearch();
    }
}
