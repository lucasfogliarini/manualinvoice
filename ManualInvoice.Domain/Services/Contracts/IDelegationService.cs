using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IDelegationService : IService
    {
        IQueryable<Delegation> Search();
        Delegation Get(int id);
        Task<Delegation> Create(Delegation delegation, string[] delegationTo);
        Delegation Update(Delegation delegation);
        bool Delete(Delegation delegation);
    }
}

