﻿using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IAuthorizationService : IService
    {
        Task<UserAuthorizationDto> GetUserAuthorizationAsync(string userName, string system = "manualinvoice");
    }
}
