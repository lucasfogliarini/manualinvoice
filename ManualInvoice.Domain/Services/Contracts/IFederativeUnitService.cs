﻿using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IFederativeUnitService : IService
    {
        Task<IList<FederativeUnitDto>> GetAllFederativeUnitsAsync();
    }
}
