﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ISystemConfigurationRepository : IEntityRepository<SystemConfiguration>
    {
        IQueryable<SystemConfiguration> QuerySearch();
    }
}
