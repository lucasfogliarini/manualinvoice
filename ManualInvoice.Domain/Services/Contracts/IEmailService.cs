﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IEmailService : IService
    {
        IQueryable<Email> Search();
        Email Create(Email email);
        Email Update(Email email);
    }
}
