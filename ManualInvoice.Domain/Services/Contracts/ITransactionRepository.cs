﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionRepository : IEntityRepository<Transaction>
    {
        IQueryable<Transaction> QuerySearch();
    }
}
