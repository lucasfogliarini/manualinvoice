﻿using System.Linq;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IApprovalViewRepository : IEntityRepository<ApprovalView>
    {
        IQueryable<ApprovalView> QuerySearch();
    }
}
