﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Infrastructure.Repository;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ISkuFileRepository : IFileRepository<ImportedSkuDto>
    {
    }
}
