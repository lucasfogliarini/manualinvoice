﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ILevelService : IService
    {
        IQueryable<Level> Search(); 
        Level Get(int id);
        Level Create(Level level);
        Level Update(Level level);
    }
}
