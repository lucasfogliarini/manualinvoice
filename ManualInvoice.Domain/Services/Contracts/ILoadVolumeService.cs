﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ILoadVolumeService : IService
    {
        Task<bool> CreateLoadVolume(Invoice invoice, long dofSequence, short siteCode);
    }
}
