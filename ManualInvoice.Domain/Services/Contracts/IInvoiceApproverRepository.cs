﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceApproverRepository : IEntityRepository<InvoiceApprover>
    {
        IQueryable<InvoiceApprover> QuerySearch(bool tracked = false);
    }
}
