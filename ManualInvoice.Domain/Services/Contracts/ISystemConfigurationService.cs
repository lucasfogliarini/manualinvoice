﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ISystemConfigurationService : IService
    {
        IQueryable<SystemConfiguration> Search();
        SystemConfiguration Update(SystemConfiguration systemConfiguration);
    }
}
