﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceItemRepository : IEntityRepository<InvoiceItem>
    {
        IQueryable<InvoiceItem> QuerySearch();
        IQueryable<InvoiceItem> QueryComplete(bool tracked);
    }
}
