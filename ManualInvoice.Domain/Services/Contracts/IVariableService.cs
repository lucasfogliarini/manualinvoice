﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IVariableService : IService
    {
        IQueryable<Variable> Search();
        Variable Get(int id);
        Variable Create(Variable variable);
        Variable Update(Variable variable);
    }
}
