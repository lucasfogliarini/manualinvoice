﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IUserService : IService
    {
        IQueryable<User> Search();
        Task<UserResponseDto> Search(UserRequestDto userRequest);
        User GetByName(string name);
        Task ConfigureLevelsAsync(string ntAccount, IEnumerable<int> levelIds);
        Task<User> ImportUserAsync(string ntAccount);
    }
}
