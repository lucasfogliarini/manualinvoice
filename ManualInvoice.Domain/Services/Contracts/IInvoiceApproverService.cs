﻿using ManualInvoice.Domain.Dtos.InvoiceApprover;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IInvoiceApproverService : IService
    {
        Task Create(long invoiceId, int transactionId);

        IEnumerable<InvoiceApprover> GetByInvoice(long invoiceId);

        Task<IList<InvoiceApproverResponseDto>> GetByInvoiceAsync(long invoiceId);

        IList<InvoiceApprover> Update(IList<InvoiceApprover> approvers);

        void Approve(int id, int effectiveUserId);
        Task Reject(int id, int effectiveUserId, DateTime rejectDate, int reasonId);

        Task Analyze(int id);
    }
}
