﻿using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ISecurityApiService : IService
    {
        Task<ProfilesDto> GetProfilesAsync();
        Task<UserResponseDto> GetUsersAsync(UserRequestDto userRequestDto);
        Task<UserResponseItemDto> GetUserAsync(string ntAccount);
    }
}
