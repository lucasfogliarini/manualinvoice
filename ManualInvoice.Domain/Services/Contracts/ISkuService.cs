﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ISkuService : IService
    {
        Task<SkuDto> GetBySkuCodeAsync(string skuCode, InvoiceType invoiceType, bool throwException);
        Task<string> GetCityCodeBySkuAsync(string skuCode);
        Task<ResponseDto<IEnumerable<SkuDto>>> SearchAsync(
            int skip, int take, string orderBy, bool orderByDesc, string name, InvoiceType invoiceType);
        Task<ImportItemsResult> ReadExcelFileAsync(Stream fileStream, InvoiceType invoiceType);
    }
}
