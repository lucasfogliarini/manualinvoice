﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Repository;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface IBranchRepository : IEntityRepository<Branch>
    {
        IQueryable<Branch> QuerySearch();
    }
}
