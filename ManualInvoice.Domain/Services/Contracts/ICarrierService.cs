﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ICarrierService : IService
    {
        Carrier Create(Carrier carrier);
        Carrier Update(Carrier carrier);
    }
}
