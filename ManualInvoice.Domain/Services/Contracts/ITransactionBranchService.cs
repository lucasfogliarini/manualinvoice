﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services.Contracts
{
    public interface ITransactionBranchService : IService
    {
        IQueryable<TransactionBranch> Search();
        IEnumerable<Branch> GetBranchesByTransaction(int transactionId, bool onlyActives);
        TransactionBranch Get(string id);
    }
}
