﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManualInvoice.Domain.Services
{
    public class LevelService : ServiceBase<ILevelRepository>, ILevelService
    {
        readonly IRuleValidator _ruleValidator;

        public LevelService(IUnitOfWork unitOfWork, IRuleValidator ruleValidator, ILevelRepository levelRepository)
            : base(unitOfWork, levelRepository)
        {
            _ruleValidator = ruleValidator;
        }

        public IQueryable<Level> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Level Get(int id)
        {
            var level = MainRepository.QuerySearch().SingleOrDefault(e => e.Id == id);

            if (level == null)
            {
                var message = $"Level with id '{id}' was not found.";
                throw new NotFoundException<Level>(message);
            }

            return level;
        }

        public Level Create(Level level)
        {
            Validate(level);

            MainRepository.Add(level);

            UnitOfWork.Commit();

            return level;
        }

        public Level Update(Level level)
        {
            Validate(level);

            MainRepository.Update(level);

            UnitOfWork.Commit();

            return level;
        }

        private void Validate(Level level)
        {
            _ruleValidator.Validate<LevelValidator>(level, true);
            var errors = new Dictionary<string, object>()
            {
                [nameof(level.Name)] = level.Name
            };
            Expression<Func<Level, bool>> predicate;
            if (level.Id == 0)
            {
                predicate = b => b.Name == level.Name;
            }
            else
            {
                predicate = b => b.Id != level.Id && b.Name == level.Name;
            }

            MainRepository.CheckDuplicated(predicate, errors);
        }
    }
}
