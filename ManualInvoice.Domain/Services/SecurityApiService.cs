﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Gateways;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class SecurityApiService : ISecurityApiService
    {
        private readonly ISecurityApiGateway _securityApiGateway;
        public const string SYSTEM_SECURITY = "security";

        public SecurityApiService(ISecurityApiGateway securityApiGateway)
        {
            _securityApiGateway = securityApiGateway;
        }

        public async Task<ProfilesDto> GetProfilesAsync()
        {
            var profiles = await _securityApiGateway.GetProfilesAsync();
            return profiles;
        }

        public async Task<UserResponseDto> GetUsersAsync(UserRequestDto userRequestDto)
        {
            var users = await _securityApiGateway.GetUsersAsync(userRequestDto);
            return users;
        }

        public async Task<UserResponseItemDto> GetUserAsync(string ntAccount)
        {
            var userRequest = new UserRequestDto()
            {
                Take = 1,
                NTAccount = ntAccount
            };
            var securityUsers = await GetUsersAsync(userRequest);
            var securityUser = securityUsers?.Users.FirstOrDefault();
            if (securityUser == null)
            {
                throw new NotFoundException<User>();
            }
            return securityUser;
        }
    }
}
