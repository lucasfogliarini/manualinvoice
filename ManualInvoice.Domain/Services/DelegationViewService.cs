﻿using ManualInvoice.Domain.Dtos.Delegation;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Domain.Services
{
    public class DelegationViewService : ServiceBase<IDelegationViewRepository>, IDelegationViewService
    {
        readonly IUserService _userService;

        public DelegationViewService(IUnitOfWork unitOfWork,
              IDelegationViewRepository delegationViewRepository,
              IUserService userService)
            : base(unitOfWork, delegationViewRepository)
        {
            _userService = userService;
        }

        public IQueryable<DelegationView> Search()
        {
            return MainRepository.QuerySearch();
        }

        public async Task<HasDelegationDto> GetDelegation(string userName)
        {
            HasDelegationDto result = new HasDelegationDto();
            var currentDate = DateTime.Now.Date;

            var user = _userService.GetByName(userName.Replace(@"\\", @"\"));

            var delegations = MainRepository.QueryComplete()
                .Where(x =>
                    x.PeriodFrom.Date <= currentDate.Date &&
                    x.PeriodTo.Date >= currentDate.Date &&
                    x.DelegatedUserId == user.Id)
                .ToList();

            result.HasCascade = false;
            result.HasDelegation = false;

            UserRequestDto userRequestDto = new UserRequestDto()
            {
                IsActive = true,
            };

            foreach (var d in delegations)
            {
                userRequestDto.NTAccount = d.DelegationByUserName;
                var secUser = await _userService.Search(userRequestDto);
                var profiles = secUser.Users.FirstOrDefault().Profiles;

                if (profiles.Contains("Administrator") || profiles.Contains("Approver"))
                {
                    result.HasDelegation = true;
                    if (d.IsCascade)
                    {
                        result.HasCascade = true;
                        break;
                    }
                }
            }
            return result;
        }
    }
}
