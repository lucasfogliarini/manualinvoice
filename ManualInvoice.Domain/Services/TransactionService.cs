﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;
using ManualInvoice.Domain.Services.Contracts;
using ManualInvoice.Domain.Validators;
using ManualInvoice.Infrastructure;
using ManualInvoice.Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Domain.Services
{
    public class TransactionService : ServiceBase<ITransactionRepository>, ITransactionService
    {
        private readonly IRuleValidator _ruleValidator;
        private readonly ITransactionBranchRepository _transactionBranchRepository;
        private readonly ITransactionLevelRepository _transactionLevelRepository;
        private readonly ITransactionVariableRepository _transactionVariableRepository;

        public TransactionService(
            IUnitOfWork unitOfWork,
            IRuleValidator ruleValidator,
            ITransactionRepository repository,
            ITransactionBranchRepository transactionBranchRepository,
            ITransactionLevelRepository transactionLevelRepository,
            ITransactionVariableRepository transactionVariableRepository)
            : base(unitOfWork, repository)
        {
            _ruleValidator = ruleValidator;
            _transactionBranchRepository = transactionBranchRepository;
            _transactionLevelRepository = transactionLevelRepository; ;
            _transactionVariableRepository = transactionVariableRepository;
        }

        public IQueryable<Transaction> Search()
        {
            return MainRepository.QuerySearch();
        }

        public Transaction Get(int id)
        {
            return MainRepository.QueryComplete().SingleOrDefault(e => e.Id == id);
        }

        public Transaction Create(Transaction transaction)
        {
            if (Exists(transaction))
            {
                var duplicatedProperties = new Dictionary<string, object>
                {
                    { "Name", transaction.Name }
                };
                throw new DuplicatedException("Transaction", "Can't create Transaction", duplicatedProperties);
            }

            _ruleValidator.Validate<TransactionValidator>(transaction, true);
            MainRepository.Add(transaction);
            UnitOfWork.Commit();

            return transaction;
        }

        public Transaction Update(Transaction transaction)
        {
            if (Exists(transaction))
            {
                var duplicatedProperties = new Dictionary<string, object>
                {
                    { "Name", transaction.Name }
                };
                throw new DuplicatedException("Transaction", "Can't update Transaction", duplicatedProperties);
            }

            _ruleValidator.Validate<TransactionValidator>(transaction, true);

            _transactionBranchRepository.UpdateTransactionBranches(transaction);
            _transactionLevelRepository.UpdateTransactionLevels(transaction);
            _transactionVariableRepository.UpdateTransactionVariables(transaction);

            var transactionToUpdate = new Transaction
            {
                Id = transaction.Id,
                BillToCode = transaction.BillToCode,
                IsActive = transaction.IsActive,
                Name = transaction.Name,
                Observation = transaction.Observation,
                Serie = transaction.Serie,
                ShipToCode = transaction.ShipToCode,
                Specie = transaction.Specie,
                TransactionType = transaction.TransactionType
            };

            MainRepository.Update(transactionToUpdate);

            UnitOfWork.Commit();

            return transaction;
        }

        private bool Exists(Transaction transaction)
        {
            return MainRepository
                .QuerySearch()
                .Any(r => r.Id != transaction.Id
                       && r.Name.Trim().ToUpper() == transaction.Name.Trim().ToUpper());
        }
    }
}
