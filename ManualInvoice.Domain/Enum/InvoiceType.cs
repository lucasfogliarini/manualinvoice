﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ManualInvoice.Domain.Entities
{
    public enum InvoiceType
    {
        [Description("Product")]
        Product = 1,
        [Description("Service")]
        Service = 2
    }
}