﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ManualInvoice.Domain.Enum
{
    public enum OrderSynchroStatus
    {
        [Description("None")]
        None = 0,
        [Description("StageBillTo")]
        StageBillTo = 1,
        [Description("StageShipTo")]
        StageShipTo = 2,
        [Description("StageUpdateOrder")]
        StageUpdateOrder = 3,
        [Description("StageSalesOrder")]
        StageSalesOrder = 4,
        [Description("StageLoadVolume")]
        StageLoadVolume = 5,
        [Description("Finalized")]
        Finalized = 6
    }
}
