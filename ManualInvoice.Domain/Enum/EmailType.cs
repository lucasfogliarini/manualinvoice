﻿namespace ManualInvoice.Domain.Enum
{
    public enum EmailType
    {
        ApprovedLevel = 1,
        PendingLevel = 2,
        DanfFile = 3,
        RejectedInvoice = 4
    }
}