﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Common
{
    public enum ApproverStatusType
    {
        [Description("Analyzing")]
        Analyzing = 0,
        [Description("Rejected")]
        Rejected = 1,
        [Description("Approved")]
        Approved = 2,
        [Description("Cancelled")]
        Cancelled = 3,
        [Description("Pending")]
        Pending = 4
    }
}
