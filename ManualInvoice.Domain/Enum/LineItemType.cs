﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManualInvoice.Domain.Enum
{
    public enum LineItemType
    {
        Part = 77,
        Other = 80,
        Service = 83
    }
}
