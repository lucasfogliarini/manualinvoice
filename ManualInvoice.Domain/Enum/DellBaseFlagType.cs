﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Enum
{
    public enum DellBaseFlagType
    {
        [Description("Y")]
        Base,
        [Description("N")]
        NonBase,
        [Description("R")]
        Rollup,
        [Description("-")]
        Unknown
    }
}
