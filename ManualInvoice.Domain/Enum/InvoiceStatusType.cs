﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Common
{
    public enum InvoiceStatusType
    {
        [Description("Draft")]
        Draft = 0,
        [Description("Analyzing")]
        Analyzing = 1,
        [Description("Approved")]
        Approved = 2,
        [Description("Finalized")]
        Finalized = 3,
        [Description("Rejected")]
        Rejected = 4,
        [Description("Cancelled")]
        Cancelled = 5
    }
}
