﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Entities
{
    public enum ReasonType
    {
        [Description("Reject")]
        Reject = 0,
        [Description("Request")]
        Request = 1,
        Default = Request
    }
}
