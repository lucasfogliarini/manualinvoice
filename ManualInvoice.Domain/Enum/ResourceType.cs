﻿namespace ManualInvoice.Domain.Enum
{
    public enum ResourceType
    {
        PaymentCode,
        FederativeUnit,
        NoResource
    }
}
