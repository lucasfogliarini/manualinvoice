﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Enum
{
    public enum VariableType
    {
        [Description("Freigth")]
        Freight = 20,
        [Description("Insurance")]
        Insurance = 21,
        [Description("OtherCosts")]
        OtherCosts = 22,
        [Description("ServiceTag")]
        ServiceTag = 23,
        [Description("IDDPS")]
        IDDPS = 25,
        [Description("PONumber")]
        PONumber = 27,
        [Description("OrderNumber")]
        OrderNumber = 28,
        [Description("Payment Code 1")]
        PaymentCode1 = 24,
        [Description("Payment Code 2")]
        PaymentCode2 = 29,
        [Description("Payment Code 3")]
        PaymentCode3 = 30,
        [Description("ExchangeRate")]
        ExchangeRate = 31,
        [Description("ShipmentPlace")]
        ShipmentPlace = 32,
        [Description("ShipmentState")]
        ShipmentState = 33,
        [Description("Provider Code 1")]
        ProviderCode1 = 34,
        [Description("Provider Code 2")]
        ProviderCode2 = 35,
        [Description("Provider Code 3")]
        ProviderCode3 = 36,
        [Description("NSU Code 1")]
        NSUCode1 = 37,
        [Description("NSU Code 2")]
        NSUCode2 = 38,
        [Description("NSU Code 3")]
        NSUCode3 = 39,
        [Description("Payment Value 1")]
        PaymentValue1 = 40,
        [Description("Payment Value 2")]
        PaymentValue2 = 41,
        [Description("Payment Value 3")]
        PaymentValue3 = 42,
    }
}
