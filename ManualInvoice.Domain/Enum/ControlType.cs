﻿namespace ManualInvoice.Domain.Enum
{
    public enum ControlType
    {
        Textbox, Dropdown
    }
}
