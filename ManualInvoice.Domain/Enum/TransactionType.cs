﻿using System.ComponentModel;

namespace ManualInvoice.Domain.Common
{
    public enum TransactionType
    {
        [Description("E")]
        Inbound,
        [Description("S")]
        Outbound,
    }
}
