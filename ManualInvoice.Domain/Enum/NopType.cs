﻿using System;
using System.ComponentModel;
using System.Text;

namespace ManualInvoice.Domain.Enum
{
    public enum NopType
    {
        [Description("Product")]
        Product = 1,
        [Description("Service")]
        Service = 2,
        [Description("Installment")]
        Installment = 3,
        [Description("Unknown")]
        Unknown = 4
    }
}