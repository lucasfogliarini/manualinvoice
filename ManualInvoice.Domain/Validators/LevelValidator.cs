using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class LevelValidator : AbstractValidator<Level>
    {
        public LevelValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Name).MaximumLength(60).NotEmpty();
            RuleFor(e => e.Description).MaximumLength(255).NotEmpty();
            RuleFor(e => e.Visible).NotNull();
            RuleFor(e => e.Invoice).NotNull();
            RuleFor(e => e.IsActive).NotNull();
        }
    }
}

