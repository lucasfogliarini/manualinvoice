﻿using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public sealed class CreateInvoiceValidator : AbstractValidator<Invoice>
    {
        public CreateInvoiceValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.Id).Empty();
            RuleFor(x => x.User).NotNull();
            RuleFor(x => x.User.Id).NotEmpty();
            RuleFor(x => x.Transaction).NotNull();
            RuleFor(x => x.Transaction.Id).NotEmpty();
        }
    }
}
