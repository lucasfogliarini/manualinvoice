using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class VariableValidator : AbstractValidator<Variable>
    {
        public VariableValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Name).MaximumLength(60).NotEmpty();
            RuleFor(e => e.DefaultValue).MaximumLength(255);
            RuleFor(e => e.Length).GreaterThan(0);            
            RuleFor(e => e.IsSystem).Equal(false);
            When(e => e.Id == 0, () =>
            {
                RuleFor(e => e.IsEditable).Equal(true);
            });
            RuleFor(e => e.IsEditable).NotEmpty();
        }
    }
}

