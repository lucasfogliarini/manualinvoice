﻿using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public sealed class ConfigureLevelsValidator : AbstractValidator<User>
    {
        public ConfigureLevelsValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.IsActive).Equal(true);
        }
    }
}
