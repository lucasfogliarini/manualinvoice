﻿using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    //sample code
    public class InvoiceValidator : AbstractValidator<Invoice>
    {
        public InvoiceValidator()
        {
            RuleFor(i => i.Transaction).NotEmpty();
        }
    }
}
