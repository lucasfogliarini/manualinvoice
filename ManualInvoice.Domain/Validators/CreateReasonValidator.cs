﻿using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class CreateReasonValidator : AbstractValidator<Reason>
    {
        public CreateReasonValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.Id).Empty();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
        }
    }
}
