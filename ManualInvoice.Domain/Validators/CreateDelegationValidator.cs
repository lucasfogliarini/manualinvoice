using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class CreateDelegationValidator : AbstractValidator<Delegation>
    {
        public CreateDelegationValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Id).Empty();
            RuleFor(e => e.PeriodFrom).NotNull();
            RuleFor(e => e.PeriodTo).NotNull();
            RuleFor(e => e.IsActive).NotNull();
            RuleFor(e => e.IsCascade).NotNull();
            RuleFor(e => e.IsOldApproval).NotNull();
            RuleFor(e => e.UserId).NotEmpty();
        }
    }
}

