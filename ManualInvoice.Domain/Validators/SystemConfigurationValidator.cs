using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class SystemConfigurationValidator : AbstractValidator<SystemConfiguration>
    {
        public SystemConfigurationValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.DaysDraftRequest).InclusiveBetween(1, 365).NotEmpty();
            RuleFor(e => e.EmailRemember).InclusiveBetween(1,24).NotEmpty();
        }
    }
}

