using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class UpdateDelegationValidator : AbstractValidator<Delegation>
    {
        public UpdateDelegationValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Id).NotEmpty();
            RuleFor(e => e.PeriodFrom).NotNull();
            RuleFor(e => e.PeriodTo).NotNull();
            RuleFor(e => e.IsActive).NotNull();
            RuleFor(e => e.IsCascade).NotNull();
            RuleFor(e => e.IsOldApproval).NotNull();
        }
    }
}

