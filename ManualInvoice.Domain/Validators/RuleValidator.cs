﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Logging;
using System;

namespace ManualInvoice.Domain.Validators
{
    public class RuleValidator : IRuleValidator
    {
        readonly ILogger<RuleValidator> _logger;
        public RuleValidator(ILogger<RuleValidator> logger)
        {
            _logger = logger;
        }

        public ValidationResult Validate<TValidator>(object entity, bool throwIfNotValid = false) where TValidator : IValidator
        {
            var validator = Activator.CreateInstance<TValidator>();
            var result = validator.Validate(entity);

            // TODO: Remove throwIfNotValid when Unit Tests are refactored
            if (throwIfNotValid && !result.IsValid)
            {
                var message = result.ToString();
                _logger.LogError(message);

                throw new Exceptions.ValidationException(result.Errors)
                {
                    Errors = result.Errors
                };
            }

            return result;
        }
    }
}
