using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class PaymentCodeValidator : AbstractValidator<PaymentCode>
    {
        public PaymentCodeValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Code).MaximumLength(1).NotEmpty();
            RuleFor(e => e.Description).MaximumLength(255);
        }
    }
}

