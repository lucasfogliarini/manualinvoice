using FluentValidation;
using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Domain.Validators
{
    public class TransactionValidator : AbstractValidator<Transaction>
    {
        public TransactionValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Name).MaximumLength(100).NotEmpty();
            RuleFor(e => e.Serie).MaximumLength(5).NotEmpty();
            RuleFor(e => e.Specie).MaximumLength(6).NotEmpty();
            RuleFor(e => e.TransactionType).NotEmpty();
            RuleFor(e => e.BillToCode).MaximumLength(10).NotEmpty();
            RuleFor(e => e.ShipToCode).MaximumLength(10);
            RuleFor(e => e.Observation).MaximumLength(100);
            RuleFor(e => e.IsActive).NotNull();

            RuleFor(x => x.TransactionBranches).Must(tb => tb.Count > 0)
                .WithMessage("The Transaction must have at least one Branch");

            RuleFor(x => x.TransactionLevels).Must(tl => tl.Count > 0)
                .WithMessage("The Transaction must have at least one Level of Approval");

            RuleFor(x => x.TransactionLevels).Custom((levels, context) => {
                var lastLevel = levels.OrderBy(tl => tl.SequenceApproval).LastOrDefault();
                if (lastLevel == null || lastLevel.Level?.Invoice != true)
                {
                    context.AddFailure("The Transaction must have the invoicing as the last Level of Approval.");
                }
            });
        }
    }
}
