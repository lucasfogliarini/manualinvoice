﻿using FluentValidation;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;

namespace ManualInvoice.Domain.Validators
{
    public sealed class ApproveInvoiceValidator : AbstractValidator<Invoice>
    {
        public ApproveInvoiceValidator()
        {
            RuleFor(x => x.Status)
                .Must((status) => ValidateStatus(status))
                .WithMessage("The Invoice should be in 'Draft' or 'Analyzing' Status to be approved");

            RuleFor(x => x.Status)
                .NotEqual(InvoiceStatusType.Approved)
                .WithMessage("The Invoice was already approved");
        }

        private bool ValidateStatus(InvoiceStatusType? status)
        {
            return (status == InvoiceStatusType.Analyzing || status == InvoiceStatusType.Draft);
        }
    }
}
