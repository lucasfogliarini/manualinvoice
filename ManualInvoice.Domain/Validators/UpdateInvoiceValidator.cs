﻿using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public sealed class UpdateInvoiceValidator : AbstractValidator<Invoice>
    {
        public UpdateInvoiceValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Branch).NotNull();
            RuleFor(x => x.Branch.Id).NotEmpty();
            RuleFor(x => x.Reason).NotNull();
            RuleFor(x => x.Reason.Id).NotEmpty();
            RuleFor(x => x.BillToCode).NotEmpty();
            RuleFor(x => x.TransactionBillToCode).NotEmpty();
        }
    }
}
