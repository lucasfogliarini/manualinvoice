﻿using FluentValidation;
using FluentValidation.Results;

namespace ManualInvoice.Domain.Validators
{
    public interface IRuleValidator
    {
        ValidationResult Validate<TValidator>(object entity, bool throwIfNotValid = false) where TValidator : IValidator;
    }
}
