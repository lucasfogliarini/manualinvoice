﻿using FluentValidation;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Exceptions;

namespace ManualInvoice.Domain.Validators
{
    public sealed class CancelInvoiceValidator : AbstractValidator<Invoice>
    {
        public CancelInvoiceValidator()
        {
            RuleFor(x => x.Status)
                .Equal(Common.InvoiceStatusType.Analyzing)
                .WithMessage("The Invoice should be in 'Analyzing' Status to be cancelled");

            RuleFor(x => x.Status)
                .NotEqual(Common.InvoiceStatusType.Cancelled)
                .WithMessage("The Invoice was already cancelled");
        }
    }
}
