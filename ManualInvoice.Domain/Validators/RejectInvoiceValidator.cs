﻿using FluentValidation;
using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public sealed class RejectInvoiceValidator : AbstractValidator<Invoice>
    {
        public RejectInvoiceValidator()
        {
            RuleFor(x => x.Status)
                .Must((status) => ValidateStatus(status))
                .WithMessage("The Invoice should be in 'Analyzing' Status to be rejected");

            RuleFor(x => x.Status)
                .NotEqual(InvoiceStatusType.Rejected)
                .WithMessage("The Invoice was already rejected");
        }

        private bool ValidateStatus(InvoiceStatusType? status)
        {
            return status == InvoiceStatusType.Analyzing;
        }
    }
}
