using FluentValidation;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Domain.Validators
{
    public class BranchValidator : AbstractValidator<Branch>
    {
        public BranchValidator()
        {
            RuleFor(e => e).NotNull();
            RuleFor(e => e.Name).MaximumLength(20).NotEmpty();
            RuleFor(e => e.Location).MaximumLength(6).NotEmpty();
            RuleFor(e => e.Description).MaximumLength(255).NotEmpty();
            RuleFor(e => e.IsActive).NotNull();
        }
    }
}

