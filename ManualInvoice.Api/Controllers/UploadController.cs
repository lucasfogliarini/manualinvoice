﻿using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly ISkuService _skuService;

        public UploadController(ISkuService skuService)
        {
            _skuService = skuService;
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("items")]
        public async Task<ActionResult> UploadItems(InvoiceType invoiceType)
        {
            var files = Request.Form.Files;
            ImportItemsResult result = null;

            foreach (var file in files)
            {
                using (var stream = file.OpenReadStream())
                {
                    result = await _skuService.ReadExcelFileAsync(stream, invoiceType);
                }
            }

            return Ok(result);
        }
    }
}
