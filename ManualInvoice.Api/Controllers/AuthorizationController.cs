﻿using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Controllers
{
    [AllowAnonymous]
    [ApiController] 
    [Route("api/[controller]")]
    public class AuthorizationController : ControllerBase
    {
        private readonly Domain.Services.Contracts.IAuthorizationService _authorizationService;
        private readonly IUserService _userService;

        public AuthorizationController(Domain.Services.Contracts.IAuthorizationService authorizationService, IUserService userService)
        {
            this._authorizationService = authorizationService;
            _userService = userService;
        }

        [HttpGet]
        [Route("User")]
        public async Task<UserAuthorizationResponse> GetUserAuthorizationAsync(string userName, string system = "manualinvoice")
        {
            await _userService.ImportUserAsync(userName);
            var userAuthorizationResponse = await _authorizationService.GetUserAuthorizationAsync(userName, system);
            return userAuthorizationResponse.ToResponse();
        }
    }
}
