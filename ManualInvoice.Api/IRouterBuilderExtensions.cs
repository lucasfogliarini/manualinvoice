﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Api.ODataControllers;
using ManualInvoice.Domain.Dtos;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Routing;
using Microsoft.OData.Edm;
using System.Collections.Generic;

namespace ManualInvoice.Api
{
    public static class IRouterBuilderExtensions
    {
        public static void MapODataRoutes(this IRouteBuilder routeBuilder)
        {
            routeBuilder.MapODataServiceRoute("ODataRoute", "odata", GetEdmModel());
        }

        private static IEdmModel GetEdmModel()
        {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<TransactionSummaryResponse>("Transactions");
            builder.EntitySet<TransactionBranchSummaryResponse>("TransactionsBranches");

            builder.BuildTransactions();
            builder.BuildReasons();
            builder.BuildLevels();
            builder.BuildDelegations();
            builder.BuildBranches();
            builder.BuildUsers();
            builder.BuildIssuers();
            builder.BuildInvoices();
            builder.BuildRequestDetails();
            builder.BuildOriginalInvoices();
            builder.BuildNops();
            builder.BuildSkus();
            builder.BuildVariables();
            builder.BuildPaymentCodes();
            builder.BuildApprovals();
            builder.BuildPeople();
            builder.BuildSystemConfiguration();

            builder.ComplexType<RequestInvoiceSummaryResponse>();
            builder.ComplexType<IssuerResponse>();
            builder.ComplexType<TransactionResponse>();
            builder.ComplexType<CarrierResponse>();
            builder.ComplexType<InvoiceItemResponse>();
            builder.ComplexType<CreatedInvoiceDto>();

            builder.EnableLowerCamelCase();

            return builder.GetEdmModel();
        }

        private static void BuildUsers(this ODataConventionModelBuilder builder)
        {
            var users = builder.EntitySet<UserSummaryResponse>("Users");

            //Export
            var usersExportFunction = users.EntityType.Collection.Function("Export");
            usersExportFunction.ReturnsCollection<byte>();
            usersExportFunction.Parameter<int>(nameof(UserRequestDto.Skip));
            usersExportFunction.Parameter<int>(nameof(UserRequestDto.Take));
            usersExportFunction.Parameter<bool>(nameof(UserRequestDto.OrderDesc));
            usersExportFunction.Parameter<string>(nameof(UserRequestDto.OrderBy));
            usersExportFunction.Parameter<string>(nameof(UserRequestDto.NTAccount));
            usersExportFunction.Parameter<string>(nameof(UserRequestDto.ProfileName));
            usersExportFunction.Parameter<bool?>(nameof(UserRequestDto.IsActive));

            //Search
            builder.ComplexType<UserResponseDto>();
            builder.ComplexType<UserResponseItemDto>();
            builder.ComplexType<LevelDto>();
            var usersSearchFunction = users.EntityType.Collection.Function("Search");
            usersSearchFunction.Returns<UserResponseDto>();
            usersSearchFunction.Parameter<int?>(nameof(UserRequestDto.Skip));
            usersSearchFunction.Parameter<int?>(nameof(UserRequestDto.Take));
            usersSearchFunction.Parameter<bool>(nameof(UserRequestDto.OrderDesc));
            usersSearchFunction.Parameter<string>(nameof(UserRequestDto.OrderBy));
            usersSearchFunction.Parameter<string>(nameof(UserRequestDto.NTAccount));
            usersSearchFunction.Parameter<string>(nameof(UserRequestDto.ProfileName));
            usersSearchFunction.Parameter<bool?>(nameof(UserRequestDto.IsActive));

            //ConfigureLevels
            var usersConfigureLevelsFunction = users.EntityType.Collection.Action("ConfigureLevels");
            usersConfigureLevelsFunction.Parameter<string>(UsersController.NtAccount);
            usersConfigureLevelsFunction.CollectionParameter<int>(UsersController.Levels);
        }

        private static void BuildIssuers(this ODataConventionModelBuilder builder)
        {
            builder.EntitySet<IssuerSummaryResponse>("Issuers")
                  .EntityType
                  .Collection
                  .Function("GetByPfjCode")
                  .ReturnsFromEntitySet<IssuerSummaryResponse>("Issuers")
                  .Parameter<string>("pfjCode");
        }

        private static void BuildNops(this ODataConventionModelBuilder builder)
        {
            var nops = builder.EntitySet<NopSummaryResponse>("Nops");
            var nopsGetByNopCodeFunctions = nops.EntityType.Collection.Function("GetByNopCode");
            nopsGetByNopCodeFunctions.ReturnsFromEntitySet<NopSummaryResponse>("Nops");
            nopsGetByNopCodeFunctions.Parameter<string>("nopCode");
        }

        private static void BuildSkus(this ODataConventionModelBuilder builder)
        {
            builder.ComplexType<SkuResponse>();
            builder.ComplexType<SkuItemResponse>();

            var skus = builder.EntitySet<SkuSummaryResponse>("Skus");
            var getBySkuCode = skus.EntityType.Collection.Function("GetBySkuCode");
            getBySkuCode.ReturnsFromEntitySet<SkuSummaryResponse>("Skus");
            getBySkuCode.Parameter<string>("skuCode");
            getBySkuCode.Parameter<string>("invoiceType");

            var cityCodes = skus.EntityType.Collection.Function("GetCityCodeBySku");
            cityCodes.Returns<string>();
            cityCodes.Parameter<string>("skuCode");

            var searchFunction = skus.EntityType.Collection.Function(nameof(SkusController.Search));
            searchFunction.Parameter<int>(nameof(SkuRequest.Skip));
            searchFunction.Parameter<int>(nameof(SkuRequest.Take));
            searchFunction.Parameter<string>(nameof(SkuRequest.OrderBy));
            searchFunction.Parameter<bool>(nameof(SkuRequest.OrderByDesc));
            searchFunction.Parameter<string>(nameof(SkuRequest.Name));
            searchFunction.Parameter<InvoiceType>(nameof(SkuRequest.InvoiceType));
            searchFunction.Returns<SkuResponse>();
        }

        private static void BuildBranches(this ODataConventionModelBuilder builder)
        {
            var branches = builder.EntitySet<BranchSummaryResponse>("Branches");
            var branchesGetByTransactionFunction = branches.EntityType.Collection.Function("GetByTransaction");
            branchesGetByTransactionFunction.Returns<BranchSummaryResponse>();
            branchesGetByTransactionFunction.Parameter<int>("transactionId");
            branchesGetByTransactionFunction.Parameter<bool>("onlyActives");
            branches.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }
        private static void BuildRequestDetails(this ODataConventionModelBuilder builder)
        {
            var requestDetails = builder.EntitySet<RequestDetailSummaryResponse>("RequestDetails");
            requestDetails.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }

        private static void BuildInvoices(this ODataConventionModelBuilder builder)
        {
            var invoices = builder.EntitySet<InvoiceSummaryResponse>("Invoices");
            var invoicesGetSynchroInvoicesFunction = invoices.EntityType.Collection.Function("GetSynchroInvoices");
            invoicesGetSynchroInvoicesFunction.ReturnsCollectionFromEntitySet<InvoiceSummaryResponse>("Invoices");
            invoicesGetSynchroInvoicesFunction.CollectionParameter<long>("ids");
            invoices.EntityType.Action("Cancel");
            invoices.EntityType.Action("Approve");
            invoices.EntityType.Collection.Function("Export").ReturnsCollection<byte>();

            var invoicesRejectFunction = invoices.EntityType.Action("Reject");
            invoicesRejectFunction.Parameter<int>("reasonId");

            var invoicesGetVariablesFunction = invoices.EntityType.Collection.Function("GetVariables");
            invoicesGetVariablesFunction.Returns<Response<IEnumerable<InvoiceVariableSummaryResponse>>>();
            invoicesGetVariablesFunction.Parameter<int>("invoiceId");

            var invoicesGetApproversFunction = invoices.EntityType.Collection.Function("GetApprovers");
            invoicesGetApproversFunction.Returns<IList<InvoiceApproverSummaryResponse>>();
            invoicesGetApproversFunction.Parameter<int>("invoiceId");

            builder.ComplexType<InvoiceApproverSummaryResponse>();
            builder.ComplexType<SelectItemResponse>();

            var invoicesCreateOrderAction = invoices.EntityType.Action("CreateOrder");
            invoicesCreateOrderAction.Parameter<bool>("printNf");

            invoices.EntityType.Action("CopyInvoice");
        }
        private static void BuildOriginalInvoices(this ODataConventionModelBuilder builder)
        {
            var originalInvoices = builder.EntitySet<InvoiceSummaryResponse>("Invoices");
            var getOriginalInvoices = originalInvoices.EntityType.Collection.Function("GetOriginalInvoices");
            getOriginalInvoices.ReturnsFromEntitySet<InvoiceSummaryResponse>("Invoices");
            getOriginalInvoices.Parameter<int>(nameof(OriginalInvoicesRequestDto.Skip));
            getOriginalInvoices.Parameter<int>(nameof(OriginalInvoicesRequestDto.Take));
            getOriginalInvoices.Parameter<bool>(nameof(OriginalInvoicesRequestDto.OrderDesc));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.OrderBy));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.Id));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.BranchId));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.Number));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.Order));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.DofImportNumber));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.Nop));
            getOriginalInvoices.Parameter<string>(nameof(OriginalInvoicesRequestDto.TransactionType));
        }

        private static void BuildDelegations(this ODataConventionModelBuilder builder)
        {
            var delegations = builder.EntitySet<DelegationSummaryResponse>("Delegations");
            delegations.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }

        private static void BuildReasons(this ODataConventionModelBuilder builder)
        {
            var reasons = builder.EntitySet<ReasonSummaryResponse>("Reasons");
            reasons.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }

        private static void BuildLevels(this ODataConventionModelBuilder builder)
        {
            var levels = builder.EntitySet<LevelSummaryResponse>("Levels");
            levels.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }

        private static void BuildTransactions(this ODataConventionModelBuilder builder)
        {
            var transactions = builder.EntitySet<TransactionSummaryResponse>("Transactions");
            transactions.EntityType.Collection.Function("Export").ReturnsCollection<byte>();
        }

        private static void BuildVariables(this ODataConventionModelBuilder builder)
        {
            builder.EntitySet<VariableSummaryResponse>("Variables");
        }

        private static void BuildPaymentCodes(this ODataConventionModelBuilder builder)
        {
            builder.EntitySet<PaymentCodeSummaryResponse>("PaymentCodes");
        }

        private static void BuildApprovals(this ODataConventionModelBuilder builder)
        {
            builder.EntitySet<ApprovalViewSummaryResponse>("Approvals");
        }

        private static void BuildPeople(this ODataConventionModelBuilder builder)
        {
            builder.ComplexType<PeopleResponse>();
            builder.ComplexType<PersonResponse>();
            builder.ComplexType<PersonAddressResponse>();

            var customers = builder.EntitySet<CustomerDto>("People");

            var getByCodeFunction = customers.EntityType.Collection.Function(nameof(PeopleController.GetByCode));
            getByCodeFunction.Parameter<string>(nameof(PeopleRequest.Code));
            getByCodeFunction.Returns<PersonResponse>();

            var searchFunction = customers.EntityType.Collection.Function(nameof(PeopleController.Search));
            searchFunction.Parameter<int>(nameof(PeopleRequest.Skip));
            searchFunction.Parameter<int>(nameof(PeopleRequest.Take));
            searchFunction.Parameter<string>(nameof(PeopleRequest.OrderBy));
            searchFunction.Parameter<bool>(nameof(PeopleRequest.OrderByDesc));
            searchFunction.Parameter<string>(nameof(PeopleRequest.CpfCgc));
            searchFunction.Parameter<string>(nameof(PeopleRequest.Name));
            searchFunction.Parameter<bool>(nameof(PeopleRequest.ActualOnly));
            searchFunction.Returns<PeopleResponse>();
        }
        private static void BuildSystemConfiguration(this ODataConventionModelBuilder builder)
        {
            builder.EntitySet<SystemConfigurationSummaryResponse>("SystemConfigurations");
        }
    }
}
