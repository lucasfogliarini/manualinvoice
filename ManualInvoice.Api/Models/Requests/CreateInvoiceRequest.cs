﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class CreateInvoiceRequest
    {
        public string UserName { get; set; }

        public int TransactionId { get; set; }

        public string[] OriginalInvoiceIdSelecteds { get; set; }

        public bool IsConsolidation { get; set; }

        public string ConsolidatedItemCode { get; set; }

        public string ConsolidatedItemName { get; set; }

        public decimal ConsolidatedItemUnitValue { get; set; }

        public static implicit operator Invoice(CreateInvoiceRequest request)
        {
            if (request == null)
            {
                return null;
            }

            var invoice = new Invoice();

            invoice.Transaction = new Transaction() { Id = request.TransactionId };

            return invoice;
        }

        public static implicit operator InvoiceItem(CreateInvoiceRequest request)
        {
            if (request == null || !request.IsConsolidation)
            {
                return null;
            }

            var result = new InvoiceItem();

            result.ProductCode = request.ConsolidatedItemCode;
            result.ProductName = request.ConsolidatedItemName;
            result.Price = request.ConsolidatedItemUnitValue;
            result.Quantity = 1;

            return result;
        }
    }
}
