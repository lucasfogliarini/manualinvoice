﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class LevelRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public bool Invoice { get; set; }
        public bool IsActive { get; set; }

        public static implicit operator Level(LevelRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new Level()
            {
                Name = request.Name,
                Description = request.Description,
                Invoice = request.Invoice,
				Visible = request.Visible,
                IsActive = request.IsActive,
            };
        }
    }
}
