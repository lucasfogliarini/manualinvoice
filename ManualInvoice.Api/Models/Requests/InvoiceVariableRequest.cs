﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public sealed class InvoiceVariableRequest
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int VariableId { get; set; }

        public static implicit operator InvoiceVariable(InvoiceVariableRequest request)
        {
            if (request == null)
            {
                return null;
            }

            var result = new InvoiceVariable();

            result.Id = request.Id;
            result.Value = request.Value;
            result.Variable = new Variable { Id = request.VariableId };

            return result;
        }
    }
}
