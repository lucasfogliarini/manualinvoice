﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class TransactionRequest
    {
        public string Name { get; set; }
        public string Observation { get; set; }
        public bool? IsActive { get; set; }
        public string BillToCode { get; set; }
        public string ShipToCode { get; set; }
        public string Serie { get; set; }
        public string Specie { get; set; }
        public TransactionType? TransactionType { get; set; }
        public TransactionBranch[] TransactionBranches { get; set; }
        public TransactionLevel[] TransactionLevels { get; set; }
        public TransactionVariable[] TransactionVariables { get; set; }

        public static implicit operator Transaction(TransactionRequest transactionRequest)
        {
            if (null == transactionRequest)
            {
                return null;
            }

            return new Transaction
            {
                BillToCode = transactionRequest.BillToCode,
                IsActive = transactionRequest.IsActive,
                Name = transactionRequest.Name,
                Observation = transactionRequest.Observation,
                Serie = transactionRequest.Serie,
                ShipToCode = transactionRequest.ShipToCode,
                Specie = transactionRequest.Specie,
                TransactionType = transactionRequest.TransactionType,
                TransactionBranches = transactionRequest.TransactionBranches,
                TransactionLevels = transactionRequest.TransactionLevels,
                TransactionVariables = transactionRequest.TransactionVariables
            };
        }
    }
}
