﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class PaymentCodeRequest
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool? IsCreditCard { get; set; }


        public static implicit operator PaymentCode(PaymentCodeRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new PaymentCode()
            {
                Code = request.Code,
                Description = request.Description,
                IsActive = request.IsActive,
                IsCreditCard = request.IsCreditCard,
            };
        }
    }
}
