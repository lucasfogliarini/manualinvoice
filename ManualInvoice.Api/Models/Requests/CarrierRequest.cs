﻿using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Models.Requests
{
    public class CarrierRequest
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? VolumeQuantity { get; set; }
        public string VolumeBrand { get; set; }
        public string VolumeNumbering { get; set; }
        public decimal? FreightValue { get; set; }
        public decimal? InsuranceValue { get; set; }
        public decimal? Weight { get; set; }
        public string Specie { get; set; }
        public decimal? OtherCosts { get; set; }

        public static implicit operator Carrier(CarrierRequest request)
        {
            if (request == null)
            {
                return null;
            }

            var carrier = new Carrier()
            {
                    Id = request.Id,
                    Code = request.Code,
                    Name = request.Name,
                    VolumeQuantity = request.VolumeQuantity,
                    VolumeBrand = request.VolumeBrand,
                    VolumeNumbering = request.VolumeNumbering,
                    FreightValue = request.FreightValue,
                    InsuranceValue = request.InsuranceValue,
                    Weight = request.Weight,
                    Specie = request.Specie,
                    OtherCosts = request.OtherCosts
             };

            return carrier;
        }
    }
}
