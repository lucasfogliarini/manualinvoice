﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class BranchRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool IsActive { get; set; }

        public static implicit operator Branch(BranchRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new Branch()
            {
                Name = request.Name,
                Description = request.Description,
                Location = request.Location,
                IsActive = request.IsActive
            };
        }
    }
}
