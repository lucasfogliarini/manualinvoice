﻿using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Requests
{
    public sealed class UpdateInvoiceRequest
    {
        public UpdateInvoiceRequest()
        {
            Items = new List<InvoiceItemRequest>();
            Variables = new List<InvoiceVariableRequest>();
            Approvers = new List<InvoiceApproverRequest>();
        }

        public DateTime RequestDate { get; set; }

        public int TransactionId { get; set; }

        public InvoiceType InvoiceType { get; set; }

        public int BranchId { get; set; }

        public int RequestReasonId { get; set; }

        public IssuerRequest BillTo { get; set; }

        public IssuerRequest ShipTo { get; set; }

        public CarrierRequest Carrier { get; set; }

        public string DanfeObservation { get; set; }

        public string DanfeObservationNotComp { get; set; }

        public string DanfeInstruction { get; set; }

        public IEnumerable<InvoiceItemRequest> Items { get; set; }

        public IEnumerable<InvoiceVariableRequest> Variables { get; set; }

        public IEnumerable<InvoiceApproverRequest> Approvers { get; set; }

        public static implicit operator Invoice(UpdateInvoiceRequest invoiceRequest)
        {
            if (invoiceRequest == null)
            {
                return null;
            }

            var invoice = new Invoice();

            invoice.RequestDate = invoiceRequest.RequestDate;
            invoice.InvoiceType = invoiceRequest.InvoiceType;
            invoice.Transaction = new Transaction() { Id = invoiceRequest.TransactionId };
            invoice.Branch = new Branch() { Id = invoiceRequest.BranchId };
            invoice.Reason = new Reason() { Id = invoiceRequest.RequestReasonId };

            if (invoiceRequest.BillTo != null)
            {
                invoice.TransactionBillToCode = invoiceRequest.BillTo.Nop;
                invoice.BillToAddress = invoiceRequest.BillTo.Address;
                invoice.BillToCnpj = invoiceRequest.BillTo.CpfCgc;
                invoice.BillToCode = invoiceRequest.BillTo.Code;
                invoice.BillToName = invoiceRequest.BillTo.Name;
            }

            if (invoiceRequest.ShipTo != null)
            {
                invoice.TransactionShipToCode = invoiceRequest.ShipTo.Nop;
                invoice.ShipToAddress = invoiceRequest.ShipTo.Address;
                invoice.ShipToCnpj = invoiceRequest.ShipTo.CpfCgc;
                invoice.ShipToCode = invoiceRequest.ShipTo.Code;
                invoice.ShipToName = invoiceRequest.ShipTo.Name;
            }

            if (invoiceRequest.Carrier != null)
            {
                invoice.Carrier = invoiceRequest.Carrier;
            }

            invoice.Observation = invoiceRequest.DanfeObservation;
            invoice.Instruction = invoiceRequest.DanfeInstruction;
            invoice.ObservationTwo = invoiceRequest.DanfeObservationNotComp;

            return invoice;
        }
    }
}
