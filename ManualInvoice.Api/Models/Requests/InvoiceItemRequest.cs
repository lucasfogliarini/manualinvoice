﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public sealed class InvoiceItemRequest
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Ncm { get; set; }

        public int Quantity { get; set; }

        public string RmaNumber { get; set; }

        public decimal Price { get; set; }

        public decimal TotalValue { get; set; }

        public static implicit operator InvoiceItem(InvoiceItemRequest request)
        {
            if (request == null)
            {
                return null;
            }

            var result = new InvoiceItem();

            result.Id = request.Id;
            result.Ncm = request.Ncm;
            result.Price = request.Price;
            result.ProductCode = request.Code;
            result.ProductName = request.Name;
            result.Quantity = request.Quantity;
            result.RmaCode = request.RmaNumber;

            return result;
        }
    }
}
