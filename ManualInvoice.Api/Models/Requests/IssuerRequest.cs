﻿namespace ManualInvoice.Api.Models.Requests
{
    public class IssuerRequest
    {
        public string Code { get; set; }

        public string CpfCgc { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Nop { get; set; }
    }
}
