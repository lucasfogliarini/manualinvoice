﻿namespace ManualInvoice.Api.Models.Requests
{
    public class PeopleRequest
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public string OrderBy { get; set; }
        public bool OrderByDesc { get; set; }
        public string CpfCgc { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool ActualOnly { get; set; }
    }
}
