using ManualInvoice.Domain.Entities;
using System;

namespace ManualInvoice.Api.Models.Requests
{
    public class DelegationRequest
    {
        public DateTime? DelegationDate { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public bool? IsCascade { get; set; }
        public bool? IsOldApproval { get; set; }
        public bool? IsActive { get; set; }
        public int UserId { get; set; }
        public string[] DelegationTo { get; set; }


        public static implicit operator Delegation(DelegationRequest request)
        {
            if (request == null)
            {
                return null;
            }

            return new Delegation()
            {
                DelegationDate = request.DelegationDate,
                PeriodFrom = request.PeriodFrom,
                PeriodTo = request.PeriodTo,
                IsCascade = request.IsCascade,
                IsOldApproval = request.IsOldApproval,
                IsActive = request.IsActive,
                UserId = request.UserId
            };
        }
    }
}

