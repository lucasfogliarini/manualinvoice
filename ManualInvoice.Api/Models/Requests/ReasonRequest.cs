﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class ReasonRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ReasonType ReasonType { get; set; }
        public bool IsActive { get; set; }

        public static implicit operator Reason(ReasonRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new Reason()
            {
                Name = request.Name,
                Description = request.Description,
                ReasonType = request.ReasonType,
                IsActive = request.IsActive
            };
        }
    }
}
