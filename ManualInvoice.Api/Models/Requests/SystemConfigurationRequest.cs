﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class SystemConfigurationRequest
    {
        public int Id { get; set; }
        public int DaysDraftRequest { get; set; }
        public int EmailRemember { get; set; }

        public static implicit operator SystemConfiguration(SystemConfigurationRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new SystemConfiguration()
            {
                DaysDraftRequest = request.DaysDraftRequest,
                EmailRemember = request.EmailRemember            
            };
        }
    }
}
