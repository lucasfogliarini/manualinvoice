﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class VariableRequest
    {
        public long? Length { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsAlphanumeric { get; set; }    
        public string DefaultValue { get; set; }
        public bool IsEditable { get; set; }


        public static implicit operator Variable(VariableRequest request)
        {
            if (null == request)
            {
                return null;
            }

            return new Variable()
            {
                Name = request.Name,
                IsAlphanumeric = request.IsAlphanumeric,
                DefaultValue = request.DefaultValue,
                IsActive = request.IsActive,   
                Length = request.Length,
                IsEditable = request.IsEditable,
            };
        }
    }
}
