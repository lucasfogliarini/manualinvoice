﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public sealed class InvoiceApproverRequest
    {
        public int Id { get; set; }
        public int UserId { get; set; }

        public static implicit operator InvoiceApprover(InvoiceApproverRequest request)
        {
            if (request == null)
            {
                return null;
            }

            var result = new InvoiceApprover();

            result.Id = request.Id;
            result.UserId = request.UserId;

            return result;
        }
    }
}
