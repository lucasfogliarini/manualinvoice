﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Requests
{
    public class SkuRequest
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public string OrderBy { get; set; }
        public bool OrderByDesc { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public InvoiceType InvoiceType { get; set; }
    }
}
