﻿namespace ManualInvoice.Api.Models.Requests
{
    public class OriginalInvoiceRequest
    {
        public int? Id { get; set; }
        public int? Branch { get; set; }
        public string Number { get; set; }
        public string Order { get; set; }
        public string DofImportNumber { get; set; }
    }
}
