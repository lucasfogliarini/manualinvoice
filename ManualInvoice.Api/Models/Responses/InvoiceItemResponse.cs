﻿namespace ManualInvoice.Api.Models.Responses
{
    public sealed class InvoiceItemResponse
    {
        public InvoiceItemResponse(long id, string code, string name, string ncm, int quantity, string rmaNumber, decimal price)
        {
            Id = id;
            Code = code;
            Name = name;
            Ncm = ncm;
            Quantity = quantity;
            RmaNumber = rmaNumber;
            Price = price;
            TotalValue = price * quantity;
        }

        public long Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Ncm { get; set; }

        public int Quantity { get; set; }

        public string RmaNumber { get; set; }

        public decimal Price { get; set; }

        public decimal TotalValue { get; set; }
    }
}
