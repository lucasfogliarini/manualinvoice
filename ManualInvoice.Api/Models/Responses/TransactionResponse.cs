﻿using ManualInvoice.Domain.Common;

namespace ManualInvoice.Api.Models.Responses
{
    public class TransactionResponse
    {
        public TransactionResponse()
        {
        }

        public TransactionResponse(int id, string name, TransactionType? type, string shipToCode, string billToCode)
            : this()
        {
            Id = id;
            Name = name;
            Type = type;
            NopShipTo = shipToCode;
            NopBillTo = billToCode;
            Description = $"{NopBillTo} - {Name}";
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public TransactionType? Type { get; set; }
        public string NopBillTo { get; set; }
        public string NopShipTo { get; set; }
        public string Description { get; set; }
    }
}
