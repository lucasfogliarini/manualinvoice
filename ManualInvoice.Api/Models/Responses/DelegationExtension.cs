using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class DelegationExtensions
    {
        public static IQueryable<DelegationSummaryResponse> AsSummary(this IQueryable<Delegation> source)
        {
            return source.Select(response => new DelegationSummaryResponse
            {
                Id = response.Id,
                DelegationBy = response.User.UserName,
                DelegationTo = response.DelegationUsers,
                DelegationDate = response.DelegationDate,
                PeriodFrom = response.PeriodFrom,
                PeriodTo = response.PeriodTo,
                IsCascade = response.IsCascade,
                IsOldApproval = response.IsOldApproval,
                IsActive = response.IsActive,
                UserId = response.UserId
            });
        }

        public static DelegationSummaryResponse AsSummary(this Delegation source)
        {
            return new DelegationSummaryResponse()
            {
                Id = source.Id,
                DelegationDate = source.DelegationDate,
                DelegationTo = source.DelegationUsers,
                PeriodFrom = source.PeriodFrom,
                PeriodTo = source.PeriodTo,
                IsCascade = source.IsCascade,
                IsOldApproval = source.IsOldApproval,
                IsActive = source.IsActive,
                UserId = source.UserId
            };
        }

        public static DelegationCsvResponse ExportToExcel(this DelegationSummaryResponse source)
        {
            return new DelegationCsvResponse
            {
                DelegationBy = source.DelegationBy,
                DelegationTo = source.DelegationTo != null ? 
                    string.Join(", ", source.DelegationTo.Select(d => d.User).Select(u => u.UserName)) : null,
                PeriodFrom = source.PeriodFrom.HasValue ? source.PeriodFrom.Value.ToShortDateString() : string.Empty,
                PeriodTo = source.PeriodTo.HasValue ? source.PeriodTo.Value.ToShortDateString() : string.Empty,
                Cascade = source.IsCascade.ToYesNo(),
                OldRequests = source.IsOldApproval.ToYesNo(),
                Active = source.IsActive.ToYesNo()
            };
        }
    }
}

