﻿using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class SkusExtensions
    {
        public static IEnumerable<SkuItemResponse> AsSkuResponse(this IEnumerable<SkuDto> source)
        {
            return source.Select(dto => dto.AsSkuItemResponse());
        }

        public static SkuItemResponse AsSkuItemResponse(this SkuDto source)
        {
            return new SkuItemResponse
            {
                Code = source.Code,
                Name = source.Name,
                Description = source.Description,
                NcmCode = source.NcmCode,
                Lc116 = source.Lc116,
                CityCode = source.CityCode
            };
        }
    }
}
