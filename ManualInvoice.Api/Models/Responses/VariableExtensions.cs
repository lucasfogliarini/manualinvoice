﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class VariableExtensions
    {
        public static IQueryable<VariableSummaryResponse> AsSummary(this IQueryable<Variable> source, int? id = null)
        {
            var query = source.Select(r => new VariableSummaryResponse
            {
                Id = r.Id,
                Name = r.Name,
                IsActive = r.IsActive,
                IsAlphanumeric = r.IsAlphanumeric,
                IsEditable = r.IsEditable,
                IsSystem = r.IsSystem,
                DefaultValue = r.DefaultValue,
                Length = r.Length,
            });
            if (id != null)
            {
                query = query.Where(e => e.Id == id);
            }
            return query;
        }

        public static VariableSummaryResponse AsSummary(this Variable variable)
        {
            return new VariableSummaryResponse
            {
                Id = variable.Id,
                Name = variable.Name,
                IsActive = variable.IsActive,
                IsAlphanumeric = variable.IsAlphanumeric,
                IsEditable = variable.IsEditable,
                IsSystem = variable.IsSystem,
                DefaultValue = variable.DefaultValue,
                Length = variable.Length,
            };
        }
    }
}
