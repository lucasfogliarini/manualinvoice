﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class ApprovalViewExtensions
    {
        public static IQueryable<ApprovalViewSummaryResponse> AsSummary(this IQueryable<ApprovalView> source)
        {
            var query = source.Select(r => new ApprovalViewSummaryResponse
            {
                Id = r.Id,
                InvoiceId = r.InvoiceId,
                DtRequest = r.DtRequest,
                TransactionType = r.TransactionType,
                UserId = r.UserId,
                UserName = r.UserName,
                TransactionId = r.TransactionId,
                TransactionName = r.TransactionName,
                LevelId = r.LevelId,
                SequenceApproval = r.SequenceApproval,
                ApproverUserId = r.ApproverUserId,
                ApproverId = r.ApproverId
            });           
            return query;
        }
    }
}