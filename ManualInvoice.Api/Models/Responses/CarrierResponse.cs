﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class CarrierResponse
    {
        public CarrierResponse() 
        { }
        public CarrierResponse(long id , string code, string name, long? volumeQuantity, string volumeBrand, string volumeNumbering, decimal? freightValue, decimal? insuranceValue, decimal? weight, string specie, decimal? otherCosts) : this()
        {
            Id = id;
            Code = code;
            Name = name;
            VolumeQuantity = volumeQuantity;
            VolumeBrand = volumeBrand;
            VolumeNumbering = volumeNumbering;
            FreightValue = freightValue;
            InsuranceValue = insuranceValue;
            Weight = weight;
            Specie = specie;
            OtherCosts = otherCosts;
        }
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? VolumeQuantity { get; set; }
        public string VolumeBrand { get; set; }
        public string VolumeNumbering { get; set; }
        public decimal? FreightValue { get; set; }
        public decimal? InsuranceValue { get; set; }
        public decimal? Weight { get; set; }
        public string Specie { get; set; }
        public decimal? OtherCosts { get; set; }
    }
}
