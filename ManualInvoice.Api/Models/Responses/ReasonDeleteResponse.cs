﻿namespace ManualInvoice.Api.Models.Responses
{
    public sealed class ReasonDeleteResponse
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
