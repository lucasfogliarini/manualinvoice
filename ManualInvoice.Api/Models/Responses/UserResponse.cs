﻿using System.Collections;

namespace ManualInvoice.Api.Models.Responses
{
    public class UserResponse : IDataResponse
    {
        public int Total { get; set; }
        public IEnumerable Data { get; set; }
    }
}
