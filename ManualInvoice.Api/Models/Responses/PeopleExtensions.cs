﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class PeopleExtensions
    {
        public static IEnumerable<PersonResponse> AsPeopleResponse(this IEnumerable<CustomerDto> source)
        {
            return source.Select(dto => dto.AsPersonResponse());
        }

        public static PersonResponse AsPersonResponse(this CustomerDto source)
        {
            return new PersonResponse
            {
                Id = source.Id,
                Name = source.Name,
                CpfCgc = source.CpfCgc,
                Code = source.PfjCode,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                Addresses = source.Addresses.AsPersonAddressResponse()
            };
        }

        public static IEnumerable<PersonAddressResponse> AsPersonAddressResponse(this IList<CustomerAddressDto> source)
        {
            return source.Select(dto => dto.AsPersonAddressResponse());
        }

        public static PersonAddressResponse AsPersonAddressResponse(this CustomerAddressDto source)
        {
            return new PersonAddressResponse
            {
                Id = source.Id,
                Code = source.Code,
                CityName = source.CityName,
                State = source.State,
                PublicPlace = source.PublicPlace,
                Number = source.Number,
                StartDate = source.StartDate,
                EndDate = source.EndDate
            };
        }

        public static CustomerRequestDto AsCustomerRequestDto(this PeopleRequest source)
        {
            return new CustomerRequestDto
            {
                Skip = source.Skip,
                Take = source.Take,
                OrderBy = source.OrderBy,
                OrderByDesc = source.OrderByDesc,
                CpfCgc = source.CpfCgc,
                Name = source.Name,
                ActualOnly = source.ActualOnly
            };
        }
    }
}
