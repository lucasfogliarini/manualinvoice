using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class DelegationSummaryResponse
    {
        public long Id { get; set; }
        public DateTime? DelegationDate { get; set; }
        public string DelegationBy { get; set; }
        public IEnumerable<DelegationUser> DelegationTo { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public bool? IsCascade { get; set; }
        public bool? IsOldApproval { get; set; }
        public bool? IsActive { get; set; }
        public long? UserId { get; set; }
    }
}

