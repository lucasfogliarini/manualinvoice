﻿using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using System;

namespace ManualInvoice.Api.Models.Responses
{
    public static class NopExtensions 
    {
        public static NopSummaryResponse AsSummary(this NopDto source)
        {
            return new NopSummaryResponse
            {
                NopCode = source.NopCode,
                NopType = source.NopType,
                TransactionType = source.TransactionType == "E" ? "Inbound" : "Outbound"
            };
        }
    }
}
