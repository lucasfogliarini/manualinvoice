﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class DelegationViewExtensions
    {
        public static IQueryable<DelegationViewSummaryResponse> AsSummary(this IQueryable<DelegationView> source)
        {
            var query = source.Select(r => new DelegationViewSummaryResponse
            {
                Id = r.Id,
                PeriodFrom = r.PeriodFrom,
                PeriodTo = r.PeriodTo,
                IsCascade = r.IsCascade,
                IsOldApproval = r.IsOldApproval,
                DelegationByUserId = r.DelegationByUserId,
                DelegationByUserName = r.DelegationByUserName,
                DelegationToUserId = r.DelegatedUserId
            });
            return query;
        }
    }
}