﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class TransactionExtensions
    {
        public static IQueryable<TransactionSummaryResponse> AsSummary(this IQueryable<Transaction> source, int? key = null)
        {
            var query = source.Select(e => new TransactionSummaryResponse
            {
                Id = e.Id,
                Name = e.Name,
                IsActive = e.IsActive,
                TransactionType = e.TransactionType,
                ShipToCode = e.ShipToCode,
                BillToCode = e.BillToCode,
                Observation = e.Observation,
                Serie = e.Serie,
                Specie = e.Specie,
                TransactionBranches = e.TransactionBranches,
                TransactionLevels = e.TransactionLevels.OrderBy(tl => tl.SequenceApproval),
                TransactionVariables = e.TransactionVariables
            });
            if (key != null)
            {
                query = query.Where(e => e.Id == key);
            }
            return query;
        }

        public static TransactionSummaryResponse AsSummary(this Transaction transaction)
        {
            return new TransactionSummaryResponse()
            {
                Id = transaction.Id,
                Name = transaction.Name,
                IsActive = transaction.IsActive,
                TransactionType = transaction.TransactionType,
                ShipToCode = transaction.ShipToCode,
                BillToCode = transaction.BillToCode,
                TransactionBranches = transaction.TransactionBranches,
                TransactionLevels = transaction.TransactionLevels.OrderBy(tl => tl.SequenceApproval),
                TransactionVariables = transaction.TransactionVariables,
                Observation = transaction.Observation,
                Serie = transaction.Serie,
                Specie = transaction.Specie
            };
        }

        public static TransactionCsvResponse ExportToExcel(this TransactionSummaryResponse source)
        {
            return new TransactionCsvResponse
            {
                Name = source.Name,
                BillToCode = source.BillToCode,
                TransactionBranches = source.TransactionBranches != null ?
                    string.Join(", ", source.TransactionBranches.Select(b => b.Branch?.Name).OrderBy(b => b)) : null,
                TransactionLevels = source.TransactionLevels != null ?
                    string.Join(", ", source.TransactionLevels.Select(l => l.Level?.Name).OrderBy(l => l)) : null,
                TransactionType = source.TransactionType,
                Active = source.IsActive.ToYesNo()
            };
        }
    }
}
