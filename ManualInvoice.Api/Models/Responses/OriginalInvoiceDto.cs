﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Models.Responses
{
    public class OriginalInvoiceDto
    {
        public long Id { get; set; }
        public string DofImportNumber { get; set; }
        public string Branch { get; set; }
        public string Order { get; set; }
        public string Number { get; set; }
        public string Serie { get; set; }
        public string Nop { get; set; }
        public string Customer { get; set; }
        public string Lc116Code { get; set; }
        public string SissCode { get; set; }
        public decimal TotalValue { get; set; }
    }
}
