﻿using ManualInvoice.Domain.Gateways.SkuApi.Dtos;
using System;

namespace ManualInvoice.Api.Models.Responses
{
    public static class SkuExtensions
    {
        public static SkuSummaryResponse AsSummary(this SkuDto source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new SkuSummaryResponse
            {
                Code = source.Code,
                Name = source.Name,
                Description = source.Description,
                Lc116 = source.Lc116,
                NcmCode = source.NcmCode,
                CityCode = source.CityCode
            };
        }
    }
}
