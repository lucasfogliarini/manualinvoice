﻿using ManualInvoice.Domain.Dtos.InvoiceVariable;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class InvoiceVariableExtensions
    {
        public static IDataResponse ToDataResponse(this IList<InvoiceVariableSummaryResponse> source)
        {
            return new Response<InvoiceVariableSummaryResponse>(source);
        }

        public static InvoiceVariableSummaryResponse AsSummary(this InvoiceVariableResponseDto source)
        {
            if (source == null)
            {
                return null;
            }

            var result = new InvoiceVariableSummaryResponse();

            result.Value = source.Value;
            result.Required = source.Required;
            result.Order = source.Order;
            result.IsAlfaNumeric = source.IsAlfaNumeric;
            result.ControlType = source.ControlType.ToString();
            result.Label = source.Label;
            result.Key = source.Key;
            result.Length = source.Length;
            result.VariableId = source.VariableId;

            if (source.Options != null && source.Options.Any())
            {
                result.Options = source.Options
                    .Select(x => new SelectItemResponse() 
                    {
                        Id = x.Id,
                        Name = x.Name,
                        IsCreditCard = x.IsCreditCard
                    }).ToList();
            }

            return result;
        }

        public static IList<InvoiceVariableSummaryResponse> AsSummary(this IEnumerable<InvoiceVariableResponseDto> source)
        {
            if (source == null || !source.Any())
            {
                return new List<InvoiceVariableSummaryResponse>();
            }

            return source.Select(x => x.AsSummary()).ToList();
        }
    }
}
