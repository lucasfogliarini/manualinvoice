﻿using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class LevelCsvResponse
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Invoice")]
        public string IsInvoice { get; set; }

        [DisplayName("Visible")]
        public string IsVisible { get; set; }

        [DisplayName("Active")]
        public string IsActive { get; set; }
    }
}
