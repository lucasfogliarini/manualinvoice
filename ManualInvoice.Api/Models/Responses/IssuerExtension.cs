﻿using ManualInvoice.Domain.Gateways.Dtos;
using System;

namespace ManualInvoice.Api.Models.Responses
{
    public static class IssuerExtension
    {
        public static IssuerSummaryResponse AsSummary(this CustomerDto source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new IssuerSummaryResponse
            {
                Id = source.Id,
                Name = source.Name,
                PfjCode = source.PfjCode,
                CpfCgc = source.CpfCgc,
                Address = source.GetFullAddress(),
                HasCustomerValidVigency = source.HasCustomerValidVigency,
                HasLocationValidVigency = source.HasLocationValidVigency,
                ExistingPerson = source.ExistingPerson
            };
        }
    }
}