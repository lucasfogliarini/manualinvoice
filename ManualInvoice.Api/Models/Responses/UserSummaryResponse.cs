﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class UserSummaryResponse
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Levels { get; set; }
        public string Profile { get; set; }
        public bool IsActive { get; set; }
    }
}
