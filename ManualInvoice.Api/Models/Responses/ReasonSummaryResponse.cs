﻿using ManualInvoice.Domain.Entities;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class ReasonSummaryResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public ReasonType ReasonType { get; set; }

        public bool IsActive { get; set; }
    }
}
