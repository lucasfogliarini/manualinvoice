﻿namespace ManualInvoice.Api.Models.Responses
{
    public class TransactionBranchSummaryResponse
    {
        public TransactionBranchSummaryResponse() { }

        public TransactionBranchSummaryResponse(string id, int? transactionId, int? branchId)
        {
            Id = id;
            TransactionId = transactionId;
            BranchId = branchId;
        }

        public string Id { get; set; }
        public int? TransactionId { get; set; }
        public int? BranchId { get; set; }
    }
}
