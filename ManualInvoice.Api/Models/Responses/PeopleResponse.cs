﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class PeopleResponse
    {
        public int Count { get; set; }
        public IEnumerable<PersonResponse> Value { get; set; }
    }
}
