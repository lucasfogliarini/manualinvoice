﻿using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Models.Responses
{
    public static class CarrierExtension
    {
        public static CarrierResponse AsSummary(this Carrier source)
        {
            if (source != null)
            {
                return new CarrierResponse()
                {
                    Id = source.Id,
                    Code = source.Code,
                    Name = source.Name,
                    VolumeQuantity = source.VolumeQuantity,
                    VolumeBrand = source.VolumeBrand,
                    VolumeNumbering = source.VolumeNumbering,
                    FreightValue = source.FreightValue,
                    InsuranceValue = source.InsuranceValue,
                    Weight = source.Weight,
                    Specie = source.Specie,
                    OtherCosts = source.OtherCosts
                };
            }
            return null;
        }
    }
}
