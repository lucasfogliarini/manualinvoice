﻿using System;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class PersonResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CpfCgc { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IEnumerable<PersonAddressResponse> Addresses { get; set; }
    }
}
