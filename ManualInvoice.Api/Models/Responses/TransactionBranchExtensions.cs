﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class TransactionBranchExtensions
    {
        public static IQueryable<TransactionBranchSummaryResponse> AsSummary(this IQueryable<TransactionBranch> source)
        {
            return source.Select(e => new TransactionBranchSummaryResponse(e.Id, e.Transaction.Id, e.Branch.Id));
        }
    }
}
