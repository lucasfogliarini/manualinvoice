﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class InvoiceVariableSummaryResponse
    {
        public InvoiceVariableSummaryResponse()
        {
            this.Options = new List<SelectItemResponse>();
        }

        public string Value { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public bool Required { get; set; }
        public bool IsAlfaNumeric { get; set; }
        public int Order { get; set; }
        public string ControlType { get; set; }
        public long Length { get; set; }
        public IList<SelectItemResponse> Options { get; set; }
        public int VariableId { get; set; }
    }
}
