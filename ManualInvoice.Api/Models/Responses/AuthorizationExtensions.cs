﻿using ManualInvoice.Domain.Gateways.Dtos;

namespace ManualInvoice.Api.Models.Responses
{
    public static class AuthorizationExtensions
    {
        public static UserAuthorizationResponse ToResponse(this UserAuthorizationDto source)
        {
            return new UserAuthorizationResponse
            {
                Permissions = source.Permissions,
                Token = source.Token
            };
        }
    }
}
