﻿namespace ManualInvoice.Api.Models.Responses
{
    public class SkuItemResponse
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string NcmCode { get; set; }
        public string Lc116 { get; set; }
        public string CityCode { get; set; }
    }
}
