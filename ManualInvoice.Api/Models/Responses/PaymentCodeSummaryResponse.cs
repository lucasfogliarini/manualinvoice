﻿namespace ManualInvoice.Api.Models.Responses
{
    public sealed class PaymentCodeSummaryResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool? IsCreditCard { get; set; }
    }
}
