﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using System;

namespace ManualInvoice.Api.Models.Responses
{
    public class InvoiceSummaryResponse
    {
        public long? Id { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? TransactionId { get; set; }
        public string TransactionName { get; set; }
        public TransactionType? TransactionType { get; set; }
        public string NopBillTo { get; set; }
        public string NopShipTo { get; set; }
        public string BillToCode { get; set; }
        public InvoiceStatusType? RequestStatus { get; set; }
        public InvoiceType? InvoiceType { get; set; }
        public string SynInvoiceNumber { get; set; }
        public string ServiceInvoiceNumber { get; set; }
        public DateTime? RequestDate { get; set; }
        public string CreateReason { get; set; }
        public int? CreateReasonId { get; set; }
        public string RejectReason { get; set; }
        public int? RejectReasonId { get; set; }
        public string RequestUserName { get; set; }
        public Carrier Carrier { get; set; }
    }
}
