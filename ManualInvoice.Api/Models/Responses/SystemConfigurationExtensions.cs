﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class SystemConfigurationExtensions
    {
        public static IQueryable<SystemConfigurationSummaryResponse> AsSummary(this IQueryable<SystemConfiguration> source)
        {
            var query = source.Select(r => new SystemConfigurationSummaryResponse
            {
                Id = r.Id,
                DaysDraftRequest = r.DaysDraftRequest,
                EmailRemember = r.EmailRemember       
    });           
            return query;
        }
        public static SystemConfigurationSummaryResponse AsSummary(this SystemConfiguration systemConfiguration)
        {
            return new SystemConfigurationSummaryResponse()
            {
                Id = systemConfiguration.Id,
                DaysDraftRequest = systemConfiguration.DaysDraftRequest,
                EmailRemember = systemConfiguration.EmailRemember
            };
        }

    }
}