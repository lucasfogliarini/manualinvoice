﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class InvoiceExtensions
    {
        public static IQueryable<InvoiceSummaryResponse> AsSummary(this IQueryable<Invoice> source)
        {
            return source.Select(e => new InvoiceSummaryResponse
            {
                Id = e.Id,
                BranchId = e.Branch.Id,
                BranchName = e.Branch.Name,
                TransactionId = e.Transaction.Id,
                TransactionName = e.Transaction.Name,
                TransactionType = e.Transaction.TransactionType,
                NopBillTo = e.TransactionBillToCode,
                NopShipTo = e.TransactionShipToCode,
                BillToCode = e.BillToCode,
                RequestStatus = e.Status,
                SynInvoiceNumber = e.SynInvoiceNumber,
                ServiceInvoiceNumber = e.ServiceInvoiceNumber,
                RequestDate = e.RequestDate,
                CreateReason = e.Reason.Name,
                CreateReasonId = e.Reason.Id,
                RejectReason = e.RejectReason.Name,
                RejectReasonId = e.RejectReason.Id,
                RequestUserName = e.User.UserName,
                Carrier = e.Carrier,
                InvoiceType = e.InvoiceType
            });
        }

        public static InvoiceCsvResponse ExportToExcel(this InvoiceSummaryResponse source)
        {
            return new InvoiceCsvResponse
            {
                Id = source.Id,
                TransactionName = source.TransactionName,
                TransactionType = source.TransactionType,
                NopBill = source.NopBillTo,
                NopShip = source.NopShipTo,
                Branch = source.BranchName,
                BillTo = source.BillToCode,
                Status = source.RequestStatus,
                SynInvoiceNumber = source.SynInvoiceNumber,
                ServiceInvoiceNumber = source.ServiceInvoiceNumber,
                RegisterDate = source.RequestDate,
                CreateReason = source.CreateReason,
                RejectReason = source.RejectReason,
                User = source.RequestUserName
            };
        }
    }
}
