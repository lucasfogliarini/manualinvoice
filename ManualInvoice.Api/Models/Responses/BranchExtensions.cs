﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class BranchExtensions
    {
        public static IQueryable<BranchSummaryResponse> AsSummary(this IQueryable<Branch> source)
        {
            return source.Select(b => new BranchSummaryResponse(b.Id, b.Name, b.Location, b.Description, b.IsActive));
        }

        public static IEnumerable<BranchSummaryResponse> AsSummary(this IEnumerable<Branch> source)
        {
            return source.Select(b => new BranchSummaryResponse(b.Id, b.Name, b.Location, b.Description, b.IsActive));
        }
        public static BranchSummaryResponse AsSummary(this Branch branch)
        {
            return new BranchSummaryResponse()
            {
                Id = branch.Id,
                Name = branch.Name,
                Location = branch.Location,
                Description = branch.Description,
                IsActive = branch.IsActive,
            };
        }

        public static BranchCsvResponse ExportToExcel(this BranchSummaryResponse source)
        {
            return new BranchCsvResponse
            {
                Name = source.Name,
                Description = source.Description,
                Location = source.Location,
                IsActive = source.IsActive.ToYesNo()
            };
        }
    }
}
