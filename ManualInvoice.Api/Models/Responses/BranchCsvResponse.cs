﻿using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class BranchCsvResponse
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Location")]
        public string Location { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Active")]
        public string IsActive { get; set; }
    }
}
