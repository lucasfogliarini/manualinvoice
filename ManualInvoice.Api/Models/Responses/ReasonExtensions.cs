﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class ReasonExtensions
    {
        public static IQueryable<ReasonSummaryResponse> AsSummary(this IQueryable<Reason> source)
        {
            return source.Select(r => new ReasonSummaryResponse
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                ReasonType = r.ReasonType,
                IsActive = r.IsActive
            });
        }

        public static ReasonSummaryResponse AsSummary(this Reason source)
        {
            return new ReasonSummaryResponse()
            {
                Id = source.Id,
                Name = source.Name,
                Description = source.Description,
                ReasonType = source.ReasonType,
                IsActive = source.IsActive
            };
        }

        public static ReasonCsvResponse ExportToExcel(this ReasonSummaryResponse source)
        {
            return new ReasonCsvResponse
            {
                Name = source.Name,
                Description = source.Description,
                Type = source.ReasonType,
                IsActive = source.IsActive.ToYesNo()
            };
        }
    }
}
