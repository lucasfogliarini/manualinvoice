﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class UserAuthorizationResponse
    {
        public string Token { get; set; }
        public IList<string> Permissions { get; set; }
    }
}
