﻿namespace ManualInvoice.Api.Models.Responses
{
    public class IssuerSummaryResponse
    {
        public int Id { get; set; }
        public string PfjCode { get; set; }
        public string Name { get; set; }
        public string CpfCgc { get; set; }
        public string Address { get; set; }
        public bool HasCustomerValidVigency { get; set; }
        public bool HasLocationValidVigency { get; set; }
        public bool ExistingPerson { get; set; }
    }
}
