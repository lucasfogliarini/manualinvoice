﻿using System;
using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public class RequestDetailCsvResponse
    {
        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("Transaction Name")]
        public string TransactionName { get; set; }
        [DisplayName("Request Amount")]
        public decimal RequestAmount { get; set; }
        [DisplayName("User")]
        public string RequestUserName { get; set; }
        [DisplayName("Request Date")]
        public DateTime? RequestDate { get; set; }
        [DisplayName("Approval Level")]
        public string LevelName { get; set; }
        [DisplayName("Approval Name")]
        public string ApproverName { get; set; }
        [DisplayName("Approval Date")]
        public DateTime? ApprovalDate { get; set; }
        [DisplayName("Aging Request Approval Date")]
        public string AgingRequestApprovalDate { get; set; }
        [DisplayName("NF Number")]
        public string InvoiceNumber { get; set; }
        [DisplayName("Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
        [DisplayName("Invoice Amount")]
        public decimal? InvoiceAmount { get; set; }
        [DisplayName("Last User")]
        public string LastUser { get; set;  }
        [DisplayName("Delta MR X I")]
        public decimal? Delta { get; set; }
        [DisplayName("Aging Request to Invoicing")]
        public string AgingRequestInvoiceDate { get; set; }
    }
}
