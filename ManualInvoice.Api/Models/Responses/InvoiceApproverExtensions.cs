﻿using ManualInvoice.Domain.Dtos.InvoiceApprover;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class InvoiceApproverExtensions
    {
        public static InvoiceApproverSummaryResponse AsSummary(this InvoiceApproverResponseDto source)
        {
            if (source == null)
            {
                return null;
            }

            var result = new InvoiceApproverSummaryResponse();

            result.Id = source.Id;
            result.LevelId = source.LevelId;
            result.Level = source.Level;
            result.UserId = source.UserId;
            result.User = source.User;
            result.ApprovalSequence = source.ApprovalSequence;
            result.EffectiveUser = source.EffectiveUser;
            result.Situation = source.Situation;
            result.RejectReason = source.RejectReason;
            result.ApprovalDate = source.ApprovalDate;

            if (source.Users != null && source.Users.Any())
            {
                result.Users = source.Users
                    .Select(x => new SelectItemResponse()
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();
            }

            return result;
        }

        public static IList<InvoiceApproverSummaryResponse> AsSummary(this IEnumerable<InvoiceApproverResponseDto> source)
        {
            if (source == null || !source.Any())
            {
                return new List<InvoiceApproverSummaryResponse>();
            }

            return source.Select(x => x.AsSummary()).ToList();
        }
    }
}
