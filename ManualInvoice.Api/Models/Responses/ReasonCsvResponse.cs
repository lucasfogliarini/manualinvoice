﻿using ManualInvoice.Domain.Entities;
using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class ReasonCsvResponse
    {
        [DisplayName("Name")]
        public string Name { get; set; }
        
        [DisplayName("Description")]
        public string Description { get; set; }
        
        [DisplayName("Type")]
        public ReasonType Type { get; set; }
        
        [DisplayName("Active")]
        public string IsActive { get; set; }
    }
}
