﻿using ManualInvoice.Domain.Common;
using System;
using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public class InvoiceCsvResponse
    {
        [DisplayName("Id")]
        public long? Id { get; set; }
        
        [DisplayName("Transaction")]
        public string TransactionName { get; set; }

        [DisplayName("Transaction Type")]
        public TransactionType? TransactionType { get; set; }

        [DisplayName("Nop Bill")]
        public string NopBill { get; set; }

        [DisplayName("Nop Ship")]
        public string NopShip { get; set; }

        [DisplayName("Branch")]
        public string Branch { get; set; }

        [DisplayName("Bill To")]
        public string BillTo { get; set; }

        [DisplayName("Status")]
        public InvoiceStatusType? Status { get; set; }

        [DisplayName("Inv Num")]
        public string SynInvoiceNumber { get; set; }

        [DisplayName("Serv Inv Num")]
        public string ServiceInvoiceNumber { get; set; }

        [DisplayName("Register Date")]
        public DateTime? RegisterDate { get; set; }

        [DisplayName("Create Reason")]
        public string CreateReason { get; set; }

        [DisplayName("Reject Reason")]
        public string RejectReason { get; set; }

        [DisplayName("User Name")]
        public string User { get; set; }
    }
}
