﻿using ManualInvoice.Domain.Entities;
namespace ManualInvoice.Api.Models.Responses
{
    public sealed class LevelSummaryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Visible { get; set; }
        public bool? Invoice { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}
