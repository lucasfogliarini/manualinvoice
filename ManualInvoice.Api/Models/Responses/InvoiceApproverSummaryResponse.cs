﻿using ManualInvoice.Domain.Common;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class InvoiceApproverSummaryResponse
    {
        public InvoiceApproverSummaryResponse()
        {
            Users = new List<SelectItemResponse>();
        }

        public int Id { get; set; }
        public int? UserId { get; set; }
        public string User { get; set; }
        public int LevelId { get; set; }
        public string Level { get; set; }
        public IList<SelectItemResponse> Users { get; set; }
        public ApproverStatusType? Situation { get; set; }
        public string EffectiveUser { get; set; }
        public long? ApprovalSequence { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string RejectReason { get; set; }
    }
}
