﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class LevelExtensions
    {
        public static IQueryable<LevelSummaryResponse> AsSummary(this IQueryable<Level> source)
        {
            return source.Select(r => new LevelSummaryResponse
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Invoice = r.Invoice,
                Visible = r.Visible,
                IsActive = r.IsActive
            });
        }

        public static LevelSummaryResponse AsSummary(this Level level)
        {
            return new LevelSummaryResponse()
            {
                Id = level.Id,
                Name = level.Name,
                Visible = level.Visible,
                Invoice = level.Invoice,
                Description = level.Description,
                IsActive = level.IsActive,
            };
        }

        public static LevelCsvResponse ExportToExcel(this LevelSummaryResponse source)
        {
            return new LevelCsvResponse
            {
                Name = source.Name,
                Description = source.Description,
                IsVisible = source.Visible.ToYesNo(),
                IsInvoice = source.Invoice.ToYesNo(),
                IsActive = source.IsActive.ToYesNo()
            };
        }
    }
}
