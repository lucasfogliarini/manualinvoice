﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class Response<T> : IDataResponse
    {
        public Response(IEnumerable<T> source)
        {
            Data = source;
            Total = source != null && source.Any() ? source.Count() : 0;
        }

        public int Total { get; set; }
        public IEnumerable Data { get; set; }
    }
}
