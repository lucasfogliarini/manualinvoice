﻿using System.Collections;

namespace ManualInvoice.Api.Models.Responses
{
    public interface IDataResponse
    {
        int Total { get; set; }

        IEnumerable Data { get; set; }
    }
}
