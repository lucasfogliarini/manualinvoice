﻿using System;
namespace ManualInvoice.Api.Models.Responses
{
    public sealed class DelegationViewSummaryResponse
    {
        public int Id { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public bool IsCascade { get; set; }
        public bool IsOldApproval { get; set; }
        public int DelegationByUserId { get; set; }
        public string DelegationByUserName { get; set; }
        public int DelegationToUserId { get; set; }
    }
}
