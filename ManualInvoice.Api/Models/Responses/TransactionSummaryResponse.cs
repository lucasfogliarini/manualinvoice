﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class TransactionSummaryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Observation { get; set; }
        public bool? IsActive { get; set; }
        public string BillToCode { get; set; }
        public string ShipToCode { get; set; }
        public string Serie { get; set; }
        public string Specie { get; set; }
        public TransactionType? TransactionType { get; set; }
        public IEnumerable<TransactionLevel> TransactionLevels { get; set; }
        public IEnumerable<TransactionBranch> TransactionBranches { get; set; }
        public IEnumerable<TransactionVariable> TransactionVariables { get; set; }
    }
}
