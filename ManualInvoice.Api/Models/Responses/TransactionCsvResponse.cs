﻿using ManualInvoice.Domain.Common;
using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public class TransactionCsvResponse
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("NOP (Bill To)")]
        public string BillToCode { get; set; }

        [DisplayName("Branch")]
        public string TransactionBranches { get; set; }

        [DisplayName("Level of Approval")]
        public string TransactionLevels { get; set; }

        [DisplayName("Transaction Type")]
        public TransactionType? TransactionType { get; set; }

        [DisplayName("Active")]
        public string Active { get; set; }
    }
}
