﻿using ManualInvoice.Domain.Enum;

namespace ManualInvoice.Api.Models.Responses
{
    public class NopSummaryResponse
    {
        public int Id { get; set; }
        public string NopCode { get; set; }
        public NopType NopType { get; set; }
        public string TransactionType { get; set; }
    }
}