﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class SkuResponse
    {
        public int Count { get; set; }
        public IEnumerable<SkuItemResponse> Value { get; set; }
    }
}
