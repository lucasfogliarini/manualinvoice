﻿using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Enum;
using System;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class RequestInvoiceExtensions
    {
        public static RequestInvoiceSummaryResponse AsSummary(this Invoice source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var result = new RequestInvoiceSummaryResponse();

            result.Id = source.Id;
            result.Status = source.Status;
            result.InvoiceType = source.InvoiceType;
            result.RequestDate = source.RequestDate;
            result.IsConsolidated = source.IsConsolidated;
            result.SynInvoiceNumber = source.SynInvoiceNumber;

            if (source.Transaction != null)
            {
                var transaction = source.Transaction;
                result.TransactionId = transaction.Id;
                result.Transaction = new TransactionResponse(transaction.Id, transaction.Name, transaction.TransactionType, transaction.ShipToCode, transaction.BillToCode);
            }

            if (source.Branch != null)
            {
                result.BranchId = source.Branch.Id;
            }

            if (source.Reason != null)
            {
                result.RequestReasonId = source.Reason.Id;
            }

            if (source.User != null)
            {
                result.UserName = source.User.UserName;
            }

            result.BillTo = new IssuerResponse(source.BillToCode, source.BillToCnpj, source.BillToName, source.BillToAddress, source.TransactionBillToCode);
            result.ShipTo = new IssuerResponse(source.ShipToCode, source.ShipToCnpj, source.ShipToName, source.ShipToAddress, source.TransactionShipToCode);

            result.DanfeObservation = source.Observation;
            result.DanfeInstruction = source.Instruction;
            result.DanfeObservationNotComp = source.ObservationTwo;

            result.Carrier = source.Carrier.AsSummary();

            if (source.Items != null && source.Items.Any())
            {
                result.Items = source.Items
                    .Where(item => item.DellBaseFlag != DellBaseFlagType.Rollup)
                    .Select(item => new InvoiceItemResponse(item.Id, item.ProductCode, item.ProductName, item.Ncm, item.Quantity ?? 0, item.RmaCode, item.Price ?? 0))
                    .ToList();
            }

            return result;
        }
    }
}
