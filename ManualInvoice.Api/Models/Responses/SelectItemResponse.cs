﻿namespace ManualInvoice.Api.Models.Responses
{
    public sealed class SelectItemResponse
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public bool IsCreditCard { get; set; }
    }
}
