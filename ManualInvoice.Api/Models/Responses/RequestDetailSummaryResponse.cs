﻿using System;

namespace ManualInvoice.Api.Models.Responses
{
    public class RequestDetailSummaryResponse
    {       
        public int Id { get; set; }
        public string TransactionName { get; set; }
        public decimal RequestAmount { get; set; }
        public string RequestUserName { get; set; }
        public DateTime? RequestDate { get; set; }
        public string LevelName { get; set; }
        public string ApproverName { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string AgingRequestApprovalDate { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string LastUser { get; set; }
        public decimal? Delta { get; set; }
        public string AgingRequestInvoiceDate { get; set; }      
    }
}
