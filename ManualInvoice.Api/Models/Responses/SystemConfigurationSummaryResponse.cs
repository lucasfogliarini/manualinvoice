﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;
namespace ManualInvoice.Api.Models.Responses
{
    public sealed class SystemConfigurationSummaryResponse
    {
        public int Id { get; set; }
        public int DaysDraftRequest { get; set; }
        public int EmailRemember { get; set; }
    }
}
