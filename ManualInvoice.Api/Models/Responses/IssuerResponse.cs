﻿namespace ManualInvoice.Api.Models.Responses
{
    public class IssuerResponse
    {
        public IssuerResponse()
        {
        }

        public IssuerResponse(string code, string cpfCgc, string name, string address, string nop) 
            : this()
        {
            Code = code;
            CpfCgc = cpfCgc;
            Name = name;
            Address = address;
            Nop = nop;
        }

        public string Code { get; set; }

        public string CpfCgc { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Nop { get; set; }
    }
}
