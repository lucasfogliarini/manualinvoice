﻿using ManualInvoice.Domain.Entities;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class PaymentCodeExtensions
    {
        public static IQueryable<PaymentCodeSummaryResponse> AsSummary(this IQueryable<PaymentCode> source)
        {
            var query = source.Select(r => new PaymentCodeSummaryResponse
            {
                Id = r.Id,
                Code = r.Code,
                IsActive = r.IsActive,
                Description = r.Description,
                IsCreditCard = r.IsCreditCard
            });
            return query;
        }
        public static PaymentCodeSummaryResponse AsSummary(this PaymentCode paymentCode)
        {
            return new PaymentCodeSummaryResponse
            {
                Id = paymentCode.Id,
                Code = paymentCode.Code,
                IsActive = paymentCode.IsActive,
                Description = paymentCode.Description,
                IsCreditCard = paymentCode.IsCreditCard
            };
        }
    }
}
