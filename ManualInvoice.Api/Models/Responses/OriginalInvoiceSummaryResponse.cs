﻿using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public class OriginalInvoiceSummaryResponse
    {
        public long TotalRecords { get; set; }
        public List<OriginalInvoiceDto> Invoices { get; set; }
    }
}
