namespace ManualInvoice.Api.Models.Responses
{
    public sealed class DelegationDeleteResponse
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}

