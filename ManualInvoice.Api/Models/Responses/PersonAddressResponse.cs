﻿using System;

namespace ManualInvoice.Api.Models.Responses
{
    public class PersonAddressResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string CityName { get; set; }
        public string State { get; set; }
        public string PublicPlace { get; set; }
        public string Number { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
