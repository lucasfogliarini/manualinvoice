﻿using ManualInvoice.Domain.Gateways.CommonApi.Dtos;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class OriginalInvoiceExtension
    {
        public static OriginalInvoiceSummaryResponse AsSummary(this ResponseDto<IList<FiscalDocumentSimpleDto>> source)
        {
            return new OriginalInvoiceSummaryResponse
            {
                TotalRecords = source.TotalRecords,
                Invoices = source.Data.Select(r => new OriginalInvoiceDto
                {
                    Id = r.Id,
                    DofImportNumber = r.DofImportNumber,
                    Branch = r.Branch,
                    Order = r.OrderNumber,
                    Number = r.NfOrRpsNumber,
                    Serie = r.Serie,
                    Nop = r.NopCodigo,
                    Customer = r.ShipTo,
                    Lc116Code = r.Items?.OrderBy(x => x.IdfNum).FirstOrDefault().Lc116Code,
                    SissCode = r.Items?.OrderBy(x => x.IdfNum).FirstOrDefault().SissCodigo,
                    TotalValue = r.TotalInvoicedAmount
                }).ToList()
            };
        }
    }
}
