﻿using System;
using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public class DelegationCsvResponse
    {
        [DisplayName("Delegation By")]
        public string DelegationBy { get; set; }

        [DisplayName("Delegation To")]
        public string DelegationTo { get; set; }

        [DisplayName("Period From")]
        public string PeriodFrom { get; set; }

        [DisplayName("Period To")]
        public string PeriodTo { get; set; }

        [DisplayName("Cascade")]
        public string Cascade { get; set; }

        [DisplayName("Old Requests")]
        public string OldRequests { get; set; }

        [DisplayName("Active")]
        public string Active { get; set; }
    }
}
