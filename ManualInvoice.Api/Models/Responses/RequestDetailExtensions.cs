﻿using ManualInvoice.Domain.Entities;
using System;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class RequestDetailExtensions
    {
        public static IQueryable<RequestDetailSummaryResponse> AsSummary(this IQueryable<RequestDetail> source)
        {
            return source.Select(requestDetail => new RequestDetailSummaryResponse
            {                
                Id = requestDetail.Id,
                TransactionName = requestDetail.TransactionName,
                RequestAmount = requestDetail.RequestAmount,
                RequestUserName = requestDetail.RequestUserName,
                RequestDate = requestDetail.RequestDate,
                LevelName = requestDetail.LevelName,
                ApproverName = requestDetail.ApproverName,
                ApprovalDate = requestDetail.ApprovalDate,
                InvoiceNumber = requestDetail.InvoiceNumber,
                InvoiceDate = requestDetail.InvoiceDate,
                InvoiceAmount = requestDetail.InvoiceAmount,
                Delta = requestDetail.InvoiceAmount.HasValue ? requestDetail.InvoiceAmount.Value - requestDetail.RequestAmount : default(decimal?),
                LastUser = requestDetail.ApproverName ?? requestDetail.RequestUserName,
                AgingRequestApprovalDate = TimeToString(requestDetail.AgingRequestApprovalDate, true),
                AgingRequestInvoiceDate = TimeToString(requestDetail.AgingRequestInvoiceDate, false),
            });
        }

        public static RequestDetailCsvResponse ExportToExcel(this RequestDetailSummaryResponse requestDetail)
        {
            return new RequestDetailCsvResponse
            {
                Id = requestDetail.Id,
                TransactionName = requestDetail.TransactionName,
                RequestAmount = requestDetail.RequestAmount,
                RequestUserName = requestDetail.RequestUserName,
                RequestDate = requestDetail.RequestDate,
                LevelName = requestDetail.LevelName,
                ApproverName = requestDetail.ApproverName,
                ApprovalDate = requestDetail.ApprovalDate,
                InvoiceNumber = requestDetail.InvoiceNumber,
                InvoiceDate = requestDetail.InvoiceDate,
                InvoiceAmount = requestDetail.InvoiceAmount,
                Delta = requestDetail.Delta.HasValue ? (decimal?)Math.Round(requestDetail.Delta.Value, 2) : null,
                LastUser = requestDetail.LastUser,
                AgingRequestApprovalDate = requestDetail.AgingRequestApprovalDate,
                AgingRequestInvoiceDate = requestDetail.AgingRequestInvoiceDate,
            };
        }

        private static string TimeToString(TimeSpan? time, bool inHours)
        {
            if (time.HasValue)
            {
                if (inHours)
                {
                    var minSec = time.Value.ToString(@"mm\:ss");
                    return $"{(int)time.Value.TotalHours}:{minSec}";
                }
                else
                {
                    return time.Value.ToString(@"d\.hh\:mm\:ss");
                }
            }
            return null;
        }
    }       
}
