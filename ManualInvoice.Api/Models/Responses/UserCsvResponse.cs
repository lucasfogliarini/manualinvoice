﻿using System.ComponentModel;

namespace ManualInvoice.Api.Models.Responses
{
    public class UserCsvResponse
    {
        [DisplayName("Name")]
        public string User { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Profiles")]
        public string Profiles { get; set; }

        [DisplayName("Levels of Approval")]
        public string Levels { get; set; }

        [DisplayName("Active")]
        public string Active { get; set; }
    }
}
