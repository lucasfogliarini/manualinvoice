﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;
namespace ManualInvoice.Api.Models.Responses
{
    public sealed class ApprovalViewSummaryResponse
    {
        public int Id { get; set; }
        public long InvoiceId { get; set; }
        public DateTime? DtRequest { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int TransactionId { get; set; }
        public string TransactionName { get; set; }
        public TransactionType? TransactionType { get; set; }
        public int LevelId { get; set; }
        public int SequenceApproval { get; set; }
        public int ApproverUserId { get; set; }
        public int ApproverId { get; set; }
    }
}
