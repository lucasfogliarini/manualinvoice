﻿namespace ManualInvoice.Api.Models.Responses
{
    public sealed class BranchSummaryResponse
    {
        public BranchSummaryResponse()
        {
        }

        public BranchSummaryResponse(int id, string name, string location, string description, bool? isActive)
            : this()
        {
            Id = id;
            Name = name;
            Location = location;
            Description = description;
            IsActive = isActive;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}
