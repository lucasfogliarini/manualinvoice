﻿using ManualInvoice.Domain.Entities;
using System.Collections.Generic;
namespace ManualInvoice.Api.Models.Responses
{
    public sealed class VariableSummaryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAlphanumeric { get; set; }
        public bool IsEditable { get; set; }
        public bool? IsSystem { get; set; }
        public string DefaultValue { get; set; }
        public bool IsActive { get; set; }
        public long? Length { get; set; }
    }
}
