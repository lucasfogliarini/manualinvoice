﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.Dtos;
using System.Linq;

namespace ManualInvoice.Api.Models.Responses
{
    public static class UserExtensions
    {
        public static IQueryable<UserSummaryResponse> AsSummary(this IQueryable<User> source)
        {
            return source.Select(r => new UserSummaryResponse
            {
                Id = r.Id,
                UserName = r.UserName,
                Email = r.Email,
                Levels = r.LevelUsers.Select(s => s.Level.Name).ToList(),
                Profile = "Admin", // TODO: create profiles from security api
                IsActive = r.IsActive
            });
        }

        public static IDataResponse ToDataResponse(this UserResponseDto source)
        {
            return new UserResponse
            {
                Data = source.Users,
                Total = source.Total
            };
        }

        public static UserCsvResponse ExportToExcel(this UserResponseItemDto source)
        {
            return new UserCsvResponse
            {
                User = source.NtAccount,
                Email = source.Email,
                Profiles = source.Profiles != null ?
                    string.Join(", ", source.Profiles.Select(p => p).OrderBy(p => p)) : null,
                Levels = source.Levels != null ? 
                    string.Join(", ", source.Levels.Select(l => l.Name).OrderBy(l => l)) : null,
                Active = source.IsActive.ToYesNo()
            };
        }
    }
}
