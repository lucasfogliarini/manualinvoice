﻿using ManualInvoice.Domain.Common;
using ManualInvoice.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ManualInvoice.Api.Models.Responses
{
    public sealed class RequestInvoiceSummaryResponse
    {
        public RequestInvoiceSummaryResponse()
        {
            Items = new List<InvoiceItemResponse>();
        }

        public long Id { get; set; }

        public InvoiceStatusType? Status { get; set; }
        
        public InvoiceType? InvoiceType { get; set; }

        public DateTime? RequestDate { get; set; }

        public string UserName { get; set; }

        public int TransactionId { get; set; }

        public TransactionResponse Transaction { get; set; }

        public int BranchId { get; set; }

        public int RequestReasonId { get; set; }

        public IssuerResponse BillTo { get; set; }

        public IssuerResponse ShipTo { get; set; }

        public string DanfeObservation { get; set; }

        public string DanfeObservationNotComp { get; set; }

        public string DanfeInstruction { get; set; }

        public bool IsConsolidated { get; set; }

        public IList<InvoiceItemResponse> Items { get; set; }

        public CarrierResponse Carrier { get; set; }

        public string SynInvoiceNumber { get; set; }
    }
}
