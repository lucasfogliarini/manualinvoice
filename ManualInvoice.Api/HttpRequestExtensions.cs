﻿using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace ManualInvoice.Api
{
    public static class HttpRequestExtensions
    {
        public static string GetNTAccount(this HttpRequest httpRequest)
        {
            try
            {
                var authorizationHeader = httpRequest.Headers["Authorization"];

                var tokenEncoded = authorizationHeader[0].Replace("Bearer ", "");

                var handler = new JwtSecurityTokenHandler();
                var jwtToken = handler.ReadJwtToken(tokenEncoded);
                var ntAccount = jwtToken.Claims.FirstOrDefault(e => e.Type == "winaccountname");
                return ntAccount.Value;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("NTAccount was not found.");
            }
        }
    }
}
