﻿using Microsoft.AspNet.OData.Query;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ManualInvoice.Api.Extensions
{
    public static class ODataExtensions
    {
        public static object AsTypedResult(this object item, Type resultType)
        {
            var items = new[] { item };
            var result = items.AsTypedResult(resultType);
            var enumerator = result.GetEnumerator();
            if (enumerator.MoveNext())
            {
                return enumerator.Current;
            }
            return null;
        }

        public static IEnumerable AsTypedResult(this IEnumerable items, Type resultType)
        {
            var resultItems = new ODataQueryResultItems();

            foreach (var item in items)
            {
                var queryResultItem = GetQueryResultItem(item, resultType);
                if (queryResultItem != null)
                {
                    resultItems.Attach(queryResultItem);
                }
            }

            resultItems.Accept(new ODataExportVisitor());

            return resultItems.GetItems();
        }

        public static ODataQueryResultItem GetQueryResultItem(object queryItem, Type resultType)
        {
            var itemTypeName = queryItem.GetType().Name;
            
            if (itemTypeName == resultType.Name)
            {
                return new SameTypeItem(queryItem, resultType);
            }
            else if (itemTypeName == "SelectSome`1")
            {
                return new SelectSomeItem(queryItem, resultType);
            }
            else if (itemTypeName == "SelectAll`1")
            {
                return new SelectAllItem(queryItem, resultType);
            }
            else if (itemTypeName == "SelectAllAndExpand`1")
            {
                return new SelectAllAndExpandItem(queryItem, resultType);
            }
            else if (itemTypeName == "SelectEnumerableIterator`2")
            {
                return new SelectEnumerableIteratorItem(queryItem, resultType);
            }
            else if (itemTypeName == "SingleExpandedProperty`1")
            {
                return new SingleExpandedPropertyItem(queryItem, resultType);
            }
            else if (itemTypeName == "NamedProperty`1")
            {
                return new NamedPropertyItem(queryItem, resultType);
            }
            else if (itemTypeName.StartsWith("NamedPropertyWithNext"))
            {
                return new NamedPropertyWithNextItem(queryItem, resultType);
            }
            else
            {
                throw new NotImplementedException($"OData export cannot traverse item of '{itemTypeName}' type.");
            }
        }

        public class ODataQueryResultItems
        {
            private readonly List<ODataQueryResultItem> _items;

            public ODataQueryResultItems() => _items = new List<ODataQueryResultItem>();

            public void Attach(ODataQueryResultItem item) => _items.Add(item);

            public void Accept(ODataQueryVisitor visitor) => _items.ForEach(i => i.Accept(visitor));

            internal IEnumerable GetItems() => _items.Select(i => i.Result).Where(i => i != null).ToList();
        }

        public abstract class ODataQueryResultItem
        {
            public object QueryItem { get; set; }
            public object Result { get; set; }
            public Type ResultType { get; set; }
            public abstract void Accept(ODataQueryVisitor visitor);

            public ODataQueryResultItem(object queryItem, Type resultType)
            {
                QueryItem = queryItem;
                ResultType = resultType;
            }
        }

        class SameTypeItem : ODataQueryResultItem
        {
            public SameTypeItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSameType(this);
        }

        class SelectSomeItem : ODataQueryResultItem
        {
            public SelectSomeItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSelectSome(this);
        }

        class SelectEnumerableIteratorItem : ODataQueryResultItem
        {
            public SelectEnumerableIteratorItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSelectEnumerableIterator(this);
        }

        class SelectAllItem : ODataQueryResultItem
        {
            public SelectAllItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSelectAll(this);
        }

        class SelectAllAndExpandItem : ODataQueryResultItem
        {
            public SelectAllAndExpandItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSelectAllAndExpand(this);
        }

        class SingleExpandedPropertyItem : ODataQueryResultItem
        {
            public SingleExpandedPropertyItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitSingleExpandedProperty(this);
        }

        class NamedPropertyItem : ODataQueryResultItem
        {
            public NamedPropertyItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.VisitNamedProperty(this);
        }

        class NamedPropertyWithNextItem : ODataQueryResultItem
        {
            public NamedPropertyWithNextItem(object queryItem, Type resultType)
                : base(queryItem, resultType) { }

            public override void Accept(ODataQueryVisitor visitor) => visitor.NamedPropertyWithNextItemProperty(this);
        }

        public abstract class ODataQueryVisitor
        {
            public abstract void VisitSameType(ODataQueryResultItem item);
            public abstract void VisitSelectSome(ODataQueryResultItem item);
            public abstract void VisitSelectEnumerableIterator(ODataQueryResultItem item);
            public abstract void VisitSelectAll(ODataQueryResultItem item);
            public abstract void VisitSelectAllAndExpand(ODataQueryResultItem item);
            public abstract void VisitSingleExpandedProperty(ODataQueryResultItem item);
            public abstract void VisitNamedProperty(ODataQueryResultItem item);
            public abstract void NamedPropertyWithNextItemProperty(ODataQueryResultItem item);
        }

        public class ODataExportVisitor : ODataQueryVisitor
        {
            public override void VisitSameType(ODataQueryResultItem item)
            {
                item.Result = item.QueryItem;
            }

            public override void VisitSelectSome(ODataQueryResultItem item)
            {
                var dictionary = ((ISelectExpandWrapper)item.QueryItem).ToDictionary();
                
                var obj = Activator.CreateInstance(item.ResultType);

                foreach (var keyValuePair in dictionary)
                {
                    var itemType = keyValuePair.Value?.GetType();
                    object propertyValue = null;

                    if (itemType != null && keyValuePair.Value.IsComplex())
                    {
                        var propertyType = item.ResultType
                            .GetProperty(keyValuePair.Key, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).PropertyType;
                        var tempValue = keyValuePair.Value.AsTypedResult(propertyType);

                        var t = propertyType.GenericTypeArguments[0];

                        var listType = typeof(List<>);
                        var genericListType = listType.MakeGenericType(t);
                        var listInstance = (IList)Activator.CreateInstance(genericListType);

                        foreach (var i in (IEnumerable)tempValue)
                        {
                            listInstance.Add(i);
                        }

                        propertyValue = listInstance;
                    }
                    else
                    {
                        propertyValue = keyValuePair.Value;
                    }

                    item.ResultType.GetProperty(keyValuePair.Key, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                        .SetValue(obj, propertyValue, null);
                }

                item.Result = obj;
            }

            public override void VisitSelectEnumerableIterator(ODataQueryResultItem item)
            {
                var toListMethod = item.QueryItem.GetType().GetMethods()
                    .FirstOrDefault(m => m.Name == nameof(Enumerable.ToList));
                var list = (IList)toListMethod.Invoke(item.QueryItem, null);
                var genericType = item.ResultType.GetGenericArguments()[0];
                item.Result = list.AsTypedResult(genericType);
            }

            public override void VisitSelectAll(ODataQueryResultItem item)
            {
                var instanceProperty = item.QueryItem.GetType().GetProperty("Instance");
                var instance = instanceProperty.GetValue(item.QueryItem);
              
                item.Result = instance;
            }

            public override void VisitSelectAllAndExpand(ODataQueryResultItem item)
            {
                var instanceProperty = item.QueryItem.GetType().GetProperty("Instance");
                var instance = instanceProperty.GetValue(item.QueryItem);

                var containerProperty = item.QueryItem.GetType().GetProperty("Container");
                var container = containerProperty.GetValue(item.QueryItem);

                var value = container.AsTypedResult(instance.GetType());

                var namePropertyInfo = container.GetType().GetProperty("Name");
                var nameProperty = namePropertyInfo.GetValue(container);

                var propToUpdate = instance.GetType()
                    .GetProperty(nameProperty.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                var currentValue = propToUpdate.GetValue(instance);
                if (currentValue == null)
                {
                    propToUpdate.SetValue(instance, value);
                }
                
                item.Result = instance;
            }

            public override void VisitSingleExpandedProperty(ODataQueryResultItem item)
            {
                var namePropertyInfo = item.QueryItem.GetType().GetProperty("Name");
                var nameProperty = namePropertyInfo.GetValue(item.QueryItem);

                var valuePropertyInfo = item.QueryItem.GetType().GetProperty("Value");
                var valueProperty = valuePropertyInfo.GetValue(item.QueryItem);

                var innerValueProperty = item.ResultType
                    .GetProperty(nameProperty.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                var innerValuePropertyType = innerValueProperty.PropertyType;

                item.Result = valueProperty.AsTypedResult(innerValuePropertyType);
            }

            public override void VisitNamedProperty(ODataQueryResultItem item)
            {
                var namePropertyInfo = item.QueryItem.GetType().GetProperty("Name");
                var nameProperty = namePropertyInfo.GetValue(item.QueryItem);

                var valuePropertyInfo = item.QueryItem.GetType().GetProperty("Value");
                var valueProperty = valuePropertyInfo.GetValue(item.QueryItem);

                var innerValueProperty = item.ResultType
                    .GetProperty(nameProperty.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                var innerValuePropertyType = innerValueProperty.PropertyType;

                item.Result = valueProperty.AsTypedResult(innerValuePropertyType);
            }

            public override void NamedPropertyWithNextItemProperty(ODataQueryResultItem item)
            {
                var namePropertyInfo = item.QueryItem.GetType().GetProperty("Name");
                var nameProperty = namePropertyInfo.GetValue(item.QueryItem);

                var valuePropertyInfo = item.QueryItem.GetType().GetProperty("Value");
                var valueProperty = valuePropertyInfo.GetValue(item.QueryItem);

                var nextProperties = item.QueryItem.GetType().GetProperties().Where(p => p.Name.StartsWith("Next"));
                foreach (var nextPropertyInfo in nextProperties)
                {
                    var nextProperty = nextPropertyInfo.GetValue(item.QueryItem);

                    var nextNamePropertyInfo = nextProperty.GetType().GetProperty("Name");
                    var nextNameProperty = nextNamePropertyInfo.GetValue(nextProperty);

                    var nextValuePropertyInfo = nextProperty.GetType().GetProperty("Value");
                    var nextValueProperty = nextValuePropertyInfo.GetValue(nextProperty);

                    var innerNextValueProperty = item.ResultType
                        .GetProperty(nextNameProperty.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    var innerNextValuePropertyType = innerNextValueProperty.PropertyType;
                    nextValueProperty.AsTypedResult(innerNextValuePropertyType);
                }

                var innerValueProperty = item.ResultType
                    .GetProperty(nameProperty.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                var innerValuePropertyType = innerValueProperty.PropertyType;

                item.Result = valueProperty.AsTypedResult(innerValuePropertyType);
            }
        }
    }
}
