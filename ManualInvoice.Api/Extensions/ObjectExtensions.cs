﻿using System;

namespace ManualInvoice.Api.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsComplex(this object obj)
        {
            var type = obj.GetType();

            if (type.IsSubclassOf(typeof(ValueType)) || type.Equals(typeof(string)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool IsA<T>(this object obj)
        {
            return obj is T;
        }

        public static string ToYesNo(this bool? boolean)
        {
            return ToYesNo(boolean == true);
        }

        public static string ToYesNo(this bool boolean)
        {
            return boolean ? "Yes" : "No";
        }
    }
}
