﻿using ManualInvoice.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Middlewares
{
    public class GlobalExceptionHandlerMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var problemDetails = new ProblemDetails
            {
                Title = GetTitle(exception),
                Detail = exception.Message,
                Status = GetHttpStatusCode(exception),
                Instance = context.Request.HttpContext.Request.Path,
            };

            context.Response.StatusCode = problemDetails.Status.Value;
            context.Response.ContentType = "application/problem+json";

            var json = JsonConvert.SerializeObject(problemDetails);
            return context.Response.WriteAsync(json);
        }

        private static int GetHttpStatusCode(Exception exception)
        {
            switch (exception)
            {
                case INotFoundException _:
                    return (int)HttpStatusCode.NotFound;
                case IValidationException _:
                    return (int)HttpStatusCode.BadRequest;
                case IDuplicatedException _:
                    return (int)HttpStatusCode.Conflict;
                case IForbiddenException _:
                    return (int)HttpStatusCode.Forbidden;
                default:
                    return (int)HttpStatusCode.InternalServerError;
            }
        }

        private static string GetTitle(Exception exception)
        {
            var title = "Unknown Error";

            if (exception is IDomainException)
            {
                var domainException = exception as IDomainException;
                title = domainException.Title;
            }

            return title;
        }
    }
}
