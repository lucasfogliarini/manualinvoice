﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Api.Handlers;
using ManualInvoice.Api.Middlewares;
using ManualInvoice.Data;
using ManualInvoice.Domain;
using ManualInvoice.Domain.Common;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Formatter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using OracleLoggerProvider;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ManualInvoice.Api
{
    public class Startup
    {
        const string APP_NAME = "ManualInvoiceApi";
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }
        AppSettings AppSettings { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppSettings = AddAppSettings(services);
            AddLogging(services);
            AddUnitOfWork(services);
            AddAuthentication(services);
            services.AddExceptionHandlerMiddleware();
            services.AddServices();
            services.AddValidators();
            services.AddRepositories();
            services.AddGateways(AppSettings);
            AddMvc(services);
            AddAuthorization(services);
            AddSwaggerGen(services);
            AddCors(services);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            // global cors policy
            app.UseCors(x => x
                //.AllowAnyOrigin()
                .SetIsOriginAllowed((origin) => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();

            app.UseMvc(routeBuilder =>
            {
                routeBuilder.MapODataRoutes();
                routeBuilder.Expand().Select().OrderBy().Filter().MaxTop(100).Count();
                routeBuilder.EnableDependencyInjection();
            });

            app.UseSwagger();
            app.UseSwaggerUI(swaggerOptions =>
            {
                swaggerOptions.SwaggerEndpoint("/swagger/v1/swagger.json", APP_NAME);
                swaggerOptions.RoutePrefix = string.Empty;
            });

            // By default, ExcelDataReader throws a NotSupportedException "No data is available for encoding 1252." on .NET Core.
            // This is the workaround:
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private void UseOracleLoggerProvider(ILoggingBuilder loggingBuilder)
        {
            var logConfiguration = new LogConfiguration("SERVICE_FINANCE_LOG.DELL_FINANCE_LOG");
            logConfiguration.Add("SERVER_NAME", AppSettings.LoggingServerName?.ToUpper());
            logConfiguration.Add("APPLICATION", APP_NAME.ToUpper());
            logConfiguration.Add("LOG_DATE", LogValue.Date);
            logConfiguration.Add("LOG_LEVEL", LogValue.LogLevel);
            logConfiguration.Add("LOGGER", LogValue.SourceContext);
            logConfiguration.Add("MESSAGE", LogValue.State);
            logConfiguration.Add("EXCEPTION", LogValue.Exception);
            var oracleLoggerProvider = new OracleLogProvider(AppSettings.LoggingConnectionString, logConfiguration);

            loggingBuilder.AddProvider(oracleLoggerProvider);
        }

        private void AddLogging(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddDebug();
                loggingBuilder.AddConsole();
                if (AppSettings.UseSeq())
                {
                    loggingBuilder.AddSeq(AppSettings.LoggingServerUrl);
                }
                if (AppSettings.UseOracleProvider())
                {
                    UseOracleLoggerProvider(loggingBuilder);
                }

            });
        }

        private AppSettings AddAppSettings(IServiceCollection services)
        {
            AppSettings appSettings;
            if (Environment.IsDevelopment())
            {
                appSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
                CheckConnectionStrings(appSettings);
            }
            else
            {
                appSettings = SolvePCFAppSettings(services);
            }

            services.AddSingleton(appSettings);
            return appSettings;
        }

        private void CheckConnectionStrings(AppSettings appSettings)
        {
            bool loggingConnectionStringEmpty = string.IsNullOrWhiteSpace(appSettings.LoggingConnectionString);
            bool nFManualConnectionStringEmpty = string.IsNullOrWhiteSpace(appSettings.NFManualConnectionString);
            if (loggingConnectionStringEmpty || nFManualConnectionStringEmpty)
            {
                throw new System.Exception("The ConnectionStrings must be set on appsettings.json. Try to solve following the instructions in appsettings._Development.json.");
            }
        }

        private AppSettings SolvePCFAppSettings(IServiceCollection services)
        {
            // Setup Options framework with DI
            services.AddOptions();

            // Add Steeltoe Cloud Foundry Options to service container
            services.ConfigureCloudFoundryOptions(Configuration);

            var serviceProvider = services.BuildServiceProvider();
            var serviceOptions = serviceProvider.GetService<IOptions<CloudFoundryServicesOptions>>();
            var serviceAppOptions = serviceProvider.GetService<IOptions<CloudFoundryApplicationOptions>>();
            var appSettingsService = serviceOptions.Value.ServicesList.Single(s => s.Name == "ManualInvoiceApi.AppSettings");
            var connectionStringsService = serviceOptions.Value.ServicesList.Single(s => s.Name == "ManualInvoiceApi.ConnectionStrings");

            connectionStringsService.Credentials.TryGetValue(nameof(AppSettings.NFManualConnectionString), out Credential nfManualConnectionString);
            connectionStringsService.Credentials.TryGetValue(nameof(AppSettings.LoggingConnectionString), out Credential loggingConnectionString);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.LoggingProvider), out Credential loggingProvider);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.LoggingServerUrl), out Credential loggingServerUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.SecurityKey), out Credential securityKey);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.SecurityApiUrl), out Credential securityApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.SecurityApiPassword), out Credential securityApiPassword);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.SecurityApiSystem), out Credential securityApiSystem);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.FiscalDocumentsApiUrl), out Credential fiscalDocumentsApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.CustomerApiUrl), out Credential customerApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.SkuApiUrl), out Credential skuApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.CommonApiUrl), out Credential commonApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.DirectApiUrl), out Credential directApiUrl);
            appSettingsService.Credentials.TryGetValue(nameof(AppSettings.ManualInvoiceWebUrl), out Credential manualInvoiceWebUrl);

            return new AppSettings()
            {
                NFManualConnectionString = nfManualConnectionString.Value,
                LoggingConnectionString = loggingConnectionString.Value,
                SecurityKey = securityKey.Value,
                SecurityApiUrl = securityApiUrl.Value,
                SecurityApiPassword = securityApiPassword.Value,
                SecurityApiSystem = securityApiSystem.Value,
                FiscalDocumentsApiUrl = fiscalDocumentsApiUrl.Value,
                CustomerApiUrl = customerApiUrl.Value,
                SkuApiUrl = skuApiUrl.Value,
                CommonApiUrl = commonApiUrl.Value,
                DirectApiUrl = directApiUrl.Value,
                ManualInvoiceWebUrl = manualInvoiceWebUrl.Value,
                LoggingProvider = loggingProvider.Value,
                LoggingServerUrl = loggingServerUrl.Value,
                LoggingServerName = serviceAppOptions.Value.ApplicationName
            };
        }

        private void AddUnitOfWork(IServiceCollection services)
        {
            services.AddNFManualUnitOfWork(AppSettings.NFManualConnectionString);
        }

        private void AddSwaggerGen(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = APP_NAME, Version = "v1" });

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "docs.xml");
                c.IncludeXmlComments(filePath);

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Enter into 'Value': Bearer {token}",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", new string[0] }
                });

            });
        }

        private void AddMvc(IServiceCollection services)
        {
            services.AddOData();

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));

                // see https://github.com/rbeauchamp/Swashbuckle.OData/issues/154
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }

                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            })
            .AddJsonOptions(jsonOptions =>
                jsonOptions.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
        }

        private void AddCors(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = GetSecurityKey(),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        private void AddAuthorization(IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("DellRoleBased",
                    policy => policy.Requirements.Add(new DellRoleBasedRequirement()));
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAuthorizationHandler, DellRoleBasedAuthorizationHandler>();
        }

        private SecurityKey GetSecurityKey()
        {
            var key = Encoding.UTF8.GetBytes(AppSettings.SecurityKey);
            return new SymmetricSecurityKey(key);
        }
    }
}
