﻿using ManualInvoice.Domain.Constants;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ManualInvoice.Api.Handlers
{
    public class DellRoleBasedAuthorizationHandler : AuthorizationHandler<DellRoleBasedRequirement>
    {
        private const string USER_TYPE = "user_type";
        private const string SYSTEM_TYPE = "system";
        private const string ROLE_TYPE = "role";
        private const string SYSTEM_NAME = "ManualInvoice";

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IDelegationViewService _delegationViewService;

        public DellRoleBasedAuthorizationHandler(IHttpContextAccessor contextAccessor, IDelegationViewService delegationViewService)
        {
            _contextAccessor = contextAccessor;
            _delegationViewService = delegationViewService;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, DellRoleBasedRequirement requirement)
        {
            var contextResource = context.Resource as AuthorizationFilterContext;
            var actionDescriptor = contextResource?.ActionDescriptor as ControllerActionDescriptor;

            if (actionDescriptor == null)
            {
                context.Fail();
                return;
            }

            var actionName = actionDescriptor.ActionName.ToLower();
            var controllerName = actionDescriptor.ControllerName.ToLower();

            HttpClient client = new HttpClient();

            _contextAccessor.HttpContext.Request.Headers.TryGetValue("Authorization", out Microsoft.Extensions.Primitives.StringValues values);
            if (values.Count > 0)
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"{values[0]}");

            var stream = Regex.Replace(values[0], "bearer ", "", RegexOptions.IgnoreCase).Trim();
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream) as JwtSecurityToken;

            var userType = jsonToken.Claims.FirstOrDefault(c => (c.Type == USER_TYPE));

            // Almighty system users can do all they want
            if (userType != null && userType.Value == SYSTEM_TYPE)
            {
                context.Succeed(requirement);
                return;
            }

            if (jsonToken.Claims.FirstOrDefault(c => (c.Type == ClaimTypes.Role || c.Type.ToLower() == ROLE_TYPE) && c.Value.ToLower() == $"{SYSTEM_NAME.ToLower()}.{controllerName}.{actionName}") != null)
            {
                context.Succeed(requirement);
            }
            else
            {
                var ntAccount = jsonToken.Claims.FirstOrDefault(e => e.Type == "winaccountname");
                var hasDelegation = await _delegationViewService.GetDelegation(ntAccount.Value);
                var permissions = new List<string>();

                IEnumerable<Claim> delegatedClaims = jsonToken.Claims;
                if (hasDelegation.HasDelegation)
                {
                    permissions.AddRange(DelegationPermissions.DELEGATED_PERMISSIONS);
                    if (hasDelegation.HasCascade)
                    {
                        permissions.AddRange(DelegationPermissions.DELEGATED_CASCADE_PERMISSIONS);
                    }
                }

                foreach (var permission in permissions)
                {
                    var x = new Claim(ClaimTypes.Role, permission);
                    delegatedClaims = delegatedClaims.Append(x);
                }

                if (delegatedClaims.FirstOrDefault(c => (c.Type == ClaimTypes.Role || c.Type.ToLower() == ROLE_TYPE) && c.Value.ToLower() == $"{SYSTEM_NAME.ToLower()}.{controllerName}.{actionName}") != null)
                {
                    context.Succeed(requirement);
                }
            }
            return;
        }
    }
}
