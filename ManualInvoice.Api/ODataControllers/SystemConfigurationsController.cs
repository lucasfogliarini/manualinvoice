﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class SystemConfigurationsController : ODataControllerBase
    {
        private readonly ISystemConfigurationService _systemConfigurationService;

        public SystemConfigurationsController(ISystemConfigurationService systemConfigurationService)
        {
            _systemConfigurationService = systemConfigurationService;
        }

        public IActionResult Get()
        {
            return Ok(_systemConfigurationService.Search().AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] SystemConfigurationRequest systemConfigurationRequest)
        {
            SystemConfiguration systemConfiguration = systemConfigurationRequest;
            systemConfiguration.Id = key;
            var systemConfigurationResponse = _systemConfigurationService.Update(systemConfiguration).AsSummary();
            return Ok();
        }
    }
}
