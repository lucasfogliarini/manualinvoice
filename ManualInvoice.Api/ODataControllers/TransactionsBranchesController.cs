﻿using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class TransactionsBranchesController : ODataControllerBase
    {
        private readonly ITransactionBranchService _transactionBranchService;

        public TransactionsBranchesController(ITransactionBranchService transactionBranchService)
        {
            _transactionBranchService = transactionBranchService;
        }

        public IActionResult Get()
        {
            return Ok(_transactionBranchService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] string key)
        {
            var transaction = _transactionBranchService.Get(key);

            return Ok(transaction);
        }
    }
}