﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class LevelsController : ODataControllerBase
    {
        private readonly ILevelService _levelService;

        public LevelsController(ILevelService levelService)
        {
            _levelService = levelService;
        }

        public IActionResult Get()
        {
            return Ok(_levelService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var level = _levelService.Get(key);

            return Ok(level);
        }
        public IActionResult Post([FromBody] LevelRequest levelRequest)
        {
            return Created(_levelService.Create(levelRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] LevelRequest levelRequest)
        {
            Level level = levelRequest;
            level.Id = key;
            var levelResponse = _levelService.Update(level).AsSummary();
            return Updated(levelResponse);
        }

        [ExportToExcel(typeof(LevelExtensions))]
        public IActionResult Export()
        {
            return Ok(_levelService.Search().AsSummary());
        }
    }
}


