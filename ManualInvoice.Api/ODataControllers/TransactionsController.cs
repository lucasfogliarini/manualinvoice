using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class TransactionsController : ODataControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        public IActionResult Get()
        {
            return Ok(_transactionService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var transactions = _transactionService.Search().AsSummary(key);

            if (transactions == null)
            {
                return NotFound(key);
            }

            return Ok(transactions);
        }

        [ExportToExcel(typeof(TransactionExtensions))]
        public IActionResult Export()
        {
            return Ok(_transactionService.Search().AsSummary());
        }

        public IActionResult Post([FromBody] TransactionRequest transactionRequest)
        {
            return Created(_transactionService.Create(transactionRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] TransactionRequest transactionRequest)
        {
            Transaction transaction = transactionRequest;
            transaction.Id = key;
            
            return Updated(_transactionService.Update(transaction).AsSummary());
        }
    }
}
