﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class DelegationsController : ODataControllerBase
    {
        private readonly IDelegationService _delegationService;

        public DelegationsController(IDelegationService delegationService)
        {
            _delegationService = delegationService;
        }

        public IActionResult Get()
        {
            return Ok(_delegationService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var delegation = _delegationService.Get(key);

            return Ok(delegation);
        }

        public async Task<IActionResult> Post([FromBody] DelegationRequest delegation)
        {
            return Created(await _delegationService.Create(delegation, delegation.DelegationTo));
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] DelegationRequest delegationRequest)
        {
            Delegation delegation = delegationRequest;
            delegation.Id = key;
            var reasonResponse = _delegationService.Update(delegation).AsSummary();
            return Updated(reasonResponse);
        }

        [ExportToExcel(typeof(DelegationExtensions))]
        public IActionResult Export()
        {
            return Ok(_delegationService.Search().AsSummary());
        }
    }
}
