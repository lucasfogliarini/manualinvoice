﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class ReasonsController : ODataControllerBase
    {
        private readonly IReasonService _reasonService;

        public ReasonsController(IReasonService reasonService)
        {
            _reasonService = reasonService;
        }

        public IActionResult Get()
        {
            return Ok(_reasonService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var reason = _reasonService.Get(key);

            return Ok(reason.AsSummary());
        }

        public IActionResult Post([FromBody] ReasonRequest reasonRequest)
        {
            return Created(_reasonService.Create(reasonRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] ReasonRequest reasonRequest)
        {
            Reason reason = reasonRequest;
            reason.Id = key;
            var reasonResponse = _reasonService.Update(reason).AsSummary();
            return Updated(reasonResponse);
        }

        [ExportToExcel(typeof(ReasonExtensions))]
        public IActionResult Export()
        {
            return Ok(_reasonService.Search().AsSummary());
        }
    }
}
