﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Gateways.InvoiceApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class InvoicesController : ODataControllerBase
    {
        public const string REASON_ID = "reasonId";
        public const string PRINT_NF = "printNf";
        private readonly IInvoiceService _invoiceService;
        private readonly IUserService _userService;
        private readonly ITransactionService _transactionService;

        public InvoicesController(IInvoiceService invoiceService, IUserService userService, ITransactionService transactionService)
        {
            _invoiceService = invoiceService;
            _userService = userService;
            _transactionService = transactionService;
        }

        public IActionResult Get()
        {
            return Ok(_invoiceService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var invoice = _invoiceService.Get(key);

            return Ok(invoice.AsSummary());
        }

        public async Task<IActionResult> Post([FromBody] CreateInvoiceRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var user = _userService.GetByName(request.UserName);

            if (user == null)
            {
                return BadRequest($"User from UserName '{request.UserName}' not found.");
            }

            var transaction = _transactionService.Get(request.TransactionId);

            if (transaction == null)
            {
                return BadRequest($"Transaction from Id '{request.TransactionId}' not found.");
            }

            var result = await _invoiceService.CreateInvoice(user.Id, request, transaction, request, request.OriginalInvoiceIdSelecteds, request.IsConsolidation);

            if (!result.Success)
            {
                return BadRequest(result);
            }

            return Ok(result.Response.AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] UpdateInvoiceRequest request)
        {
            IList<InvoiceItem> invoiceItems = request.Items != null && request.Items.Any() ?
                request.Items.Select(x => (InvoiceItem)x).ToList() :
                new List<InvoiceItem>();

            IList<InvoiceVariable> invoiceVariables = request.Variables != null && request.Variables.Any() ?
                request.Variables.Select(x => (InvoiceVariable)x).ToList() :
                new List<InvoiceVariable>();

            IList<InvoiceApprover> invoiceApprovers = request.Approvers != null && request.Approvers.Any() ?
                request.Approvers.Select(x => (InvoiceApprover)x).ToList() :
                new List<InvoiceApprover>();

            var response = _invoiceService.Update(key, request, invoiceItems, invoiceVariables, invoiceApprovers);

            if (!response.Success)
            {
                return BadRequest(response);
            }

            return Ok(response.Response.AsSummary());
        }

        public async Task<IActionResult> GetSynchroInvoices([FromODataUri] IEnumerable<long> ids)
        {
            var invoiceDto = await _invoiceService.GetSynchroInvoices(ids);
            return Ok(invoiceDto);
        }

        public async Task<IActionResult> GetOriginalInvoices(OriginalInvoicesRequestDto originalInvoicesRequestDto)
        {
            var invoiceDto = await _invoiceService.GetOriginalInvoices(originalInvoicesRequestDto);
            return Ok(invoiceDto.AsSummary());
        }

        [HttpPost]
        public IActionResult Cancel([FromODataUri] int key)
        {
            var userName = HttpContext.User.FindFirstValue(ClaimTypes.WindowsAccountName);

            _invoiceService.Cancel(key, userName);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Approve([FromODataUri] int key)
        {
            var userName = HttpContext.User.FindFirstValue(ClaimTypes.WindowsAccountName);

            await _invoiceService.Approve(key, userName);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Reject([FromODataUri] int key, ODataActionParameters parameters)
        {
            var reasonId = parameters[REASON_ID] as int?;
            var userName = HttpContext.User.FindFirstValue(ClaimTypes.WindowsAccountName);

            await _invoiceService.Reject(key, reasonId ?? default(int), userName);

            return Ok();
        }

        [ExportToExcel(typeof(InvoiceExtensions))]
        public IActionResult Export()
        {
            return Ok(_invoiceService.Search().AsSummary());
        }

        public async Task<IActionResult> GetVariables([FromODataUri]int invoiceId)
        {
            var result = await _invoiceService.GetInvoiceVariablesAsync(invoiceId);

            var response = result.AsSummary().ToDataResponse();

            return Ok(response);
        }

        public async Task<IActionResult> GetApprovers([FromODataUri]int invoiceId)
        {
            var result = await _invoiceService.GetInvoiceApproversAsync(invoiceId);

            var response = result.AsSummary();

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder([FromODataUri] int key, ODataActionParameters parameters)
        {
            var result = await _invoiceService.CreateOrder(key);

            if (result.Errors != null && result.Errors.Any())
            {
                return BadRequest(result);
            }

            await _invoiceService.ExecuteStageSalesOrder(result);

            if (parameters.TryGetValue(PRINT_NF, out object printNf) && printNf.Equals(true))
            {
                await _invoiceService.IssueInvoiceImmediately(result);
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CopyInvoice([FromODataUri] int key)
        {
            var userName = HttpContext.User.Claims.FirstOrDefault(
                x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname")
                ?.Value;

            var user = _userService.GetByName(userName);

            if (user == null)
            {
                return BadRequest($"User from UserName '{userName}' not found.");
            }

            var result = await _invoiceService.CopyInvoice(key, user);

            var response = result.AsSummary();

            return Ok(response);
        }
    }
}