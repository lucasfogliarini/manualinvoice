﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Gateways.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class UsersController : ODataControllerBase
    {
        public const string NtAccount = "NtAccount";
        public const string Levels = "Levels";
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Get()
        {
            return Ok(_userService.Search().AsSummary());
        }

        public async Task<IActionResult> Search(UserRequestDto userRequestDto)
        {
            var userResponse = await _userService.Search(userRequestDto);
            return Ok(userResponse.ToDataResponse());
        }

        [ExportToExcel(typeof(UserExtensions))]
        public async Task<IActionResult> Export(UserRequestDto userRequestDto)
        {
            return await Search(userRequestDto);
        }

        [HttpPost]
        public async Task<IActionResult> ConfigureLevels(ODataActionParameters parameters)
        {
            var ntAccount = parameters[NtAccount] as string;
            var levels = parameters[Levels] as IEnumerable<int>;
            await _userService.ConfigureLevelsAsync(ntAccount, levels);
            return Ok();
        }
    }
}


