﻿using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class NopsController : ODataControllerBase
    {
        private readonly INopService _nopService;

        public NopsController(INopService nopService)
        {
            _nopService = nopService;
        }

        public async Task<IActionResult> GetByNopCode([FromODataUri] string nopCode)
        {
            var nopDto = await _nopService.GetByNopCodeAsync(nopCode);
            return Ok(nopDto.AsSummary());
        }
    }
}
