﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class VariablesController : ODataControllerBase
    {
        private readonly IVariableService _variableService;

        public VariablesController(IVariableService variableService)
        {
            _variableService = variableService;
        }

        public IActionResult Get()
        {
            return Ok(_variableService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var variable = _variableService.Get(key).AsSummary();

            return Ok(variable);
        }

        public IActionResult Post([FromBody] VariableRequest variableRequest)
        {
            return Created(_variableService.Create(variableRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] VariableRequest variableRequest)
        {
            Variable variable = variableRequest;
            variable.Id = key;
            var variableResponse = _variableService.Update(variable).AsSummary();
            return Updated(variableResponse);
        }
    }
}


