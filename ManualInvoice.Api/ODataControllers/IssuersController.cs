﻿using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class IssuersController : ODataControllerBase
    {
        private readonly IIssuerService _issuerService;

        public IssuersController(IIssuerService issuerService)
        {
            _issuerService = issuerService;
        }

        public async Task<IActionResult> GetByPfjCode([FromODataUri] string pfjCode)
        {
            var issuerDto = await _issuerService.GetByPfjCodeAsync(pfjCode.ToUpper());
            return Ok(issuerDto.AsSummary());
        }
    }
}
