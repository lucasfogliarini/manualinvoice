﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class SkusController : ODataControllerBase
    {
        private readonly ISkuService _skuService;

        public SkusController(ISkuService skuService)
        {
            _skuService = skuService;
        }

        public async Task<IActionResult> GetBySkuCode([FromODataUri] string skuCode, InvoiceType invoiceType)
        {
            var shouldThrowException = true;
            var skuDto = await _skuService.GetBySkuCodeAsync(skuCode, invoiceType, shouldThrowException);
            return Ok(skuDto.AsSummary());
        }

        public async Task<IActionResult> GetCityCodeBySku([FromODataUri] string skuCode)
        {
            var cityCode = await _skuService.GetCityCodeBySkuAsync(skuCode);
            return Ok(cityCode);
        }

        [HttpGet]
        public async Task<IActionResult> Search(SkuRequest skuRequest)
        {
            //Replace from coded slash - Issue when send encoded space and slash in name
            skuRequest.Name = skuRequest.Name?.Replace(@"<!\!>", "/");
            var value = await _skuService.SearchAsync(
                skuRequest.Skip, skuRequest.Take, skuRequest.OrderBy,
                skuRequest.OrderByDesc, skuRequest.Name, skuRequest.InvoiceType);

            var response = new SkuResponse
            {
                Value = value.Data?.AsSkuResponse(),
                Count = value.TotalRecords
            };

            return Ok(response);
        }
    }
}
