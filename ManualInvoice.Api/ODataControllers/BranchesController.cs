﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class BranchesController : ODataControllerBase
    {
        private readonly IBranchService _branchService;

        public BranchesController(IBranchService branchService)
        {
            _branchService = branchService;
        }

        public IActionResult Get()
        {
            return Ok(_branchService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var branch = _branchService.Get(key);

            return Ok(branch);
        }
        public IActionResult Post([FromBody] BranchRequest branchRequest)
        {
            return Created(_branchService.Create(branchRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] BranchRequest branchRequest)
        {
            Branch branch = branchRequest;
            branch.Id = key;
            var branchResponse = _branchService.Update(branch).AsSummary();
            return Updated(branchResponse);
        }

        public IActionResult GetByTransaction([FromODataUri] int transactionId, [FromODataUri] bool onlyActives)
        {
            var branches = _branchService.GetBranchesByTransaction(transactionId, onlyActives);

            return Ok(branches.AsSummary());
        }

        [ExportToExcel(typeof(BranchExtensions))]
        public IActionResult Export()
        {
            return Ok(_branchService.Search().AsSummary());
        }
    }
}
