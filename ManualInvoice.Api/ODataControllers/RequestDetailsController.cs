﻿using ManualInvoice.Api.Filters;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class RequestDetailsController : ODataControllerBase
    {
        private readonly IInvoiceService _invoiceService;

        public RequestDetailsController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        public IActionResult Get()
        {
            var requestDetails = _invoiceService.GetRequestDetails();
            return Ok(requestDetails.AsSummary());
        }

        [ExportToExcel(typeof(RequestDetailExtensions))]
        public IActionResult Export()
        {
            return Get();
        }

    }
}


