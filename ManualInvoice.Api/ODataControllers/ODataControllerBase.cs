﻿using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;

namespace ManualInvoice.Api.ODataControllers
{
    [EnableQuery]
    [Authorize(Policy = "DellRoleBased")]
    public class ODataControllerBase : ODataController
    {
    }
}
