﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Gateways.CustomerApi.Dtos;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ManualInvoice.Api.ODataControllers
{
    public class PeopleController : ODataControllerBase
    {
        private readonly IPeopleService _customerService;

        public PeopleController(IPeopleService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByCode(string code)
        {
            var response = await _customerService.GetByCodeAsync(code);
            return Ok(response?.AsPersonResponse());
        }

        [HttpGet]
        public async Task<IActionResult> Search(PeopleRequest customerRequest)
        {
            customerRequest.Name = customerRequest.Name?.Replace(@"<!\!>", "/");
            var request = customerRequest.AsCustomerRequestDto();
            
            var value = await _customerService.SearchAsync(request);

            var response = new PeopleResponse
            {
                Value = value.Data?.AsPeopleResponse(),
                Count = value.TotalRecords
            };

            return Ok(response);
        }
    }
}
