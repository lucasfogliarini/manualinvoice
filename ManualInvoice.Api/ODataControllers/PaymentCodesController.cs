﻿using ManualInvoice.Api.Models.Requests;
using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Entities;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace ManualInvoice.Api.ODataControllers
{
    public class PaymentCodesController : ODataControllerBase
    {
        private readonly IPaymentCodeService _paymentCodeService;

        public PaymentCodesController(IPaymentCodeService paymentCodeService)
        {
            _paymentCodeService = paymentCodeService;
        }

        public IActionResult Get()
        {
            return Ok(_paymentCodeService.Search().AsSummary());
        }

        public IActionResult Get([FromODataUri] int key)
        {
            var paymentCode = _paymentCodeService.Get(key).AsSummary();

            return Ok(paymentCode);
        }

        public IActionResult Post([FromBody] PaymentCodeRequest paymentCodeRequest)
        {
            return Created(_paymentCodeService.Create(paymentCodeRequest).AsSummary());
        }

        public IActionResult Put([FromODataUri] int key, [FromBody] PaymentCodeRequest paymentCodeRequest)
        {
            PaymentCode paymentCode = paymentCodeRequest;
            paymentCode.Id = key;
            var variableResponse = _paymentCodeService.Update(paymentCode).AsSummary();
            return Updated(variableResponse);
        }
    }
}


