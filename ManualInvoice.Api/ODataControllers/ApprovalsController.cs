﻿using ManualInvoice.Api.Models.Responses;
using ManualInvoice.Domain.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ManualInvoice.Api.ODataControllers
{
    public class ApprovalsController : ODataControllerBase
    {
        private readonly IApprovalViewService _approvalViewService;

        public ApprovalsController(IApprovalViewService approvalViewService)
        {
            _approvalViewService = approvalViewService;
        }

        public IActionResult Get()
        {
            string userName = HttpContext.User.FindFirstValue(ClaimTypes.WindowsAccountName);
            return Ok(_approvalViewService.Search(userName).AsSummary());
        }

    }
}


