﻿using ManualInvoice.Api.Extensions;
using ManualInvoice.Api.Models.Responses;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ManualInvoice.Api.Filters
{
    public class ExportToExcelAttribute : EnableQueryAttribute
    {
        private static readonly string BR_WINDOWS_TZ = "E. South America Standard Time";
        private static readonly string BR_LINUX_TZ = "America/Sao_Paulo";
        private readonly Type _extensionsType;
        private Type _elemenClrType;

        public ExportToExcelAttribute(Type extensionsType)
        {
            _extensionsType = extensionsType;
        }

        public override IQueryable ApplyQuery(IQueryable queryable, ODataQueryOptions queryOptions)
        {
            // Get the generic T type from the IQueryable<T> result
            _elemenClrType = queryOptions.Context.ElementClrType;
            return queryable;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            var csv = GetCsvContent(context);

            if (!string.IsNullOrWhiteSpace(csv))
            {
                var bytes = Encoding.UTF8.GetBytes(csv);
                context.Result = new FileContentResult(bytes, "text/csv");
            }
        }

        private string GetCsvContent(ResultExecutingContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (!context.Result.IsA<OkObjectResult>())
            {
                return null;
            }

            var value = ((OkObjectResult)context.Result).Value;
            IEnumerable list = null;

            if (value is IQueryable)
            {
                var query = value as IQueryable;
                list = query.AsTypedResult(_elemenClrType);
            }
            else if (value is IDataResponse)
            {
                var response = value as IDataResponse;
                list = response.Data;
            }

            var exportToExcelMethodInfo = GetExportToExcelMethod();
            var properties = exportToExcelMethodInfo.ReturnType.GetProperties();

            var csvBuilder = new StringBuilder();
            csvBuilder.AppendLine(GetCsvHeader(properties));

            foreach (var item in list)
            {
                var obj = GetExportableObject(exportToExcelMethodInfo, item);
                csvBuilder.AppendLine(GetCsvLine(properties, obj));
            }

            return csvBuilder.ToString();
        }

        private MethodInfo GetExportToExcelMethod()
        {
            return _extensionsType.GetMethods().FirstOrDefault(m => m.Name == "ExportToExcel");
        }

        private object GetExportableObject(MethodInfo exportToExcelMethodInfo, object item)
        {
            return exportToExcelMethodInfo.Invoke(item, new[] { item });
        }

        private string GetCsvHeader(PropertyInfo[] properties)
        {
            var header = string.Join(',', properties.Select(p =>
            {
                var attribute = p.GetCustomAttributes(typeof(DisplayNameAttribute), false)
                    .FirstOrDefault() as DisplayNameAttribute;
                return attribute != null ? attribute.DisplayName : p.Name;
            }));

            return header;
        }

        private string GetCsvLine(PropertyInfo[] properties, object obj)
        {
            var line = string.Join(',', properties
                .Select(prop => prop.GetValue(obj))
                .Select(prop => ResolveTimeOffset(prop))
                .Select(value => value == null ? string.Empty : value.ToString())
                .Select(value => value.Replace("\r\n", " "))
                .Select(value => value.Replace("\n", " "))
                .Select(value => value.Trim())
                .Select(value => value.Contains(',') ? $"\"{value}\"" : value)
                .Select(value => value.ToString().StartsWith('0')
                              && int.TryParse(value.ToString(), out _) ? $"\"=\"\"{value}\"\"\"" : value));

            return line;
        }

        private object ResolveTimeOffset(object prop)
        {
            if (prop != null && (prop is DateTime || prop is DateTime?))
            {
                // This workaround needs to be made because of https://github.com/dotnet/corefx/issues/2538
                var timezone = TimeZoneInfo.GetSystemTimeZones()
                    .Where(tz => tz.Id == BR_WINDOWS_TZ || tz.Id == BR_LINUX_TZ)
                    .FirstOrDefault();

                var dateTime = (DateTime)prop;

                if (timezone != null && timezone.IsDaylightSavingTime(dateTime) && dateTime < new DateTime(2019, 02, 17))
                {
                    dateTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime.ToUniversalTime(), timezone);
                }
                else
                {
                    // Force adjust DateTime to Brasilia time (UTC-03:00) without DaylightSavingTime after 02/17/2019.
                    dateTime = dateTime.ToUniversalTime().AddHours(-3);
                }

                prop = dateTime;
            }

            return prop;
        }
    }
}
